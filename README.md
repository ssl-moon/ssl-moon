# SSL MOON [WIP] [![pipeline status](https://gitlab.com/lightcyphers-open/csirt/ssl-moon/ssl-moon/badges/main/pipeline.svg)](https://gitlab.com/lightcyphers-open/csirt/ssl-moon/ssl-moon/-/commits/main)

![logo](./guides/images/cover_logo_big.svg)


## SSL Monitoring and Observability of Open Networks

SSL MOON is a service for batch checking domains hosting web servers for potential misconfigurations or security issues. This service does not aim to make **intrusive checks** like port scanning or application of common known exploits, instead it aims to access and analyze it in the way the user would interact.

The checks that are performed currently on the domains can be categorized in the following categories:

- HTTP - a http get request is fired to the server (in the same manner as a browser would do). On the received response body and headers a series of checks are performed;
- DNS - a series of checks are peformed on DNS servers related to the domain;
- Certificates - HTTPS certificates are fetched, expiry date and chain integrity is verified, additionally the CA is verified against the trusted CA list provided by Mozilla.


Detailed information about checks that are performed for the host can be found [here](/guides/description/checks.md).

## Demo

A deployed demo of this service can be found [here](https://sslmoon-dev.lightcyphers.com/).

## Getting Started

The installation guide can be found [here](/guides/installation/installation.md).


## Contribution

If you would like to contribute to this project, please follow these steps:

1. Fork this repository and clone it to your local machine.
2. Create a new branch for the feature or fix you wish to make.
3. Commit your changes and push to your branch.
4. Submit a Pull Request to this repository to add your changes.

## Questions and Issues

If you have questions or encounter issues in using or developing this project, please create an issue in this repository.

## Features

- [x] Persistent checking of domains with retries;
- [x] Docker image for deploying on cloud service (consult container registry);
- [x] Ability to use both postgres and sqlite3 as storage;
- [x] User interface containing detailed information about the checks and their results;
- [ ] KPI about overall health of a group of domains;
- [ ] Implement checks that are not finished yet;
- [ ] Guide on how to configure the domains;
- [ ] Guide on the docker image;
- [ ] Stable version;
- [ ] Admin interface;
- [ ] Notification system;
- [ ] Ranking system for overall rating of the domain security;
- [ ] Mailserver security checks.
