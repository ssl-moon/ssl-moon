ExUnit.start()

# Sqlite doesn't support async option, so don't enable it

# Ecto.Adapters.SQL.Sandbox.mode(SslMoon.Repo.checks_repo(), :manual)
# Ecto.Adapters.SQL.Sandbox.mode(SslMoon.Repo.tasks_repo(), :manual)

{:ok, _} = Application.ensure_all_started(:ex_machina)

# Http client
Mox.defmock(SslMoon.Http.MockHttpClient, for: SslMoon.Http.HttpClient)
Application.put_env(:ssl_moon, :http_client, SslMoon.Http.MockHttpClient)

# Inet client
Mox.defmock(SslMoon.Dns.MockDnsClient, for: SslMoon.Dns.DnsClient)
Application.put_env(:ssl_moon, :dns_client, SslMoon.Dns.MockDnsClient)

# Tcp client
Mox.defmock(SslMoon.Tcp.MockTcpClient, for: SslMoon.Tcp.TcpClient)
Application.put_env(:ssl_moon, :tcp_client, SslMoon.Tcp.MockTcpClient)

# Ssl client
Mox.defmock(SslMoon.Ssl.MockSslCleint, for: SslMoon.Ssl.SslClient)
Application.put_env(:ssl_moon, :ssl_client, SslMoon.Ssl.MockSslCleint)
