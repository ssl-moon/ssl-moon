defmodule SslMoon.Storage.ChecksTest do
  use SslMoon.DataCase

  alias SslMoon.Storage.Checks
  alias SslMoon.Repo
  alias SslMoon.Schemas.Check

  import SslMoon.ChecksFactory

  test "create_check/1 creates new check" do
    Checks.create_check(%{
      domain_name: "test.com",
      check_name: "test_check",
      version: 0,
      status: :success,
      check_result: %{test: :test}
    })

    assert Repo.checks_repo().all(Check) != []
  end

  test "create_check!/1 creates a new check" do
    Checks.create_check!(%{
      domain_name: "test.com",
      check_name: "test_check",
      version: 0,
      status: :success,
      check_result: %{test: :test}
    })

    assert Repo.checks_repo().all(Check) != []
  end

  test "create_check!/1 raises error on invalid schema" do
    assert_raise Ecto.InvalidChangesetError, fn ->
      Checks.create_check!(%{
        domain_name: "test.com",
        check_name: "test_check",
        version: "not_valid",
        status: :success,
        check_result: %{test: :test}
      })
    end
  end

  test "get_latest_check/1 returns the correct last check" do
    check_name = "test_check_name"
    domain = "test.com"

    time = DateTime.utc_now()

    checks_group =
      build(:checks_group, %{checked_at: time}) |> insert()

    build(:check, %{check_name: check_name, check_group_id: checks_group.id})
    |> insert()

    checks_group2 =
      build(:checks_group, %{checked_at: DateTime.add(time, 10)}) |> insert()

    check2 =
      build(:check, %{check_name: check_name, check_group_id: checks_group2.id})
      |> insert()

    assert Checks.get_latest_check(domain, check_name).id == check2.id
  end

  # This function should throw in case it doesn't find the entry, not return nil
  test "get_latest_check!/1 returns the correct last check" do
    check_name = "test_check_name"
    domain = "test.com"

    time = DateTime.utc_now()

    checks_group =
      build(:checks_group, %{checked_at: time}) |> insert()

    build(:check, %{check_name: check_name, check_group_id: checks_group.id})
    |> insert()

    checks_group2 =
      build(:checks_group, %{checked_at: DateTime.add(time, 10)}) |> insert()

    check2 =
      build(:check, %{check_name: check_name, check_group_id: checks_group2.id})
      |> insert()

    assert Checks.get_latest_check!(domain, check_name).id == check2.id
  end

  test "get_checks_count/0 returns the correct count" do
    for _i <- 1..10 do
      build(:check) |> insert()
    end

    assert Checks.get_checks_count() == 10
  end
end
