defmodule SslMoon.ChecksExecutorTest do
  use ExUnit.Case, async: true

  alias SslMoon.Checks
  alias SslMoon.Checks.CheckResult

  setup do
    Code.compiler_options(ignore_module_conflict: true)
  end

  test "on_error/3 is called when error is returned from functions" do
    defmodule TestExecutor do
      use SslMoon.Checks.Executor

      def perform_check(_check, _arg) do
        {:error, "Error from perform_check"}
      end

      def qualify_check(_check, _url, _data) do
        {:ok, :success}
      end

      def on_error(_check, _url, error, _stacktrace) do
        {:ok, %CheckResult{status: :failure, data: error}}
      end
    end

    defmodule TestExecutor2 do
      use SslMoon.Checks.Executor

      def perform_check(_check, _arg) do
        {:ok, %{}}
      end

      def qualify_check(_check, _url, _data) do
        {:error, "Error from save_data"}
      end

      def on_error(_check, _url, error, _stacktrace) do
        {:ok, %CheckResult{status: :failure, data: error}}
      end
    end

    defmodule TestRouter do
      use SslMoon.Checks.Router

      checks do
        check "test_check" do
          version(1)
          executor(TestExecutor)
          template(SslMoon.DummyTemplate)
          migrator(SslMoon.DummyMigrator)
          validation_schema(SslMoon.DummySchema)
        end

        check "test_check_2" do
          version(1)
          executor(TestExecutor2)
          template(SslMoon.DummyTemplate)
          migrator(SslMoon.DummyMigrator)
          validation_schema(SslMoon.DummySchema)
        end
      end
    end

    result =
      Checks.execute_check(Checks.get_check_by_name(TestRouter, "test_check"), "test.com")

    assert result == {:ok, %CheckResult{status: :failure, data: "Error from perform_check"}}

    result =
      Checks.execute_check(Checks.get_check_by_name(TestRouter, "test_check_2"), "test.com")

    assert result == {:ok, %CheckResult{status: :failure, data: "Error from save_data"}}
  end

  test "on_error/3 is called when exception is raised" do
    defmodule TestExecutor do
      use SslMoon.Checks.Executor

      def perform_check(_check, _arg) do
        raise "Exception from perform_check"
      end

      def qualify_check(_check, _url, _data) do
        {:error, "Error from save_data"}
      end

      def on_error(_check, _url, error, _stacktrace) do
        {:ok, %CheckResult{status: :failure, data: error.message}}
      end
    end

    defmodule TestExecutor2 do
      use SslMoon.Checks.Executor

      def perform_check(_check, _arg) do
        {:ok, %{}}
      end

      def qualify_check(_check, _url, _data) do
        raise "Exception from save_data"
      end

      def on_error(_check, _url, error, _stacktrace) do
        {:ok, %CheckResult{status: :failure, data: error.message}}
      end
    end

    defmodule TestRouter do
      use SslMoon.Checks.Router

      checks do
        check "test_check" do
          version(1)
          executor(TestExecutor)
          template(SslMoon.DummyTemplate)
          migrator(SslMoon.DummyMigrator)
          validation_schema(SslMoon.DummySchema)
        end

        check "test_check_2" do
          version(1)
          executor(TestExecutor2)
          template(SslMoon.DummyTemplate)
          migrator(SslMoon.DummyMigrator)
          validation_schema(SslMoon.DummySchema)
        end
      end
    end

    result = Checks.execute_check(Checks.get_check_by_name(TestRouter, "test_check"), "test.com")
    assert result == {:ok, %CheckResult{status: :failure, data: "Exception from perform_check"}}

    result =
      Checks.execute_check(Checks.get_check_by_name(TestRouter, "test_check_2"), "test.com")

    assert result == {:ok, %CheckResult{status: :failure, data: "Exception from save_data"}}
  end
end
