defmodule SslMoon.ChecksMigratorTest do
  use ExUnit.Case, async: true

  setup do
    Code.compiler_options(ignore_module_conflict: true)
  end

  test "raises error on no definition" do
    assert_raise RuntimeError, fn ->
      defmodule Dummy do
        use SslMoon.Checks.Migrator
      end
    end
  end

  test "raises error on empty migrator definition" do
    assert_raise RuntimeError, fn ->
      defmodule Dummy do
        use SslMoon.Checks.Migrator

        migrator do
        end
      end
    end
  end

  test "raises error on duplicated definitions" do
    assert_raise RuntimeError, fn ->
      defmodule Dummy do
        use SslMoon.Checks.Migrator

        migrator do
          migrate(version: 1, migration_fun: fn data -> data end)
          migrate(version: 1, migration_fun: fn data -> data end)
        end
      end
    end
  end

  test "generates the expected functions after compilation" do
    defmodule Dummy do
      use SslMoon.Checks.Migrator

      migrator do
        migrate(version: 1, migration_fun: fn data -> data end)
        migrate(version: 2, migration_fun: fn data -> data end)
      end
    end

    functions = Dummy.__info__(:functions)

    # Check that migrate/1 was generated
    assert Enum.find(functions, nil, fn {func_name, arity} ->
             func_name == :migrate and arity == 3
           end) != nil

    # Check that versions/0 was generated
    assert Enum.find(functions, nil, fn {func_name, arity} ->
             func_name == :versions and arity == 0
           end) != nil
  end

  test "versions/0 function is generated correctly and elements are ordered" do
    defmodule Dummy do
      use SslMoon.Checks.Migrator

      migrator do
        migrate(version: 3, migration_fun: fn data -> data end)
        migrate(version: 1, migration_fun: fn data -> data end)
        migrate(version: 2, migration_fun: fn data -> data end)
      end
    end

    defmodule Dummy2 do
      use SslMoon.Checks.Migrator

      migrator do
        migrate(version: 3, migration_fun: fn data -> data end)
        migrate(version: 2, migration_fun: fn data -> data end)
      end
    end

    assert Dummy.versions() == [1, 2, 3]
    assert Dummy2.versions() == [2, 3]
  end

  test "migrate/3 migrates as expected different versions" do
    defmodule Dummy do
      use SslMoon.Checks.Migrator

      migrator do
        migrate(version: 3, migration_fun: fn %{version: version} -> %{version: version + 1} end)
        migrate(version: 1, migration_fun: fn %{version: version} -> %{version: version + 1} end)
        migrate(version: 2, migration_fun: fn %{version: version} -> %{version: version + 1} end)
      end
    end

    # version 3
    assert Dummy.migrate(0, 3, %{version: 0}) == %{version: 3}
    assert Dummy.migrate(1, 3, %{version: 1}) == %{version: 3}
    assert Dummy.migrate(2, 3, %{version: 2}) == %{version: 3}
    assert Dummy.migrate(3, 3, %{version: 3}) == %{version: 3}

    # version 2
    assert Dummy.migrate(0, 2, %{version: 0}) == %{version: 2}
    assert Dummy.migrate(1, 2, %{version: 1}) == %{version: 2}
    assert Dummy.migrate(2, 2, %{version: 2}) == %{version: 2}

    # version 1
    assert Dummy.migrate(0, 1, %{version: 0}) == %{version: 1}
    assert Dummy.migrate(1, 1, %{version: 1}) == %{version: 1}

    # version 0
    assert Dummy.migrate(0, 0, %{version: 0}) == %{version: 0}
  end
end
