defmodule SslMoon.RouterTest do
  use ExUnit.Case, async: true

  setup do
    Code.compiler_options(ignore_module_conflict: true)
  end

  test "doesn't compile without required attributes" do
    assert_raise RuntimeError, fn ->
      defmodule Dummy do
        use SslMoon.Checks.Router

        checks do
          check "test_check" do
          end
        end
      end
    end
  end

  test "doesn't compile with duplicate check names" do
    defmodule Dummy do
      use SslMoon.Checks.Router

      assert_raise RuntimeError, fn ->
        checks do
          check "test_check" do
            version(1)
            executor(SslMoon.DummyExecutor)
            template(SslMoon.DummyTemplate)
            migrator(SslMoon.DummyMigrator)
            validation_schema(SslMoon.DummySchema)
          end

          check "test_check" do
            version(1)
            executor(SslMoon.DummyExecutor)
            template(SslMoon.DummyTemplate)
            migrator(SslMoon.DummyMigrator)
            validation_schema(SslMoon.DummySchema)
          end

          check "test_check_1" do
            version(1)
            executor(SslMoon.DummyExecutor)
            template(SslMoon.DummyTemplate)
            migrator(SslMoon.DummyMigrator)
            validation_schema(SslMoon.DummySchema)
          end

          check "test_check_1" do
            version(1)
            executor(SslMoon.DummyExecutor)
            template(SslMoon.DummyTemplate)
            migrator(SslMoon.DummyMigrator)
            validation_schema(SslMoon.DummySchema)
          end
        end
      end
    end
  end

  test "doesn't compile with invalid modules" do
    # Ensure doesn't fail for the default case
    defmodule Dummy do
      use SslMoon.Checks.Router

      checks do
        check "test_check" do
          version(1)
          executor(SslMoon.DummyExecutor)
          template(SslMoon.DummyTemplate)
          migrator(SslMoon.DummyMigrator)
          validation_schema(SslMoon.DummySchema)
        end
      end
    end

    assert_raise ArgumentError, fn ->
      defmodule Dummy do
        use SslMoon.Checks.Router

        checks do
          check "test_check" do
            version(1)
            executor(NotExistingModuleTest)
            template(SslMoon.DummyTemplate)
            migrator(SslMoon.DummyMigrator)
            validation_schema(SslMoon.DummySchema)
          end
        end
      end
    end

    assert_raise ArgumentError, fn ->
      defmodule Dummy do
        use SslMoon.Checks.Router

        checks do
          check "test_check" do
            version(1)
            executor(SslMoon.DummyExecutor)
            template(NotExistingModuleTest)
            migrator(SslMoon.DummyMigrator)
            validation_schema(SslMoon.DummySchema)
          end
        end
      end
    end

    assert_raise ArgumentError, fn ->
      defmodule Dummy do
        use SslMoon.Checks.Router

        checks do
          check "test_check" do
            version(1)
            executor(SslMoon.DummyExecutor)
            template(SslMoon.DummyTemplate)
            migrator(NotExistingModuleTest)
            validation_schema(SslMoon.DummySchema)
          end
        end
      end
    end

    assert_raise ArgumentError, fn ->
      defmodule Dummy do
        use SslMoon.Checks.Router

        checks do
          check "test_check" do
            version(1)
            executor(SslMoon.DummyExecutor)
            template(SslMoon.DummyTemplate)
            migrator(SslMoon.DummyMigrator)
            validation_schema(NotExistingModuleTest)
          end
        end
      end
    end
  end

  test "generates the correct function" do
    defmodule Dummy do
      use SslMoon.Checks.Router

      checks do
        check "test_check" do
          version(1)
          executor(SslMoon.DummyExecutor)
          template(SslMoon.DummyTemplate)
          migrator(SslMoon.DummyMigrator)
          validation_schema(SslMoon.DummySchema)
        end
      end
    end

    assert Dummy.__info__(:functions) |> Keyword.has_key?(:checks)

    assert [check] = Dummy.checks()

    assert Map.has_key?(check, :name)
    assert Map.has_key?(check, :version)
    assert Map.has_key?(check, :template)
    assert Map.has_key?(check, :migrator)
    assert Map.has_key?(check, :executor)
  end
end
