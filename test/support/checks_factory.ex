defmodule SslMoon.ChecksFactory do
  @moduledoc """
    Factory for checks repo.
  """
  use ExMachina.Ecto, repo: SslMoon.Repo.checks_repo()

  def check_factory do
    %SslMoon.Schemas.Check{
      check_name: "test_check",
      version: 0,
      status: :success,
      check_result: %{test: :test},
      check_group_id: nil
    }
  end

  def checks_group_factory do
    %SslMoon.Schemas.CheckGroup{
      domain_name: "test.com"
    }
  end
end
