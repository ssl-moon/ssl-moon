defmodule SslMoon.Checks.SubresourceIntegrity.Factory do
  @moduledoc false
  use ExMachina

  alias SslMoon.Http.HttpClient.{Response, MaybeRedirect}

  def http_response_factory do
    %Response{
      status_code: 200,
      body: "",
      headers: [
        {"Vary", "Accept-Encoding"},
        {"Set-Cookie",
         "fr=0cVsTTfKwKqX98IKh..BlJW-Y.kx.AAA.0.0.BlJW-Y.AWWoklL-Ysg; expires=Mon, 08-Jan-2024 15:36:56 GMT; Max-Age=7776000; path=/; domain=.facebook.com; secure; httponly"},
        {"Set-Cookie",
         "sb=mG8lZWRU8CSu_Dh8PUktiVgr; expires=Wed, 13-Nov-2024 15:36:56 GMT; Max-Age=34560000; path=/; domain=.facebook.com; secure; httponly"},
        {"report-to",
         "{\"max_age\":259200,\"endpoints\":[{\"url\":\"https:\\/\\/www.facebook.com\\/ajax\\/browser_error_reports\\/?device_level=unknown\"}]}"},
        {"content-security-policy",
         "default-src data: blob: 'self' https://*.fbsbx.com 'unsafe-inline' *.facebook.com *.fbcdn.net 'unsafe-eval';script-src *.facebook.com *.fbcdn.net *.facebook.net *.google-analytics.com *.google.com 127.0.0.1:* 'unsafe-inline' blob: data: 'self' connect.facebook.net 'unsafe-eval';style-src fonts.googleapis.com *.fbcdn.net data: *.facebook.com 'unsafe-inline';connect-src *.facebook.com facebook.com *.fbcdn.net *.facebook.net wss://*.facebook.com:* wss://*.whatsapp.com:* wss://*.fbcdn.net attachment.fbsbx.com ws://localhost:* blob: *.cdninstagram.com 'self' http://localhost:3103 wss://gateway.facebook.com wss://edge-chat.facebook.com wss://snaptu-d.facebook.com wss://kaios-d.facebook.com/ v.whatsapp.net *.fbsbx.com *.fb.com;font-src data: *.gstatic.com *.facebook.com *.fbcdn.net *.fbsbx.com;img-src *.fbcdn.net *.facebook.com data: https://*.fbsbx.com *.tenor.co media.tenor.com facebook.com *.cdninstagram.com fbsbx.com fbcdn.net *.giphy.com connect.facebook.net *.carriersignal.info blob: android-webview-video-poster: googleads.g.doubleclick.net www.googleadservices.com *.whatsapp.net *.fb.com *.oculuscdn.com;media-src *.cdninstagram.com blob: *.fbcdn.net *.fbsbx.com www.facebook.com *.facebook.com https://*.giphy.com data:;frame-src *.doubleclick.net *.google.com *.facebook.com www.googleadservices.com *.fbsbx.com fbsbx.com data: www.instagram.com *.fbcdn.net https://paywithmybank.com https://sandbox.paywithmybank.com;worker-src blob: *.facebook.com data:;block-all-mixed-content;upgrade-insecure-requests;"},
        {"document-policy", "force-load-at-top"},
        {"permissions-policy",
         "accelerometer=(), ambient-light-sensor=(), bluetooth=(), camera=(self), geolocation=(self), gyroscope=(), hid=(), idle-detection=(), magnetometer=(), microphone=(self), midi=(), payment=(), screen-wake-lock=(), serial=(), usb=()"},
        {"cross-origin-resource-policy", "cross-origin"},
        {"cross-origin-opener-policy", "unsafe-none"},
        {"Pragma", "no-cache"},
        {"Cache-Control", "private, no-cache, no-store, must-revalidate"},
        {"Expires", "Sat, 01 Jan 2000 00:00:00 GMT"},
        {"X-Content-Type-Options", "nosniff"},
        {"X-XSS-Protection", "0"},
        {"X-Frame-Options", "DENY"},
        {"Content-Type", "text/html; charset=\"utf-8\""},
        {"X-FB-Debug",
         "Yu+QhJIl1+lLuuTPUdqtTWnQxD9M63RuAl1GpIv6T48w19IdZY04AyzS1Uj0Y55FHp6te5MVfqbWR3gD3jwG4Q=="},
        {"Date", "Tue, 10 Oct 2023 15:36:56 GMT"},
        {"Transfer-Encoding", "chunked"},
        {"Alt-Svc", "h3=\":443\"; ma=86400"},
        {"Connection", "keep-alive"}
      ],
      request_url: "https://test.com/",
      request: %SslMoon.Http.HttpClient.Request{
        url: "http://test.com/",
        body: "",
        headers: [],
        method: :get,
        options: [],
        params: []
      }
    }
  end

  def http_redirect_factory do
    %MaybeRedirect{
      status_code: 301,
      redirect_url: "https://www.test.com/",
      headers: [],
      request_url: "https://test.com/",
      request: %SslMoon.Http.HttpClient.Request{
        url: "http://test.com/",
        body: "",
        headers: [],
        method: :get,
        options: [],
        params: []
      }
    }
  end
end
