defmodule SslMoon.Checks.FrameOptionsTest.Factory do
  @moduledoc false
  use ExMachina

  alias SslMoon.Http.HttpClient.{Response, MaybeRedirect}

  def http_response_factory do
    %Response{
      status_code: 200,
      body:
        "<!doctype html><html itemscope=\"\" itemtype=\"http://schema.org/WebPage\" lang=\"ro-MD\"><head><meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\"><meta content=\"/images/branding/googleg/1x/googleg_standard_color_128dp.png\" itemprop=\"image\"><title>Google</title><script nonce=\"B-fDE5snFGyhp0inB6bD_g\">(function(){var _g={kEI:'RmAlZeLaBb6hi-gP1I2zwA0',kEXPI:'0,1365467,207,4804,2316,383,246,5,1129120,1197765,380726,16114,28684,22431,1361,12315,17584,4998,17075,41316,2891,8348,3406,606,63304,13721,20583,4,59617,27032,6642,7596,1,11943,30211,2,39761,5679,1020,31122,4568,6259,23418,1252,33064,2,2,1,26632,8155,23350,874,19633,8,1921,9779,42459,20199,20136,14,82,20206,8377,18988,5375,2265,765,5629,10187,1804,21012,1825,10260,2171,2414,2839,6561,1632,26702,7914,18154,5212716,906,109,2,195,576,47,47,5994100,97,2803117,3311,141,795,28682,696,8,119,4,8,5,8855580,15085465,579,4043528,16672,36926,2532,2988,12,33,3,805,584,3,631,1474,3,821,1393125,23759270,12799,8408,2879,1595,111,978,2441,6695,1966,4370,2978,669,450,2665,298,6020,877,3,3945,2635,1213,5,1899,2048,1951,1239,640,3235,303,2546,281,7138,780,1952,758,166,3399,1638,726,101,2081,5030,495,40,314,207,3729,2,76,172,34,2766,168,188,884,603,3,387,2,1291,2,6,55,910,2,528,196,1791,3,262,1700,1194,118,454,683,4,409,468,439,274,7,1,8,136,110,131,760,91,125,570,18,193,10,390,2428,1192,52,590,634,1628,840,19,1361,1054,454,51,231,1,48,6,1124,697,901,3,115,836,104,131,2,673,851,353,128,2,1194,74,727,3,2,2,2,198,459,14,15,4610,482,596,219,10,205,4,422',kBL:'dnl7',kOPI:89978449};(function(){var a;(null==(a=window.google)?0:a.stvsc)?google.kEI=_g.kEI:window.google=_g;}).call(this);})();(function(){google.sn='webhp';google.kHL='ro-MD';})();(function(){\nvar h=this||self;function l(){return void 0!==window.google&&void 0!==window.google.kOPI&&0!==window.google.kOPI?window.google.kOPI:null};var m,n=[];function p(a){for(var b;a&&(!a.getAttribute||!(b=a.getAttribute(\"eid\")));)a=a.parentNode;return b||m}function q(a){for(var b=null;a&&(!a.getAttribute||!(b=a.getAttribute(\"leid\")));)a=a.parentNode;return b}function r(a){/^http:/i.test(a)&&\"https:\"===window.location.protocol&&(google.ml&&google.ml(Error(\"a\"),!1,{src:a,glmm:1}),a=\"\");return a}\nfunction t(a,b,c,d,k){var e=\"\";-1===b.search(\"&ei=\")&&(e=\"&ei=\"+p(d),-1===b.search(\"&lei=\")&&(d=q(d))&&(e+=\"&lei=\"+d));d=\"\";var g=-1===b.search(\"&cshid=\")&&\"slh\"!==a,f=[];f.push([\"zx\",Date.now().toString()]);h._cshid&&g&&f.push([\"cshid\",h._cshid]);c=c();null!=c&&f.push([\"opi\",c.toString()]);for(c=0;c<f.length;c++){if(0===c||0<c)d+=\"&\";d+=f[c][0]+\"=\"+f[c][1]}return\"/\"+(k||\"gen_204\")+\"?atyp=i&ct=\"+String(a)+\"&cad=\"+(b+e+d)};m=google.kEI;google.getEI=p;google.getLEI=q;google.ml=function(){return null};google.log=function(a,b,c,d,k,e){e=void 0===e?l:e;c||(c=t(a,b,e,d,k));if(c=r(c)){a=new Image;var g=n.length;n[g]=a;a.onerror=a.onload=a.onabort=function(){delete n[g]};a.src=c}};google.logUrl=function(a,b){b=void 0===b?l:b;return t(\"\",a,b)};}).call(this);(function(){google.y={};google.sy=[];google.x=function(a,b){if(a)var c=a.id;else{do c=Math.random();while(google.y[c])}google.y[c]=[a,b];return!1};google.sx=function(a){google.sy.push(a)};google.lm=[];google.plm=function(a){google.lm.push.apply(google.lm,a)};google.lq=[];google.load=function(a,b,c){google.lq.push([[a],b,c])};google.loadAll=function(a,b){google.lq.push([a,b])};google.bx=!1;google.lx=function(){};var d=[];google.fce=function(a,b,c,e){d.push([a,b,c,e])};google.qce=d;}).call(this);google.f={};(function(){\ndocument.documentElement.addEventListener(\"submit\",function(b){var a;if(a=b.target){var c=a.getAttribute(\"data-submitfalse\");a=\"1\"===c||\"q\"===c&&!a.elements.q.value?!0:!1}else a=!1;a&&(b.preventDefault(),b.stopPropagation())},!0);document.documentElement.addEventListener(\"click\",function(b){var a;a:{for(a=b.target;a&&a!==document.documentElement;a=a.parentElement)if(\"A\"===a.tagName){a=\"1\"===a.getAttribute(\"data-nohref\");break a}a=!1}a&&b.preventDefault()},!0);}).call(this);</script><style>#gbar,#guser{font-size:13px;padding-top:1px !important;}#gbar{height:22px}#guser{padding-bottom:7px !importa",
      headers: [
        {"Date", "Tue, 10 Oct 2023 14:31:34 GMT"},
        {"Expires", "-1"},
        {"Cache-Control", "private, max-age=0"},
        {"Content-Type", "text/html; charset=ISO-8859-1"},
        {"Content-Security-Policy-Report-Only",
         "object-src 'none';base-uri 'self';script-src 'nonce-B-fDE5snFGyhp0inB6bD_g' 'strict-dynamic' 'report-sample' 'unsafe-eval' 'unsafe-inline' https: http:;report-uri https://csp.withgoogle.com/csp/gws/other-hp"},
        {"P3P", "CP=\"This is not a P3P policy! See g.co/p3phelp for more info.\""},
        {"Server", "gws"},
        {"X-XSS-Protection", "0"},
        {"Set-Cookie",
         "1P_JAR=2023-10-10-14; expires=Thu, 09-Nov-2023 14:31:34 GMT; path=/; domain=.google.com; Secure"},
        {"Set-Cookie",
         "AEC=Ackid1Q2s6N3FyY0nbkh6YdI-vAvyOJmn5Ca-4HjhiC5areNRar9nOvxxZo; expires=Sun, 07-Apr-2024 14:31:34 GMT; path=/; domain=.google.com; Secure; HttpOnly; SameSite=lax"},
        {"Set-Cookie",
         "NID=511=AJDYHKzP7WO78EmvLLNHrUez7dV0CMSsnu9ELVHQVrYE2Ukn78zoY4UysWAhiypxB4PPvAspf7qDSI9o37SH024wZv0aKK3oxw3rET1nweqcjveNtoSuleVbI_SNgfvWHWavhLbvDi_PE7V6D0tDmCsDneBwdGmd3vbfI44YFOA; expires=Wed, 10-Apr-2024 14:31:34 GMT; path=/; domain=.google.com; HttpOnly"},
        {"Alt-Svc", "h3=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000"},
        {"Accept-Ranges", "none"},
        {"Vary", "Accept-Encoding"},
        {"Transfer-Encoding", "chunked"}
      ],
      request_url: "https://www.test.com/",
      request: %SslMoon.Http.HttpClient.Request{
        url: "http://test.com/",
        body: "",
        headers: [],
        method: :get,
        options: [],
        params: []
      }
    }
  end

  def http_redirect_factory do
    %MaybeRedirect{
      status_code: 301,
      redirect_url: "https://www.test.com/",
      headers: [],
      request_url: "https://test.com/",
      request: %SslMoon.Http.HttpClient.Request{
        url: "http://test.com/",
        body: "",
        headers: [],
        method: :get,
        options: [],
        params: []
      }
    }
  end
end
