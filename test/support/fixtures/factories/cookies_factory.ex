defmodule SslMoon.Checks.Cookies.Factory do
  @moduledoc false
  use ExMachina

  def http_response_factory do
    %SslMoon.Http.HttpClient.Response{
      status_code: 200,
      body:
        "<!DOCTYPE html>\n<html lang=\"ru\" id=\"facebook\" class=\"no_js\">\n<head><meta charset=\"utf-8\" /><meta name=\"referrer\" content=\"default\" id=\"meta_referrer\" /><script nonce=\"vs6crHIk\">function envFlush(a){function b(b){for(var c in a)b[c]=a[c]}window.requireLazy?window.requireLazy([\"Env\"],b):(window.Env=window.Env||{},b(window.Env))}envFlush({\"useTrustedTypes\":false,\"isTrustedTypesReportOnly\":false,\"ajaxpipe_token\":\"AXgk6fvLzRcsfUKGpuw\",\"remove_heartbeat\":false,\"stack_trace_limit\":30,\"timesliceBufferSize\":5000,\"show_invariant_decoder\":false,\"compat_iframe_token\":\"AQ5i37PLUxOWcSR7NO0\",\"isCQuick\":false});</script><script nonce=\"vs6crHIk\">(function(a){function b(b){if(!window.openDatabase)return;b.I_AM_INCOGNITO_AND_I_REALLY_NEED_WEBSQL=function(a,b,c,d){return window.openDatabase(a,b,c,d)};window.openDatabase=function(){throw new Error()}}b(a)})(this);</script><style nonce=\"vs6crHIk\"></style><script nonce=\"vs6crHIk\">__DEV__=0;</script><noscript><meta http-equiv=\"refresh\" content=\"0; URL=/?_fb_noscript=1\" /></noscript><link rel=\"manifest\" id=\"MANIFEST_LINK\" href=\"/data/manifest/\" crossorigin=\"use-credentials\" /><title id=\"pageTitle\">Facebook — Выполните вход или зарегистрируйтесь</title><meta property=\"og:site_name\" content=\"Facebook\" /><meta property=\"og:url\" content=\"https://www.facebook.com/\" /><meta property=\"og:image\" content=\"https://www.facebook.com/images/fb_logo/app-facebook-circle-bp.png\" /><meta property=\"og:locale\" content=\"ru_RU\" /><link rel=\"alternate\" media=\"only screen and (max-width: 640px)\" href=\"https://m.facebook.com/\" /><link rel=\"alternate\" media=\"handheld\" href=\"https://m.facebook.com/\" /><meta name=\"description\" content=\"&#x412;&#x43e;&#x439;&#x434;&#x438;&#x442;&#x435; &#x43d;&#x430; Facebook, &#x447;&#x442;&#x43e;&#x431;&#x44b; &#x43e;&#x431;&#x449;&#x430;&#x442;&#x44c;&#x441;&#x44f; &#x441; &#x434;&#x440;&#x443;&#x437;&#x44c;&#x44f;&#x43c;&#x438;, &#x440;&#x43e;&#x434;&#x441;&#x442;&#x432;&#x435;&#x43d;&#x43d;&#x438;&#x43a;&#x430;&#x43c;&#x438; &#x438; &#x437;&#x43d;&#x430;&#x43a;&#x43e;&#x43c;&#x44b;&#x43c;&#x438;.\" /><link rel=\"canonical\" href=\"https://www.facebook.com/\" /><link rel=\"icon\" href=\"https://static.xx.fbcdn.net/rsrc.php/yv/r/B8BxsscfVBr.ico\" /><link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yJ/l/0,cross/SOGW1WkjbV_.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"ircEhJI\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yO/l/0,cross/I2wPU5r07is.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"Pud6B2Z\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yo/l/0,cross/7IIi5yzUqo1.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"vrinQF3\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yJ/l/0,cross/ZHhTKgghkXG.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"sM46+8q\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yi/l/0,cross/yotEdcUw9Gj.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"DcLQ9Pg\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yc/l/0,cross/1FPNULrhhBJ.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"0Bj1L9r\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yV/l/0,cross/_bzWjvAFjKO.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"JG0XRy3\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yF/l/0,cross/LSAcIwftMnp.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"Pj6j22R\" />\n<script src=\"https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/wyGjpP54TBv.js?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"7cG5YqM\" nonce=\"vs6crHIk\"></script>\n<script nonce=\"vs6crHIk\">requireLazy([\"HasteSupportData\"],function(m){m.handle({\"clpData\":{\"1838142\":{\"r\":1,\"s\":1},\"4883\":{\"r\":1,\"s\":1},\"1814852\":{\"r\":1},\"1848815\":{\"r\":10000,\"s\":1}},\"gkxData\":{\"676837\":{\"result\":false,\"hash\":null},\"708253\":{\"result\":false,\"hash\":null},\"1167394\":{\"result\":false,\"hash\":null},\"1073500\":{\"result\":false,\"hash\":null},\"1224637\":{\"result\":false,\"hash\":null},\"1263340\":",
      headers: [
        {"Vary", "Accept-Encoding"},
        {"report-to",
         "{\"max_age\":259200,\"endpoints\":[{\"url\":\"https:\\/\\/www.facebook.com\\/ajax\\/browser_error_reports\\/?device_level=unknown\"}]}, {\"max_age\":3600,\"endpoints\":[{\"url\":\"https:\\/\\/www.facebook.com\\/ajax\\/browser_error_reports\\/?device_level=unknown\"}],\"group\":\"network-errors\"}"},
        {"content-security-policy",
         "default-src data: blob: 'self' https://*.fbsbx.com 'unsafe-inline' *.facebook.com *.fbcdn.net 'unsafe-eval';script-src *.facebook.com *.fbcdn.net *.facebook.net *.google-analytics.com *.google.com 127.0.0.1:* 'unsafe-inline' blob: data: 'self' connect.facebook.net 'unsafe-eval';style-src fonts.googleapis.com *.fbcdn.net data: *.facebook.com 'unsafe-inline';connect-src *.facebook.com facebook.com *.fbcdn.net *.facebook.net wss://*.facebook.com:* wss://*.whatsapp.com:* wss://*.fbcdn.net attachment.fbsbx.com ws://localhost:* blob: *.cdninstagram.com 'self' http://localhost:3103 wss://gateway.facebook.com wss://edge-chat.facebook.com wss://snaptu-d.facebook.com wss://kaios-d.facebook.com/ v.whatsapp.net *.fbsbx.com *.fb.com;font-src data: *.gstatic.com *.facebook.com *.fbcdn.net *.fbsbx.com;img-src *.fbcdn.net *.facebook.com data: https://*.fbsbx.com *.tenor.co media.tenor.com facebook.com *.cdninstagram.com fbsbx.com fbcdn.net *.giphy.com connect.facebook.net *.carriersignal.info blob: android-webview-video-poster: googleads.g.doubleclick.net www.googleadservices.com *.whatsapp.net *.fb.com *.oculuscdn.com;media-src *.cdninstagram.com blob: *.fbcdn.net *.fbsbx.com www.facebook.com *.facebook.com https://*.giphy.com data:;frame-src *.doubleclick.net *.google.com *.facebook.com www.googleadservices.com *.fbsbx.com fbsbx.com data: www.instagram.com *.fbcdn.net https://paywithmybank.com https://sandbox.paywithmybank.com;worker-src blob: *.facebook.com data:;block-all-mixed-content;upgrade-insecure-requests;"},
        {"nel", "{\"report_to\":\"network-errors\",\"max_age\":3600,\"failure_fraction\":0.01}"},
        {"document-policy", "force-load-at-top"},
        {"permissions-policy",
         "accelerometer=(), ambient-light-sensor=(), bluetooth=(), camera=(self), geolocation=(self), gyroscope=(), hid=(), idle-detection=(), magnetometer=(), microphone=(self), midi=(), payment=(), screen-wake-lock=(), serial=(), usb=()"},
        {"cross-origin-resource-policy", "cross-origin"},
        {"cross-origin-opener-policy", "unsafe-none"},
        {"Pragma", "no-cache"},
        {"Cache-Control", "private, no-cache, no-store, must-revalidate"},
        {"Expires", "Sat, 01 Jan 2000 00:00:00 GMT"},
        {"X-Content-Type-Options", "nosniff"},
        {"X-XSS-Protection", "0"},
        {"X-Frame-Options", "DENY"},
        {"Strict-Transport-Security", "max-age=15552000; preload"},
        {"Content-Type", "text/html; charset=\"utf-8\""},
        {"X-FB-Debug",
         "loQB2aFVut0hfbWzWxtLe+ftfUXAFqNiGtlBtFhyXGTUNv6svsXlGdoN23oKNcLPTSe8W+4k3CUrAIoRTmGz8A=="},
        {"Date", "Wed, 18 Oct 2023 12:09:43 GMT"},
        {"Transfer-Encoding", "chunked"},
        {"Alt-Svc", "h3=\":443\"; ma=86400"},
        {"Connection", "keep-alive"}
      ],
      request_url: "https://www.facebook.com/",
      request: %SslMoon.Http.HttpClient.Request{
        url: "http://test.com/",
        body: "",
        headers: [],
        method: :get,
        options: [],
        params: []
      }
    }
  end
end
