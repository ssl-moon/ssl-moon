defmodule SslMoon.DummyChecksRouter do
  @moduledoc """
    Fixture for checks router.
  """
  use SslMoon.Checks.Router

  checks do
    check "test_check" do
      version(1)
      executor(SslMoon.DummyExecutor)
      template(SslMoon.DummyTemplate)
      migrator(SslMoon.DummyMigrator)
      validation_schema(SslMoon.DummySchema)
    end

    check "test_check_1" do
      version(1)
      executor(SslMoon.DummyExecutor)
      template(SslMoon.DummyTemplate)
      migrator(SslMoon.DummyMigrator)
      validation_schema(SslMoon.DummySchema)
    end
  end
end
