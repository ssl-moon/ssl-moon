defmodule SslMoon.DummyExecutor do
  use SslMoon.Checks.Executor

  @moduledoc """
    Executor fixture.
  """

  def perform_check(%SslMoon.Checks.Check{}, _url) do
    {:ok, %{}}
  end

  def qualify_check(%SslMoon.Checks.Check{}, _url, _data) do
    {:ok, :success}
  end
end
