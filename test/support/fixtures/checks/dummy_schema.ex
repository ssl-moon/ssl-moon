defmodule SslMoon.DummySchema do
  @moduledoc false

  use SslMoon.Checks.Schema
  import Ecto.Changeset

  embedded_schema do
    field :test, :string
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [:test])
  end
end
