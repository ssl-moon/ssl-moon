defmodule SslMoon.Checks.ExecutorTest do
  @moduledoc """
    Module containing test utilities for testing executors.
  """
  import SslMoon.Checks.RouterTest

  alias SslMoon.Checks
  alias SslMoon.Checks.Check
  alias SslMoon.Schemas.Check, as: CheckSchema

  @doc """
    Performs execution of specified executor module.

    The injected check is a generated placeholder and might not contain real data.
  """
  def execute_check(executor_module, domain_name) do
    check = create_check()

    executor_module.execute_check(check, domain_name)
  end

  @doc """
    Creates a check response struct that has both the parent and check schema casted and validated.
  """
  @spec create_response(
          Check.t(),
          status :: :success | :warning | :failure | :inactive | :not_found,
          check_result :: map()
        ) :: CheckSchema.t()
  def create_response(check, status, check_result) do
    result =
      CheckSchema.changeset(%CheckSchema{}, %{
        check_name: check.name,
        version: check.version,
        status: status,
        check_result: check_result
      })
      |> Ecto.Changeset.apply_action!(:update)

    Checks.cast_check_result(check, result)
  end
end
