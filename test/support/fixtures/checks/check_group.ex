defmodule SslMoon.CheckGroupFixture do
  @moduledoc false
  import SslMoon.ChecksFactory

  def check_group_fixture() do
    build(:checks_group, %{checked_at: DateTime.utc_now()}) |> insert()
  end
end
