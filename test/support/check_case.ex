defmodule SslMoon.CheckCase do
  @moduledoc """
    This module defines the setup for tests requiring
    access to the checks layer.

    You may define functions here to be used as helpers in
    your tests.

    This case supports the following tags:

    * `executor` module (required)
    * `template` module (required)
    * `validation_schema` module (required)

    * `check_name` string (optional), defaults to "test_check"
    * `display_name` string (optional), defaults to "Test Check"
    * `version` integer (optional), defauls to 0

    The module exports the following contexts:

    * `check` - generated check
    * `host` - a string with a generated host
    * `checks_group` - a check group entry
  """
  use ExUnit.CaseTemplate

  import SslMoon.CheckGroupFixture

  using do
    quote do
      import SslMoon.CheckCase
    end
  end

  setup tags do
    Code.compiler_options(ignore_module_conflict: true)

    check_name = tags[:check_name] || "test_check"
    display_name = tags[:display_name] || "Test Check"
    version = tags[:version] || 0
    executor = tags.executor
    template = tags.template
    validation_schema = tags.validation_schema

    defmodule TestRouter do
      @moduledoc false
      use SslMoon.Checks.Router

      checks do
        check(check_name) do
          display_name(display_name)
          version(version)
          executor(executor)
          template(template)
          validation_schema(validation_schema)
        end
      end
    end

    [check: TestRouter.checks() |> hd()]
  end

  setup do
    [host: "test.com"]
  end

  setup do
    checks_group = check_group_fixture()
    [checks_group: checks_group]
  end
end
