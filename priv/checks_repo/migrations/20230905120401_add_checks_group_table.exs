defmodule SslMoon.ChecksRepo.Sqlite.Migrations.AddChecksGroupTable do
  use Ecto.Migration

  def change do
    create table("checks_group") do
      add :domain_name, :string, null: false
      add :checked_at, :naive_datetime, null: false

      timestamps()
    end
  end
end
