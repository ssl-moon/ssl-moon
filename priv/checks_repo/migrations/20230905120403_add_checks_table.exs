defmodule SslMoon.ChecksRepo.Migrations.AddChecksTable do
  use Ecto.Migration

  def change do
    create table("checks") do
      add :check_name, :string
      add :version, :integer
      add :status, :string
      add :check_result, :map
      add :check_group_id, references(:checks_group), null: true

      timestamps()
    end
  end
end
