import Config

db_engine = System.get_env("DATABASE_ENGINE") || "sqlite"
config :ssl_moon, db_engine: db_engine

{checks_repo, tasks_repo} =
  if db_engine == "postgres" do
    {SslMoon.ChecksRepo.Postgres, SslMoon.TasksRepo.Postgres}
  else
    {SslMoon.ChecksRepo.Sqlite, SslMoon.TasksRepo.Sqlite}
  end

oban_engine =
  if db_engine == "postgres" do
    Oban.Engines.Basic
  else
    Oban.Engines.Lite
  end

config :ssl_moon,
  ecto_repos: [checks_repo, tasks_repo]

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :ssl_moon, SslMoon.HostPathsRepo,
  database: Path.expand("../db/domains_test.db", Path.dirname(__ENV__.file)),
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# Configure your database
config :ssl_moon, SslMoon.ChecksRepo.Sqlite,
  database: Path.expand("../db/ssl_moon_checks_test.db", Path.dirname(__ENV__.file)),
  journal_mode: :memory,
  temp_store: :memory,
  stacktrace: true,
  show_sensitive_data_on_connection_error: true,
  pool_size: 10,
  pool: Ecto.Adapters.SQL.Sandbox,
  priv: "priv/checks_repo"

config :ssl_moon, SslMoon.TasksRepo.Sqlite,
  database: Path.expand("../db/ssl_moon_tasks_test.db", Path.dirname(__ENV__.file)),
  journal_mode: :memory,
  temp_store: :memory,
  stacktrace: true,
  show_sensitive_data_on_connection_error: true,
  pool_size: 10,
  pool: Ecto.Adapters.SQL.Sandbox,
  priv: "priv/tasks_repo"

config :ssl_moon, SslMoon.ChecksRepo.Postgres,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  database: "ssl_moon_checks_test",
  stacktrace: true,
  show_sensitive_data_on_connection_error: true,
  pool_size: 10,
  pool: Ecto.Adapters.SQL.Sandbox,
  priv: "priv/checks_repo"

config :ssl_moon, SslMoon.TasksRepo.Postgres,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  database: "ssl_moon_tasks_test",
  stacktrace: true,
  show_sensitive_data_on_connection_error: true,
  pool_size: 10,
  pool: Ecto.Adapters.SQL.Sandbox,
  priv: "priv/tasks_repo"

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :ssl_moon, SslMoonWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "RZ1p7zAWuU/3BeyE7XKhwHhzbCdSMgg7IvjL3E69iuh56xNLFd6LgTyh4b463U8V",
  server: false

# In test we don't send emails.
config :ssl_moon, SslMoon.Mailer, adapter: Swoosh.Adapters.Test

# Disable swoosh api client as it is only required for production adapters.
config :swoosh, :api_client, false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :ssl_moon, Oban,
  engine: oban_engine,
  repo: tasks_repo,
  plugins: [
    Oban.Plugins.Pruner,
    {Oban.Plugins.Cron,
     crontab: [
       {"@daily", SslMoon.Oban.BatchEnqueueWorker}
     ]}
  ],
  queues: [
    batch: 10,
    # IMPORTANT: This is the rate limit for checks
    check: 10
  ],
  testing: :disabled
