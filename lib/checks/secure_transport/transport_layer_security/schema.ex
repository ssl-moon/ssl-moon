defmodule SslMoon.Checks.TransportLayerSecurity.Schema do
  @moduledoc """
  Transport Layer Security schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  @primary_key false
  embedded_schema do
    field(:tlsv1, :boolean, default: false)
    field(:tlsv1_1, :boolean, default: false)
    field(:tlsv1_2, :boolean, default: false)
    field(:tlsv1_3, :boolean, default: false)
  end

  @impl true
  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [:tlsv1, :tlsv1_1, :tlsv1_2, :tlsv1_3])
    |> validate_required([:tlsv1, :tlsv1_1, :tlsv1_2, :tlsv1_3])
  end
end
