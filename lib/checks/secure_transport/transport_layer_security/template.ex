defmodule SslMoon.Checks.TransportLayerSecurity.Template do
  @moduledoc """
  The `TransportLayerSecurityComponent` module provides functions to render the checks for Transport Layer Security.
  """

  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :warning -> render_warning(assigns)
      :inactive -> render_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <div class="card glass p-3">
        <table class="table table-zebra">
          <thead>
            <tr>
              <th><%= gettext("Status") %></th>
              <th><%= gettext("TLS version") %></th>
              <th class="text-center"><%= gettext("Description") %></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1 do %>
                  <MaterialIcons.check_circle size={24} class="fill-green-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>
              <td>
                <b>TLS Version 1.0</b>
              </td>
              <td>
                <%= tlsv1_description() %>
              </td>
            </tr>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1_1 do %>
                  <MaterialIcons.check_circle size={24} class="fill-green-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>
              <td>
                <b><%= gettext("TLS Version 1.1") %></b>
              </td>
              <td>
                <%= tlsv1_1_description() %>
              </td>
            </tr>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1_2 do %>
                  <MaterialIcons.check_circle size={24} class="fill-green-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>

              <td>
                <b><%= gettext("TLS Version 1.2") %></b>
              </td>
              <td>
                <%= tlsv1_2_description() %>
              </td>
            </tr>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1_3 do %>
                  <MaterialIcons.check_circle size={24} class="fill-green-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>

              <td>
                <b><%= gettext("TLS Version 1.3") %></b>
              </td>
              <td>
                <%= tlsv1_3_description() %>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br />

      <p class="p-3">
        <b>
          <%= gettext("TLS versions are up to date") %>
        </b>
        <br /> <br />
        <%= gettext(
          "This host employs the most current cryptographic standards and enhancements to ensure secure communication. Utilizing the latest TLS (Transport Layer Security) standards, it adopts up-to-date protocols and encryption techniques, thereby guaranteeing robust security, encryption, and authentication. This safeguards transmitted data against known vulnerabilities and threats, making it vital for protecting sensitive information and adhering to security standards and regulations."
        ) %>
      </p>
    </div>
    """
  end

  def render_warning(assigns) do
    ~H"""
    <div test_tag="check_warning">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-warning">
        <MaterialIcons.warning style="outlined" size={42} class="fill-yellow-600" />
        <span><%= gettext("Check completed with warnings") %></span>
      </div>
      <br />
      <div class="card glass p-3">
        <table class="table table-zebra">
          <thead>
            <tr>
              <th><%= gettext("Status") %></th>
              <th><%= gettext("TLS version") %></th>
              <th class="text-center"><%= gettext("Description") %></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1 do %>
                  <MaterialIcons.warning size={24} class="fill-yellow-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>
              <td>
                <b><%= gettext("TLS Version 1.0") %></b>
              </td>
              <td>
                <%= tlsv1_description() %>
              </td>
            </tr>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1_1 do %>
                  <MaterialIcons.warning size={24} class="fill-yellow-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>
              <td>
                <b><%= gettext("TLS Version 1.1") %></b>
              </td>
              <td>
                <%= tlsv1_1_description() %>
              </td>
            </tr>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1_2 do %>
                  <MaterialIcons.check_circle size={24} class="fill-green-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>

              <td>
                <b><%= gettext("TLS Version 1.2") %></b>
              </td>
              <td>
                <%= tlsv1_2_description() %>
              </td>
            </tr>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1_3 do %>
                  <MaterialIcons.check_circle size={24} class="fill-green-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>

              <td>
                <b><%= gettext("TLS Version 1.3") %></b>
              </td>
              <td>
                <%= tlsv1_3_description() %>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("Old TLS versions are still in use") %>
        </b>
        <br /> <br />
        <%= gettext(
          "This host uses depricated TLS versions, This host relies on deprecated TLS versions, which implies that it employs outdated security protocols. This could potentially expose users to known vulnerabilities and increase the site's vulnerability to cyberattacks. The use of deprecated TLS (Transport Layer Security) protocols indicates a reliance on older and less secure TLS versions for secure communication. This introduces security risks due to the known vulnerabilities associated with these outdated protocols and may lead to non-compliance with security standards. Additionally, careful attention must be given to compatibility issues with older clients. To enhance security and adhere to best practices, it is advisable to upgrade to more secure TLS versions.meaning the site relies on outdated security protocols, potentially exposing users to known vulnerabilities and making the site more susceptible to cyberattacks."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <div class="card glass p-3">
        <table class="table table-zebra">
          <thead>
            <tr>
              <th><%= gettext("Status") %></th>
              <th><%= gettext("TLS version") %></th>
              <th class="text-center"><%= gettext("Description") %></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1 do %>
                  <MaterialIcons.warning size={24} class="fill-yellow-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>
              <td>
                <b><%= gettext("TLS Version 1.0") %></b>
              </td>
              <td>
                <%= tlsv1_description() %>
              </td>
            </tr>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1_1 do %>
                  <MaterialIcons.warning size={24} class="fill-yellow-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>
              <td>
                <b><%= gettext("TLS Version 1.1") %></b>
              </td>
              <td>
                <%= tlsv1_1_description() %>
              </td>
            </tr>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1_2 do %>
                  <MaterialIcons.check_circle size={24} class="fill-green-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>

              <td>
                <b><%= gettext("TLS Version 1.2") %></b>
              </td>
              <td>
                <%= tlsv1_2_description() %>
              </td>
            </tr>
            <tr>
              <td>
                <%= if @check_data.check_result.tlsv1_3 do %>
                  <MaterialIcons.check_circle size={24} class="fill-green-600" />
                <% else %>
                  <MaterialIcons.remove size={24} class="fill-gray-600" />
                <% end %>
              </td>

              <td>
                <b><%= gettext("TLS Version 1.3") %></b>
              </td>
              <td>
                <%= tlsv1_3_description() %>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("Modern TLS versions not on use") %>
        </b>
        <br /> <br />
        <%= gettext(
          "The host does not use the lastest TLS version, which can make the platform more vulnerable to cyber threats and potentially put user data at risk. This puts the platform at a higher risk of cyber threats and raises concerns about the safety of user data. Using outdated TLS protocols leaves the host exposed to known vulnerabilities and increases vulnerability to cyberattacks. This not only jeopardizes data security but can also result in non-compliance with security standards. While upgrading to modern TLS versions is crucial for improved security, careful consideration of compatibility with older clients is necessary."
        ) %>
      </p>
    </div>
    """
  end

  defp render_inactive(assigns) do
    ~H"""
    <div test_tag="check_inactive">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Currently, this feature is not active for this host") %></span>
      </div>
    </div>
    """
  end

  defp template_description do
    gettext(
      "TLS, which stands for Transport Layer Security, is a cryptographic protocol designed to provide secure communication over a computer network, such as the internet. It is the successor to SSL (Secure Sockets Layer), and although the terms are sometimes used interchangeably in the industry, TLS is the more modern and secure version."
    )
  end

  defp tlsv1_description do
    gettext(
      "TLS 1.0, introduced in 1999, offers encryption and authentication for network communications, but due to discovered vulnerabilities, it's now largely deprecated in favor of more secure versions."
    )
  end

  defp tlsv1_1_description do
    gettext(
      "TLS 1.1, introduced in 2006 to provide secure web communication, but it's now considered outdated and potentially vulnerable compared to newer versions."
    )
  end

  defp tlsv1_2_description do
    gettext(
      "TLS 1.2, introduced in 2008, is an improved cryptographic protocol for secure web communication, offering enhanced security features and algorithms over its predecessors."
    )
  end

  defp tlsv1_3_description do
    gettext(
      "TLS 1.3, introduced in 2018, is the latest version of the cryptographic protocol for secure web communication, offering streamlined handshake processes and advanced security features compared to previous versions."
    )
  end
end
