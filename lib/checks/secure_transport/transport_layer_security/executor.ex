defmodule SslMoon.Checks.TransportLayerSecurity.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.TransportLayerSecurity.Executor` module is a vital component of the SslMoon security suite, specifically designed to assess the Transport Layer Security (TLS) configurations of a given URL. TLS is the backbone of secure communication on the internet, providing essential encryption and authentication mechanisms to protect data transmission.

    This module encompasses two primary functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: Initiates a TLS configuration check for the specified domain. It tests the domain's server for various TLS versions (TLS 1.0, 1.1, 1.2, and 1.3) to determine which versions are supported. The function returns a map indicating the support status for each TLS version.

    - `qualify_check/3`: Analyzes the results from `perform_check/2`. The check is categorized as `:success` if only the more secure and modern versions of TLS (1.2 and/or 1.3) are supported. A `:warning` is given if older, less secure versions (1.0 or 1.1) are supported. The check results in `:failure` if neither TLS 1.2 nor 1.3 is supported.

  ## Usage

    To use this module, define a check for TLS and provide a target domain:

    ```elixir
    # Define the check and the domain
    check = "transport_layer_security"
    domain = "example.com"

    # Perform the TLS check
    {:ok, check_result} = SslMoon.Checks.TransportLayerSecurity.Executor.perform_check(check, domain)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.TransportLayerSecurity.Executor.qualify_check(check, domain, check_result)

    # `status` will indicate the outcome of the check (:success, :warning, or :failure)
    ```
    Ensuring up-to-date and secure TLS configurations is crucial for maintaining the integrity and confidentiality of data in transit over the internet.

  ## Completion Status

    The module for Transport Layer Security checks is currently in a normalized state, functioning effectively for its primary purpose. However, there is scope for further improvements and the potential addition of new checks. Future development efforts will focus on enhancing the module's capabilities, possibly including more comprehensive checks for various aspects of TLS configurations and standards. This could involve addressing advanced security features, adapting to new TLS protocols, and refining the detection mechanisms for potential vulnerabilities. The aim is to continuously improve the module, ensuring it remains up-to-date and effective in a rapidly evolving cybersecurity landscape.
  """
  require Logger

  alias SslMoon.Ssl.SslClient

  @impl true
  def perform_check(_check, url) do
    {:ok, check_tls(url)}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      case data do
        %{tlsv1_2: true, tlsv1_3: true, tlsv1: false, tlsv1_1: false} ->
          :success

        %{tlsv1_2: true, tlsv1_3: false, tlsv1: false, tlsv1_1: false} ->
          :success

        %{tlsv1_2: false, tlsv1_3: true, tlsv1: false, tlsv1_1: false} ->
          :success

        %{tlsv1: true} ->
          :warning

        %{tlsv1_1: true} ->
          :warning

        _ ->
          :failure
      end

    {:ok, status}
  end

  def check_tls(domain) do
    %{
      tlsv1: check_tls_version(domain, :tlsv1),
      tlsv1_1: check_tls_version(domain, :"tlsv1.1"),
      tlsv1_2: check_tls_version(domain, :"tlsv1.2"),
      tlsv1_3: check_tls_version(domain, :"tlsv1.3")
    }
  end

  def check_tls_version(domain, version) do
    options = [
      verify: :verify_none,
      ssl_imp: :just_tls,
      versions: [version]
    ]

    case SslClient.connect(domain, 443, options) do
      {:ok, socket} ->
        SslClient.close(socket)
        true

      {:error, _reason} ->
        false
    end
  end
end
