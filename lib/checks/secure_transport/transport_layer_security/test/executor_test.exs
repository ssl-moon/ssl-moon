defmodule SslMoon.Checks.TransportLayerSecurityTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.TransportLayerSecurity.Executor
  @moduletag template: SslMoon.Checks.TransportLayerSecurity.Template
  @moduletag validation_schema: SslMoon.Checks.TransportLayerSecurity.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "sucess status when tlsv1.2 and tlsv1.3 is enabled", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Ssl.MockSslCleint
    |> expect(
      :connect,
      4,
      fn
        "test.com",
        443,
        [
          verify: :verify_none,
          ssl_imp: :just_tls,
          versions: [:tlsv1]
        ] ->
          {:error, :socket}

        "test.com",
        443,
        [
          verify: :verify_none,
          ssl_imp: :just_tls,
          versions: [:"tlsv1.1"]
        ] ->
          {:error, :socket}

        "test.com",
        443,
        [
          verify: :verify_none,
          ssl_imp: :just_tls,
          versions: _other
        ] ->
          {:ok, :socket}
      end
    )
    |> expect(:close, 2, fn :socket ->
      :ok
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :success
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "warning status when legacy tls versions are enabled", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Ssl.MockSslCleint
    |> expect(
      :connect,
      4,
      fn "test.com", 443, _flags ->
        {:ok, :socket}
      end
    )
    |> expect(:close, 4, fn :socket ->
      :ok
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :warning
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when no tls version can be exchanged with host", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Ssl.MockSslCleint
    |> expect(
      :connect,
      4,
      fn "test.com", 443, _flags ->
        {:error, :error}
      end
    )

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :failure
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end
end
