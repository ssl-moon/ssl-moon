defmodule SslMoon.Checks.TransportLayerSecurityTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.TransportLayerSecurity.Executor
  @moduletag template: SslMoon.Checks.TransportLayerSecurity.Template
  @moduletag validation_schema: SslMoon.Checks.TransportLayerSecurity.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        tlsv1: false,
        tlsv1_1: false,
        tlsv1_2: true,
        tlsv1_3: true
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render warning template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :warning, %{
        tlsv1: false,
        tlsv1_1: false,
        tlsv1_2: true,
        tlsv1_3: true
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_warning">|
  end

  test "render failure template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        tlsv1: false,
        tlsv1_1: false,
        tlsv1_2: false,
        tlsv1_3: false
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :inactive, %{
        tlsv1: false,
        tlsv1_1: false,
        tlsv1_2: false,
        tlsv1_3: false
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :not_found, %{})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
