defmodule SslMoon.Checks.Certificates.Template do
  @moduledoc """
  The `CertificatesComponent` module provides functions to render the checks for SSL Certificates.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-700" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <div class="card glass overflow-hidden">
        <div class="card-body p-3">
          <div class="flex items-center">
            <MaterialIcons.gpp_good style="outlined" size={42} class="fill-green-800 mr-4" />
            <div>
              <p class="font-semibold text-xl mb-2"><%= @domain_name %></p>

              <span class="font-medium text-gray-500"><%= gettext("Issuer") %>:</span> <%= @check_data.check_result.peer_certificate.certificate[
                "issuer"
              ]["aggregated"] %>
              <p>
                <span class="font-medium text-gray-500"><%= gettext("Not Before") %>:</span> <%= @check_data.check_result.peer_certificate.certificate[
                  "not_before"
                ] %>
              </p>
              <p>
                <span class="font-medium text-gray-500"><%= gettext("Not After") %>:</span> <%= @check_data.check_result.peer_certificate.certificate[
                  "not_after"
                ] %>
              </p>
              <p>
                <span class="font-medium text-gray-500">
                  <%= gettext("Signature Algorithm") %>:
                </span>
                <%= @check_data.check_result.peer_certificate.certificate[
                  "signature_algorithm"
                ] %>
              </p>
            </div>
          </div>
          <br />
          <details class="collapse collapse-arrow bg-base-200 w-80">
            <summary class="collapse-title px-8 font-medium">
              <%= gettext("Show details") %>
            </summary>
            <div class="collapse-content">
              <div class="overflow-x-auto">
                <table class="table table-zebra">
                  <tbody>
                    <tr>
                      <th class="w-1/3"><%= gettext("Fingerprint") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["fingerprint"] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Issuer") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["issuer"][
                          "aggregated"
                        ] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Not After") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["not_after"] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Not Before") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["not_before"] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Serial number") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["serial_number"] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Signature Algorithm") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate[
                          "signature_algorithm"
                        ] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Subject") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["subject"][
                          "aggregated"
                        ] %>
                      </td>
                    </tr>
                    <tr>
                      <div class="overflow-x-auto">
                        <table class="table w-full">
                          <thead>
                            <tr>
                              <th><%= gettext("Extensions:") %></th>
                            </tr>
                          </thead>
                          <tbody>
                            <%= for {key, value} <- @check_data.check_result.peer_certificate.certificate["extensions"] do %>
                              <tr>
                                <th class="w-1/3"><%= key %></th>
                                <td class="break-word">
                                  <%= value %>
                                </td>
                              </tr>
                            <% end %>
                          </tbody>
                        </table>
                      </div>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </details>
        </div>
      </div>
      <br />
      <div class="border  border-gray rounded-2xl p-3">
        <p class=" p-3 text-lg  font-semibold "><%= gettext("Certificate Chain") %></p>
        <%= for certificate <- @check_data.check_result.chain_certificates do %>
          <div class="card bg-base-100 glass overflow-hidden">
            <div class="card-body p-3">
              <div class="flex items-center">
                <MaterialIcons.gpp_good style="outlined" size={32} class="fill-green-800 mr-4" />
                <div>
                  <p class="font-semibold mb-2">
                    <%= certificate.certificate["issuer"]["CN"] %>
                  </p>

                  <span class="font-small text-gray-500"><%= gettext("Issuer") %>:</span> <%= certificate.certificate[
                    "issuer"
                  ][
                    "aggregated"
                  ] %>
                  <p>
                    <span class="font-small text-gray-500">
                      <%= gettext("Not Before") %>:
                    </span>
                    <%= certificate.certificate[
                      "not_before"
                    ] %>
                  </p>
                  <p>
                    <span class="font-small text-gray-500">
                      <%= gettext("Not After") %>:
                    </span>
                    <%= certificate.certificate[
                      "not_after"
                    ] %>
                  </p>
                  <p>
                    <span class="font-small text-gray-500">
                      <%= gettext("Signature Algorithm") %>:
                    </span>
                    <%= certificate.certificate[
                      "signature_algorithm"
                    ] %>
                  </p>
                </div>
              </div>
              <br />
              <details class="collapse collapse-arrow bg-base-200 w-80">
                <summary class="collapse-title px-8 font-medium">
                  <%= gettext("Show details") %>
                </summary>
                <div class="collapse-content">
                  <div class="overflow-x-auto">
                    <table class="table table-zebra">
                      <tbody>
                        <tr>
                          <th class="w-1/3"><%= gettext("Fingerprint") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["fingerprint"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Issuer") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["issuer"]["aggregated"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Not After") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["not_after"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Not Before") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["not_before"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Serial number") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["serial_number"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Signature Algorithm") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["signature_algorithm"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Subject") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["subject"]["aggregated"] %>
                          </td>
                        </tr>
                        <tr>
                          <div class="overflow-x-auto">
                            <table class="table w-full">
                              <thead>
                                <tr>
                                  <th><%= gettext("Extensions:") %></th>
                                </tr>
                              </thead>
                              <tbody>
                                <%= for {key, value} <- certificate.certificate["extensions"] do %>
                                  <tr>
                                    <th class="w-1/3"><%= key %></th>
                                    <td class="break-word">
                                      <%= value %>
                                    </td>
                                  </tr>
                                <% end %>
                              </tbody>
                            </table>
                          </div>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </details>
            </div>
          </div>
          <br />
        <% end %>
      </div>
      <p class="p-3">
        <b><%= gettext("Valid Certificate and Certificate Chain") %></b> <br /> <br />
        <%= gettext(
          "This certificate is valid and has a valid certificate chain, meanubg the certificate has been issued by a trusted Certificate Authority (CA) and is considered reliable. SSL certificates are used to secure websites, and their validity is crucial for data encryption and user trust. The certificate chain consists of the server's certificate, intermediate certificates, and a root CA certificate. Browsers validate this chain to ensure that the website's identity has been verified and that data transmission is secure. In essence, a valid SSL certificate with a valid certificate chain ensures encrypted and trustworthy communication between users and the website."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <div class="card glass overflow-hidden">
        <div class="card-body p-3">
          <div class="flex items-center">
            <MaterialIcons.gpp_bad style="outlined" size={42} class="fill-green-800 mr-4" />
            <div>
              <p class="font-semibold text-xl mb-2"><%= @domain_name %></p>

              <span class="font-medium text-gray-500"><%= gettext("Issuer") %>:</span> <%= @check_data.check_result.peer_certificate.certificate[
                "issuer"
              ]["aggregated"] %>
              <p>
                <span class="font-medium text-gray-500">
                  <%= gettext("Not Before") %>:
                </span>
                <%= @check_data.check_result.peer_certificate.certificate[
                  "not_before"
                ] %>
              </p>
              <p>
                <span class="font-medium text-gray-500">
                  <%= gettext("Not After") %>:
                </span>
                <%= @check_data.check_result.peer_certificate.certificate[
                  "not_after"
                ] %>
              </p>
              <p>
                <span class="font-medium text-gray-500">
                  <%= gettext("Signature Algorithm") %>:
                </span>
                <%= @check_data.check_result.peer_certificate.certificate[
                  "signature_algorithm"
                ] %>
              </p>
            </div>
          </div>
          <br />
          <details class="collapse collapse-arrow bg-base-200 w-80">
            <summary class="collapse-title px-8 font-medium">
              <%= gettext("Show details") %>
            </summary>
            <div class="collapse-content">
              <div class="overflow-x-auto">
                <table class="table table-zebra">
                  <tbody>
                    <tr>
                      <th class="w-1/3"><%= gettext("Fingerprint") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["fingerprint"] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Issuer") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["issuer"][
                          "aggregated"
                        ] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Not After") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["not_after"] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Not Before") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["not_before"] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Serial number") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["serial_number"] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Signature Algorithm") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate[
                          "signature_algorithm"
                        ] %>
                      </td>
                    </tr>
                    <tr>
                      <th class="w-1/3"><%= gettext("Subject") %></th>
                      <td class="break-all">
                        <%= @check_data.check_result.peer_certificate.certificate["subject"][
                          "aggregated"
                        ] %>
                      </td>
                    </tr>
                    <tr>
                      <div class="overflow-x-auto">
                        <table class="table w-full">
                          <thead>
                            <tr>
                              <th><%= gettext("Extensions:") %></th>
                            </tr>
                          </thead>
                          <tbody>
                            <%= for {key, value} <- @check_data.check_result.peer_certificate.certificate["extensions"] do %>
                              <tr>
                                <th class="w-1/3"><%= key %></th>
                                <td class="break-word">
                                  <%= value %>
                                </td>
                              </tr>
                            <% end %>
                          </tbody>
                        </table>
                      </div>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </details>
        </div>
      </div>
      <br />
      <div class="border  border-gray rounded-2xl p-3">
        <p class=" p-3 text-lg  font-semibold "><%= gettext("Certificate Chain") %></p>
        <%= for certificate <- @check_data.check_result.chain_certificates do %>
          <div class="card bg-base-100 glass overflow-hidden">
            <div class="card-body p-3">
              <div class="flex items-center">
                <MaterialIcons.gpp_bad style="outlined" size={32} class="fill-green-800 mr-4" />
                <div>
                  <p class="font-semibold mb-2">
                    <%= certificate.certificate["issuer"][
                      "CN"
                    ] %>
                  </p>

                  <span class="font-small text-gray-500"><%= gettext("Issuer") %>:</span> <%= certificate.certificate[
                    "issuer"
                  ][
                    "aggregated"
                  ] %>
                  <p>
                    <span class="font-small text-gray-500">
                      <%= gettext("Not Before") %>:
                    </span>
                    <%= certificate.certificate[
                      "not_before"
                    ] %>
                  </p>
                  <p>
                    <span class="font-small text-gray-500">
                      <%= gettext("Not After") %>:
                    </span>
                    <%= certificate.certificate[
                      "not_after"
                    ] %>
                  </p>
                  <p>
                    <span class="font-small text-gray-500">
                      <%= gettext("Signature Algorithm") %>:
                    </span>
                    <%= certificate.certificate[
                      "signature_algorithm"
                    ] %>
                  </p>
                </div>
              </div>
              <br />
              <details class="collapse collapse-arrow bg-base-200 w-80">
                <summary class="collapse-title px-8 font-medium">
                  <%= gettext("Show details") %>
                </summary>
                <div class="collapse-content">
                  <div class="overflow-x-auto">
                    <table class="table table-zebra">
                      <tbody>
                        <tr>
                          <th class="w-1/3"><%= gettext("Fingerprint") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["fingerprint"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Issuer") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["issuer"]["aggregated"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Not After") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["not_after"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Not Before") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["not_before"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Serial number") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["serial_number"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Signature Algorithm") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["signature_algorithm"] %>
                          </td>
                        </tr>
                        <tr>
                          <th class="w-1/3"><%= gettext("Subject") %></th>
                          <td class="break-all">
                            <%= certificate.certificate["subject"]["aggregated"] %>
                          </td>
                        </tr>
                        <tr>
                          <div class="overflow-x-auto">
                            <table class="table w-full">
                              <thead>
                                <tr>
                                  <th><%= gettext("Extensions:") %></th>
                                </tr>
                              </thead>
                              <tbody>
                                <%= for {key, value} <- certificate.certificate["extensions"] do %>
                                  <tr>
                                    <th class="w-1/3"><%= key %></th>
                                    <td class="break-word">
                                      <%= value %>
                                    </td>
                                  </tr>
                                <% end %>
                              </tbody>
                            </table>
                          </div>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </details>
            </div>
          </div>
          <br />
        <% end %>
      </div>
      <br />
      <p class="p-3">
        <b><%= gettext("This certificate is not valid") %></b> <br /> <br />
        <%= gettext(
          "This certificate and its chain is not valid, it signifies potential security and trust issues. An invalid SSL certificate could mean it has expired, been revoked, or not issued by a trusted Certificate Authority (CA). Additionally, an invalid certificate chain may indicate missing or misconfigured intermediate certificates, making it impossible to establish a secure connection to the website. In such cases, web browsers will typically display warnings to users, cautioning them about potential security risks, and it's advisable to avoid interacting with such websites to protect sensitive data from potential threats."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    gettext(
      "SSL/TLS certificates are digital certificates used to secure communication between web browsers and servers on the World Wide Web. They enable encrypted connections, protecting sensitive data during transmission. These certificates verify the authenticity of websites and establish trust, contributing to web security and search engine rankings."
    )
  end
end
