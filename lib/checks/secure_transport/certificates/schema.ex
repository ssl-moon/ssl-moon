defmodule SslMoon.Checks.Certificates.Schema do
  @moduledoc """
  Certificate schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  alias SslMoon.Checks.Certificates.Certificate

  @primary_key false
  embedded_schema do
    embeds_one :peer_certificate, Certificate
    embeds_many :chain_certificates, Certificate
  end

  @impl true
  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [])
    |> cast_embed(:peer_certificate, with: &Certificate.changeset/2)
    |> cast_embed(:chain_certificates, with: &Certificate.changeset/2)
  end
end
