defmodule SslMoon.Checks.Certificates.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.Certificates.Executor` module is a critical component of the SslMoon security suite, aimed at verifying the validity and configuration of SSL/TLS certificates for a given URL. Certificates are fundamental for ensuring secure communication over the web, as they provide encryption and authenticate the identity of the website.

    This module implements `perform_check/2` and `qualify_check/3`, which together assess the certificates of a web server.

  ## Functions

    - `perform_check/2`: Initiates a certificate check for the specified URL. It retrieves the SSL/TLS certificate chain from the server, including the peer certificate and any intermediate certificates. The function returns a map containing detailed information about these certificates.

    - `qualify_check/3`: Evaluates the certificates obtained from `perform_check/2`. The check is marked as `:success` if all certificates in the chain are valid, and `:failure` if any certificate is invalid or expired.

  ## Usage

    To use this module, specify a check for SSL/TLS certificates and provide a target URL:

    ```elixir
    # Define the check and the URL
    check = "certificates"
    url = "example.com"

    # Perform the certificates check
    {:ok, check_result} = SslMoon.Checks.Certificates.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.Certificates.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success or :failure)
    ```
    This module is crucial for ensuring the integrity and trustworthiness of SSL/TLS certificates, which are essential for secure web communication.

  ## Current Status and Limitations

    Note: This module is not yet complete and may require further development to fully assess all aspects of SSL/TLS certificate validation. Users should be aware of this limitation.

  ## Completion Status

    This module, while functional in its basic capacity, has not yet reached its full potential in terms of SSL/TLS certificate validation. It currently serves as a foundational tool for assessing certificate validity, but further development is necessary to cover all aspects comprehensively. The immediate focus for future development includes expanding the module's capabilities to thoroughly assess a wider range of certificate attributes and potential security concerns. Efforts will also be directed towards enhancing the module's ability to adapt to new SSL/TLS standards and practices as they emerge. Users can expect regular updates that aim to address the current limitations and continuously improve the module's accuracy and reliability in certificate validation.
  """
  require Logger
  alias SslMoon.Ssl.SslClient

  @impl true
  def perform_check(_check, url) do
    {:ok, certificates} = SslClient.get_chain_certificates(url)
    [peer | other_certs] = certificates

    {:ok,
     %{
       peer_certificate: cert_to_map(peer),
       chain_certificates: Enum.map(other_certs, &cert_to_map/1)
     }}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    all_certs = [data.peer_certificate | data.chain_certificates]
    valid = !Enum.any?(all_certs, fn cert -> !is_cert_valid(cert) end)

    status =
      case valid do
        true -> :success
        false -> :failure
      end

    {:ok, status}
  end

  defp is_cert_valid(%{status: :valid}) do
    true
  end

  defp is_cert_valid(_cert) do
    false
  end

  defp cert_to_map(nil) do
    nil
  end

  defp cert_to_map(%{certificate: cert} = cert_schema) do
    Map.put(cert_schema, :certificate, SslMoon.Ssl.Certificate.parse_der(cert))
  end
end
