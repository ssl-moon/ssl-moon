defmodule SslMoon.Checks.Certificates.Certificate do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field :certificate, :map

    field :status, Ecto.Enum,
      values: [:valid, :hostname_check_failed, :cert_expired, :unknown_ca, :selfsigned_peer]
  end

  def changeset(cert, params) do
    cert
    |> cast(params, [:certificate, :status])
    |> validate_required([:certificate, :status])
  end
end
