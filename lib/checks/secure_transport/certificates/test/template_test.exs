defmodule SslMoon.Checks.CertificateChainTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.Certificates.Executor
  @moduletag template: SslMoon.Checks.Certificates.Template
  @moduletag validation_schema: SslMoon.Checks.Certificates.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        peer_certificate: %{
          id: nil,
          certificate: %{
            "extensions" => %{
              "authorityInfoAccess" =>
                "CA Issuers - URI:http://cacerts.digicert.com/DigiCertTLSRSASHA2562020CA1-1.crt\nOCSP - URI:http://ocsp.digicert.com\n",
              "authorityKeyIdentifier" =>
                "keyid:B7:6B:A2:EA:A8:AA:84:8C:79:EA:B4:DA:0F:98:B2:C5:95:76:B9:F4\n",
              "basicConstraints" => "CA:FALSE",
              "certificatePolicies" =>
                "Policy: 2.23.140.1.2.2\n  CPS: http://www.digicert.com/CPS",
              "crlDistributionPoints" =>
                " URI:http://crl4.digicert.com/DigiCertTLSRSASHA2562020CA1-4.crl\nFull Name:\nFull Name:\n URI:http://crl3.digicert.com/DigiCertTLSRSASHA2562020CA1-4.crl",
              "ctlSignedCertificateTimestamp" =>
                "BIIBagFoAHYAdv-IPwq2-5VRwmHM9Ye6NLSkzbsp3GhCCp_mZ0xaOnQAAAGGEYhfUwAABAMARzBFAiBSm96s1AU28hv5o2sVp75OFlFwU5jQ0HLfJTOvC_myNgIhAIuDi6cUxttOPYneQ4tmXE4cZPyiQlmZkSG6Y2KVSE_UAHYAc9meiRtMlnigIH1HneayxhzQUV5xGSqMa4AQesF3crUAAAGGEYhfMwAABAMARzBFAiEA9ooKKKACWSwCnHgsiLxjMdxMbj_lsx0KEaLtpfhcNcYCIHWeBpkmgeGARE79I2Q1fvjkUo3s0U7v9rGIPaAdQ7BFAHYASLDja9qmRzQP5WoC-p0w6xxSActW3SyB2bu_qznYhHMAAAGGEYhfIQAABAMARzBFAiEA7X0QQ_tQtu1FUE_95IzCtspq96ZGDmsKqS7Ubqj28C8CID0lrpkKL_bjOFjDGJvd122oIQTZoLwGg8aCMEE6mHfJ",
              "extendedKeyUsage" =>
                "TLS Web server authentication, TLS Web client authentication",
              "keyUsage" => "Digital Signature, Key Encipherment",
              "subjectAltName" => "DNS:www.example.md, DNS:example.md",
              "subjectKeyIdentifier" =>
                "2A:DD:AC:CE:FD:AD:34:79:9C:49:3A:3C:6A:67:04:CE:77:A1:97:5C"
            },
            "fingerprint" => "6C:81:16:83:7A:E2:F4:66:77:8C:CB:AD:F3:52:09:E7:BC:95:4E:1E",
            "issuer" => %{
              "C" => "US",
              "CN" => "DigiCert TLS RSA SHA256 2020 CA1",
              "L" => nil,
              "O" => "DigiCert Inc",
              "OU" => nil,
              "ST" => nil,
              "aggregated" => "CN=DigiCert TLS RSA SHA256 2020 CA1, O=DigiCert Inc, C=US",
              "emailAddress" => nil
            },
            "not_after" => "2024-03-04T23:59:59Z",
            "not_before" => "2023-02-02T00:00:00Z",
            "serial_number" => "E046AE92FAC7C1E7E83C8C0607C7A6A",
            "signature_algorithm" => "sha256, rsa",
            "subject" => %{
              "C" => "MD",
              "CN" => "example.md",
              "L" => "Chisinau",
              "O" => "OTP Bank S.A.",
              "OU" => nil,
              "ST" => nil,
              "aggregated" => "CN=example.md, O=OTP Bank S.A., L=Chisinau, C=MD",
              "emailAddress" => nil
            }
          },
          status: :valid
        },
        chain_certificates: [
          %{
            id: nil,
            certificate: %{
              "extensions" => %{
                "authorityInfoAccess" =>
                  "CA Issuers - URI:http://cacerts.digicert.com/DigiCertGlobalRootCA.crt\nOCSP - URI:http://ocsp.digicert.com\n",
                "authorityKeyIdentifier" =>
                  "keyid:03:DE:50:35:56:D1:4C:BB:66:F0:A3:E2:1B:1B:C3:97:B2:3D:D1:55\n",
                "basicConstraints" => "CA:TRUE",
                "certificatePolicies" =>
                  "Policy: 2.23.140.1.2.3\nPolicy: 2.23.140.1.2.2\nPolicy: 2.23.140.1.2.1\nPolicy: 2.23.140.1.1",
                "crlDistributionPoints" =>
                  " URI:http://crl4.digicert.com/DigiCertGlobalRootCA.crl\nFull Name:\nFull Name:\n URI:http://crl3.digicert.com/DigiCertGlobalRootCA.crl",
                "extendedKeyUsage" =>
                  "TLS Web server authentication, TLS Web client authentication",
                "keyUsage" => "Digital Signature, Key Cert Sign, C R L Sign",
                "subjectKeyIdentifier" =>
                  "B7:6B:A2:EA:A8:AA:84:8C:79:EA:B4:DA:0F:98:B2:C5:95:76:B9:F4"
              },
              "fingerprint" => "69:38:FD:4D:98:BA:B0:3F:AA:DB:97:B3:43:96:83:1E:37:80:AE:A1",
              "issuer" => %{
                "C" => "US",
                "CN" => "DigiCert Global Root CA",
                "L" => nil,
                "O" => "DigiCert Inc",
                "OU" => "www.digicert.com",
                "ST" => nil,
                "aggregated" =>
                  "CN=DigiCert Global Root CA, O=DigiCert Inc, OU=www.digicert.com, C=US",
                "emailAddress" => nil
              },
              "not_after" => "2030-09-23T23:59:59Z",
              "not_before" => "2020-09-24T00:00:00Z",
              "serial_number" => "A3508D55C292B017DF8AD65C00FF7E4",
              "signature_algorithm" => "sha256, rsa",
              "subject" => %{
                "C" => "US",
                "CN" => "DigiCert TLS RSA SHA256 2020 CA1",
                "L" => nil,
                "O" => "DigiCert Inc",
                "OU" => nil,
                "ST" => nil,
                "aggregated" => "CN=DigiCert TLS RSA SHA256 2020 CA1, O=DigiCert Inc, C=US",
                "emailAddress" => nil
              }
            },
            status: :valid
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render failure template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        peer_certificate: %{
          id: nil,
          certificate: %{
            "extensions" => %{
              "authorityInfoAccess" =>
                "CA Issuers - URI:http://r3.i.lencr.org/\nOCSP - URI:http://r3.o.lencr.org\n",
              "authorityKeyIdentifier" =>
                "keyid:14:2E:B3:17:B7:58:56:CB:AE:50:09:40:E6:1F:AF:9D:8B:14:C2:C6\n",
              "basicConstraints" => "CA:FALSE",
              "certificatePolicies" => "Policy: 2.23.140.1.2.1",
              "ctlSignedCertificateTimestamp" =>
                "BIHwAO4AdQDatr9rP7W2Ip-bwrtca-hwkXFsu1GEhTS9pD0wSNf7qwAAAYtSgmXCAAAEAwBGMEQCIDMkmwtxGJP-OgrPHm1EKtn8oMtfhYoW39LngIiSR8-cAiBjERFTi_2xuaqQ2Tkkufu-vZIE-LfgzAxprHGXvy4b1gB1ADtTd3U-LbmAToswWwb-QDtn2E_D9Me9AA0tcm_h-tQXAAABi1KCZcsAAAQDAEYwRAIgDi0pw64jlm8Ifwb4O-5GTOTCKpzcbVj7-31xXnQWgL4CIEeurAiHwcgizers2uz902MKZaRyFMZwLSvnzvIew20y",
              "extendedKeyUsage" =>
                "TLS Web server authentication, TLS Web client authentication",
              "keyUsage" => "Digital Signature, Key Encipherment",
              "subjectAltName" => "DNS:www.example.md, DNS:example.md",
              "subjectKeyIdentifier" =>
                "3C:56:E1:95:DD:D7:C8:0A:0F:68:4D:A8:7F:80:EF:2D:3E:A6:97:E7"
            },
            "fingerprint" => "09:D1:E0:C2:35:6F:D2:86:B2:69:84:BA:16:E9:7D:9C:D0:D0:28:7D",
            "issuer" => %{
              "C" => "US",
              "CN" => "R3",
              "L" => nil,
              "O" => "Let's Encrypt",
              "OU" => nil,
              "ST" => nil,
              "aggregated" => "CN=R3, O=Let's Encrypt, C=US",
              "emailAddress" => nil
            },
            "not_after" => "2024-01-19T12:52:38Z",
            "not_before" => "2023-10-21T12:52:39Z",
            "serial_number" => "31EC5D859407AD1A2374A165B47104B3E32",
            "signature_algorithm" => "sha256, rsa",
            "subject" => %{
              "C" => nil,
              "CN" => "example.md",
              "L" => nil,
              "O" => nil,
              "OU" => nil,
              "ST" => nil,
              "aggregated" => "CN=example.md",
              "emailAddress" => nil
            }
          },
          status: :valid
        },
        chain_certificates: [
          %{
            id: nil,
            certificate: %{
              "extensions" => %{
                "authorityInfoAccess" => "CA Issuers - URI:http://x1.i.lencr.org/\n",
                "authorityKeyIdentifier" =>
                  "keyid:79:B4:59:E6:7B:B6:E5:E4:01:73:80:08:88:C8:1A:58:F6:E9:9B:6E\n",
                "basicConstraints" => "CA:TRUE",
                "certificatePolicies" =>
                  "Policy: 1.3.6.1.4.1.44947.1.1.1\nPolicy: 2.23.140.1.2.1",
                "crlDistributionPoints" => "Full Name:\n URI:http://x1.c.lencr.org/",
                "extendedKeyUsage" =>
                  "TLS Web client authentication, TLS Web server authentication",
                "keyUsage" => "Digital Signature, Key Cert Sign, C R L Sign",
                "subjectKeyIdentifier" =>
                  "14:2E:B3:17:B7:58:56:CB:AE:50:09:40:E6:1F:AF:9D:8B:14:C2:C6"
              },
              "fingerprint" => "A0:53:37:5B:FE:84:E8:B7:48:78:2C:7C:EE:15:82:7A:6A:F5:A4:05",
              "issuer" => %{
                "C" => "US",
                "CN" => "ISRG Root X1",
                "L" => nil,
                "O" => "Internet Security Research Group",
                "OU" => nil,
                "ST" => nil,
                "aggregated" => "CN=ISRG Root X1, O=Internet Security Research Group, C=US",
                "emailAddress" => nil
              },
              "not_after" => "2025-09-15T16:00:00Z",
              "not_before" => "2020-09-04T00:00:00Z",
              "serial_number" => "912B084ACF0C18A753F6D62E25A75F5A",
              "signature_algorithm" => "sha256, rsa",
              "subject" => %{
                "C" => "US",
                "CN" => "R3",
                "L" => nil,
                "O" => "Let's Encrypt",
                "OU" => nil,
                "ST" => nil,
                "aggregated" => "CN=R3, O=Let's Encrypt, C=US",
                "emailAddress" => nil
              }
            },
            status: :valid
          },
          %{
            id: nil,
            certificate: %{
              "extensions" => %{
                "authorityInfoAccess" =>
                  "CA Issuers - URI:http://apps.identrust.com/roots/dstrootcax3.p7c\n",
                "authorityKeyIdentifier" =>
                  "keyid:C4:A7:B1:A4:7B:2C:71:FA:DB:E1:4B:90:75:FF:C4:15:60:85:89:10\n",
                "basicConstraints" => "CA:TRUE",
                "certificatePolicies" =>
                  "Policy: 1.3.6.1.4.1.44947.1.1.1\n  CPS: http://cps.root-x1.letsencrypt.org",
                "crlDistributionPoints" =>
                  "Full Name:\n URI:http://crl.identrust.com/DSTROOTCAX3CRL.crl",
                "keyUsage" => "Key Cert Sign, C R L Sign",
                "subjectKeyIdentifier" =>
                  "79:B4:59:E6:7B:B6:E5:E4:01:73:80:08:88:C8:1A:58:F6:E9:9B:6E"
              },
              "fingerprint" => "93:3C:6D:DE:E9:5C:9C:41:A4:0F:9F:50:49:3D:82:BE:03:AD:87:BF",
              "issuer" => %{
                "C" => nil,
                "CN" => "DST Root CA X3",
                "L" => nil,
                "O" => "Digital Signature Trust Co.",
                "OU" => nil,
                "ST" => nil,
                "aggregated" => "CN=DST Root CA X3, O=Digital Signature Trust Co.",
                "emailAddress" => nil
              },
              "not_after" => "2024-09-30T18:14:03Z",
              "not_before" => "2021-01-20T19:14:03Z",
              "serial_number" => "4001772137D4E942B8EE76AA3C640AB7",
              "signature_algorithm" => "sha256, rsa",
              "subject" => %{
                "C" => "US",
                "CN" => "ISRG Root X1",
                "L" => nil,
                "O" => "Internet Security Research Group",
                "OU" => nil,
                "ST" => nil,
                "aggregated" => "CN=ISRG Root X1, O=Internet Security Research Group, C=US",
                "emailAddress" => nil
              }
            },
            status: :unknown_ca
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :inactive, %{})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :not_found, %{})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
