defmodule SslMoon.Checks.CertificateChainTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks.Certificates.Fixture
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.Certificates.Executor
  @moduletag template: SslMoon.Checks.Certificates.Template
  @moduletag validation_schema: SslMoon.Checks.Certificates.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "success status when certificate was fetched and is valid", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Ssl.MockSslCleint
    |> expect(:get_chain_certificates, fn "test.com" ->
      {:ok, valid_chain_fixture()}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :success
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when chain has invalid CA", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Ssl.MockSslCleint
    |> expect(:get_chain_certificates, fn "test.com" ->
      resp = invalid_ca_chain_fixture()

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :failure
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when one of the certificates in chain is expired", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Ssl.MockSslCleint
    |> expect(:get_chain_certificates, fn "test.com" ->
      resp = expired_cert_fixture()

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :failure
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when certificate hostname is incorrect", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Ssl.MockSslCleint
    |> expect(:get_chain_certificates, fn "test.com" ->
      resp = invalid_hostname_fixture()

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :failure
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end
end
