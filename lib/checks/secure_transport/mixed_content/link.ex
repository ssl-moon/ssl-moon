defmodule SslMoon.Checks.MixedContent.Link do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field :total, {:array, :string}
    field :unsafe, {:array, :string}
  end

  def changeset(cert, params) do
    cert
    |> cast(params, [:total, :unsafe])
    |> validate_required([:total, :unsafe])
  end
end
