defmodule SslMoon.Checks.MixedContentTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.MixedContent.Executor
  @moduletag template: SslMoon.Checks.MixedContent.Template
  @moduletag validation_schema: SslMoon.Checks.MixedContent.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        "css" => %{"total" => [], "unsafe" => []},
        "links" => %{"total" => ["https://www.example.md/"], "unsafe" => []},
        "media" => %{"total" => [], "unsafe" => []},
        "scripts" => %{"total" => [], "unsafe" => []}
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render warning template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :warning, %{
        "css" => %{"total" => [], "unsafe" => []},
        "links" => %{"total" => [], "unsafe" => []},
        "media" => %{
          "total" => ["https://www.example.md/"],
          "unsafe" => ["https://www.example.md/"]
        },
        "scripts" => %{"total" => [], "unsafe" => []}
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_warning">|
  end

  test "render failure template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        "css" => %{"total" => ["http://www.example.md/"], "unsafe" => ["http://www.example.md/"]},
        "links" => %{
          "total" => ["http://www.example.md/"],
          "unsafe" => ["http://www.example.md/"]
        },
        "media" => %{
          "total" => ["http://www.example.md/"],
          "unsafe" => ["http://www.example.md/"]
        },
        "scripts" => %{"total" => [], "unsafe" => []}
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :inactive, %{})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :not_found, %{})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
