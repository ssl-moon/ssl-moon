defmodule SslMoon.Checks.MixedContent.Schema do
  @moduledoc """
  Mixed Content schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset
  alias SslMoon.Checks.MixedContent.Link

  @primary_key false

  embedded_schema do
    embeds_one :scripts, Link
    embeds_one :css, Link
    embeds_one :media, Link
    embeds_one :links, Link
  end

  @impl true
  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [])
    |> cast_embed(:scripts, with: &Link.changeset/2)
    |> cast_embed(:css, with: &Link.changeset/2)
    |> cast_embed(:media, with: &Link.changeset/2)
    |> cast_embed(:links, with: &Link.changeset/2)
  end
end
