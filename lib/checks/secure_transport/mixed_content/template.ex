defmodule SslMoon.Checks.MixedContent.Template do
  @moduledoc """
  The `MixedContentComponent` module provides functions to render the checks for Mixed Content.
  """

  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :warning -> render_warning(assigns)
      :inactive -> render_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>

      <%!-- Scripts --%>
      <div :if={length(@check_data.check_result.scripts.total) > 0} class="card glass p-3 my-5">
        <div class="flex">
          <MaterialIcons.javascript style="outlined" size={56} class="fill-black-700" />
          <div>
            <b>
              <%= length(@check_data.check_result.scripts.total) %> <%= gettext("script(s)") %>
            </b>
            <br />
            <span class="flex">
              <MaterialIcons.check_circle size={22} class="fill-green-600" />
              &nbsp; <%= gettext("All script files are secured") %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium "><%= gettext("View resources") %></summary>
          <div class="collapse-content collapse-arrow">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base"><%= gettext("Total Resources") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.scripts.total do %>
                  <tr>
                    <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </details>
      </div>

      <%!-- CSS --%>
      <div :if={length(@check_data.check_result.css.total) > 0} class="card glass p-3 my-5">
        <div class="flex">
          <MaterialIcons.css style="outlined" size={56} class="fill-black-700" />
          <div>
            <b>
              <%= length(@check_data.check_result.css.total) %> <%= gettext("stylesheets(s)") %>
            </b>
            <br />
            <span class="flex">
              <MaterialIcons.check_circle size={22} class="fill-green-600" />
              &nbsp; <%= gettext("All stylesheets files are secured") %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium "><%= gettext("View resources") %></summary>
          <div class="collapse-content collapse-arrow">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base"><%= gettext("Total Resources") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.css.total do %>
                  <tr>
                    <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </details>
      </div>

      <%!-- Media --%>
      <div :if={length(@check_data.check_result.media.total) > 0} class="card glass p-3 my-5">
        <div class="flex ">
          <MaterialIcons.perm_media style="outlined" size={46} class="fill-black-700" />
          <div class="px-3">
            <b>
              <%= length(@check_data.check_result.media.total) %> <%= gettext("media files(s)") %>
            </b>
            <br />
            <span class="flex">
              <MaterialIcons.check_circle size={22} class="fill-green-600" />
              &nbsp; <%= gettext("All media files are secured") %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium "><%= gettext("View resources") %></summary>
          <div class="collapse-content collapse-arrow">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base"><%= gettext("Total Resources") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.media.total do %>
                  <tr>
                    <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </details>
      </div>

      <%!-- Links --%>
      <div :if={length(@check_data.check_result.links.total) > 0} class="card glass p-3 my-5">
        <div class="flex ">
          <MaterialIcons.link style="outlined" size={46} class="fill-black-700" />
          <div class="px-3">
            <b>
              <%= length(@check_data.check_result.links.total) %> <%= gettext("link(s)") %>
            </b>
            <br />
            <span class="flex">
              <MaterialIcons.check_circle size={22} class="fill-green-600" />
              &nbsp; <%= gettext("All links are secured") %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium "><%= gettext("View links") %></summary>
          <div class="collapse-content collapse-arrow">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base"><%= gettext("Total Links") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.links.total do %>
                  <tr>
                    <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </details>
      </div>
      <p class="p-3">
        <b><%= gettext("All resources are secured") %></b> <br /> <br />
        <%= gettext(
          "The resources are encrypted and protected from potential eavesdropping or tampering. When the resources for mixed content are protected, it means that measures have been taken to secure resources (such as images, scripts, or stylesheets) loaded via HTTP on a web page served over HTTPS. This protection involves encrypting these resources during transmission to prevent eavesdropping and tampering. Additionally, features like Subresource Integrity (SRI) can be used to verify the integrity of these resources. These efforts collectively ensure that even mixed content remains secure, maintaining the overall safety and trustworthiness of the web page."
        ) %>
      </p>
    </div>
    """
  end

  defp render_warning(assigns) do
    ~H"""
    <div test_tag="check_warning">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-warning">
        <MaterialIcons.warning style="outlined" size={42} class="fill-yellow-600" />
        <span><%= gettext("Check completed with warnings") %></span>
      </div>
      <br />
      <%!-- Scripts --%>
      <div :if={length(@check_data.check_result.scripts.total) > 0} class="card glass p-3 my-5">
        <div class="flex">
          <MaterialIcons.javascript style="outlined" size={56} class="fill-black-700" />
          <div>
            <b>
              <%= length(@check_data.check_result.scripts.total) %> <%= gettext("script(s)") %>
            </b>
            <br />
            <span class="flex">
              <MaterialIcons.check_circle size={22} class="fill-green-600" />
              &nbsp; <%= gettext("All script files are secured") %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium "><%= gettext("View resources") %></summary>
          <div class="collapse-content collapse-arrow">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base"><%= gettext("Total Resources") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.scripts.total do %>
                  <tr>
                    <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </details>
      </div>

      <%!-- CSS --%>
      <div :if={length(@check_data.check_result.css.total) > 0} class="card glass p-3 my-5">
        <div class="flex">
          <MaterialIcons.css style="outlined" size={56} class="fill-black-700" />
          <div>
            <b>
              <%= length(@check_data.check_result.css.total) %> <%= gettext("stylesheets(s)") %>
            </b>
            <br />
            <span class="flex">
              <MaterialIcons.check_circle size={22} class="fill-green-600" />
              &nbsp; <%= gettext("All stylesheets files are secured") %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium ">View resources</summary>
          <div class="collapse-content collapse-arrow">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base"><%= gettext("Total Resources") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.css.total do %>
                  <tr>
                    <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </details>
      </div>

      <%!-- Media --%>
      <div :if={length(@check_data.check_result.media.total) > 0} class="card glass p-3 my-5">
        <div class="flex ">
          <MaterialIcons.perm_media style="outlined" size={46} class="fill-black-700" />
          <div class="px-3">
            <b>
              <%= length(@check_data.check_result.media.total) %> <%= gettext("media files(s)") %>
            </b>
            <br />
            <span class="flex">
              <%= if length(@check_data.check_result.media.unsafe)==0 do %>
                <MaterialIcons.check_circle size={22} class="fill-green-600" />
                &nbsp; <%= gettext("All media files are secured") %>
              <% else %>
                <MaterialIcons.warning size={22} class="fill-yellow-600" />
                &nbsp; <%= length(@check_data.check_result.media.unsafe) %> <%= gettext(
                  "media files are unsafe"
                ) %>
              <% end %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium "><%= gettext("View resources") %></summary>
          <div class="collapse-content collapse-arrow">
            <table :if={length(@check_data.check_result.media.unsafe) > 0} class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base"><%= gettext("Unsafe Resources") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.media.unsafe do %>
                  <tr>
                    <td><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base"><%= gettext("Total Resources") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.media.total do %>
                  <tr>
                    <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </details>
      </div>

      <%!-- Links --%>
      <div :if={length(@check_data.check_result.links.total) > 0} class="card glass p-3 my-5">
        <div class="flex ">
          <MaterialIcons.link style="outlined" size={46} class="fill-black-700" />
          <div class="px-3">
            <b>
              <%= length(@check_data.check_result.links.total) %> <%= gettext("link(s)") %>
            </b>
            <br />
            <span class="flex">
              <%= if length(@check_data.check_result.links.unsafe)==0 do %>
                <MaterialIcons.check_circle size={22} class="fill-green-600" />
                &nbsp; <%= gettext("All links are secured") %>
              <% else %>
                <MaterialIcons.warning size={22} class="fill-yellow-600" />
                &nbsp; <%= length(@check_data.check_result.links.unsafe) %> <%= gettext(
                  "link(s) are unsafe"
                ) %>
              <% end %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium "><%= gettext("View links") %></summary>
          <div class="collapse-content collapse-arrow">
            <table :if={length(@check_data.check_result.links.unsafe) > 0} class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base text-yellow-700"><%= gettext("Unsafe Links") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.links.unsafe do %>
                  <tr>
                    <td class="font-mono break-all text-sm py-1 text-yellow-700"><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th class="text-base"><%= gettext("Total Links") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for resource <- @check_data.check_result.links.total do %>
                  <tr>
                    <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </details>
      </div>
      <p class="p-3">
        <b><%= gettext("Some resources are not secure") %></b> <br /> <br />
        <%= gettext(
          "Only some resources or assets are encrypted and protected, while others are potentially exposed to risks. When only some resources are protected on a web page, it means that a mix of secure (HTTPS) and insecure (HTTP) resources are loaded. Secure resources are encrypted and safe from eavesdropping, while insecure ones lack encryption and pose security risks. Browsers typically warn users about mixed content. To ensure security, developers should aim to make all resources secure via HTTPS by updating URLs and server configurations, as mixed content can compromise web page integrity and user trust."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <div :if={@check_data.check_result != %{}}>
        <%!-- Scripts --%>
        <div :if={length(@check_data.check_result.scripts.total) > 0} class="card glass p-3 my-5">
          <div class="flex">
            <MaterialIcons.javascript style="outlined" size={56} class="fill-black-700" />
            <div>
              <b>
                <%= length(@check_data.check_result.scripts.total) %> <%= gettext("script(s)") %>
              </b>
              <br />
              <span class="flex">
                <%= if length(@check_data.check_result.scripts.unsafe)==0 do %>
                  <MaterialIcons.check_circle size={22} class="fill-green-600" />
                  &nbsp; <%= gettext("All stylesheets files are secured") %>
                <% else %>
                  <MaterialIcons.warning size={22} class="fill-yellow-600" />
                  &nbsp; <%= length(@check_data.check_result.scripts.unsafe) %>
                  <%= gettext("script(s) files are unsafe") %>
                <% end %>
              </span>
            </div>
          </div>
          <br />
          <details class="collapse border border-base-300 collapse-arrow">
            <summary class="collapse-title font-medium "><%= gettext("View resources") %></summary>
            <div class="collapse-content collapse-arrow">
              <table
                :if={length(@check_data.check_result.scripts.unsafe) > 0}
                class="table table-zebra"
              >
                <thead>
                  <tr>
                    <th class="text-base text-yellow-700"><%= gettext("Unsafe Resources") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.scripts.unsafe do %>
                    <tr>
                      <td class="font-mono break-all text-sm py-1 text-yellow-700">
                        <%= resource %>
                      </td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
              <table class="table table-zebra">
                <thead>
                  <tr>
                    <th class="text-base"><%= gettext("Total Resources") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.scripts.total do %>
                    <tr>
                      <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </details>
        </div>

        <%!-- CSS --%>
        <div :if={length(@check_data.check_result.css.total) > 0} class="card glass p-3 my-5">
          <div class="flex">
            <MaterialIcons.css style="outlined" size={56} class="fill-black-700" />
            <div>
              <b>
                <%= length(@check_data.check_result.css.total) %> <%= gettext("stylesheets(s)") %>
              </b>
              <br />
              <span class="flex">
                <%= if length(@check_data.check_result.css.unsafe)==0 do %>
                  <MaterialIcons.check_circle size={22} class="fill-green-600" />
                  &nbsp; <%= gettext("All stylesheets files are secured") %>
                <% else %>
                  <MaterialIcons.warning size={22} class="fill-yellow-600" />
                  &nbsp; <%= length(@check_data.check_result.css.unsafe) %> <%= gettext(
                    "stylesheet(s) files are unsafe"
                  ) %>
                <% end %>
              </span>
            </div>
          </div>
          <br />
          <details class="collapse border border-base-300 collapse-arrow">
            <summary class="collapse-title font-medium "><%= gettext("View resources") %></summary>
            <div class="collapse-content collapse-arrow">
              <table :if={length(@check_data.check_result.css.unsafe) > 0} class="table table-zebra">
                <thead>
                  <tr>
                    <th class="text-base text-yellow-700"><%= gettext("Unsafe Resources") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.css.unsafe do %>
                    <tr>
                      <td class="font-mono break-all text-sm py-1 text-yellow-700">
                        <%= resource %>
                      </td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
              <table class="table table-zebra">
                <thead>
                  <tr>
                    <th class="text-base"><%= gettext("Total Resources") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.css.total do %>
                    <tr>
                      <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </details>
        </div>

        <%!-- Media --%>
        <div :if={length(@check_data.check_result.media.total) > 0} class="card glass p-3 my-5">
          <div class="flex ">
            <MaterialIcons.perm_media style="outlined" size={46} class="fill-black-700" />
            <div class="px-3">
              <b>
                <%= length(@check_data.check_result.media.total) %> <%= gettext("media files(s)") %>
              </b>
              <br />
              <span class="flex">
                <%= if length(@check_data.check_result.media.unsafe)==0 do %>
                  <MaterialIcons.check_circle size={22} class="fill-green-600" />
                  &nbsp; <%= gettext("All media files are secured") %>
                <% else %>
                  <MaterialIcons.warning size={22} class="fill-yellow-600" />
                  &nbsp; <%= length(@check_data.check_result.media.unsafe) %> <%= gettext(
                    "media files are unsafe"
                  ) %>
                <% end %>
              </span>
            </div>
          </div>
          <br />
          <details class="collapse border border-base-300 collapse-arrow">
            <summary class="collapse-title font-medium "><%= gettext("View resources") %></summary>
            <div class="collapse-content collapse-arrow">
              <table :if={length(@check_data.check_result.media.unsafe) > 0} class="table table-zebra">
                <thead>
                  <tr>
                    <th class="text-base"><%= gettext("Unsafe Resources") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.media.unsafe do %>
                    <tr>
                      <td><%= resource %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
              <table class="table table-zebra">
                <thead>
                  <tr>
                    <th class="text-base"><%= gettext("Total Resources") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.media.total do %>
                    <tr>
                      <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </details>
        </div>

        <%!-- Links --%>
        <div :if={length(@check_data.check_result.links.total) > 0} class="card glass p-3 my-5">
          <div class="flex ">
            <MaterialIcons.link style="outlined" size={46} class="fill-black-700" />
            <div class="px-3">
              <b>
                <%= length(@check_data.check_result.links.total) %> <%= gettext("link(s)") %>
              </b>
              <br />
              <span class="flex">
                <%= if length(@check_data.check_result.links.unsafe)==0 do %>
                  <MaterialIcons.check_circle size={22} class="fill-green-600" />
                  &nbsp; <%= gettext("All links are secured") %>
                <% else %>
                  <MaterialIcons.warning size={22} class="fill-yellow-600" />
                  &nbsp; <%= length(@check_data.check_result.links.unsafe) %> <%= gettext(
                    "link(s) are unsafe"
                  ) %>
                <% end %>
              </span>
            </div>
          </div>
          <br />
          <details class="collapse border border-base-300 collapse-arrow">
            <summary class="collapse-title font-medium "><%= gettext("View links") %></summary>
            <div class="collapse-content collapse-arrow">
              <table :if={length(@check_data.check_result.links.unsafe) > 0} class="table table-zebra">
                <thead>
                  <tr>
                    <th class="text-base text-yellow-700"><%= gettext("Unsafe Links") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.links.unsafe do %>
                    <tr>
                      <td class="font-mono break-all text-sm py-1 text-yellow-700">
                        <%= resource %>
                      </td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
              <table class="table table-zebra">
                <thead>
                  <tr>
                    <th class="text-base"><%= gettext("Total Links") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.links.total do %>
                    <tr>
                      <td class="font-mono break-all text-sm py-1"><%= resource %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </details>
        </div>
      </div>

      <p class="p-3">
        <b><%= gettext("Resources are not secured") %></b> <br /> <br />
        <%= gettext(
          "The assets and links are not encrypted and are vulnerable to eavesdropping or tampering. When resources are not protected at all, it means that none of the content on a web page is loaded securely through HTTPS. Instead, all resources are transmitted over insecure HTTP, exposing them to potential eavesdropping and tampering. This lack of protection poses security risks, may trigger browser warnings, erodes user trust, and can lead to non-compliance with security standards and search engine ranking issues. To ensure web security and user confidence, it is recommended to use HTTPS to encrypt all web resources."
        ) %>
      </p>
    </div>
    """
  end

  defp render_inactive(assigns) do
    ~H"""
    <div test_tag="check_inactive">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Currently, this feature is not active for this host") %></span>
      </div>
    </div>
    """
  end

  defp template_description do
    gettext(
      "Mixed content refers to a security vulnerability where a website incorporates both secure (HTTPS) and insecure (HTTP) elements. This combination poses a risk as attackers can exploit the insecure elements to compromise the overall website security."
    )
  end
end
