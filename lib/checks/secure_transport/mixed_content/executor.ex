defmodule SslMoon.Checks.MixedContent.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.MixedContent.Executor` module is a crucial part of the SslMoon security suite, designed to identify and assess mixed content issues in web applications. Mixed content occurs when a secure site (loaded via HTTPS) includes resources, such as scripts, stylesheets, media, or other links, loaded via an insecure HTTP connection. This can compromise the security of the entire page, making it vulnerable to eavesdropping or man-in-the-middle attacks.

    This module includes two primary functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: Conducts a thorough check for mixed content on the given URL. It fetches the page content and analyzes it for various types of resources (scripts, CSS, media elements, and hyperlinks) that might be loaded insecurely (via HTTP).

    - `qualify_check/3`: Evaluates the findings from `perform_check/2`. The status of the check is categorized as `:success` if no insecure content is found, `:warning` if only media or links are loaded insecurely, and `:failure` if any scripts or CSS are loaded insecurely or if the response structure is unexpected.

  ## Usage

    To use this module, specify a check for mixed content and provide a target URL:

    ```elixir
    # Define the check and the URL
    check = "mixed_content"
    url = "example.com"

    # Perform the mixed content check
    {:ok, check_result} = SslMoon.Checks.MixedContent.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.MixedContent.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :warning, :failure, or :inactive)
    ```
    Addressing mixed content issues is essential for maintaining the security and integrity of HTTPS websites, protecting users from potential security risks.

  ## Completion Status

    Currently, the module has reached a normalized operational state, successfully performing basic MixedContent verifications. However, it is acknowledged that there is significant work to be done before the module can be considered to have reached a stable and final state. One key area identified for improvement is the MixedContent verification method itself. Plans are in place to either enhance the efficiency of the existing method or to replace it with a more advanced and effective solution. This ongoing development will focus on making the module more robust, efficient, and capable of handling a wider range of mixed content scenarios in web applications, aligning with evolving web standards and security practices.
  """
  require Logger

  alias SslMoon.Http.HttpClient
  alias SslMoon.Http.HttpUtils

  @media_elements ["img", "audio", "video", "object"]

  @impl true
  def perform_check(_check, url) do
    {:ok, check_mixed_content(url)}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      case data do
        %{
          scripts: %{total: [], unsafe: []},
          css: %{total: [], unsafe: []},
          media: %{total: [], unsafe: []},
          links: %{total: [], unsafe: []}
        } ->
          :inactive

        %{
          scripts: %{unsafe: []},
          css: %{unsafe: []},
          media: %{unsafe: []},
          links: %{unsafe: []}
        } ->
          :success

        %{
          scripts: %{unsafe: []},
          css: %{unsafe: []},
          media: %{unsafe: _media},
          links: %{unsafe: _links}
        } ->
          :warning

        %{
          scripts: %{unsafe: _scripts},
          css: %{unsafe: _css},
          media: %{unsafe: _media},
          links: %{unsafe: _links}
        } ->
          :failure

        %{} ->
          :inactive
      end

    {:ok, status}
  end

  defp check_mixed_content(url) do
    case HttpUtils.get_follow_redirect(url) do
      {:ok, %HttpClient.Response{} = response} ->
        check_page(response)

      {:error, _reason} ->
        %{}
    end
  end

  defp check_page(response) do
    # parse the HTML response
    parsed_html = Floki.parse_document!(response.body)

    # Check different types of mixed content
    %{
      scripts: check_scripts(parsed_html),
      css: check_css(parsed_html),
      media: check_media(parsed_html),
      links: check_links(parsed_html)
    }
  end

  defp check_scripts(parsed_html) do
    scripts =
      parsed_html
      |> Floki.find("script[src]")
      |> Floki.attribute("src")

    mixed_content_links = Enum.filter(scripts, &String.starts_with?(&1, "http://"))

    %{
      total: scripts,
      unsafe: mixed_content_links
    }
  end

  defp check_css(parsed_html) do
    css_links =
      parsed_html
      |> Floki.find("link[href]")
      |> Floki.attribute("href")

    mixed_content_links = Enum.filter(css_links, &String.starts_with?(&1, "http://"))

    %{
      total: css_links,
      unsafe: mixed_content_links
    }
  end

  defp check_media(parsed_html) do
    media_sources =
      @media_elements
      |> Enum.map(fn e -> parsed_html |> Floki.find("#{e}[src]") |> Floki.attribute("src") end)
      |> List.flatten()

    mixed_content_links = Enum.filter(media_sources, &String.starts_with?(&1, "http://"))

    %{
      total: media_sources,
      unsafe: mixed_content_links
    }
  end

  defp check_links(parsed_html) do
    link_urls =
      parsed_html
      |> Floki.find("a[href]")
      |> Floki.attribute("href")

    mixed_content_links = Enum.filter(link_urls, &String.starts_with?(&1, "http://"))

    %{
      total: link_urls,
      unsafe: mixed_content_links
    }
  end
end
