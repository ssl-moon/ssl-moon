defmodule SslMoon.Checks.Cookies.Schema do
  @moduledoc """
  Cookies schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  alias SslMoon.Checks.Cookies.Cookie

  @primary_key false

  embedded_schema do
    embeds_many(:cookies, Cookie, on_replace: :delete)
  end

  @impl true
  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [])
    |> cast_embed(:cookies, with: &Cookie.changeset/2)
  end
end
