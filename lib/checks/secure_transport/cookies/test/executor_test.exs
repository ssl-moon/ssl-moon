defmodule SslMoon.Checks.CookiesTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks.Cookies.Factory
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.Cookies.Executor
  @moduletag template: SslMoon.Checks.Cookies.Template
  @moduletag validation_schema: SslMoon.Checks.Cookies.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "success status on valid and secure cookies", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn
      :get, "test.com", _, _, _ ->
        resp = build(:http_response)

        cookies_headers = [
          {"Set-Cookie",
           "fr=0pxk1iK3HTRDONHMH..BlL8sG.kg.AAA.0.0.BlL8sG.AWVyEsneKzk; expires=Tue, 16-Jan-2024 12:09:42 GMT; Max-Age=7776000; path=/; domain=.facebook.com; secure; httponly"},
          {"Set-Cookie",
           "sb=BssvZeL17GDJiU3R6AvoXcP-; expires=Thu, 21-Nov-2024 12:09:42 GMT; Max-Age=34560000; path=/; domain=.facebook.com; secure; httponly"}
        ]

        resp = Map.put(resp, :headers, cookies_headers)

        {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :success
    assert Enum.count(resp.data.cookies) == 2
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "inactive status when no cookies are set", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn
      :get, "test.com", _, _, _ ->
        resp = build(:http_response)
        {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :inactive
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when the cookies are not secure", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn
      :get, "test.com", _, _, _ ->
        resp = build(:http_response)

        cookies_headers = [
          {"Set-Cookie",
           "sb=BssvZeL17GDJiU3R6AvoXcP-; expires=Thu, 21-Nov-2024 12:09:42 GMT; Max-Age=34560000; path=/"}
        ]

        resp = Map.put(resp, :headers, cookies_headers)

        {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :failure
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end
end
