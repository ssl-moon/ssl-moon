defmodule SslMoon.Checks.CookiesTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.Cookies.Executor
  @moduletag template: SslMoon.Checks.Cookies.Template
  @moduletag validation_schema: SslMoon.Checks.Cookies.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        cookies: [
          %{
            http_cookie: "eb0cbcadd37ef7c2bb7884d70407f994",
            value: "8ms95eql4c9miv445t01p5n7q0",
            domain: "energbank.com",
            http_only: true,
            secure: true,
            expires: nil,
            same_site: nil,
            path: "/"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render warning template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :warning, %{
        cookies: [
          %{
            http_cookie: "eb0cbcadd37ef7c2bb7884d70407f994",
            value: "8ms95eql4c9miv445t01p5n7q0",
            domain: "energbank.com",
            http_only: false,
            secure: true,
            expires: nil,
            same_site: nil,
            path: "/"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_warning">|
  end

  test "render failure template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        cookies: [
          %{
            http_cookie: "eb0cbcadd37ef7c2bb7884d70407f994",
            value: "8ms95eql4c9miv445t01p5n7q0",
            domain: "energbank.com",
            http_only: true,
            secure: false,
            expires: nil,
            same_site: nil,
            path: "/"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :inactive, %{
        cookies: []
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :not_found, %{})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
