defmodule SslMoon.Checks.Cookies.Template do
  @moduledoc """
  The `CookiesComponent` module provides functions to render the checks for Cookies.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :warning -> render_warning(assigns)
      :inactive -> render_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <div class="card glass p-3">
        <div class="overflow-x-auto">
          <table class="table table-zebra">
            <thead>
              <tr>
                <th class="text-center"><%= gettext("Domain") %></th>
                <th class="text-center"><%= gettext("Expires") %></th>
                <th class="text-center"><%= gettext("HTTP Cookie") %></th>
                <th><%= gettext("HTTP Only") %></th>
                <th class="text-center"><%= gettext("Path") %></th>
                <th class="text-center"><%= gettext("Same Site") %></th>
                <th><%= gettext("Secure") %></th>
                <th class="text-center"><%= gettext("Value") %></th>
              </tr>
            </thead>
            <tbody>
              <%= for cookie <- @check_data.check_result.cookies do %>
                <tr>
                  <td class="text-center"><b><%= cookie.domain %></b></td>
                  <td class="text-center"><%= cookie.expires %></td>
                  <td class="text-center"><%= cookie.http_cookie %></td>
                  <td>
                    <%= if cookie.http_only do %>
                      <MaterialIcons.check_circle size={24} class="fill-green-600" />
                    <% else %>
                      <MaterialIcons.warning size={24} class="fill-yellow-600" />
                    <% end %>
                  </td>
                  <td class="text-center"><%= cookie.path %></td>
                  <td class="text-center"><%= cookie.same_site %></td>
                  <td>
                    <%= if cookie.secure do %>
                      <MaterialIcons.check_circle size={24} class="fill-green-600" />
                    <% else %>
                      <MaterialIcons.warning size={24} class="fill-yellow-600" />
                    <% end %>
                  </td>
                  <td class="w-1/3 font-light text-xs subpixel-antialiased break-all">
                    <%= cookie.value %>
                  </td>
                </tr>
              <% end %>
            </tbody>
          </table>
        </div>
      </div>
      <br />

      <p class="p-3">
        <b>
          <%= gettext("Cookies are secured") %>
        </b>
        <br /> <br />
        <%= gettext(
          "The cookies are secured through the implementation of various protective measures, including encryption, script access prevention, cross-origin control, appropriate expiration settings, data validation, secure token handling, regular audits, robust logging, monitoring, and adherence to security policies. These measures ensure that the cookies are well-protected against potential security threats, contributing to a safe and secure web environment."
        ) %>
      </p>
    </div>
    """
  end

  defp render_warning(assigns) do
    ~H"""
    <div test_tag="check_warning">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-warning">
        <MaterialIcons.warning style="outlined" size={42} class="fill-yellow-600" />
        <span><%= gettext("Check completed with warnings") %></span>
      </div>
      <br />
      <div class="card glass p-3">
        <div class="overflow-x-auto">
          <table class="table table-zebra">
            <thead>
              <tr>
                <th class="text-center"><%= gettext("Domain") %></th>
                <th class="text-center"><%= gettext("Expires") %></th>
                <th class="text-center"><%= gettext("HTTP Cookie") %></th>
                <th><%= gettext("HTTP Only") %></th>
                <th class="text-center"><%= gettext("Path") %></th>
                <th class="text-center"><%= gettext("Same Site") %></th>
                <th><%= gettext("Secure") %></th>
                <th class="text-center"><%= gettext("Value") %></th>
              </tr>
            </thead>
            <tbody>
              <%= for cookie <- @check_data.check_result.cookies do %>
                <tr>
                  <td class="text-center"><b><%= cookie.domain %></b></td>
                  <td class="text-center"><%= cookie.expires %></td>
                  <td class="text-center"><%= cookie.http_cookie %></td>
                  <td>
                    <%= if cookie.http_only do %>
                      <MaterialIcons.check_circle size={24} class="fill-green-600" />
                    <% else %>
                      <MaterialIcons.warning size={24} class="fill-yellow-600" />
                    <% end %>
                  </td>
                  <td class="text-center"><%= cookie.path %></td>
                  <td class="text-center"><%= cookie.same_site %></td>
                  <td>
                    <%= if cookie.secure do %>
                      <MaterialIcons.check_circle size={24} class="fill-green-600" />
                    <% else %>
                      <MaterialIcons.warning size={24} class="fill-yellow-600" />
                    <% end %>
                  </td>
                  <td class="w-1/3 font-light text-xs subpixel-antialiased break-all">
                    <%= cookie.value %>
                  </td>
                </tr>
              <% end %>
            </tbody>
          </table>
        </div>
      </div>
      <br />
      <p class="p-3">
        <b><%= gettext("Some cookies are not secure") %></b> <br /> <br />
        <%= gettext(
          "Some cookies are not secured, it means that certain protective measures have not been applied, leaving them vulnerable to potential security threats like data interception, script access, and injection attacks. This can lead to security breaches, unauthorized access, and other risks. Proper security practices should be implemented to safeguard cookies and the sensitive data they may contain."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <div class="card glass p-3">
        <div class="overflow-x-auto">
          <table class="table table-zebra">
            <thead>
              <tr>
                <th class="text-center"><%= gettext("Domain") %></th>
                <th class="text-center"><%= gettext("Expires") %></th>
                <th class="text-center"><%= gettext("HTTP Cookie") %></th>
                <th><%= gettext("HTTP Only") %></th>
                <th class="text-center"><%= gettext("Path") %></th>
                <th class="text-center"><%= gettext("Same Site") %></th>
                <th><%= gettext("Secure") %></th>
                <th class="text-center"><%= gettext("Value") %></th>
              </tr>
            </thead>
            <tbody>
              <%= for cookie <- @check_data.check_result.cookies do %>
                <tr>
                  <td class="text-center"><b><%= cookie.domain %></b></td>
                  <td class="text-center"><%= cookie.expires %></td>
                  <td class="text-center"><%= cookie.http_cookie %></td>
                  <td>
                    <%= if cookie.http_only do %>
                      <MaterialIcons.check_circle size={24} class="fill-green-600" />
                    <% else %>
                      <MaterialIcons.warning size={24} class="fill-yellow-600" />
                    <% end %>
                  </td>
                  <td class="text-center"><%= cookie.path %></td>
                  <td class="text-center"><%= cookie.same_site %></td>
                  <td>
                    <%= if cookie.secure do %>
                      <MaterialIcons.check_circle size={24} class="fill-green-600" />
                    <% else %>
                      <MaterialIcons.warning size={24} class="fill-yellow-600" />
                    <% end %>
                  </td>
                  <td class="w-1/3 font-light text-xs subpixel-antialiased break-all">
                    <%= cookie.value %>
                  </td>
                </tr>
              <% end %>
            </tbody>
          </table>
        </div>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("Cookies are not secured") %>
        </b>
        <br /> <br />
        <%= gettext(
          "The cookies shown uptop are not secured at all, they lack protective measures, rendering them highly susceptible to security threats. This vulnerability exposes users to potential data theft, unauthorized access, and exploitation of web vulnerabilities. Without safeguards like encryption, the HttpOnly flag, and proper SameSite attribute configuration, these cookies can be intercepted, accessed by scripts, and sent in cross-origin requests, making them targets for various attacks, including XSS and CSRF. Extended exposure due to overly long expiration periods and the absence of input validation and secure token handling further compound the risks. Implementing comprehensive security practices is essential to protect cookies and the sensitive data they contain, ensuring a safer online environment."
        ) %>
      </p>
    </div>
    """
  end

  defp render_inactive(assigns) do
    ~H"""
    <div test_tag="check_inactive">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Currently, this feature is not active for this host") %></span>
      </div>
    </div>
    """
  end

  defp template_description do
    gettext(
      "Cookie security involves protecting user cookies from unauthorized access and misuse. It includes measures such as using secure cookies over HTTPS and implementing appropriate cookie attributes to prevent script access, ensuring the privacy and integrity of user data."
    )
  end
end
