defmodule SslMoon.Checks.Cookies.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.Cookies.Executor` module is an important part of the SslMoon security suite, focused on evaluating the security attributes of cookies set by a web application. Properly configured cookies are essential for ensuring the security and privacy of user sessions and data.

    This module includes two main functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: This function initiates a check for cookies on the specified URL. It makes an HTTP request to the URL and analyzes the 'Set-Cookie' headers in the response, returning a map containing the details of these cookies.

    - `qualify_check/3`: Evaluates the cookies obtained from `perform_check/2`. The check is marked as `:success` if all cookies have 'Secure' and 'HttpOnly' attributes, `:warning` if 'HttpOnly' is missing, and `:failure` if 'Secure' is missing. If no cookies are found, the status is marked as `:inactive`.

  ## Usage

    To use this module, define a check for cookies and specify a target URL:

    ```elixir
    # Define the check and the URL
    check = "cookies"
    url = "example.com"

    # Perform the cookie security check
    {:ok, check_result} = SslMoon.Checks.Cookies.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.Cookies.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :warning, :inactive, or :failure)
    ```
    Assessing cookie security is crucial for web applications to protect user data and guard against common web vulnerabilities like Cross-Site Scripting (XSS) and Cross-Site Request Forgery (CSRF).

  ## Completion Status

    The module has been brought to a final state, effectively performing its primary function of assessing cookie security in web applications. This includes evaluating necessary attributes to protect against vulnerabilities such as Cross-Site Scripting (XSS) and Cross-Site Request Forgery (CSRF). However, it is recognized that the landscape of web security is continuously evolving. As such, while the module is complete in its current form, there is always potential for incorporating additional changes and validations. Future updates may focus on enhancing the module's capabilities to address emerging cookie-related security concerns, optimizing performance, and expanding its validation criteria to cover a broader range of cookie security scenarios.
  """
  require Logger
  alias SslMoon.Http.HttpClient
  alias SslMoon.Http.HttpUtils

  @impl true
  def perform_check(_check, url) do
    {:ok, %{cookies: check_cookies_are_secure(url)}}
  end

  @impl true
  def qualify_check(_check, _url, %{cookies: data}) do
    status =
      case data do
        [] ->
          :inactive

        cookies ->
          check_cookies(cookies)
      end

    {:ok, status}
  end

  def check_cookies(cookies) do
    secure =
      if Enum.all?(cookies, &Map.has_key?(&1, "secure")) do
        true
      else
        false
      end

    http_only =
      if Enum.all?(cookies, &Map.has_key?(&1, "httponly")) do
        true
      else
        false
      end

    case({secure, http_only}) do
      {false, _} -> :failure
      {_, false} -> :warning
      _ -> :success
    end
  end

  def check_cookies_are_secure(url) do
    case HttpUtils.get_follow_redirect(url) do
      {:ok, %HttpClient.Response{} = response} ->
        get_cookie_from_header(response.headers, url)

      {:error, _reason} ->
        []
    end
  end

  defp get_cookie_from_header(header, url) do
    header
    |> Enum.filter(fn {key, _value} -> String.match?(key, ~r/\Aset-cookie\z/i) end)
    |> Enum.map(fn {_key, value} -> parse_cookie(value, url) end)
  end

  defp parse_cookie(string, url) do
    [head | tail] =
      string
      |> String.split(";")

    tail =
      tail
      |> Enum.map(fn string -> split_string_tail(string) end)

    head =
      head
      |> split_string_head()

    [{key, value}] = Map.to_list(head)

    List.foldl(tail, %{}, fn item, acc ->
      Map.merge(acc, item)
    end)
    |> Map.merge(%{
      "http_cookie" => key,
      "value" => value,
      "domain" => url
    })
  end

  defp split_string_tail(string) do
    case String.split(string, "=", parts: 2) do
      [key, value] ->
        %{
          (String.replace(String.trim(key), ~r/[A-Z]/, &("_" <> String.downcase(&1)))
           |> String.replace_prefix("_", "")) => String.trim(value)
        }

      _ ->
        %{
          (String.replace(String.trim(string), ~r/[A-Z]/, &("_" <> String.downcase(&1)))
           |> String.replace_prefix("_", "")) => true
        }
    end
  end

  defp split_string_head(string) do
    case String.split(string, "=", parts: 2) do
      [key, value] -> %{String.trim(key) => String.trim(value)}
      _ -> %{String.trim(string) => true}
    end
  end
end
