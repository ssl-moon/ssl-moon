defmodule SslMoon.Checks.Cookies.Cookie do
  @moduledoc """
  Cookie schema.
  """
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:http_cookie, :string)
    field(:value, :string)
    field(:domain, :string)
    field(:http_only, :boolean, default: false)
    field(:secure, :boolean, default: false)
    field(:expires, :string)
    field(:same_site, :string)
    field(:path, :string)
  end

  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [
      :http_cookie,
      :value,
      :domain,
      :http_only,
      :secure,
      :expires,
      :same_site,
      :path
    ])
    |> validate_required([:http_cookie])
  end
end
