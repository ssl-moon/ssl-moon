defmodule SslMoon.Checks.CertificationAuthorityAuthorization.Template do
  @moduledoc """
  The `CertificationAuthorityAuthorizationComponent` module provides functions to render the checks for Certification Authority Authorization.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :failure -> TemplateHelpers.render_generic_failure(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <div class="overflow-x-auto">
        <table class="table table-zebra">
          <thead>
            <tr>
              <th><%= gettext("Type") %></th>
              <th><%= gettext("Authority") %></th>
              <th><%= gettext("Flags") %></th>
            </tr>
          </thead>
          <tbody>
            <%= for auth <- @check_data.check_result.caa do %>
              <tr>
                <th><%= auth.type %></th>
                <th><%= auth.authority %></th>
                <th><%= auth.flags %></th>
              </tr>
            <% end %>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b><%= gettext("This domain has authorized several Certificate Authorities") %></b> <br />
        <br />
        <%= gettext(
          "This domain has multiple CAA values in its DNS records, it indicates that the domain owner has authorized several Certificate Authorities (CAs) to issue SSL/TLS certificates for that domain. This setup provides flexibility, allowing the domain owner to use different CAs for various types of certificates, such as regular SSL or Extended Validation certificates. It also offers redundancy and reliability, ensuring continuous certificate availability even if one CA faces issues. Additionally, this approach aids in risk management and compliance with organizational policies by diversifying the sources of SSL/TLS certificates."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    gettext(
      "Certification Authority Authorization (CAA) is a DNS record that allows domain owners to define which Certificate Authorities (CAs) can issue certificates for their domains. This enhances security by preventing unauthorized issuance of SSL/TLS certificates."
    )
  end
end
