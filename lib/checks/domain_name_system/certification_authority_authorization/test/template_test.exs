defmodule SslMoon.Checks.CertificationAuthorityAuthorizationTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.CertificationAuthorityAuthorization.Executor
  @moduletag template: SslMoon.Checks.CertificationAuthorityAuthorization.Template
  @moduletag validation_schema: SslMoon.Checks.CertificationAuthorityAuthorization.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        caa: [
          %{
            authority: "example.com",
            type: "issue",
            flags: 0
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render failure template on invalid caa", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        caa: [
          %{
            authority: "failure",
            type: "failure",
            flags: 0
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render succes template on multimple caa", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        caa: [
          %{
            authority: "comodoca.com",
            type: "issue",
            flags: 0
          },
          %{
            authority: "comodoca.com",
            type: "issue",
            flags: 0
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :inactive, %{caa: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :not_found, %{caa: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
