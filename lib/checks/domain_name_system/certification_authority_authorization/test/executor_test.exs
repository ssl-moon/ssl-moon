defmodule SslMoon.Checks.CertificationAuthorityAuthorizationTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.CertificationAuthorityAuthorization.Executor
  @moduletag template: SslMoon.Checks.CertificationAuthorityAuthorization.Template
  @moduletag validation_schema: SslMoon.Checks.CertificationAuthorityAuthorization.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "success status when the response is in the correct format", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Dns.MockDnsClient
    |> expect(:resolve, fn ^host, :caa, _ ->
      {:ok,
       [
         %SslMoon.Dns.DnsResponse{
           domain: "#{host}.",
           type: :caa,
           class: :in,
           ttl: "2132",
           data: "0 issue \"digicert.com\""
         }
       ]}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :success
    assert resp.data == %{caa: [%{flags: 0, type: "issue", authority: "digicert.com"}]}

    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end

  test "inactive status when no data is present", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Dns.MockDnsClient
    |> expect(:resolve, fn "test.com", :caa, _ ->
      {:ok, []}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :inactive

    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end

  # This test has undefined behavior, possible there are bugs in executor implementation

  # test "test_certification_authority_authorization_error" do
  #   SslMoon.Inet.MockDnsClient
  #   |> expect(:resolve, fn "test.com", :in, :caa, _, _ ->
  #     {:error, :nxdomain}
  #   end)

  #   assert execute_check(CertificationAuthorityAuthorization, "test.com") == {:invalid, :a}
  # end
end
