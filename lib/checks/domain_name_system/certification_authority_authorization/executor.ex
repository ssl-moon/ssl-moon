defmodule SslMoon.Checks.CertificationAuthorityAuthorization.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.CertificationAuthorityAuthorization.Executor` module is a critical component of the SslMoon security suite, aimed at verifying the Certification Authority Authorization (CAA) records of a domain. CAA records are DNS resource records that specify which certificate authorities (CAs) are allowed to issue certificates for a domain, playing a vital role in the security of SSL/TLS certificates.

    The module provides two primary functions, `perform_check/2` and `qualify_check/3`, to facilitate this verification process.

  ## Functions

    - `perform_check/2`: Initiates the process of fetching CAA records for a given domain. This function utilizes the `SslMoon.Dns.DnsClient` to resolve the domain's CAA records and returns the results in a map.

    - `qualify_check/3`: Evaluates the fetched CAA records to determine the domain's compliance with CAA policies. The function categorizes the check status as `:success` if CAA records are present, `:inactive` if no records are found, or `:failure` for any other cases.

  ## Usage

    To use this module, you need to provide a specific check for CAA records and a target domain as follows:

    ```elixir
    # Define the check and the domain
    check = "caa"
    domain = "example.com"

    # Perform the CAA check
    {:ok, check_result} = SslMoon.Checks.CertificationAuthorityAuthorization.Executor.perform_check(check, domain)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.CertificationAuthorityAuthorization.Executor.qualify_check(check, domain, check_result)

    # `status` will indicate the outcome of the check (:success, :inactive, or :failure)
    ```
    This module helps in enhancing the security of SSL/TLS implementations by ensuring that CAA policies are correctly set and followed.

  ## Current Status and Improvements

    Note: This module currently exhibits buggy behavior and requires improvements. Developers are advised to use it with caution and contribute towards its enhancement for more reliable functionality.

  ## Completion Status

    The module has been brought to a normalized state, meaning that it functions according to the basic requirements and expectations. However, it is important to acknowledge that there is still work to be done to bring this module to a stable and final state. Future developments will focus on refining the functionalities, enhancing performance, addressing potential security vulnerabilities, and ensuring compliance with the latest standards and best practices in the field.
  """
  require Logger
  alias SslMoon.Dns.DnsClient
  alias SslMoon.Dns.DnsHelper

  @impl true
  def perform_check(_check, url) do
    {:ok, %{caa: fetch_caa_records(url)}}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      case data.caa do
        [] ->
          :inactive

        [_ | _] ->
          :success

        _ ->
          :failure
      end

    {:ok, status}
  end

  defp fetch_caa_records(domain) do
    case DnsClient.resolve(domain, :caa) do
      {:ok, records} ->
        Enum.map(records, fn el ->
          {:ok, resp} = DnsHelper.parse_response(el.type, el.data)
          resp
        end)

      {:error, _reason} ->
        []
    end
  end
end
