defmodule SslMoon.Checks.CertificationAuthorityAuthorization.Caa do
  @moduledoc """
  Caa schema.
  """
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:authority, :string)
    field(:type, :string)
    field(:flags, :integer)
  end

  def changeset(caa, attrs) do
    caa
    |> cast(attrs, [:authority, :type, :flags])
    |> validate_required([:authority, :type, :flags])
  end
end
