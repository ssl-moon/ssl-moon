defmodule SslMoon.Checks.CertificationAuthorityAuthorization.Schema do
  @moduledoc """
  Certification Authority Authorization schema.
  """
  use SslMoon.Checks.Schema

  import Ecto.Changeset
  alias SslMoon.Checks.CertificationAuthorityAuthorization.Caa

  @primary_key false

  embedded_schema do
    embeds_many(:caa, Caa, on_replace: :delete)
  end

  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [])
    |> cast_embed(:caa, with: &Caa.changeset/2)
  end
end
