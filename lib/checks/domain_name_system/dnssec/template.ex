defmodule SslMoon.Checks.DNSSEC.Template do
  @moduledoc """
  The `DNSSECComponent` module provides functions to render the checks for DNSSEC.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <br />
      <p class="p-3">
        <b><%= gettext("DNSSEC is configured properly") %></b> <br /> <br />
        <%= gettext(
          "The domain is securely configured with DNSSEC, ensuring data integrity and protection against spoofing by adding a layer of security to DNS records by ensuring their authenticity and integrity. It utilizes public key cryptography to digitally sign DNS records, enabling DNS resolvers to verify their authenticity. This protects against attacks like DNS poisoning by ensuring that the DNS data has not been tampered with in transit. However, it's important to note that DNSSEC does not encrypt data, it only authenticates it. Proper configuration and maintenance of DNSSEC are crucial to prevent DNS lookup failures."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    gettext(
      "DNSSEC, which stands for Domain Name System Security Extensions, is a set of extensions to DNS that adds an additional layer of security to the domain name system lookup and exchange processes."
    )
  end
end
