defmodule SslMoon.Checks.DNSSECTest.ExecutorTest do
  @moduledoc """
  SslMoon.Checks.DNSSECTest
  """
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.DNSSEC.Executor
  @moduletag template: SslMoon.Checks.DNSSEC.Template
  @moduletag validation_schema: SslMoon.Checks.DNSSEC.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "success status when one or more RRSIG records is found", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Dns.MockDnsClient
    |> expect(:resolve, fn ^host, :rrsig, _ ->
      {:ok,
       [
         %SslMoon.Dns.DnsResponse{
           domain: "example.com.",
           type: :rrsig,
           class: :in,
           ttl: "77573",
           data:
             "A 13 2 86400 20231123223519 20231102232720 2182 example.com. HyoRKXN+cV0ewSOTnITZ9QkI2qAlVlhkaK/HcykpgNF+/J70j/Hg8ou1 aeEfsppUOfMZAYWCK2XA9FAEhRJYhg=="
         }
       ]}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :success
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "inactive status when domain doesn't contain any RRSIG records", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Dns.MockDnsClient
    |> expect(:resolve, fn ^host, :rrsig, _ ->
      {:ok, []}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :inactive
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end
end
