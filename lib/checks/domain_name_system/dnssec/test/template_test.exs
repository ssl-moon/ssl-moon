defmodule SslMoon.Checks.DNSSECTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.DNSSEC.Executor
  @moduletag template: SslMoon.Checks.DNSSEC.Template
  @moduletag validation_schema: SslMoon.Checks.DNSSEC.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        records: [
          %{
            domain: "example.com.",
            type: :rrsig,
            class: :in,
            ttl: "77573",
            data:
              "A 13 2 86400 20231123223519 20231102232720 2182 example.com. HyoRKXN+cV0ewSOTnITZ9QkI2qAlVlhkaK/HcykpgNF+/J70j/Hg8ou1 aeEfsppUOfMZAYWCK2XA9FAEhRJYhg=="
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :inactive, %{records: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :not_found, %{records: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
