defmodule SslMoon.Checks.DNSSEC.DnsRecord do
  @moduledoc """
  DNS response schema.
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias SslMoon.Dns.DnsClient

  @primary_key false

  embedded_schema do
    field :domain, :string
    field :class, Ecto.Enum, values: [:in]
    field :type, Ecto.Enum, values: DnsClient.record_atoms()
    field :ttl, :string
    field :data, :string
  end

  def changeset(record, params) do
    record
    |> cast(params, [:domain, :class, :type, :ttl, :data])
    |> validate_required([:domain, :class, :type, :ttl, :data])
  end
end
