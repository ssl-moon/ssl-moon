defmodule SslMoon.Checks.DNSSEC.Schema do
  @moduledoc """
  DNS SEC schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  alias SslMoon.Checks.DNSSEC.DnsRecord

  @primary_key false

  embedded_schema do
    embeds_many(:records, DnsRecord, on_replace: :delete)
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [])
    |> cast_embed(:records, with: &DnsRecord.changeset/2)
  end
end
