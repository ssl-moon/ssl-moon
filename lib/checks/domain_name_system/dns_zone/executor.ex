defmodule SslMoon.Checks.DNSZone.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.DNSZone.Executor` module is part of the SslMoon security suite, focused on conducting checks related to the DNS Zone of a domain. This module is designed to assess the operational status of DNS servers, verifying their availability and responsiveness.

    The module is comprised of two key functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: Initiates the DNS Zone check for a given domain. It uses `SslMoon.Dns.DnsClient` to perform an NS lookup and determines the operational status of each name server. The function returns a map containing details about name servers, their IP addresses, and their operational status.

    - `qualify_check/3`: Evaluates the results from `perform_check/2`. The check is deemed successful if all name servers are operational, and a failure otherwise.

  ## Usage

    To use this module, specify a check for the DNS Zone and a target domain as follows:

    ```elixir
    # Define the check and the domain
    check = "dns_zone"
    domain = "example.com"

    # Perform the DNS Zone check
    {:ok, check_result} = SslMoon.Checks.DNSZone.Executor.perform_check(check, domain)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.DNSZone.Executor.qualify_check(check, domain, check_result)

    # `status` will indicate the outcome of the check (:success or :failure)
    ```
    This module is vital for ensuring the reliability and availability of DNS services, which are crucial for the proper functioning of any web domain.

  ## Current Status and Limitations

    Note: This module is not fully operable and may exhibit limited functionality. Users should exercise caution and be aware of potential inaccuracies in the results. Ongoing efforts are being made to enhance its capabilities.

  ## Completion Status

    The module has been brought to a normalized state, meaning that it functions according to the basic requirements and expectations. However, it is important to acknowledge that there is still work to be done to bring this module to a stable and final state. Future developments will focus on refining the functionalities, enhancing performance, addressing potential security vulnerabilities, and ensuring compliance with the latest standards and best practices in the field.
  """
  require Logger
  alias SslMoon.Dns.DnsClient
  alias SslMoon.Tcp.TcpClient

  @impl true
  def perform_check(_check, url) do
    {:ok, %{servers: get_nams_servers_and_addresses(url)}}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      if check_status(data.servers) do
        :success
      else
        :failure
      end

    {:ok, status}
  end

  # The check is not fully operable
  defp get_nams_servers_and_addresses(domain) do
    case DnsClient.nslookup(domain) do
      {:ok, responses} ->
        Enum.map(responses, fn el ->
          ip_v4 = get_addr_and_ping(el.data, :inet)
          ip_v6 = get_addr_and_ping(el.data, :inet6)
          operational = ping_server_ip(el.data)

          %{
            name_server: el.data,
            ip_v4: ip_v4,
            ip_v6: ip_v6,
            operational: operational
          }
        end)

      {:error, _reason} ->
        []
    end
  end

  defp check_status(dns) do
    case dns do
      [] -> :failure
      dns -> Enum.all?(dns, fn server -> server.operational end)
    end
  end

  defp get_addr_and_ping(domain, family) do
    {:ok, ips} = DnsClient.getaddrs(domain, family)

    Enum.map(ips, fn ip ->
      %{
        ip: ip,
        operational: ping_server_ip(ip)
      }
    end)
  end

  defp ping_server_ip(ip_address) do
    case TcpClient.connect(ip_address, 80, [], 5000) do
      {:ok, socket} ->
        TcpClient.close(socket)
        true

      {:error, _reason} ->
        false
    end
  end
end
