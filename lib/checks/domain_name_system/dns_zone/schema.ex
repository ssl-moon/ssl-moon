defmodule SslMoon.Checks.DNSZone.Schema do
  @moduledoc """
  DNS Zone schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  alias SslMoon.Checks.DNSZone.Server

  @primary_key false

  embedded_schema do
    # field(:name, :string)
    embeds_many(:servers, Server, on_replace: :delete)
  end

  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [])
    |> cast_embed(:servers, with: &Server.changeset/2)
  end
end
