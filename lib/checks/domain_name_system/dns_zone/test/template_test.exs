defmodule SslMoon.Checks.DNSZoneTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.DNSZone.Executor
  @moduletag template: SslMoon.Checks.DNSZone.Template
  @moduletag validation_schema: SslMoon.Checks.DNSZone.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        servers: [
          %{
            name_server: "ns2.succes.md.",
            operational: true,
            ip_v4: [%{"ip" => "194.247.52.110", "operational" => true}],
            ip_v6: []
          },
          %{
            name_server: "ns.succes.md.",
            operational: true,
            ip_v4: [%{"ip" => "194.247.52.211", "operational" => true}],
            ip_v6: []
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render failure template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        servers: [
          %{
            name_server: "ns2.succes.md.",
            operational: false,
            ip_v4: [%{"ip" => "194.247.52.110", "operational" => false}],
            ip_v6: []
          },
          %{
            name_server: "ns.succes.md.",
            operational: false,
            ip_v4: [%{"ip" => "194.247.52.211", "operational" => false}],
            ip_v6: []
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :not_found, %{servers: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
