defmodule SslMoon.Checks.DNSZoneTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.DNSZone.Executor
  @moduletag template: SslMoon.Checks.DNSZone.Template
  @moduletag validation_schema: SslMoon.Checks.DNSZone.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "failure status on servers that are not pingable???", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Dns.MockDnsClient
    |> expect(:nslookup, fn ^host ->
      {:ok,
       [
         %SslMoon.Dns.DnsResponse{
           domain: "maib.md.",
           type: :ns,
           class: :in,
           ttl: "3276",
           data: "ns1.maib.md."
         },
         %SslMoon.Dns.DnsResponse{
           domain: "maib.md.",
           type: :ns,
           class: :in,
           ttl: "3276",
           data: "ns2.maib.md."
         }
       ]}
    end)
    |> expect(:getaddrs, 4, fn
      "ns1.maib.md.", :inet ->
        {:ok, ["91.250.245.100"]}

      "ns1.maib.md.", :inet6 ->
        {:ok, []}

      "ns2.maib.md.", :inet ->
        {:ok, ["91.250.245.98"]}

      "ns2.maib.md.", :inet6 ->
        {:ok, []}
    end)

    SslMoon.Tcp.MockTcpClient
    |> expect(:connect, 4, fn _, _, _, _ -> {:error, :timeout} end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :failure

    assert resp.data == %{
             servers: [
               %{
                 operational: false,
                 ip_v4: [%{ip: "91.250.245.100", operational: false}],
                 ip_v6: [],
                 name_server: "ns1.maib.md."
               },
               %{
                 operational: false,
                 ip_v4: [%{ip: "91.250.245.98", operational: false}],
                 ip_v6: [],
                 name_server: "ns2.maib.md."
               }
             ]
           }

    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end
end
