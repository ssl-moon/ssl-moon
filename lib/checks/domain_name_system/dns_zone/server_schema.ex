defmodule SslMoon.Checks.DNSZone.Server do
  @moduledoc """
  Server schema.
  """
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:name_server, :string)
    field(:operational, :boolean, default: false)
    field(:ip_v4, {:array, :map})
    field(:ip_v6, {:array, :map})
  end

  def changeset(server, attrs) do
    server
    |> cast(attrs, [:name_server, :operational, :ip_v4, :ip_v6])
    |> validate_required([:name_server, :operational])
  end
end
