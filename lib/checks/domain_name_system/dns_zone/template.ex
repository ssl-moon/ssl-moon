defmodule SslMoon.Checks.DNSZone.Template do
  @moduledoc """
  The `DNSZoneComponent` module provides functions to render the checks for DNS Zone.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <div class="overflow-x-auto">
        <table :if={length(@check_data.check_result.servers) > 0} class="table table-zebra">
          <thead>
            <tr>
              <th><%= gettext("Nameserver") %></th>
              <th><%= gettext("Operational") %></th>
              <th><%= gettext("IPv4") %></th>
              <th><%= gettext("IPv6") %></th>
            </tr>
          </thead>
          <tbody>
            <%= for server <- @check_data.check_result.servers do %>
              <tr>
                <td><%= server.name_server %></td>
                <td>
                  <%= if server.operational do %>
                    <MaterialIcons.check_circle class="fill-green-600" />
                  <% else %>
                    <MaterialIcons.cancel class="fill-red-600" />
                  <% end %>
                </td>
                <td>
                  <%= for ip_map <-server.ip_v4 do %>
                    <ul>
                      <li>
                        <div class={[
                          if(ip_map["operational"], do: "badge-success", else: "badge-error"),
                          "badge"
                        ]}>
                          <%= ip_map["ip"] %>
                        </div>
                      </li>
                    </ul>
                  <% end %>
                </td>
                <td>
                  <%= for ip_map <-server.ip_v6 do %>
                    <ul>
                      <li>
                        <div class={[
                          if(ip_map["operational"], do: "badge-success", else: "badge-error"),
                          "badge"
                        ]}>
                          <%= ip_map["ip"] %>
                        </div>
                      </li>
                    </ul>
                  <% end %>
                </td>
              </tr>
            <% end %>
          </tbody>
        </table>
      </div>
      <br />
      <%= if length(@check_data.check_result.servers) > 0 do %>
        <p class="p-3">
          <b><%= gettext("The nameserver are operational") %></b> <br /> <br />
          <%= gettext(
            "When a nameserver is operational, it means that it is successfully performing its key function of translating domain names into IP addresses, ensuring that users can access websites and online services. It maintains high availability and reliability, avoiding significant downtime or interruptions. The nameserver responds to DNS queries quickly, contributing to efficient internet performance. Additionally, it operates securely, protecting against cyber threats like DNS spoofing or hijacking, and adheres to established internet standards for compatibility and effectiveness within the global internet infrastructure."
          ) %>
        </p>
      <% else %>
        <p class="p-3">
          <b><%= gettext("Nameservers not found") %></b> <br />
          <br /> <%= gettext(
            "It's not possible to list any of the nameservers. Due to privacy settings that hide domain details, technical issues like server outages or misconfigurations, security measures that obscure nameserver information, or incomplete propagation of new or changed DNS settings. Additionally, access to nameserver information might be restricted within certain networks or organizations for security reasons, or there could be errors in the databases that track DNS information. In such cases, alternative methods or direct contact with the domain registrar or hosting service may be required to obtain this information."
          ) %>
        </p>
      <% end %>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <div class="overflow-x-auto">
        <table class="table table-zebra w-full">
          <thead>
            <tr>
              <th><%= gettext("Nameserver") %></th>
              <th><%= gettext("Operational") %></th>
              <th><%= gettext("IPv4") %></th>
              <th><%= gettext("IPv6") %></th>
            </tr>
          </thead>
          <tbody>
            <%= for server <- @check_data.check_result.servers do %>
              <tr>
                <td><%= server.name_server %></td>
                <td>
                  <%= if server.operational do %>
                    <MaterialIcons.check_circle class="fill-green-600" />
                  <% else %>
                    <MaterialIcons.cancel class="fill-red-600" />
                  <% end %>
                </td>
                <td>
                  <%= for ip_map <-server.ip_v4 do %>
                    <ul>
                      <li class="flex">
                        <div class={[
                          if(ip_map["operational"], do: "badge-success", else: "badge-error"),
                          "badge"
                        ]}>
                          <%= ip_map["ip"] %>
                        </div>
                      </li>
                    </ul>
                  <% end %>
                </td>
                <td>
                  <%= for ip_map <-server.ip_v6 do %>
                    <ul>
                      <li>
                        <div class={[
                          if(ip_map["operational"], do: "badge-success", else: "badge-error"),
                          "badge"
                        ]}>
                          <%= ip_map["ip"] %>
                        </div>
                      </li>
                    </ul>
                  <% end %>
                </td>
              </tr>
            <% end %>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b><%= gettext("The nameservers are currently inactive") %></b> <br /> <br />
        <%= gettext(
          "When the nameservers are not operational or inactive, it indicates they are failing to perform the crucial task of resolving name to its IP address. This failure could be due to server outages, maintenance specific to these nameservers, errors in their DNS configuration, or network connectivity issues. They might also be overwhelmed by high traffic volumes or incapacitated by targeted cyber attacks like DDoS. Additionally, software bugs in the DNS server software could contribute to their non-functionality. As a result of these issues, the nameservers are unable to handle DNS queries effectively, leading to access problems for its websites or online services."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    gettext(
      "A nameserver is a vital part of the DNS system, translating domain names to IP addresses and facilitating internet access by efficiently routing traffic."
    )
  end
end
