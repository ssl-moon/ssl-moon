defmodule SslMoon.Checks.ContentTypeOptionsTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks.ContentTypeOptionsTest.Factory
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.ContentTypeOptions.Executor
  @moduletag template: SslMoon.Checks.ContentTypeOptions.Template
  @moduletag validation_schema: SslMoon.Checks.ContentTypeOptions.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "works as expected when redirect is involved", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      {:ok, build(:http_redirect)}
    end)
    |> expect(:request, fn :get, "https://www.test.com/", _, _, _ ->
      resp = build(:http_response)
      nosniff_header = {"X-Content-Type-Options", "nosniff"}

      resp = Map.put(resp, :headers, [nosniff_header | resp.headers])
      {:ok, resp}
    end)

    {:ok, result} = execute_check(check, host)

    assert result.status == :success
    assert result.data == %{content_type_options: ["nosniff"]}

    assert {:ok, _res} = create_check(check, result, checks_group.id, host)
  end

  test "success status when the header is present and in the correct format", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      nosniff_header = {"X-Content-Type-Options", "nosniff"}
      resp = build(:http_response)

      resp = Map.put(resp, :headers, [nosniff_header | resp.headers])
      {:ok, resp}
    end)

    {:ok, result} = execute_check(check, host)

    assert result.status == :success
    assert result.data == %{content_type_options: ["nosniff"]}

    assert {:ok, _res} = create_check(check, result, checks_group.id, host)
  end

  test "inactive status when the header is missing", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      {:ok, build(:http_response)}
    end)

    {:ok, result} = execute_check(check, host)
    assert result.status == :inactive

    assert {:ok, _res} = create_check(check, result, checks_group.id, host)
  end

  test "failure status when the header contains invalid text", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      err_header = {"X-Content-Type-Options", "error"}
      resp = build(:http_response)

      resp = Map.put(resp, :headers, [err_header | resp.headers])
      {:ok, resp}
    end)

    {:ok, result} = execute_check(check, host)
    assert result.status == :failure

    assert {:ok, _res} = create_check(check, result, checks_group.id, host)
  end

  test "failure status when the header is duplicated", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      duplicated_headers = [
        {"X-Content-Type-Options", "nosniff"},
        {"X-Frame-Options", "SAMEORIGIN"},
        {"Cache-Control", "max-age=0, must-revalidate, no-cache, no-store, private"},
        {"Pragma", "no-cache"},
        {"Expires", "Fri, 01 Jan 1990 00:00:00 GMT"},
        {"X-Content-Type-Options", "nosniff"}
      ]

      resp = build(:http_response)
      resp = Map.put(resp, :headers, duplicated_headers)

      {:ok, resp}
    end)

    {:ok, result} = execute_check(check, host)
    assert result.status == :failure

    assert {:ok, _res} = create_check(check, result, checks_group.id, host)
  end
end
