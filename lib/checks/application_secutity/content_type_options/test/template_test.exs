defmodule SslMoon.Checks.ContentTypeOptionsTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.ContentTypeOptions.Executor
  @moduletag template: SslMoon.Checks.ContentTypeOptions.Template
  @moduletag validation_schema: SslMoon.Checks.ContentTypeOptions.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :success, %{content_type_options: ["nosniff"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render failure template on invalid header content", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :failure, %{content_type_options: ["error"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render failure template on duplicated headers", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{content_type_options: ["nosniff", "nosniff"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :inactive, %{content_type_options: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end
end
