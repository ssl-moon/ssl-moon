defmodule SslMoon.Checks.ContentTypeOptions.Schema do
  @moduledoc """
  Content Type Options schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:content_type_options, {:array, :string})
  end

  @impl true
  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [:content_type_options])
    |> validate_required([:content_type_options])
  end
end
