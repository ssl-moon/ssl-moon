defmodule SslMoon.Checks.ContentTypeOptions.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    `SslMoon.Checks.ContentTypeOptions.Executor` is a module designed to perform security checks on HTTP response headers, specifically focusing on the `X-Content-Type-Options` header. It's part of the `SslMoon` suite of security tools, aiming to ensure web application compliance with best security practices.

    The main functionality of this module is to initiate a check for the presence and correctness of the `X-Content-Type-Options` header in HTTP responses. It uses the `SslMoon.Http.HttpClient` for sending HTTP requests and processes the responses to assess security compliance.

  ## Functions

    - `perform_check/2`: This function initiates the content type options check. It takes a predefined check (`"content_type_options"`) and a URL, returning a tuple with the check results.

    - `qualify_check/3`: This function qualifies the check based on the data obtained from `perform_check/2`. It evaluates the presence and value of the `X-Content-Type-Options` header and returns the status of the check (`:success`, `:inactive`, or `:failure`).

  ## Usage

    To use this module, define a check for `content_type_options` and provide a target URL. The `perform_check/2` function will initiate the check, and `qualify_check/3` will assess the results. For example:

    ```elixir
    # Define the check and the URL
    check = "content_type_options"
    url = "example.com"

    # Perform the content type options check
    {:ok, check_result} = SslMoon.Checks.ContentTypeOptions.Executor.perform_check(check, url)

    # Qualify the check based on the result obtained from `perform_check/2`
    {:ok, status} = SslMoon.Checks.ContentTypeOptions.Executor.qualify_check(check, url, check_result)

    # `status` will contain the outcome of the check (:success, :inactive, or :failure)
    ```
    This module uses pattern matching and recursion to handle HTTP redirects and extract the necessary header information, ensuring a thorough check.

  ## Completion Status

    This module has been brought to a final state with all planned functionalities implemented. However, it's important to note that there's always room for improvement based on emerging best practices and evolving security standards. Future enhancements or adjustments may be added to further refine the module or address new security considerations.

    Note: This module is part of an ongoing project and may be subject to updates and improvements. Always refer to the latest documentation for up-to-date information.
  """
  require Logger
  alias SslMoon.Http.HttpUtils

  @impl true
  def perform_check(_check, url) do
    result = check_x_content_type_options(url)
    {:ok, %{content_type_options: result}}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      case data.content_type_options do
        ["nosniff"] ->
          :success

        [] ->
          :inactive

        [_ | _] ->
          :failure

        _ ->
          :failure
      end

    {:ok, status}
  end

  defp check_x_content_type_options(url) do
    case HttpUtils.get_follow_redirect(url) do
      {:ok, %SslMoon.Http.HttpClient.Response{} = resp} ->
        get_x_content_type_options_from_header(resp.headers)

      {:error, _reason} ->
        []
    end
  end

  defp get_x_content_type_options_from_header(header) do
    header
    |> Enum.filter(fn {key, _value} -> String.match?(key, ~r/\AX-Content-Type-Options\z/i) end)
    |> Enum.map(fn {_key, value} -> value end)
  end
end
