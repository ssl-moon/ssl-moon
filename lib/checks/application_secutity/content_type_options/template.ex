defmodule SslMoon.Checks.ContentTypeOptions.Template do
  @moduledoc """
  The `ContentTypeOptions` module provides functions to render the checks for Content Type Options.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  # Add the actual header, remove hardcoded value
  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <div class="overflow-x-auto">
        <table class="table">
          <thead>
            <tr>
              <th>
                <%= gettext("Header") %>
              </th>
              <th>
                <%= gettext("Value") %>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <b>
                  <%= gettext("X-Content-Type-Options") %>
                </b>
              </td>
              <td><%= List.first(@check_data.check_result.content_type_options) %></td>
            </tr>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("The required header is present.") %>
        </b>
        <br /> <br />
        <%= gettext(
          "When the \"nosniff\" value is present in the X-Content-Type-Options HTTP header, it instructs the browser to strictly adhere to the MIME type specified in the Content-Type header and not attempt to guess or sniff the MIME type. This enhances security by preventing the browser from misinterpreting the content type of a resource, which can protect against certain types of attacks like Cross-Site Scripting (XSS) and code injection. Essentially, it ensures that browsers handle and execute content only as explicitly declared, reducing the risk of malicious content being executed."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <div class="overflow-x-auto">
        <table class="table">
          <thead>
            <tr>
              <th>
                <%= gettext("Header") %>
              </th>
              <th>
                <%= gettext("Value") %>
              </th>
            </tr>
          </thead>
          <tbody>
            <%= for header <- @check_data.check_result.content_type_options do %>
              <tr>
                <td>
                  <b>
                    <%= gettext("X-Content-Type-Options") %>
                  </b>
                </td>
                <td><%= header %></td>
              </tr>
            <% end %>
          </tbody>
        </table>
      </div>
      <br />
      <%= if length(@check_data.check_result.content_type_options) > 1 do %>
        <p class="p-3">
          <b>
            <%= gettext("Multiple X-Content-Type-Options headers are not allowed.") %>
          </b>
          <br /> <br />
          <%= gettext(
            "When the X-Content-Type-Options HTTP header has multiple values, it leads to non-standard behavior and may result in unpredictable handling by different browsers. Some browsers might use the first or last value, while others could ignore the header entirely, potentially defaulting to MIME type sniffing. For reliable and consistent security, it's best to use only a single, correct value (nosniff) for this header. Multiple values can undermine the intended security benefits of the \"nosniff\" directive."
          ) %>
        </p>
      <% else %>
        <p class="p-3">
          <b>
            <%= gettext("The required header is not present.") %>
          </b>
          <br /> <br />
          <%= gettext(
            "When the \"nosniff\" value is absent from the X-Content-Type-Options HTTP header, browsers may perform MIME type sniffing, leading to potential security risks. Without nosniff, there's a higher chance that browsers could incorrectly interpret or execute content, like treating a malicious script as a harmless file type. This increases the risk of security vulnerabilities, such as Cross-Site Scripting (XSS) attacks. It also makes the behavior of web content less predictable across different browsers and places greater responsibility on developers to accurately specify MIME types to ensure safe and consistent handling of web content."
          ) %>
        </p>
      <% end %>
    </div>
    """
  end

  defp template_description do
    gettext(
          "The X-Content-Type-Options header in web security is used to prevent browsers from MIME-sniffing a response away from the specified content type. When set to the value nosniff, it instructs the browser not to override the Content-Type header set by the server, mitigating potential MIME-based attacks. This protection ensures that web resources are served and interpreted strictly as intended by the server."
        )
  end
end
