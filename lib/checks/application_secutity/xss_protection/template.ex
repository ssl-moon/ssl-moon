defmodule SslMoon.Checks.XSSProtection.Template do
  @moduledoc """
  The `XSSProtectionComponent` module provides functions to render the checks for XSSProtection.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :warning -> render_warning(assigns)
      :failure -> render_failure(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <div class="overflow-x-auto">
        <table class="table">
          <thead>
            <tr>
              <th>
                <%= gettext("Header") %>
              </th>
              <th>
                <%= gettext("Value") %>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <b>
                  <%= gettext("X-XSS-Protection") %>
                </b>
              </td>
              <td><%= @check_data.check_result.xss_protection %></td>
            </tr>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("The required header is present.") %>
        </b>
        <br /> <br />
        <%= gettext(
          "The %{check_value} value effectively disables the in-built XSS protection provided by the browser. The use of this setting implies confidence in the site's own security systems to effectively guard against XSS attacks, and a belief that the browser's built-in protection might unnecessarily interfere with the site's functionality. However, with the evolution of web security standards and the advent of more advanced security practices like Content Security Policy (CSP), reliance on just the X-XSS-Protection header is becoming less common. Modern browsers are also moving away from this header, focusing instead on their own internal XSS protection methods.",
          check_value: @check_data.check_result.xss_protection
        ) %>
      </p>
    </div>
    """
  end

  defp render_warning(assigns) do
    ~H"""
    <div test_tag="check_warning">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-warning">
        <MaterialIcons.warning style="outlined" size={42} class="fill-yellow-600" />
        <span><%= gettext("Check completed with warnings") %></span>
      </div>
      <br />
      <div class="overflow-x-auto">
        <table class="table">
          <thead>
            <tr>
              <th>
                <%= gettext("Header") %>
              </th>
              <th>
                <%= gettext("Value") %>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <b>
                  <%= gettext("X-XSS-Protection") %>
                </b>
              </td>
              <td><%= @check_data.check_result.xss_protection %></td>
            </tr>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("The header might contain unwanted values.") %>
        </b>
        <br /> <br />
        <%= gettext(
          "The %{check_value} value activates the browser's built-in reflective XSS protection. This setting enables the XSS filtering feature in the browser. If a potential XSS attack is detected, the browser will try to sanitize the page by removing the malicious script, allowing the rest of the content to be displayed. If the header is set to \"1; mode=block\", the browser takes a stricter approach by blocking the entire page if an XSS attack is detected, instead of trying to clean it.",
          check_value: @check_data.check_result.xss_protection
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <div class="overflow-x-auto">
        <table class="table">
          <thead>
            <tr>
              <th>
                <%= gettext("Header") %>
              </th>
              <th>
                <%= gettext("Value") %>
              </th>
            </tr>
          </thead>
          <tbody>
            <%= for header <-  @check_data.check_result.xss_protection do %>
              <tr>
                <td>
                  <b>
                    <%= gettext("X-XSS-Protection") %>
                  </b>
                </td>
                <td><%= header %></td>
              </tr>
            <% end %>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("Multiple X-XSS-Protection headers are not allowed.") %>
        </b>
        <br /> <br />
        <%= gettext(
          "The header contains multiple conflicting values, the behavior is undefined and varies across browsers. When the XSS protection header includes multiple conflicting values, it results in undefined behavior due to varying interpretations by different browsers. Such inconsistencies can inadvertently weaken security measures, as each browser might enforce different security rules. Therefore, it's crucial to define security headers clearly and without ambiguity to guarantee consistent protection across all web browsers."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    gettext(
      "Cross-Site Scripting (XSS) is a vulnerability that allows attackers to inject malicious scripts into web pages viewed by other users. To protect against XSS, it's essential to validate and sanitize all user input, use secure coding practices like output encoding, and implement security measures like Content Security Policy (CSP)."
    )
  end
end
