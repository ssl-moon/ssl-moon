defmodule SslMoon.Checks.XSSProtectionTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.XSSProtection.Executor
  @moduletag template: SslMoon.Checks.XSSProtection.Template
  @moduletag validation_schema: SslMoon.Checks.XSSProtection.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :success, %{xss_protection: ["0"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render failure template on invalid header content", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :failure, %{xss_protection: ["error"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render failure template on duplicated headers", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{xss_protection: ["0", "1; mode=block"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render warning template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :warning, %{xss_protection: ["1; mode=block"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_warning"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :inactive, %{xss_protection: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end
end
