defmodule SslMoon.Checks.XSSProtectionTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks.XSSProtectionTest.Factory
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.XSSProtection.Executor
  @moduletag template: SslMoon.Checks.XSSProtection.Template
  @moduletag validation_schema: SslMoon.Checks.XSSProtection.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "success status when the header is present and in correct format", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      xss_header = {"X-XSS-Protection", "0"}

      resp = build(:http_response)
      resp = Map.put(resp, :headers, [xss_header | resp.headers])

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :success
    assert resp.data == %{xss_protection: ["0"]}

    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end

  test "inactive status when the header is missing", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, "test.com", _, _, _ ->
      {:ok, build(:http_response)}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :inactive

    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when the content of the header is not valid", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, "test.com", _, _, _ ->
      xss_header = {"X-XSS-Protection", "zero"}

      resp = build(:http_response)
      resp = Map.put(resp, :headers, [xss_header | resp.headers])

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :failure

    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when there is are duplicated headers definitions", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, "test.com", _, _, _ ->
      duplicated_headers = [
        {"Server", "nginx"},
        {"Date", "Tue, 10 Oct 2023 14:46:43 GMT"},
        {"Content-Type", "text/html; charset=UTF-8"},
        {"Transfer-Encoding", "chunked"},
        {"Connection", "keep-alive"},
        {"X-Content-Type-Options", "nosniff"},
        {"X-Frame-Options", "SAMEORIGIN"},
        {"Cache-Control", "max-age=0, must-revalidate, no-cache, no-store, private"},
        {"Pragma", "no-cache"},
        {"Expires", "Fri, 01 Jan 1990 00:00:00 GMT"},
        {"Vary", "Accept-Encoding"},
        {"Set-Cookie",
         "XSRF-TOKEN=eyJpdiI6InRkMnY5dXZnaWY5dHNtS0QxUXhHWEE9PSIsInZhbHVlIjoiWVV3SEd1UmIvSnV5S1BvQUxCK0JmazdVSTFOTnJzSGpkVS91RWtJa1VudmIySzRwbE8zb3kxV1Z2eWdZdGpXNWRBajB0SXh4UXpvTlpsWWI0K3F0Yk0rUVhOeHhsMGw0b0swZk1PUFlhSkxHZnZJc1FoRWY0RUt4cXkvSkk5QTQiLCJtYWMiOiJmNzU1ZjE2Njg2NWE5ZDEyOTVjY2QzMjViNTg1MWFmYjNjNzkxMjNkZTY4YzAwY2U3ZmJlYTdmMjBkZTU5NzQzIiwidGFnIjoiIn0%3D; expires=Tue, 10-Oct-2023 16:46:43 GMT; Max-Age=7200; path=/; samesite=lax;HttpOnly;Secure;HttpOnly;Secure"},
        {"Set-Cookie",
         "maibmd_session=eyJpdiI6IlVLaXpZZ08wQjZFNjVBV08xR2UxRmc9PSIsInZhbHVlIjoiRnVOTmltdk5meVovd0Q3Z1ZPc1BKMXd4UE1KQ2NJb2M5Q3hWaDQ4dHhaUUVObTZveDAxNGJQY0dDVTlGWXpxQzMxY0xxRUhZd082c1FMaU1LMyt3RGYrL2J1SnFlVGlld3FrbG5QZjdqM0I0SnQ2ZDc0cjQ2eVIySE1veFNVTlQiLCJtYWMiOiIxYmFmMmM4N2U0NWU2NTRlZWE2Yzk2NGJjOWM4MGVmYmMwNWU0OTZhN2Y5OTk1ZWRjOWQwNTA0NTc5MTJjYmNlIiwidGFnIjoiIn0%3D; expires=Tue, 10-Oct-2023 16:46:43 GMT; Max-Age=7200; path=/; httponly; samesite=lax;HttpOnly;Secure;HttpOnly;Secure"},
        {"X-XSS-Protection", "0"},
        {"Content-Security-Policy",
         "default-src 'self' data: *; script-src 'self' 'unsafe-inline' 'unsafe-eval' data: *;script-src-elem 'self' 'unsafe-inline' data: *; connect-src 'self' data: *; img-src 'self' data: *; style-src 'self' 'unsafe-inline' data: *;base-uri 'self';form-action 'self'"},
        {"Content-Security-Policy", "block-all-mixed-content"},
        {"X-Frame-Options", "SAMEORIGIN"},
        {"X-XSS-Protection", "1; mode=block"},
        {"X-Content-Type-Options", "nosniff"},
        {"Strict-Transport-Security", "max-age=31536000; includeSubDomains"},
        {"Set-Cookie",
         "visid_incap_2840468=6HY7cMSKT6KgCpBIto7BpNJjJWUAAAAAQUIPAAAAAADhHctQaMzeA11NnDwSaBFX; expires=Tue, 08 Oct 2024 22:24:13 GMT; HttpOnly; path=/; Domain=.maib.md"},
        {"Set-Cookie",
         "incap_ses_324_2840468=b3tJUcXg0168uz7QCRV/BNJjJWUAAAAAcCXVtKDJ2bTcEfxx+54OiA==; path=/; Domain=.maib.md"},
        {"X-CDN", "Imperva"},
        {"X-Iinfo",
         "9-21960167-21960175 NNNN CT(62 70 0) RT(1696949201671 192) q(0 0 1 -1) r(2 7) U12"}
      ]

      resp = build(:http_response) |> Map.put(:headers, duplicated_headers)
      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :failure

    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end
end
