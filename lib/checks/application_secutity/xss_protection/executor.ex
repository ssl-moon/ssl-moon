defmodule SslMoon.Checks.XSSProtection.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.XSSProtection.Executor` module is part of the SslMoon security suite, focused on assessing Cross-Site Scripting (XSS) protection mechanisms implemented in web applications. This module evaluates the `X-XSS-Protection` HTTP header to determine if adequate measures are in place to prevent XSS attacks.

    The module consists of two primary functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: This function is responsible for initiating the XSS protection check against a specified URL. It sends an HTTP request and extracts the `X-XSS-Protection` header from the response, returning a map containing the XSS protection details.

    - `qualify_check/3`: This function assesses the extracted XSS protection details from `perform_check/2`. It categorizes the check's status as `:success`, `:warning`, `:inactive`, or `:failure`, depending on the configuration of the `X-XSS-Protection` header.

  ## Usage

    To utilize this module, specify a check for XSS protection and a target URL as follows:

    ```elixir
    # Define the check and the URL
    check = "xss_protection"
    url = "http://example.com"

    # Perform the XSS protection check
    {:ok, check_result} = SslMoon.Checks.XSSProtection.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.XSSProtection.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check, ranging from :success to :failure
    ```
    This module plays a crucial role in ensuring that web applications have proper defenses against XSS attacks, which are a common and dangerous type of security vulnerability.

  ## Completion Status

    This module has been brought to a final state with all planned functionalities implemented. However, it's important to note that there's always room for improvement based on emerging best practices and evolving security standards. Future enhancements or adjustments may be added to further refine the module or address new security considerations.

    Note: This module is part of an ongoing project and may be subject to updates and improvements. Always refer to the latest documentation for up-to-date information.
  """
  require Logger
  alias SslMoon.Http.HttpClient
  alias SslMoon.Http.HttpUtils

  @impl true
  def perform_check(_check, url) do
    {:ok, %{xss_protection: check_xss_protection(url)}}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      case data.xss_protection do
        ["0"] ->
          :success

        ["1"] ->
          :warning

        ["1; mode=block"] ->
          :warning

        ["1; report=" <> _report_uri] ->
          :warning

        [] ->
          :inactive

        [_ | _] ->
          :failure

        _ ->
          :failure
      end

    {:ok, status}
  end

  defp check_xss_protection(url) do
    case HttpUtils.get_follow_redirect(url) do
      {:ok, %HttpClient.Response{} = response} ->
        get_x_xss_protection_from_header(response.headers)

      {:error, _reason} ->
        []
    end
  end

  defp get_x_xss_protection_from_header(header) do
    header
    |> Enum.filter(fn {key, _value} -> String.match?(key, ~r/\AX-XSS-Protection\z/i) end)
    |> Enum.map(fn {_key, value} -> value end)
  end
end
