defmodule SslMoon.Checks.XSSProtection.Schema do
  @moduledoc """
  XSS Protection schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:xss_protection, {:array, :string})
  end

  @impl true
  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [:xss_protection])
    |> validate_required([:xss_protection])
  end
end
