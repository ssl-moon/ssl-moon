defmodule SslMoon.Checks.FrameOptionsTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.FrameOptions.Executor
  @moduletag template: SslMoon.Checks.FrameOptions.Template
  @moduletag validation_schema: SslMoon.Checks.FrameOptions.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :success, %{frame_options: ["SAMEORIGIN"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render failure template on invalid header content", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :failure, %{frame_options: ["error"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render failure template on duplicated headers", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{frame_options: ["SAMEORIGIN", "SAMEORIGIN"]})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :inactive, %{frame_options: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end
end
