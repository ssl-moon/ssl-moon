defmodule SslMoon.Checks.FrameOptionsTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks.FrameOptionsTest.Factory
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.FrameOptions.Executor
  @moduletag template: SslMoon.Checks.FrameOptions.Template
  @moduletag validation_schema: SslMoon.Checks.FrameOptions.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "works as expected when redirect is involved", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      {:ok, build(:http_redirect)}
    end)
    |> expect(:request, fn :get, "https://www.test.com/", _, _, _ ->
      resp = build(:http_response)
      sameorigin_header = {"X-Frame-Options", "SAMEORIGIN"}

      resp = Map.put(resp, :headers, [sameorigin_header | resp.headers])
      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :success
    assert resp.data == %{frame_options: ["SAMEORIGIN"]}

    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end

  test "success status when the header is present and in correct format", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      sameorigin_header = {"X-Frame-Options", "SAMEORIGIN"}

      resp = build(:http_response)
      resp = Map.put(resp, :headers, [sameorigin_header | resp.headers])

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :success
    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end

  test "inactive status when the header is missing", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      {:ok, build(:http_response)}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :inactive
    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when the header has invalid contents", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      sameorigin_error_header = {"X-Frame-Options", "SAMEORIGINALS"}

      resp = build(:http_response)
      resp = Map.put(resp, :headers, [sameorigin_error_header | resp.headers])

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :failure
    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when there are frame options duplicated headers", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, "test.com", _, _, _ ->
      duplicated_headers = [
        {"X-Frame-Options", "SAMEORIGIN"},
        {"Cache-Control", "max-age=0, must-revalidate, no-cache, no-store, private"},
        {"Pragma", "no-cache"},
        {"Expires", "Fri, 01 Jan 1990 00:00:00 GMT"},
        {"Vary", "Accept-Encoding"},
        {"Set-Cookie",
         "XSRF-TOKEN=eyJpdiI6InRkMnY5dXZnaWY5dHNtS0QxUXhHWEE9PSIsInZhbHVlIjoiWVV3SEd1UmIvSnV5S1BvQUxCK0JmazdVSTFOTnJzSGpkVS91RWtJa1VudmIySzRwbE8zb3kxV1Z2eWdZdGpXNWRBajB0SXh4UXpvTlpsWWI0K3F0Yk0rUVhOeHhsMGw0b0swZk1PUFlhSkxHZnZJc1FoRWY0RUt4cXkvSkk5QTQiLCJtYWMiOiJmNzU1ZjE2Njg2NWE5ZDEyOTVjY2QzMjViNTg1MWFmYjNjNzkxMjNkZTY4YzAwY2U3ZmJlYTdmMjBkZTU5NzQzIiwidGFnIjoiIn0%3D; expires=Tue, 10-Oct-2023 16:46:43 GMT; Max-Age=7200; path=/; samesite=lax;HttpOnly;Secure;HttpOnly;Secure"},
        {"Set-Cookie",
         "maibmd_session=eyJpdiI6IlVLaXpZZ08wQjZFNjVBV08xR2UxRmc9PSIsInZhbHVlIjoiRnVOTmltdk5meVovd0Q3Z1ZPc1BKMXd4UE1KQ2NJb2M5Q3hWaDQ4dHhaUUVObTZveDAxNGJQY0dDVTlGWXpxQzMxY0xxRUhZd082c1FMaU1LMyt3RGYrL2J1SnFlVGlld3FrbG5QZjdqM0I0SnQ2ZDc0cjQ2eVIySE1veFNVTlQiLCJtYWMiOiIxYmFmMmM4N2U0NWU2NTRlZWE2Yzk2NGJjOWM4MGVmYmMwNWU0OTZhN2Y5OTk1ZWRjOWQwNTA0NTc5MTJjYmNlIiwidGFnIjoiIn0%3D; expires=Tue, 10-Oct-2023 16:46:43 GMT; Max-Age=7200; path=/; httponly; samesite=lax;HttpOnly;Secure;HttpOnly;Secure"},
        {"X-XSS-Protection", "1; mode=block"},
        {"Content-Security-Policy",
         "default-src 'self' data: *; script-src 'self' 'unsafe-inline' 'unsafe-eval' data: *;script-src-elem 'self' 'unsafe-inline' data: *; connect-src 'self' data: *; img-src 'self' data: *; style-src 'self' 'unsafe-inline' data: *;base-uri 'self';form-action 'self'"},
        {"Content-Security-Policy", "block-all-mixed-content"},
        {"X-Frame-Options", "SAMEORIGIN"}
      ]

      resp = build(:http_response)
      resp = Map.put(resp, :headers, duplicated_headers)
      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :failure
    assert {:ok, _resp} = create_check(check, resp, checks_group.id, host)
  end
end
