defmodule SslMoon.Checks.FrameOptions.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.FrameOptions.Executor` module is a part of the SslMoon security checking suite, specifically designed to assess the security of web frames by checking the `X-Frame-Options` HTTP response header. This module ensures that the web pages are protected against clickjacking attacks by verifying the correct configuration of frame options.

    The module provides two main functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: This function initiates the frame options check for a given URL. It sends an HTTP request to the specified URL and extracts the `X-Frame-Options` header from the response. The function returns a map with the frame options found.

    - `qualify_check/3`: Based on the result from `perform_check/2`, this function evaluates the frame options. It determines the status of the check, categorizing it as `:success` if the options are set to either "DENY" or "SAMEORIGIN", `:inactive` if no options are set, and `:failure` for any other scenarios.

  ## Usage

    To utilize this module, you should provide a specific check for frame options and a target URL. For example:

    ```elixir
    # Define the check and the URL
    check = "frame_options"
    url = "http://example.com"

    # Perform the frame options check
    {:ok, check_result} = SslMoon.Checks.FrameOptions.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.FrameOptions.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :inactive, or :failure)
    ```
    This module is essential for ensuring that web applications are not susceptible to framing attacks, which is a critical aspect of web security.

  ## Completion Status

    This module has been brought to a final state with all planned functionalities implemented. However, it's important to note that there's always room for improvement based on emerging best practices and evolving security standards. Future enhancements or adjustments may be added to further refine the module or address new security considerations.

    Note: This module is part of an ongoing project and may be subject to updates and improvements. Always refer to the latest documentation for up-to-date information.
  """
  require Logger
  alias SslMoon.Http.HttpUtils

  @impl true
  def perform_check(_check, url) do
    {:ok, %{frame_options: check_frame_options(url)}}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      case data.frame_options do
        ["DENY"] ->
          :success

        ["SAMEORIGIN"] ->
          :success

        [] ->
          :inactive

        [_ | _] ->
          :failure

        _ ->
          :failure
      end

    {:ok, status}
  end

  defp check_frame_options(url) do
    case HttpUtils.get_follow_redirect(url) do
      {:ok, %SslMoon.Http.HttpClient.Response{} = response} ->
        get_x_frame_options_from_header(response.headers)

      {:error, _reason} ->
        []
    end
  end

  defp get_x_frame_options_from_header(header) do
    header
    |> Enum.filter(fn {key, _value} -> String.match?(key, ~r/\AX-Frame-Options\z/i) end)
    |> Enum.map(fn {_key, value} -> value end)
  end
end
