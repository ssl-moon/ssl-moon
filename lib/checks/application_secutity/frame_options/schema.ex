defmodule SslMoon.Checks.FrameOptions.Schema do
  @moduledoc """
  Frame Options schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:frame_options, {:array, :string})
  end

  @impl true
  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [:frame_options])
    |> validate_required([:frame_options])
  end
end
