defmodule SslMoon.Checks.FrameOptions.Template do
  @moduledoc """
  The `FrameOptionsComponent` module provides functions to render the checks for Frame Options.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <div class="overflow-x-auto">
        <table class="table">
          <thead>
            <tr>
              <th>
                <%= gettext("Header") %>
              </th>
              <th>
                <%= gettext("Value") %>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <b>
                  <%= gettext("X-Frame-Options") %>
                </b>
              </td>
              <td><%= @check_data.check_result.frame_options %></td>
            </tr>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("The required header is present.") %>
        </b>
        <br /> <br />
        <%= gettext(
          "When the %{check_value} value is present in the X-Frame-Options header of a web page, it prohibits all other websites, from framing the content of that page. This means that no external site or even pages from the same website can display the content within frames, iframes, objects, or similar HTML tags. This setting is a strict security measure to protect against clickjacking attacks, ensuring the highest level of protection by completely disallowing any kind of framing of the page.",
          check_value: @check_data.check_result.frame_options
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <div class="overflow-x-auto">
        <table class="table">
          <thead>
            <tr>
              <th>
                <%= gettext("Header") %>
              </th>
              <th>
                <%= gettext("Value") %>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <%= for header <-@check_data.check_result.frame_options do %>
                <tr>
                  <td>
                    <b>
                      <%= gettext("X-Frame-Options") %>
                    </b>
                  </td>
                  <td><%= header %></td>
                </tr>
              <% end %>
            </tr>
          </tbody>
        </table>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("Detected values do not match the expected requirements") %>
        </b>
        <br /> <br />
        <%= gettext(
          "When the X-Frame-Options header on a web page does not include the SAMEORIGIN or DENY values, it means there are no explicit restrictions against framing by other websites. This makes the page more susceptible to clickjacking attacks. However, it's also possible for the header to contain multiple values, in which case the browser's behavior can vary based on its specific implementation and compatibility with those values. Additionally, protection might rely on the browser's default settings or other security frameworks like the Content Security Policy (CSP)."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description() do
    gettext(
      "The `X-Frame-Options` is an HTTP header that prevents web pages from being displayed inside frames, helping protect against clickjacking attacks. It offers directives like `DENY` (blocks all framing), `SAMEORIGIN` (allows framing only by the same origin), and `ALLOW-FROM` (permits framing from a specified URI). For more granular control, the Content Security Policy (CSP) `frame-ancestors` directive can be used as an alternative."
    )
  end
end
