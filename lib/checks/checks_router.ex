defmodule SslMoon.DefaultChecksRouter do
  @moduledoc """
    The checks router that will be used by this application.

    Should contain all the checks that are and will be performed by the application.
  """
  use SslMoon.Checks.Router

  checks do
    check "application_secutity_content_type_options_check" do
      display_name("Content Type Options")
      version(0)
      executor(SslMoon.Checks.ContentTypeOptions.Executor)
      template(SslMoon.Checks.ContentTypeOptions.Template)
      validation_schema(SslMoon.Checks.ContentTypeOptions.Schema)
    end

    check "application_secutity_frame_options_check" do
      display_name("Frame Options")
      version(0)
      executor(SslMoon.Checks.FrameOptions.Executor)
      template(SslMoon.Checks.FrameOptions.Template)
      validation_schema(SslMoon.Checks.FrameOptions.Schema)
    end

    check "application_secutity_xss_protection_check" do
      display_name("XSS Protection")
      version(0)
      executor(SslMoon.Checks.XSSProtection.Executor)
      template(SslMoon.Checks.XSSProtection.Template)
      validation_schema(SslMoon.Checks.XSSProtection.Schema)
    end

    check "domain_name_system_certification_authority_authorization_check" do
      display_name("CAA")
      version(0)
      executor(SslMoon.Checks.CertificationAuthorityAuthorization.Executor)
      template(SslMoon.Checks.CertificationAuthorityAuthorization.Template)
      validation_schema(SslMoon.Checks.CertificationAuthorityAuthorization.Schema)
    end

    check "domain_name_system_dns_zone_check" do
      display_name("DNS Zone")
      version(0)
      executor(SslMoon.Checks.DNSZone.Executor)
      template(SslMoon.Checks.DNSZone.Template)
      validation_schema(SslMoon.Checks.DNSZone.Schema)
    end

    check "domain_name_system_dnssec_check" do
      display_name("DNS Sec")
      version(0)
      executor(SslMoon.Checks.DNSSEC.Executor)
      template(SslMoon.Checks.DNSSEC.Template)
      validation_schema(SslMoon.Checks.DNSSEC.Schema)
    end

    check "modern_security_features_content_security_policy_check" do
      display_name("Content Security Policy")
      version(0)
      executor(SslMoon.Checks.ContentSecurityPolicy.Executor)
      template(SslMoon.Checks.ContentSecurityPolicy.Template)
      validation_schema(SslMoon.Checks.ContentSecurityPolicy.Schema)
    end

    check "modern_security_features_strict_transport_security_check" do
      display_name("Strict Transport Security")
      version(0)
      executor(SslMoon.Checks.StrictTransportSecurity.Executor)
      template(SslMoon.Checks.StrictTransportSecurity.Template)
      validation_schema(SslMoon.Checks.StrictTransportSecurity.Schema)
    end

    check "modern_security_features_subresource_integrity_check" do
      display_name("Subresource Integrity")
      version(0)
      executor(SslMoon.Checks.SubresourceIntegrity.Executor)
      template(SslMoon.Checks.SubresourceIntegrity.Template)
      validation_schema(SslMoon.Checks.SubresourceIntegrity.Schema)
    end

    check "protocols_http_check" do
      display_name("HTTP")
      version(0)
      executor(SslMoon.Checks.HttpCheck.Executor)
      template(SslMoon.Checks.HttpCheck.Template)
      validation_schema(SslMoon.Checks.HttpCheck.Schema)
    end

    check "protocols_https_check" do
      display_name("HTTPS")
      version(0)
      executor(SslMoon.Checks.HttpsCheck.Executor)
      template(SslMoon.Checks.HttpsCheck.Template)
      validation_schema(SslMoon.Checks.HttpsCheck.Schema)
    end

    check "protocols_www_http_check" do
      display_name("WWW HTTP")
      version(0)
      executor(SslMoon.Checks.WwwHttpCheck.Executor)
      template(SslMoon.Checks.WwwHttpCheck.Template)
      validation_schema(SslMoon.Checks.WwwHttpCheck.Schema)
    end

    check "protocols_www_https_check" do
      display_name("WWW HTTPS")
      version(0)
      executor(SslMoon.Checks.WwwHttpsCheck.Executor)
      template(SslMoon.Checks.WwwHttpsCheck.Template)
      validation_schema(SslMoon.Checks.WwwHttpsCheck.Schema)
    end

    check "secure_transport_certificates_check" do
      display_name("Certificates")
      version(0)
      executor(SslMoon.Checks.Certificates.Executor)
      template(SslMoon.Checks.Certificates.Template)
      validation_schema(SslMoon.Checks.Certificates.Schema)
    end

    check "secure_transport_cookies_check" do
      display_name("Cookies")
      version(0)
      executor(SslMoon.Checks.Cookies.Executor)
      template(SslMoon.Checks.Cookies.Template)
      validation_schema(SslMoon.Checks.Cookies.Schema)
    end

    check "secure_transport_mixed_content_check" do
      display_name("Mixed Content")
      version(0)
      executor(SslMoon.Checks.MixedContent.Executor)
      template(SslMoon.Checks.MixedContent.Template)
      validation_schema(SslMoon.Checks.MixedContent.Schema)
    end

    check "secure_transport_transport_layer_security_check" do
      display_name("Transport Layer Security")
      version(0)
      executor(SslMoon.Checks.TransportLayerSecurity.Executor)
      template(SslMoon.Checks.TransportLayerSecurity.Template)
      validation_schema(SslMoon.Checks.TransportLayerSecurity.Schema)
    end
  end
end
