defmodule SslMoon.TestMigrator do
  @moduledoc """
    Dummy migrator.

    REMOVE WHEN YOU WILL MIGRATE TO THE NEW SYSTEM.
  """
  use SslMoon.Checks.Migrator

  migrator do
    migrate(version: 1, migration_fun: &migrate_v1/1)
  end

  def migrate_v1(data) do
    data
  end
end
