defmodule SslMoon.Checks.ContentSecurityPolicyTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks.ContentSecurityPolicy.Factory
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.ContentSecurityPolicy.Executor
  @moduletag template: SslMoon.Checks.ContentSecurityPolicy.Template
  @moduletag validation_schema: SslMoon.Checks.ContentSecurityPolicy.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "sucess status on correct header content", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      resp = build(:http_response)

      csp_header =
        {"content-security-policy",
         "default-src data: blob: 'self' https://*.fbsbx.com 'unsafe-inline' *.facebook.com *.fbcdn.net 'unsafe-eval';script-src *.facebook.com *.fbcdn.net *.facebook.net *.google-analytics.com *.google.com 127.0.0.1:* 'unsafe-inline' blob: data: 'self' connect.facebook.net 'unsafe-eval';style-src fonts.googleapis.com *.fbcdn.net data: *.facebook.com 'unsafe-inline';connect-src *.facebook.com facebook.com *.fbcdn.net *.facebook.net wss://*.facebook.com:* wss://*.whatsapp.com:* wss://*.fbcdn.net attachment.fbsbx.com ws://localhost:* blob: *.cdninstagram.com 'self' http://localhost:3103 wss://gateway.facebook.com wss://edge-chat.facebook.com wss://snaptu-d.facebook.com wss://kaios-d.facebook.com/ v.whatsapp.net *.fbsbx.com *.fb.com;font-src data: *.gstatic.com *.facebook.com *.fbcdn.net *.fbsbx.com;img-src *.fbcdn.net *.facebook.com data: https://*.fbsbx.com *.tenor.co media.tenor.com facebook.com *.cdninstagram.com fbsbx.com fbcdn.net *.giphy.com connect.facebook.net *.carriersignal.info blob: android-webview-video-poster: googleads.g.doubleclick.net www.googleadservices.com *.whatsapp.net *.fb.com *.oculuscdn.com;media-src *.cdninstagram.com blob: *.fbcdn.net *.fbsbx.com www.facebook.com *.facebook.com https://*.giphy.com data:;frame-src *.doubleclick.net *.google.com *.facebook.com www.googleadservices.com *.fbsbx.com fbsbx.com data: www.instagram.com *.fbcdn.net https://paywithmybank.com https://sandbox.paywithmybank.com;worker-src blob: *.facebook.com data:;block-all-mixed-content;upgrade-insecure-requests;"}

      resp = Map.put(resp, :headers, [csp_header | resp.headers])
      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :success
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "inactive status on missing the header", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      resp = build(:http_response)
      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :inactive
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when multiple csp headers are present", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      resp = build(:http_response)

      duplicated_headers =
        [
          {"Server", "nginx"},
          {"Date", "Tue, 10 Oct 2023 14:46:43 GMT"},
          {"Content-Type", "text/html; charset=UTF-8"},
          {"Transfer-Encoding", "chunked"},
          {"Connection", "keep-alive"},
          {"X-Content-Type-Options", "nosniff"},
          {"X-Frame-Options", "SAMEORIGIN"},
          {"Cache-Control", "max-age=0, must-revalidate, no-cache, no-store, private"},
          {"Pragma", "no-cache"},
          {"Expires", "Fri, 01 Jan 1990 00:00:00 GMT"},
          {"Vary", "Accept-Encoding"},
          {"Set-Cookie",
           "XSRF-TOKEN=eyJpdiI6InRkMnY5dXZnaWY5dHNtS0QxUXhHWEE9PSIsInZhbHVlIjoiWVV3SEd1UmIvSnV5S1BvQUxCK0JmazdVSTFOTnJzSGpkVS91RWtJa1VudmIySzRwbE8zb3kxV1Z2eWdZdGpXNWRBajB0SXh4UXpvTlpsWWI0K3F0Yk0rUVhOeHhsMGw0b0swZk1PUFlhSkxHZnZJc1FoRWY0RUt4cXkvSkk5QTQiLCJtYWMiOiJmNzU1ZjE2Njg2NWE5ZDEyOTVjY2QzMjViNTg1MWFmYjNjNzkxMjNkZTY4YzAwY2U3ZmJlYTdmMjBkZTU5NzQzIiwidGFnIjoiIn0%3D; expires=Tue, 10-Oct-2023 16:46:43 GMT; Max-Age=7200; path=/; samesite=lax;HttpOnly;Secure;HttpOnly;Secure"},
          {"Set-Cookie",
           "maibmd_session=eyJpdiI6IlVLaXpZZ08wQjZFNjVBV08xR2UxRmc9PSIsInZhbHVlIjoiRnVOTmltdk5meVovd0Q3Z1ZPc1BKMXd4UE1KQ2NJb2M5Q3hWaDQ4dHhaUUVObTZveDAxNGJQY0dDVTlGWXpxQzMxY0xxRUhZd082c1FMaU1LMyt3RGYrL2J1SnFlVGlld3FrbG5QZjdqM0I0SnQ2ZDc0cjQ2eVIySE1veFNVTlQiLCJtYWMiOiIxYmFmMmM4N2U0NWU2NTRlZWE2Yzk2NGJjOWM4MGVmYmMwNWU0OTZhN2Y5OTk1ZWRjOWQwNTA0NTc5MTJjYmNlIiwidGFnIjoiIn0%3D; expires=Tue, 10-Oct-2023 16:46:43 GMT; Max-Age=7200; path=/; httponly; samesite=lax;HttpOnly;Secure;HttpOnly;Secure"},
          {"X-XSS-Protection", "1; mode=block"},
          {"Content-Security-Policy",
           "default-src 'self' data: *; script-src 'self' 'unsafe-inline' 'unsafe-eval' data: *;script-src-elem 'self' 'unsafe-inline' data: *; connect-src 'self' data: *; img-src 'self' data: *; style-src 'self' 'unsafe-inline' data: *;base-uri 'self';form-action 'self'"},
          {"Content-Security-Policy", "block-all-mixed-content"},
          {"X-Frame-Options", "SAMEORIGIN"},
          {"X-XSS-Protection", "1; mode=block"},
          {"X-Content-Type-Options", "nosniff"},
          {"Strict-Transport-Security", "max-age=31536000; includeSubDomains"},
          {"Set-Cookie",
           "visid_incap_2840468=6HY7cMSKT6KgCpBIto7BpNJjJWUAAAAAQUIPAAAAAADhHctQaMzeA11NnDwSaBFX; expires=Tue, 08 Oct 2024 22:24:13 GMT; HttpOnly; path=/; Domain=.maib.md"},
          {"Set-Cookie",
           "incap_ses_324_2840468=b3tJUcXg0168uz7QCRV/BNJjJWUAAAAAcCXVtKDJ2bTcEfxx+54OiA==; path=/; Domain=.maib.md"},
          {"X-CDN", "Imperva"},
          {"X-Iinfo",
           "9-21960167-21960175 NNNN CT(62 70 0) RT(1696949201671 192) q(0 0 1 -1) r(2 7) U12"}
        ]

      resp = Map.put(resp, :headers, duplicated_headers)
      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :failure
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end
end
