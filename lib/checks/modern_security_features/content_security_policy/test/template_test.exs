defmodule SslMoon.Checks.ContentSecurityPolicyTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.ContentSecurityPolicy.Executor
  @moduletag template: SslMoon.Checks.ContentSecurityPolicy.Template
  @moduletag validation_schema: SslMoon.Checks.ContentSecurityPolicy.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        content_security_policy: [
          %{
            "default-src" => "http://127.0.0.1:* https: data: 'unsafe-inline' 'unsafe-eval'"
          }
        ],
        content_security_policy_report_only: []
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render failure template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        content_security_policy: [
          %{
            "default-src" => "http://127.0.0.1:* https: data: 'unsafe-inline' 'unsafe-eval'"
          },
          %{
            "default-src" => "http://127.0.0.1:* https: data: 'unsafe-inline' 'unsafe-eval'"
          }
        ],
        content_security_policy_report_only: []
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :inactive, %{
        content_security_policy: [],
        content_security_policy_report_only: []
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :not_found, %{
        content_security_policy: [],
        content_security_policy_report_only: []
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
