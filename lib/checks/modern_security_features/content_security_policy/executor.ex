defmodule SslMoon.Checks.ContentSecurityPolicy.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.ContentSecurityPolicy.Executor` module is a specialized component of the SslMoon security suite, aimed at evaluating the implementation of Content Security Policy (CSP) in web applications. CSP is an important security standard used to prevent various types of attacks, such as Cross-Site Scripting (XSS) and data injection attacks.

    This module primarily includes two functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: Initiates a check for CSP headers in the HTTP response from a specified URL. It retrieves both the `Content-Security-Policy` and `Content-Security-Policy-Report-Only` headers, if present, and returns their parsed values.

    - `qualify_check/3`: Based on the results from `perform_check/2`, this function evaluates the CSP implementation. It determines whether the CSP headers are correctly set, marking the check as `:success` if they are, `:inactive` if no CSP headers are found, or `:failure` in other cases.

  ## Usage

    To use this module, specify a check for the Content Security Policy and a target URL:

    ```elixir
    # Define the check and the URL
    check = "content_security_policy"
    url = "http://example.com"

    # Perform the CSP check
    {:ok, check_result} = SslMoon.Checks.ContentSecurityPolicy.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.ContentSecurityPolicy.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :inactive, or :failure)
    ```

    Proper implementation of CSP is crucial for enhancing web application security by specifying which content sources are valid and thereby reducing the risk of injection attacks.

  ## Completion Status

    The ContentSecurityPolicy module is currently implemented correctly and fulfills its primary functionality. However, there remains scope for further enhancements, particularly in the areas of validation and additional features. Future work will focus on refining the validation processes to cover a broader range of scenarios and potential edge cases. Additionally, improvements may include optimizing performance, enhancing the module's resilience against new and evolving security threats, and expanding its capabilities to align with upcoming standards in web security. The module's development will continue with an aim to not only maintain its effectiveness but also to adapt and expand its functionalities in response to user feedback and changing security landscapes.
  """
  require Logger
  alias SslMoon.Http.HttpUtils

  @impl true
  def perform_check(_check, url) do
    {:ok, check_content_security_policy(url)}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      case data do
        %{
          content_security_policy: [],
          content_security_policy_report_only: []
        } ->
          :inactive

        %{
          content_security_policy: [%{}],
          content_security_policy_report_only: [%{}]
        } ->
          :success

        %{
          content_security_policy: [%{}],
          content_security_policy_report_only: []
        } ->
          :success

        %{
          content_security_policy: [],
          content_security_policy_report_only: [%{}]
        } ->
          :success

        _ ->
          :failure
      end

    {:ok, status}
  end

  defp check_content_security_policy(url) do
    case HttpUtils.get_follow_redirect(url) do
      {:ok, %SslMoon.Http.HttpClient.Response{} = response} ->
        %{
          content_security_policy: get_content_security_policy_from_header(response.headers),
          content_security_policy_report_only:
            get_content_security_policy_report_only_from_header(response.headers)
        }

      {:error, _reason} ->
        %{
          content_security_policy: [],
          content_security_policy_report_only: []
        }
    end
  end

  defp get_content_security_policy_from_header(header) do
    header
    |> Enum.filter(fn {key, _value} -> String.match?(key, ~r/\AContent-Security-Policy\z/i) end)
    |> Enum.map(fn {_key, value} -> parse_content_security_policy(value) end)
  end

  defp get_content_security_policy_report_only_from_header(header) do
    header
    |> Enum.filter(fn {key, _value} ->
      String.match?(key, ~r/\AContent-Security-Policy-Report-Only\z/i)
    end)
    |> Enum.map(fn {_key, value} -> parse_content_security_policy(value) end)
  end

  defp parse_content_security_policy(csp) do
    csp
    |> String.split(";", trim: true)
    |> Enum.reduce(%{}, fn item, acc ->
      [key | value] = String.split(item, " ", parts: 2)
      Map.put(acc, key, Enum.join(value, " "))
    end)
  end
end
