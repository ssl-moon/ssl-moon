defmodule SslMoon.Checks.ContentSecurityPolicy.Schema do
  @moduledoc """
  Content Security Policy schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:content_security_policy, {:array, :map})
    field(:content_security_policy_report_only, {:array, :map})
  end

  @impl true
  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [:content_security_policy, :content_security_policy_report_only])
  end
end
