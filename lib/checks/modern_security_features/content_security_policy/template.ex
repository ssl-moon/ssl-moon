defmodule SslMoon.Checks.ContentSecurityPolicy.Template do
  @moduledoc """
  The `ContentSecurityPolicy Component` module provides functions to render the checks for Content Security Policy.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @doc """
  Renders the checks for DNS Zone.
  """

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :inactive -> render_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <div :if={length(@check_data.check_result.content_security_policy) > 0} class="card glass">
        <h2 class="card-title p-3">
          <%= gettext("Content Security Policy:") %>
        </h2>
        <%= for check <- @check_data.check_result.content_security_policy do %>
          <div class="border rounded-xl m-2">
            <div class="overflow-x-auto">
              <table class="table w-2/3">
                <thead>
                  <tr>
                    <th>
                      <%= gettext("CSP header values:") %>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <%= for {key, value} <- check  do %>
                    <tr>
                      <th><%= key %></th>
                      <td class="w-1/2">
                        <%= if key=="block-all-mixed-content" do %>
                          <MaterialIcons.check_circle
                            style="outlined"
                            size={22}
                            class="fill-green-600"
                          />
                        <% else %>
                          <%= value %>
                        <% end %>
                      </td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </div>
        <% end %>
      </div>
      <br />
      <div
        :if={length(@check_data.check_result.content_security_policy_report_only) > 0}
        class="card glass"
      >
        <h2 class="card-title p-3">
          <%= gettext("Content Security Policy Report Only:") %>
        </h2>
        <%= for check <- @check_data.check_result.content_security_policy_report_only do %>
          <div class="border rounded-xl m-2">
            <div class="overflow-x-auto">
              <table class="table w-2/3">
                <thead>
                  <tr>
                    <th>
                      <%= gettext("CSP Report Only header values:") %>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <%= for {key, value} <- check  do %>
                    <tr>
                      <th><%= key %></th>
                      <td class="w-1/2">
                        <%= if key=="block-all-mixed-content" do %>
                          <MaterialIcons.check_circle
                            style="outlined"
                            size={22}
                            class="fill-green-600"
                          />
                        <% else %>
                          <%= value %>
                        <% end %>
                      </td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </div>
        <% end %>
      </div>
      <p class="p-3">
        <b>
          <%= gettext("Content Secure Policy header is present") %>
        </b>
        <br /> <br />
        <%= gettext(
          "When a domain has a Content Security Policy (CSP) header, it means that the website has implemented rules to control which external resources (like scripts, images, and stylesheets) can be loaded and executed. This policy helps prevent security vulnerabilities, such as Cross-Site Scripting (XSS) attacks, by restricting where content can be sourced from. The CSP is enforced by the user's browser, which blocks any resources that do not conform to the specified rules. Implementing CSP enhances the security of the website but requires careful setup to ensure it doesn't interfere with legitimate website functions. It's an important part of a comprehensive web security strategy."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <div :if={length(@check_data.check_result.content_security_policy) > 0} class="card glass">
        <h2 class="card-title p-3">
          <%= gettext("Content Security Policy:") %>
        </h2>
        <%= for check <- @check_data.check_result.content_security_policy do %>
          <div class="border rounded-xl m-2">
            <div class="overflow-x-auto">
              <table class="table w-2/3">
                <thead>
                  <tr>
                    <th>
                      <%= gettext("CSP header values:") %>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <%= for {key, value} <- check  do %>
                    <tr>
                      <th><%= key %></th>
                      <td class="w-1/2">
                        <%= if key=="block-all-mixed-content" do %>
                          <MaterialIcons.check_circle
                            style="outlined"
                            size={22}
                            class="fill-green-600"
                          />
                        <% else %>
                          <%= value %>
                        <% end %>
                      </td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </div>
        <% end %>
      </div>
      <br />
      <div
        :if={length(@check_data.check_result.content_security_policy_report_only) > 0}
        class="card glass"
      >
        <h2 class="card-title p-3">
          <%= gettext("Content Security Policy Report Only:") %>
        </h2>
        <%= for check <- @check_data.check_result.content_security_policy_report_only do %>
          <div class="border rounded-xl m-2">
            <div class="overflow-x-auto">
              <table class="table w-2/3">
                <thead>
                  <tr>
                    <th>
                      <%= gettext("CSP Report Only header values:") %>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <%= for {key, value} <- check  do %>
                    <tr>
                      <th><%= key %></th>
                      <td class="w-1/2">
                        <%= if key=="block-all-mixed-content" do %>
                          <MaterialIcons.check_circle
                            style="outlined"
                            size={22}
                            class="fill-green-600"
                          />
                        <% else %>
                          <%= value %>
                        <% end %>
                      </td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </div>
        <% end %>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("Content Secure Policy multiple headers detected") %>
        </b>
        <br /> <br />
        <%= gettext(
          "Using multiple Content Security Policy (CSP) headers can lead to management complexity and the risk of conflicting rules, making it challenging to maintain consistent security policies. Differences in how browsers interpret these headers can result in unpredictable behavior and compatibility issues. The increased complexity can also lead to errors, potentially breaking website functionality or causing security vulnerabilities. Additionally, debugging and resolving issues become more difficult with multiple overlapping or contradictory CSP directives."
        ) %>
      </p>
    </div>
    """
  end

  defp render_inactive(assigns) do
    ~H"""
    <div test_tag="check_inactive">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Currently, this feature is not active for this host") %></span>
      </div>
      <br />
      <p class="p-3">
        <b>
          <%= gettext("Content Secure Policy header is not present") %>
        </b>
        <br /> <br />
        <%= gettext(
          "When a Content Security Policy (CSP) header is not present on a website, it means the site is not using this layer of security to restrict where content can be loaded from, potentially increasing vulnerability to attacks like Cross-Site Scripting (XSS). Without CSP, the site relies more heavily on other security measures like input sanitization, which may not be as effective against content injection attacks. Browsers will default to their own less strict security policies, potentially leaving the site more exposed to exploits. The absence of CSP also allows for unrestricted loading of external resources, which can be a security risk if not managed carefully."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    gettext(
      "Content Security Policy (CSP) is a web security mechanism that dictates which sources of content can be loaded on a webpage to prevent threats like Cross-Site Scripting (XSS). Through HTTP headers, site owners can set approved content sources and report any policy violations."
    )
  end
end
