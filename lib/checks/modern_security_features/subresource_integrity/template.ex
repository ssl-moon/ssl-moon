defmodule SslMoon.Checks.SubresourceIntegrity.Template do
  @moduledoc """
  The `SubresourceIntegrity Component` module provides functions to render the checks for Subresource Integrity.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :warning -> render_warning(assigns)
      :inactive -> render_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <%= if length(@check_data.check_result.scripts)==0 and length(@check_data.check_result.stylesheet)==0 do %>
        <p class="p-3">
          <b><%= gettext("External resources are not present.") %></b> <br /> <br />
          <%= gettext(
            "Hosting all essential files, assets, and dependencies for a website on the local server or within the same domain centralizes control, enhancing security and simplifying content management. This approach ensures more reliable performance, as it reduces dependency on external sources, potentially speeding up loading times due to fewer DNS lookups and external requests. However, it may forego the advantages of broader content distribution and optimized delivery speeds that external services like Content Delivery Networks (CDNs) offer. Overall, this strategy offers a trade-off between improved security and performance reliability versus the efficiency of a geographically distributed hosting approach."
          ) %>
        </p>
      <% else %>
        <div :if={length(@check_data.check_result.scripts) > 0} class="card glass p-3">
          <div class="flex">
            <MaterialIcons.javascript style="outlined" size={56} class="fill-black-700" />
            <div>
              <b>
                <%= length(@check_data.check_result.scripts) %> <%= gettext("script(s)") %>
              </b>
              <br />
              <span class="flex">
                <MaterialIcons.check_circle size={22} class="fill-green-600" />
                &nbsp; <%= calc_secured(@check_data.check_result.scripts) %>
              </span>
            </div>
          </div>
          <br />
          <details class="collapse border border-base-300 collapse-arrow">
            <summary class="collapse-title font-medium ">
              <%= gettext("View resources") %>
            </summary>
            <div class="collapse-content collapse-arrow">
              <div class="overflow-x-auto">
                <table class="table table-zebra">
                  <thead>
                    <tr>
                      <th>
                        <%= gettext("Resource Type") %>
                      </th>
                      <th>
                        <%= gettext("Resource Source") %>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <%= for resource <- @check_data.check_result.scripts do %>
                      <tr>
                        <td class={[
                          if(resource.status, do: "text-green-700", else: "text-yellow-700")
                        ]}>
                          <b>
                            <%= case resource.type do %>
                              <% :local -> %>
                                <%= gettext("Local") %>
                              <% :external -> %>
                                <%= gettext("External") %>
                              <% :contains_hash -> %>
                                <%= gettext("External (hashed)") %>
                            <% end %>
                          </b>
                        </td>
                        <td><%= resource.source %></td>
                      </tr>
                    <% end %>
                  </tbody>
                </table>
              </div>
            </div>
          </details>
        </div>
        <br />
        <div :if={length(@check_data.check_result.stylesheet) > 0} class="card glass p-3">
          <div class="flex">
            <MaterialIcons.css style="outlined" size={56} class="fill-black-700" />
            <div>
              <b>
                <%= length(@check_data.check_result.stylesheet) %> <%= gettext("stylesheets(s)") %>
              </b>
              <br />
              <span class="flex">
                <MaterialIcons.check_circle size={22} class="fill-green-600" />
                &nbsp; <%= calc_secured(@check_data.check_result.stylesheet) %>
              </span>
            </div>
          </div>
          <br />
          <details class="collapse border border-base-300 collapse-arrow">
            <summary class="collapse-title font-medium ">
              <%= gettext("View resources") %>
            </summary>
            <div class="collapse-content collapse-arrow">
              <div class="overflow-x-auto">
                <table class="table table-zebra">
                  <thead>
                    <tr>
                      <th><%= gettext("Resource Type") %></th>
                      <th><%= gettext("Resource Source") %></th>
                    </tr>
                  </thead>
                  <tbody>
                    <%= for resource <- @check_data.check_result.stylesheet do %>
                      <tr>
                        <td class={[
                          if(resource.status, do: "text-green-700", else: "text-yellow-700")
                        ]}>
                          <b>
                            <%= case resource.type do %>
                              <% :local -> %>
                                <%= gettext("Local") %>
                              <% :external -> %>
                                <%= gettext("External") %>
                              <% :contains_hash -> %>
                                <%= gettext("External (hashed)") %>
                            <% end %>
                          </b>
                        </td>
                        <td><%= resource.source %></td>
                      </tr>
                    <% end %>
                  </tbody>
                </table>
              </div>
            </div>
          </details>
        </div>
        <p class="p-3">
          <b><%= gettext("Subresource Integrity is present") %></b> <br /> <br />
          <%= gettext(
            "When Subresource Integrity (SRI) is implemented on a website that hosts all its essential files, assets, and dependencies locally or within the same domain, the security and integrity of the website are significantly enhanced. SRI ensures that all locally hosted resources are verified for integrity, preventing the execution of tampered or malicious content. This combination of local hosting and SRI leads to a robust security posture, as it minimizes external vulnerabilities and ensures the authenticity of all resources. Additionally, it maintains the performance benefits of local hosting, such as faster loading times and reduced reliance on external services, while adding an extra layer of protection against content manipulation."
          ) %>
        </p>
      <% end %>
    </div>
    """
  end

  defp render_warning(assigns) do
    ~H"""
    <div test_tag="check_warning">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-warning">
        <MaterialIcons.warning style="outlined" size={42} class="fill-yellow-600" />
        <span><%= gettext("Check completed with warnings") %></span>
      </div>
      <br />
      <div :if={length(@check_data.check_result.scripts) > 0} class="card glass p-3">
        <div class="flex">
          <MaterialIcons.javascript style="outlined" size={56} class="fill-black-700" />
          <div>
            <b>
              <%= length(@check_data.check_result.scripts) %> <%= gettext("script(s)") %>
            </b>
            <br />
            <span class="flex">
              <%= if all_secured?(@check_data.check_result.scripts) do %>
                <MaterialIcons.check_circle size={22} class="fill-green-600" />
              <% else %>
                <MaterialIcons.warning size={22} class="fill-yellow-600" />
              <% end %>
              &nbsp; <%= calc_secured(@check_data.check_result.scripts) %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium ">
            <%= gettext("View resources") %>
          </summary>
          <div class="collapse-content collapse-arrow">
            <div class="overflow-x-auto">
              <table class="table table-zebra">
                <thead>
                  <tr>
                    <th><%= gettext("Resource Type") %></th>
                    <th><%= gettext("Resource Source") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.scripts do %>
                    <tr>
                      <td class={[
                        if(resource.status, do: "text-green-700", else: "text-yellow-700")
                      ]}>
                        <b>
                          <%= case resource.type do %>
                            <% :local -> %>
                              <%= gettext("Local") %>
                            <% :external -> %>
                              <%= gettext("External") %>
                            <% :contains_hash -> %>
                              <%= gettext("External (hashed)") %>
                          <% end %>
                        </b>
                      </td>
                      <td><%= resource.source %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </div>
        </details>
      </div>
      <br />
      <div :if={length(@check_data.check_result.stylesheet) > 0} class="card glass p-3">
        <div class="flex">
          <MaterialIcons.css style="outlined" size={56} class="fill-black-700" />
          <div>
            <b>
              <%= length(@check_data.check_result.stylesheet) %> <%= gettext("stylesheets(s)") %>
            </b>
            <br />
            <span class="flex">
              <%= if all_secured?(@check_data.check_result.stylesheet) do %>
                <MaterialIcons.check_circle size={22} class="fill-green-600" />
              <% else %>
                <MaterialIcons.warning size={22} class="fill-yellow-600" />
              <% end %>
              &nbsp; <%= calc_secured(@check_data.check_result.stylesheet) %>
            </span>
          </div>
        </div>
        <br />
        <details class="collapse border border-base-300 collapse-arrow">
          <summary class="collapse-title font-medium ">
            <%= gettext("View resources") %>
          </summary>
          <div class="collapse-content collapse-arrow">
            <div class="overflow-x-auto">
              <table class="table table-zebra">
                <thead>
                  <tr>
                    <th><%= gettext("Resource Type") %></th>
                    <th><%= gettext("Resource Source") %></th>
                  </tr>
                </thead>
                <tbody>
                  <%= for resource <- @check_data.check_result.stylesheet do %>
                    <tr>
                      <td class={[
                        if(resource.status, do: "text-green-700", else: "text-yellow-700")
                      ]}>
                        <b>
                          <%= case resource.type do %>
                            <% :local -> %>
                              <%= gettext("Local") %>
                            <% :external -> %>
                              <%= gettext("External") %>
                            <% :contains_hash -> %>
                              <%= gettext("External (hashed)") %>
                          <% end %>
                        </b>
                      </td>
                      <td><%= resource.source %></td>
                    </tr>
                  <% end %>
                </tbody>
              </table>
            </div>
          </div>
        </details>
      </div>
      <p class="p-3">
        <b><%= gettext("Subresource Integrity is present with warnings") %></b> <br /> <br />
        <%= gettext(
          "When not all resources on a website have Subresource Integrity (SRI) implemented correctly, and some external resources lack the necessary integrity hashes, the website's security is potentially compromised. This situation creates vulnerabilities, especially for resources loaded from third-party servers, where the site owner has less control. Consequently, the website may be exposed to security risks like malicious script execution, impacting both the site's integrity and the safety of its users."
        ) %>
      </p>
    </div>
    """
  end

  defp render_inactive(assigns) do
    ~H"""
    <div test_tag="check_inactive">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Currently, this feature is not active for this host") %></span>
      </div>
      <br />
      <p class="p-3">
        <b><%= gettext("Subresource Integrity is not present") %></b> <br /> <br />
        <%= gettext(
          "When Subresource Integrity (SRI) is not present on a website, there is no mechanism in place to verify the integrity of external resources. This absence of SRI leaves the website vulnerable to potential security risks, as there's no assurance that the resources being loaded are untampered and safe. Malicious actors could exploit this lack of verification to inject harmful scripts or manipulate content, compromising the website's security and potentially harming its users. In summary, the absence of SRI represents a security gap, as it fails to protect against the integrity compromise of external resources, making the website more susceptible to threats."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    gettext(
      "Subresource Integrity (SRI) is a web development security mechanism that guarantees the authenticity and integrity of external resources like scripts and stylesheets. By including a cryptographic hash in the resource reference, browsers can verify that the resources haven't been tampered with before loading and executing them."
    )
  end

  defp calc_secured(resources) do
    counter = resources |> Enum.count(& &1.status)
    "#{counter} #{gettext("resources secured out of")} #{length(resources)}"
  end

  defp all_secured?(resources) do
    resources |> Enum.count(& &1.status) == length(resources)
  end
end
