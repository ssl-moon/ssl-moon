defmodule SslMoon.Checks.SubresourceIntegrity.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.SubresourceIntegrity.Executor` module is an essential component of the SslMoon security suite, designed to verify the implementation of Subresource Integrity (SRI) in web applications. SRI is a security feature that enables browsers to verify that resources fetched from external servers have not been tampered with, providing an additional layer of security against certain types of attacks, such as code injection.

    This module contains two primary functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: Initiates an SRI check for the given URL. It parses the HTML content to identify external scripts and stylesheets, and then verifies whether they include correct SRI hashes.

    - `qualify_check/3`: Evaluates the results of `perform_check/2`. The check is marked as `:success` if all external resources either have valid SRI hashes or are local (thus not requiring SRI), and `:warning` if there are any external resources without valid SRI hashes.

  ## Usage

    To use this module, specify a check for Subresource Integrity and a target URL:

    ```elixir
    # Define the check and the URL
    check = "subresource_integrity"
    url = "http://example.com"

    # Perform the SRI check
    {:ok, check_result} = SslMoon.Checks.SubresourceIntegrity.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.SubresourceIntegrity.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :warning, or :inactive)
    ```
    Ensuring SRI is correctly implemented is vital for the security of web applications, particularly for those that load resources from external sources.

  ## Current Status and Limitations

    TODO: This module currently needs improvement to fully and accurately perform SRI checks. Users should be aware of potential limitations in its current implementation.

  ## Completion Status

    As it stands, the module is in a developmental phase and has not yet reached its full potential. The primary area for improvement lies in the module's capability to perform Subresource Integrity (SRI) checks accurately and comprehensively. Work is underway to address these limitations and enhance the module's effectiveness in this regard. Future development will focus on refining the SRI checking process, ensuring that it is robust and reliable. Additionally, plans include updating the module to keep pace with evolving web standards and security best practices. The aim is to provide users with a tool that is not only functional but also comprehensive in its approach to SRI validation.
  """

  require Logger
  alias SslMoon.Http.HttpUtils
  alias SslMoon.Http.HttpClient

  @impl true
  def perform_check(_check, url) do
    {:ok, check_subresource_integrity(url)}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      case data do
        %{
          scripts: _,
          stylesheet: _
        } ->
          check_status(data)

        _ ->
          :inactive
      end

    {:ok, status}
  end

  defp check_status(data) do
    if Enum.all?(data.stylesheet, fn stylesheet -> stylesheet.status end) &&
         Enum.all?(data.scripts, fn script -> script.status end) do
      :success
    else
      :warning
    end
  end

  defp check_subresource_integrity(url) do
    {:ok, %SslMoon.Http.HttpClient.Response{} = response} = HttpUtils.get_follow_redirect(url)
    check_sri(response, url)
  end

  defp check_sri(response, url) do
    scripts_list =
      response.body
      |> parse_html_script()
      |> check_local_resources(url)

    stylesheet_list =
      response.body
      |> parse_html_stylesheet()
      |> check_local_resources(url)

    %{
      scripts: scripts_list,
      stylesheet: stylesheet_list
    }
  end

  defp parse_html_script(body) do
    script_sources =
      body
      |> Floki.parse_document!()
      |> Floki.find("script")
      |> Enum.filter(fn {tag, attrs, _content} ->
        tag == "script" and Enum.any?(attrs, fn {key, _value} -> key == "src" end)
      end)
      |> Enum.map(fn
        {"script", attributes, _} ->
          integrity_tuple = Enum.find(attributes, fn {key, _} -> key == "integrity" end)
          src_tuple = Enum.find(attributes, fn {key, _} -> key == "src" end)
          {integrity_tuple, src_tuple}
          check_hash(integrity_tuple, src_tuple)
      end)

    script_sources
  end

  defp parse_html_stylesheet(body) do
    stylesheet_sources =
      body
      |> Floki.parse_document!()
      |> Floki.find("link")
      |> Enum.filter(fn {_, attrs, _content} ->
        Enum.any?(attrs, fn {key, value} ->
          {key, value} == {"rel", "stylesheet"}
        end)
      end)
      |> Enum.map(fn
        {"link", attributes, _} ->
          integrity_tuple = Enum.find(attributes, fn {key, _} -> key == "integrity" end)
          src_tuple = Enum.find(attributes, fn {key, _} -> key == "href" end)
          check_hash(integrity_tuple, src_tuple)
      end)

    stylesheet_sources
  end

  defp check_hash(integrity_tuple, src_tuple) do
    if integrity_tuple && src_tuple do
      check_hashed_source(integrity_tuple, src_tuple)
    else
      {_, src} = src_tuple
      {:error, src, nil}
    end
  end

  defp check_hashed_source({_, integrity}, {_, src} = src_tuple) do
    hash = String.slice(integrity, 7..-1)
    hashspec = String.slice(integrity, 0, 6)

    case download_resource(src) do
      nil ->
        {:error, src, :hashed}

      body ->
        compare_hashes(hashspec, body, hash, src_tuple)
    end
  end

  defp download_resource(url) do
    case HttpClient.request(:get, url, "", [], ssl: [verify: :verify_none]) do
      {:ok, response} -> response.body
      {:error, _reason} -> nil
    end
  end

  defp compare_hashes(hashspec, body, hash, {_, src}) do
    actual_hash =
      :crypto.hash(String.to_atom(hashspec), body)
      |> :base64.encode_to_string()

    if to_string(actual_hash) == to_string(hash) do
      {:ok, src, :contains_hash}
    else
      {:error, src, :contains_hash}
    end
  end

  defp check_local_resources(resources, url) do
    Enum.map(resources, &analyze_resource(&1, url))
  end

  defp analyze_resource({:error, src, nil}, url) do
    if is_local?(src, url) do
      %{status: true, source: src, type: :local}
    else
      %{status: false, source: src, type: :external}
    end
  end

  defp analyze_resource({:ok, src, hash}, _url) do
    %{status: true, source: src, type: hash}
  end

  defp analyze_resource({:error, src, hash}, _url) do
    %{status: false, source: src, type: hash}
  end

  defp is_local?(src, url) do
    String.first(src) == "/" || String.contains?(src, url) || src == ""
  end
end
