defmodule SslMoon.Checks.SubresourceIntegrityTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks
  import SslMoon.Checks.SubresourceIntegrity.Factory

  @moduletag executor: SslMoon.Checks.SubresourceIntegrity.Executor
  @moduletag template: SslMoon.Checks.SubresourceIntegrity.Template
  @moduletag validation_schema: SslMoon.Checks.SubresourceIntegrity.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "success status when there are no subresources ????", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      resp = build(:http_response)

      body =
        "<!doctype html><html itemscope=\"\" itemtype=\"http://schema.org/WebPage\" lang=\"ro-MD\"><head><meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\"><meta content=\"/images/branding/googleg/1x/googleg_standard_color_128dp.png\" itemprop=\"image\"><title>Google</title><script nonce=\"B-fDE5snFGyhp0inB6bD_g\">(function(){var _g={kEI:'RmAlZeLaBb6hi-gP1I2zwA0',kEXPI:'0,1365467,207,4804,2316,383,246,5,1129120,1197765,380726,16114,28684,22431,1361,12315,17584,4998,17075,41316,2891,8348,3406,606,63304,13721,20583,4,59617,27032,6642,7596,1,11943,30211,2,39761,5679,1020,31122,4568,6259,23418,1252,33064,2,2,1,26632,8155,23350,874,19633,8,1921,9779,42459,20199,20136,14,82,20206,8377,18988,5375,2265,765,5629,10187,1804,21012,1825,10260,2171,2414,2839,6561,1632,26702,7914,18154,5212716,906,109,2,195,576,47,47,5994100,97,2803117,3311,141,795,28682,696,8,119,4,8,5,8855580,15085465,579,4043528,16672,36926,2532,2988,12,33,3,805,584,3,631,1474,3,821,1393125,23759270,12799,8408,2879,1595,111,978,2441,6695,1966,4370,2978,669,450,2665,298,6020,877,3,3945,2635,1213,5,1899,2048,1951,1239,640,3235,303,2546,281,7138,780,1952,758,166,3399,1638,726,101,2081,5030,495,40,314,207,3729,2,76,172,34,2766,168,188,884,603,3,387,2,1291,2,6,55,910,2,528,196,1791,3,262,1700,1194,118,454,683,4,409,468,439,274,7,1,8,136,110,131,760,91,125,570,18,193,10,390,2428,1192,52,590,634,1628,840,19,1361,1054,454,51,231,1,48,6,1124,697,901,3,115,836,104,131,2,673,851,353,128,2,1194,74,727,3,2,2,2,198,459,14,15,4610,482,596,219,10,205,4,422',kBL:'dnl7',kOPI:89978449};(function(){var a;(null==(a=window.google)?0:a.stvsc)?google.kEI=_g.kEI:window.google=_g;}).call(this);})();(function(){google.sn='webhp';google.kHL='ro-MD';})();(function(){\nvar h=this||self;function l(){return void 0!==window.google&&void 0!==window.google.kOPI&&0!==window.google.kOPI?window.google.kOPI:null};var m,n=[];function p(a){for(var b;a&&(!a.getAttribute||!(b=a.getAttribute(\"eid\")));)a=a.parentNode;return b||m}function q(a){for(var b=null;a&&(!a.getAttribute||!(b=a.getAttribute(\"leid\")));)a=a.parentNode;return b}function r(a){/^http:/i.test(a)&&\"https:\"===window.location.protocol&&(google.ml&&google.ml(Error(\"a\"),!1,{src:a,glmm:1}),a=\"\");return a}\nfunction t(a,b,c,d,k){var e=\"\";-1===b.search(\"&ei=\")&&(e=\"&ei=\"+p(d),-1===b.search(\"&lei=\")&&(d=q(d))&&(e+=\"&lei=\"+d));d=\"\";var g=-1===b.search(\"&cshid=\")&&\"slh\"!==a,f=[];f.push([\"zx\",Date.now().toString()]);h._cshid&&g&&f.push([\"cshid\",h._cshid]);c=c();null!=c&&f.push([\"opi\",c.toString()]);for(c=0;c<f.length;c++){if(0===c||0<c)d+=\"&\";d+=f[c][0]+\"=\"+f[c][1]}return\"/\"+(k||\"gen_204\")+\"?atyp=i&ct=\"+String(a)+\"&cad=\"+(b+e+d)};m=google.kEI;google.getEI=p;google.getLEI=q;google.ml=function(){return null};google.log=function(a,b,c,d,k,e){e=void 0===e?l:e;c||(c=t(a,b,e,d,k));if(c=r(c)){a=new Image;var g=n.length;n[g]=a;a.onerror=a.onload=a.onabort=function(){delete n[g]};a.src=c}};google.logUrl=function(a,b){b=void 0===b?l:b;return t(\"\",a,b)};}).call(this);(function(){google.y={};google.sy=[];google.x=function(a,b){if(a)var c=a.id;else{do c=Math.random();while(google.y[c])}google.y[c]=[a,b];return!1};google.sx=function(a){google.sy.push(a)};google.lm=[];google.plm=function(a){google.lm.push.apply(google.lm,a)};google.lq=[];google.load=function(a,b,c){google.lq.push([[a],b,c])};google.loadAll=function(a,b){google.lq.push([a,b])};google.bx=!1;google.lx=function(){};var d=[];google.fce=function(a,b,c,e){d.push([a,b,c,e])};google.qce=d;}).call(this);google.f={};(function(){\ndocument.documentElement.addEventListener(\"submit\",function(b){var a;if(a=b.target){var c=a.getAttribute(\"data-submitfalse\");a=\"1\"===c||\"q\"===c&&!a.elements.q.value?!0:!1}else a=!1;a&&(b.preventDefault(),b.stopPropagation())},!0);document.documentElement.addEventListener(\"click\",function(b){var a;a:{for(a=b.target;a&&a!==document.documentElement;a=a.parentElement)if(\"A\"===a.tagName){a=\"1\"===a.getAttribute(\"data-nohref\");break a}a=!1}a&&b.preventDefault()},!0);}).call(this);</script><style>#gbar,#guser{font-size:13px;padding-top:1px !important;}#gbar{height:22px}#guser{padding-bottom:7px !importa"

      resp = Map.put(resp, :body, body)

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :success
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "warning status on external resources ???", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, "test.com", _, _, _ ->
      resp = build(:http_response)

      body =
        "<!DOCTYPE html>\n<html lang=\"ru\" id=\"facebook\" class=\"no_js\">\n<head><meta charset=\"utf-8\" /><meta name=\"referrer\" content=\"default\" id=\"meta_referrer\" /><script nonce=\"B39ypldT\">function envFlush(a){function b(b){for(var c in a)b[c]=a[c]}window.requireLazy?window.requireLazy([\"Env\"],b):(window.Env=window.Env||{},b(window.Env))}envFlush({\"useTrustedTypes\":false,\"isTrustedTypesReportOnly\":false,\"ajaxpipe_token\":\"AXivhnLnGjr0Z6j9xWs\",\"remove_heartbeat\":false,\"stack_trace_limit\":30,\"timesliceBufferSize\":5000,\"show_invariant_decoder\":false,\"compat_iframe_token\":\"AQ7tSDRctYJY71pLZgc\",\"isCQuick\":false});</script><script nonce=\"B39ypldT\">(function(a){function b(b){if(!window.openDatabase)return;b.I_AM_INCOGNITO_AND_I_REALLY_NEED_WEBSQL=function(a,b,c,d){return window.openDatabase(a,b,c,d)};window.openDatabase=function(){throw new Error()}}b(a)})(this);</script><style nonce=\"B39ypldT\"></style><script nonce=\"B39ypldT\">__DEV__=0;</script><noscript><meta http-equiv=\"refresh\" content=\"0; URL=/?_fb_noscript=1\" /></noscript><link rel=\"manifest\" id=\"MANIFEST_LINK\" href=\"/data/manifest/\" crossorigin=\"use-credentials\" /><title id=\"pageTitle\">Facebook — Выполните вход или зарегистрируйтесь</title><meta property=\"og:site_name\" content=\"Facebook\" /><meta property=\"og:url\" content=\"https://www.facebook.com/\" /><meta property=\"og:image\" content=\"https://www.facebook.com/images/fb_logo/app-facebook-circle-bp.png\" /><meta property=\"og:locale\" content=\"ru_RU\" /><link rel=\"alternate\" media=\"only screen and (max-width: 640px)\" href=\"https://m.facebook.com/\" /><link rel=\"alternate\" media=\"handheld\" href=\"https://m.facebook.com/\" /><meta name=\"description\" content=\"&#x412;&#x43e;&#x439;&#x434;&#x438;&#x442;&#x435; &#x43d;&#x430; Facebook, &#x447;&#x442;&#x43e;&#x431;&#x44b; &#x43e;&#x431;&#x449;&#x430;&#x442;&#x44c;&#x441;&#x44f; &#x441; &#x434;&#x440;&#x443;&#x437;&#x44c;&#x44f;&#x43c;&#x438;, &#x440;&#x43e;&#x434;&#x441;&#x442;&#x432;&#x435;&#x43d;&#x43d;&#x438;&#x43a;&#x430;&#x43c;&#x438; &#x438; &#x437;&#x43d;&#x430;&#x43a;&#x43e;&#x43c;&#x44b;&#x43c;&#x438;.\" /><link rel=\"canonical\" href=\"https://www.facebook.com/\" /><link rel=\"icon\" href=\"https://static.xx.fbcdn.net/rsrc.php/yv/r/B8BxsscfVBr.ico\" /><link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yY/l/0,cross/4qFqTquP-fc.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"/uaktyf\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yO/l/0,cross/I2wPU5r07is.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"Pud6B2Z\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yJ/l/0,cross/Xyoav1gLypl.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"GrqIbBd\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yX/l/0,cross/4JeW63xJevY.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"t29hWfM\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yi/l/0,cross/yotEdcUw9Gj.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"DcLQ9Pg\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yc/l/0,cross/1FPNULrhhBJ.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"0Bj1L9r\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/yV/l/0,cross/_bzWjvAFjKO.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"JG0XRy3\" />\n<link type=\"text/css\" rel=\"stylesheet\" href=\"https://static.xx.fbcdn.net/rsrc.php/v3/y3/l/0,cross/ikFECARVllV.css?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"qgYJ9Ln\" />\n<script src=\"https://static.xx.fbcdn.net/rsrc.php/v3/yL/r/C7x9HQY1590.js?_nc_x=Ij3Wp8lg5Kz\" data-bootloader-hash=\"4MtYcQP\" nonce=\"B39ypldT\"></script>\n<script nonce=\"B39ypldT\">requireLazy([\"HasteSupportData\"],function(m){m.handle({\"clpData\":{\"1838142\":{\"r\":1,\"s\":1},\"4883\":{\"r\":1,\"s\":1},\"1814852\":{\"r\":1},\"1848815\":{\"r\":10000,\"s\":1}},\"gkxData\":{\"676837\":{\"result\":false,\"hash\":\"AT4N8wBZA8ctCdHwN5s\"},\"708253\":{\"result\":false,\"hash\":\"AT5n4hBL3YTMnQWtHw0\"},\"1167394\":{\"result\":false,\"hash\":\"AT7BpN-tlUPwbIIFHIo\"},\"1073500\":{\"result\":false,\"hash\":\"AT7"

      resp = Map.put(resp, :body, body)

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :warning
    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  # This test makes no sense

  # test "test_subresource_integrity_inactive" do
  #   with_mock HTTPoison,
  #     get: fn "emobias.md", _, _ ->
  #       {:error, %HTTPoison.Error{reason: :nxdomain, id: nil}}
  #     end do
  #     assert SubresourceIntegrity.check_subresource_integrity("emobias.md") == %{}
  #   end
  # end
end
