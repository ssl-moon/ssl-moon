defmodule SslMoon.Checks.SubresourceIntegrityTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.SubresourceIntegrity.Executor
  @moduletag template: SslMoon.Checks.SubresourceIntegrity.Template
  @moduletag validation_schema: SslMoon.Checks.SubresourceIntegrity.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        scripts: [
          %{
            status: true,
            type: :local,
            source: "/templates/allrounder-3/js/jquery-1.9.1.min.js"
          }
        ],
        stylesheet: [
          %{
            status: true,
            type: :local,
            source: "/templates/allrounder-3/css/template.css"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render warning template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :warning, %{
        scripts: [
          %{
            status: false,
            type: :local,
            source: "/templates/allrounder-3/js/jquery-1.9.1.min.js"
          }
        ],
        stylesheet: [
          %{
            status: false,
            type: :local,
            source: "/templates/allrounder-3/css/template.css"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_warning"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :inactive, %{
        scripts: [],
        stylesheet: []
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :not_found, %{})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
