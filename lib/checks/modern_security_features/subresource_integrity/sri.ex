defmodule SslMoon.Checks.SubresourceIntegrity.SRI do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field :source, :string
    field :status, :boolean
    field :type, Ecto.Enum, values: [:external, :local, :hashed, :contains_hash]
  end

  def changeset(sri, params) do
    sri
    |> cast(params, [:source, :status, :type])
    |> validate_required([:source, :status, :type])
  end
end
