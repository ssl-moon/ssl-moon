defmodule SslMoon.Checks.SubresourceIntegrity.Schema do
  @moduledoc """
    Subresource Integrity schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset
  alias SslMoon.Checks.SubresourceIntegrity.SRI

  @primary_key false

  embedded_schema do
    embeds_many :stylesheet, SRI
    embeds_many :scripts, SRI
  end

  @impl true
  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [])
    |> cast_embed(:stylesheet, with: &SRI.changeset/2)
    |> cast_embed(:scripts, with: &SRI.changeset/2)
  end
end
