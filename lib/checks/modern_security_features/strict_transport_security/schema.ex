defmodule SslMoon.Checks.StrictTransportSecurity.Schema do
  @moduledoc """
  Strict Transport Security schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  alias SslMoon.Checks.StrictTransportSecurity.Hsts

  @primary_key false

  embedded_schema do
    embeds_many(:hsts, Hsts, on_replace: :delete)
  end

  @impl true
  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [])
    |> cast_embed(:hsts, with: &Hsts.changeset/2)
  end
end
