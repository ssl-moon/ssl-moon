defmodule SslMoon.Checks.StrictTransportSecurity.Template do
  @moduledoc """
  The `StrictTransportSecurity Component` module provides functions to render the checks for Content Strict Transport Security.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :warning -> render_warning(assigns)
      :inactive -> render_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <%= for check <- @check_data.check_result.hsts do %>
        <div class="border rounded-xl m-2">
          <div class="overflow-x-auto">
            <table class="table">
              <thead>
                <tr>
                  <th>
                    <%= gettext("HSTS header values:") %>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th><%= gettext("Max Age:") %></th>
                  <td>
                    <%= check.max_age %> <%= gettext("seconds") %> (<%= seconds_to_time(check.max_age) %>)
                  </td>
                </tr>
                <tr>
                  <th><%= gettext("Include subdomains:") %></th>
                  <td>
                    <MaterialIcons.check_circle size={22} class="fill-green-600" />
                  </td>
                </tr>
                <tr>
                  <th><%= gettext("Preload:") %></th>
                  <td>
                    <MaterialIcons.check_circle size={22} class="fill-green-600" />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      <% end %>
      <p class="p-3">
        <b><%= gettext("HSTS is configured properly") %></b> <br /> <br />
        <%= gettext(
          "HSTS is properly configured with a single `Strict-Transport-Security` header, an appropriate `max-age` set, and the `includeSubDomains` and `preload` values are present. The max-age directive in HSTS settings specifies the time for which a browser enforces HTTPS, typically set to at least a year for long-term security. Including the includeSubDomains directive extends this security to all subdomains, protecting them from attacks like Man-In-The-Middle (MITM). The preload directive allows domains to be preloaded in browsers' HSTS lists, ensuring HTTPS is used from the first visit, enhancing security before any HSTS headers are received. This preloading is crucial for preventing interception and tampering during the initial connection. Overall, these HSTS configurations significantly improve web security by ensuring encrypted communication and safeguarding both the site and its users from specific cyber threats."
        ) %>
      </p>
    </div>
    """
  end

  defp render_warning(assigns) do
    ~H"""
    <div test_tag="check_warning">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-warning">
        <MaterialIcons.warning style="outlined" size={42} class="fill-yellow-600" />
        <span><%= gettext("Check completed with warnings") %></span>
      </div>
      <br />
      <%= for check <- @check_data.check_result.hsts do %>
        <div class="border rounded-xl p-2">
          <div class="overflow-x-auto">
            <table class="table">
              <thead>
                <tr>
                  <th>
                    <%= gettext("HSTS header values:") %>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th><%= gettext("Max Age:") %></th>
                  <td>
                    <%= check.max_age %> <%= gettext("seconds") %> (<%= seconds_to_time(check.max_age) %>)
                  </td>
                </tr>
                <tr>
                  <th><%= gettext("Include subdomains:") %></th>
                  <td>
                    <%= if check.includeSubDomains do %>
                      <MaterialIcons.check_circle size={22} class="fill-green-600" />
                    <% else %>
                      <MaterialIcons.warning size={22} class="fill-yellow-600" />
                    <% end %>
                  </td>
                </tr>
                <tr>
                  <th><%= gettext("Preload:") %></th>
                  <td>
                    <%= if check.preload do %>
                      <MaterialIcons.check_circle size={22} class="fill-green-600" />
                    <% else %>
                      <MaterialIcons.warning size={22} class="fill-yellow-600" />
                    <% end %>
                  </td>
                </tr>
                <tr>
                  <th><%= gettext("Location:") %></th>
                  <td>
                    <%= check.location %>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <br />
      <% end %>
      <br />
      <p class="p-3">
        <b><%= gettext("The configuration has some non-critical issues.") %></b>
        <br /> <br />
        <%= gettext(
          "The HSTS evaluation is finished and has presented warnings, pointing to possible concerns or imperfect setups in its deployment. HSTS warnings usually indicate issues with the security policy's setup, such as a too-short max-age value, which doesn't provide long-term security. The absence of the includeSubDomains directive can leave subdomains vulnerable, as the HSTS policy won't apply to them. Not including the preload directive means the domain isn't on browsers' preload lists, increasing risk during the first connection. Multiple or incorrectly formatted HSTS headers can lead to confusion and improper policy enforcement. Lastly, sending HSTS headers over HTTP is ineffective as the policy is designed to enhance HTTPS security."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <%= if hd(@check_data.check_result.hsts).max_age==nil do %>
        <div class="border rounded-xl p-2">
          <div class="overflow-x-auto">
            <table class="table">
              <thead>
                <tr>
                  <th>
                    <%= gettext("HSTS header values:") %>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th class="w-1/2"><%= gettext("Max Age:") %></th>
                  <td>
                    ;
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <br />
        <p class="p-3">
          <b><%= gettext("Invalid HSTS header value") %></b> <br /> <br />
          <%= gettext(
            "The \"max_age\" variable in the header seems to be unset, which isn't a valid age value. When the max-age variable in the HSTS header is unset, it leads to no specified duration for enforcing secure HTTPS connections, reducing the effectiveness of the HSTS policy. This lack of a defined max-age can expose the site to security risks, such as Man-In-The-Middle attacks, due to potential use of unsecured HTTP connections. Additionally, without a set max-age, the domain may not be eligible for inclusion in browsers' HSTS preload lists, further compromising long-term security."
          ) %>
        </p>
      <% else %>
        <%= for check <- @check_data.check_result.hsts do %>
          <div class="border rounded-xl p-2">
            <div class="overflow-x-auto">
              <table class="table">
                <thead>
                  <tr>
                    <th>
                      <%= gettext("HSTS header values:") %>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th><%= gettext("Max Age:") %></th>
                    <td>
                      <%= check.max_age %> <%= gettext("seconds") %> (<%= seconds_to_time(
                        check.max_age
                      ) %>)
                    </td>
                  </tr>
                  <tr>
                    <th><%= gettext("Include subdomains:") %></th>
                    <td>
                      <%= if check.includeSubDomains do %>
                        <MaterialIcons.check_circle size={22} class="fill-green-600" />
                      <% else %>
                        <MaterialIcons.warning size={22} class="fill-yellow-600" />
                      <% end %>
                    </td>
                  </tr>
                  <tr>
                    <th><%= gettext("Preload:") %></th>
                    <td>
                      <%= if check.preload do %>
                        <MaterialIcons.check_circle size={22} class="fill-green-600" />
                      <% else %>
                        <MaterialIcons.warning size={22} class="fill-yellow-600" />
                      <% end %>
                    </td>
                  </tr>
                  <tr>
                    <th><%= gettext("Location:") %></th>
                    <td>
                      <%= check.location %>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <br />
        <% end %>
        <p class="p-3">
          <b><%= gettext("Multiple HSTS headers detected") %></b> <br /> <br />
          <%= gettext(
            "Multiple HSTS headers indicate a misconfiguration, leading to conflicting policies and confusion about which security rules the browser should follow. This inconsistency in browser handling can result in weak security enforcement, exposing the site to potential attacks. Such misconfigurations can also hinder a domain's eligibility for inclusion in browsers' HSTS preload lists. Resolving this requires consolidating the HSTS headers into a single, coherent policy for effective security management."
          ) %>
        </p>
      <% end %>
    </div>
    """
  end

  defp render_inactive(assigns) do
    ~H"""
    <div test_tag="check_inactive">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Currently, this feature is not active for this host") %></span>
      </div>
    </div>
    """
  end

  defp template_description do
    gettext(
      "HTTP Strict Transport Security (HSTS) is a security policy mechanism that helps to protect websites against protocol downgrade attacks and cookie hijacking. It allows web servers to declare that web browsers (or other complying user agents) should interact with it using only secure HTTPS connections and never via the insecure HTTP protocol."
    )
  end

  defp seconds_to_time(seconds) when is_integer(seconds) do
    seconds_in_a_day = 86_400
    days_in_a_month = 30.44
    days_in_a_year = 365

    if seconds >= seconds_in_a_day * days_in_a_year do
      years = Float.round(seconds / (seconds_in_a_day * days_in_a_year))
      "#{trunc(years)} #{gettext("years")}"
    else
      months = Float.round(seconds / (seconds_in_a_day * days_in_a_month))
      "#{trunc(months)} #{gettext("months")}"
    end
  end
end
