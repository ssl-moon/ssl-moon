defmodule SslMoon.Checks.StrictTransportSecurity.Hsts do
  @moduledoc """
  HTTP Strict Transport Security  schema.
  """
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:max_age, :integer)
    field(:includeSubDomains, :boolean, default: false)
    field(:preload, :boolean, default: false)
    field(:location, :string)
  end

  def changeset(hsts, params) do
    hsts
    |> cast(params, [:max_age, :includeSubDomains, :preload, :location])
  end
end
