defmodule SslMoon.Checks.StrictTransportSecurity.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.StrictTransportSecurity.Executor` module is a key part of the SslMoon security suite, focused on verifying the implementation of HTTP Strict Transport Security (HSTS) in web applications. HSTS is a web security policy mechanism that helps to protect websites against man-in-the-middle attacks like protocol downgrade attacks and cookie hijacking.

    This module provides two main functions, `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: This function performs an HSTS check on the specified URL. It sends an HTTP request and parses the `Strict-Transport-Security` header from the response, returning a map with the HSTS details.

    - `qualify_check/3`: Evaluates the HSTS configuration based on the results from `perform_check/2`. The check status is determined as `:success` if HSTS is properly configured with `max-age`, `includeSubDomains`, and `preload`, `:warning` if only `max-age` is set, `:inactive` if no HSTS headers are found, and `:failure` in other cases.

  ## Usage

    To utilize this module, specify a check for Strict Transport Security and a target URL:

    ```elixir
    # Define the check and the URL
    check = "strict_transport_security"
    url = "http://example.com"

    # Perform the HSTS check
    {:ok, check_result} = SslMoon.Checks.StrictTransportSecurity.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.StrictTransportSecurity.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :warning, :inactive, or :failure)
    ```

    Ensuring proper HSTS implementation is vital for securing web communications and safeguarding against common web attacks.

  ## Completion Status

    The StrictTransportSecurity module has been implemented correctly and is operational, effectively assessing the HTTP Strict Transport Security (HSTS) configuration of web applications. However, there is room for further enhancements, particularly in the realm of additional validations and optimization. Future development plans include improving the module's ability to handle a wider range of HSTS configurations and edge cases. Efforts will also be made to optimize performance and ensure that the module remains robust against evolving web security standards and practices. Continuous improvement is a priority, with a focus on refining the module's effectiveness and adaptability.
  """
  require Logger
  alias SslMoon.Http.HttpUtils

  @impl true
  def perform_check(_check, url) do
    {:ok, %{hsts: check_hsts(url)}}
  end

  @impl true
  def qualify_check(_check, _url, data) do
    status =
      case data.hsts do
        [] ->
          :inactive

        [
          %{
            max_age: nil
          }
        ] ->
          :failure

        [
          %{
            max_age: _,
            includeSubDomains: true,
            preload: true
          }
        ] ->
          :success

        [
          %{
            max_age: _
          }
        ] ->
          :warning

        [_ | _] ->
          :failure
      end

    {:ok, status}
  end

  defp check_hsts(url) do
    case HttpUtils.get_follow_redirect(url) do
      {:ok, %SslMoon.Http.HttpClient.Response{} = response} ->
        get_strict_transport_security_from_header(response)

      {:error, _reason} ->
        []
    end
  end

  defp get_strict_transport_security_from_header(response) do
    response.headers
    |> Enum.filter(fn {key, _value} -> String.match?(key, ~r/\AStrict-Transport-Security\z/i) end)
    |> Enum.map(fn {_key, value} -> parse_hsts(value, response.request_url) end)
  end

  defp parse_hsts(hsts, request_url) do
    hsts
    |> String.split(";", trim: true)
    |> Enum.reduce(%{}, fn part, acc ->
      parse_part(part, acc)
    end)
    |> Map.put(:location, request_url)
  end

  defp parse_part(part, acc) do
    part = String.trim(part)

    case String.split(part, "=", trim: true) do
      [key, value] ->
        key = String.trim(key)
        key_atom = String.replace(key, "-", "_") |> String.to_atom()
        Map.put(acc, key_atom, value)

      [flag] ->
        flag = String.trim(flag)
        flag_atom = String.replace(flag, "-", "_") |> String.to_atom()

        if flag_atom == :max_age do
          Map.put(acc, flag_atom, nil)
        else
          Map.put(acc, flag_atom, true)
        end
    end
  end
end
