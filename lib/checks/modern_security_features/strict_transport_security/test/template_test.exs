defmodule SslMoon.Checks.StrictTransportSecurityTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.StrictTransportSecurity.Executor
  @moduletag template: SslMoon.Checks.StrictTransportSecurity.Template
  @moduletag validation_schema: SslMoon.Checks.StrictTransportSecurity.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        hsts: [
          %{
            max_age: 63_072_000,
            includeSubDomains: true,
            preload: true,
            location: "https://example.com"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render warning template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :warning, %{
        hsts: [
          %{
            max_age: 63_072_000,
            includeSubDomains: false,
            preload: false,
            location: "https://example.com"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_warning"|
  end

  test "render failure template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        hsts: [
          %{
            max_age: nil,
            includeSubDomains: false,
            preload: false,
            location: "https://example.com"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry = create_response(check, :inactive, %{hsts: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :not_found, %{hsts: []})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
