defmodule SslMoon.Checks.StrictTransportSecurityTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks
  import SslMoon.Checks.StrictTransportSecurity.Factory

  @moduletag executor: SslMoon.Checks.StrictTransportSecurity.Executor
  @moduletag template: SslMoon.Checks.StrictTransportSecurity.Template
  @moduletag validation_schema: SslMoon.Checks.StrictTransportSecurity.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "success status on present header with correct format", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      resp = build(:http_response)
      sts_header = {"Strict-Transport-Security", "max-age=15552000; includeSubDomains; preload"}

      resp =
        Map.put(resp, :headers, [sts_header | resp.headers])
        |> Map.put(:request_url, "test.com")

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)

    assert resp.status == :success

    assert resp.data == %{
             hsts: [
               %{
                 location: "test.com",
                 preload: true,
                 max_age: "15552000",
                 includeSubDomains: true
               }
             ]
           }

    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "inactive status when the header is not present", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      resp = build(:http_response)
      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :inactive

    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when the header content is invalid", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      resp = build(:http_response)
      sts_header = {"strict-transport-security", "max-age=;"}

      resp =
        Map.put(resp, :headers, [sts_header | resp.headers])
        |> Map.put(:request_url, "test.com")

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :failure

    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end

  test "failure status when the headers are duplicated", %{
    check: check,
    host: host,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, ^host, _, _, _ ->
      resp = build(:http_response)

      duplicated_headers = [
        {"strict-transport-security", "max-age=15552000; preload"},
        {"Strict-Transport-Security", "max-age=15552000; preload"}
      ]

      resp =
        Map.put(resp, :headers, duplicated_headers)
        |> Map.put(:request_url, "test.com")

      {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, host)
    assert resp.status == :failure

    assert {:ok, _res} = create_check(check, resp, checks_group.id, host)
  end
end
