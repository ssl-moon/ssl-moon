defmodule SslMoon.Checks.TemplateHelpers do
  @moduledoc """
    Helper templates to render default messages about checks.
  """
  import Phoenix.Component
  import SslMoonWeb.Gettext

  def render_generic_inactive(assigns) do
    ~H"""
    <div test_tag="check_inactive" class="alert alert-success">
      <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
      <span><%= gettext("Currently, this feature is not active for this host") %></span>
    </div>
    """
  end

  def render_generic_no_check(assigns) do
    ~H"""
    <div test_tag="check_not_found" class="alert alert-info">
      <MaterialIcons.info style="outlined" size={42} class="fill-blue-600" />
      <span><%= gettext("Currently, there is no check for this domain.") %></span>
    </div>
    """
  end

  def render_generic_failure(assigns) do
    ~H"""
    <div test_tag="check_failure" class="alert alert-error">
      <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
      <span><%= gettext("Check failed") %></span>
    </div>
    """
  end
end
