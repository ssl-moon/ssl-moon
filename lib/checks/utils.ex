defmodule SslMoon.Checks.Utils do
  @moduledoc """
  Utils module.
  """

  require Logger

  def find_header_value(headers, key) do
    headers
    |> Enum.find_value(fn {k, v} -> String.match?(key, ~r/\A#{k}\z/i) && v end)
  end

  def ssl_connect(url) do
    :ssl.connect(to_charlist(url), 443, [verify: :verify_none], 5000)
  end

  def ssl_connect(domain, port, options) do
    :ssl.connect(to_charlist(domain), port, options, 5000)
  end

  def ssl_peercert(socket) do
    :ssl.peercert(socket)
  end
end
