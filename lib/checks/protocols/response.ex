defmodule SslMoon.Checks.Protocols.Response do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field :type, Ecto.Enum, values: [:redirect, :response]
    field :headers, :map
    field :redirect_url, :string
    field :status_code, :integer
    field :request_url, :string
  end

  def changeset(response, attrs) do
    response
    |> cast(attrs, [:type, :headers, :redirect_url, :status_code, :request_url])
  end
end
