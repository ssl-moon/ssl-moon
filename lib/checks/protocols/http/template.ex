defmodule SslMoon.Checks.HttpCheck.Template do
  @moduledoc """
  The `HttpComponent` module provides functions to render the checks for Http.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers
  alias SslMoon.Checks.Protocols.Utils

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <%= for request <- @check_data.check_result.http do %>
        <div class="card glass p-3">
          <div class="p-3">
            <p class="flex items-center text-lg text-blue-500 ">
              <MaterialIcons.link style="outlined" size={22} /> &nbsp <%= request.request_url %>
            </p>
            <span class="text-sm text-green-700 font-semibold">
              <%= gettext("STATUS") %> <%= request.status_code %> <%= Utils.show_status_code(
                request.status_code
              ) %>
            </span>
          </div>
          <div class="overflow-x-auto">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th><%= gettext("Header") %></th>
                  <th><%= gettext("Value") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for {key, value} <- request.headers do %>
                  <tr>
                    <th class="w-80"><%= key %></th>
                    <td>
                      <%= value %>
                    </td>
                    <td></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </div>
        <br />
      <% end %>
      <p class="p-3">
        <b>
          <%= gettext("HTTP is properly configured") %>
        </b>
        <br /> <br />
        <%= gettext(
          "Proper configuration of HTTP involves setting up web servers and security measures, such as HTTPS, to ensure secure data transmission. It includes optimizing content delivery through compression and efficient caching mechanisms. Redirection rules and error handling are established to enhance user experience and navigate errors gracefully. Resource optimization techniques are applied to images and scripts for faster loading times. Access control, monitoring, and compliance with standards like OWASP Top Ten are also key aspects of a well-configured HTTP setup."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <%= for request <- @check_data.check_result.http do %>
        <div class="card glass p-3">
          <div class="p-3">
            <p class="flex items-center text-lg text-blue-500 ">
              <MaterialIcons.link style="outlined" size={22} /> &nbsp <%= request.request_url %>
            </p>
            <span class="text-sm text-green-700 font-semibold">
              <%= gettext("STATUS") %> <%= request.status_code %> <%= Utils.show_status_code(
                request.status_code
              ) %>
            </span>
          </div>
          <div class="overflow-x-auto">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th><%= gettext("Header") %></th>
                  <th><%= gettext("Value") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for {key, value} <- request.headers do %>
                  <tr>
                    <th class="w-80"><%= key %></th>
                    <td>
                      <%= value %>
                    </td>
                    <td></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </div>
        <br />
      <% end %>
      <p class="p-3">
        <b><%= gettext("HTTP is not properly configured") %></b> <br /> <br />
        <%= gettext(
          "When HTTP is not properly configured, it can lead to a range of issues, including security vulnerabilities, slow website performance, and a poor user experience. Common problems include the absence of HTTPS encryption, misconfigured security headers, and server-related issues. Content delivery may suffer due to a lack of compression and caching, resulting in slow page load times. Improper URL redirection and error handling can frustrate users. Inadequate resource optimization and failure to implement access controls can also impact website functionality and security. Monitoring gaps and non-compliance with standards may leave the website vulnerable to attacks and operational problems."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    gettext(
      "HTTP, or Hypertext Transfer Protocol, is the underlying protocol used for communication between web browsers and servers on the internet. It enables the transfer of hypertext, such as HTML pages, images, and other resources, across the World Wide Web."
    )
  end
end
