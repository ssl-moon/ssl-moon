defmodule SslMoon.Checks.HttpTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks.Http.Factory
  import SslMoon.Checks

  @moduletag executor: SslMoon.Checks.HttpCheck.Executor
  @moduletag template: SslMoon.Checks.HttpCheck.Template
  @moduletag validation_schema: SslMoon.Checks.HttpCheck.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "success status on hops from http to https", %{check: check, checks_group: checks_group} do
    SslMoon.Http.MockHttpClient
    |> expect(:request, 3, fn
      :get, "http://test.com", _, _, _ ->
        resp = build(:http_redirect)

        redirect_url = "https://test.com"
        resp = Map.put(resp, :redirect_url, redirect_url)

        {:ok, resp}

      :get, "https://test.com", _, _, _ ->
        resp = build(:http_redirect)

        redirect_url = "https://www.test.com"
        resp = Map.put(resp, :redirect_url, redirect_url)

        {:ok, resp}

      :get, "https://www.test.com", _, _, _ ->
        resp = build(:http_response)

        req_url = "https://www.test.com"
        resp = Map.put(resp, :request_url, req_url)

        {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, "test.com")

    assert resp.status == :success
    assert Enum.count(resp.data.http) == 3
    assert {:ok, _res} = create_check(check, resp, checks_group.id, "test.com")
  end

  test "inactive status when the host is unreachable", %{check: check, checks_group: checks_group} do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, "http://test.com", _, _, _ ->
      {:error, :timeout}
    end)

    {:ok, resp} = execute_check(check, "test.com")

    assert resp.status == :inactive
    assert {:ok, _res} = create_check(check, resp, checks_group.id, "test.com")
  end

  test "failure status when http doesn't get redirected to https", %{
    check: check,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, 2, fn
      :get, "http://test.com", _, _, _ ->
        resp = build(:http_redirect)

        redirect_url = "http://www.test.com"
        resp = Map.put(resp, :redirect_url, redirect_url)

        {:ok, resp}

      :get, "http://www.test.com", _, _, _ ->
        resp = build(:http_response)

        req_url = "http://www.test.com"
        resp = Map.put(resp, :request_url, req_url)

        {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, "test.com")

    assert resp.status == :failure
    assert {:ok, _res} = create_check(check, resp, checks_group.id, "test.com")
  end
end
