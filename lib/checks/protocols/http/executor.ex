defmodule SslMoon.Checks.HttpCheck.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.HttpCheck.Executor` module is part of the SslMoon security suite, focusing on validating the HTTP configuration of a given URL. This module assesses whether URLs are properly redirected to their secure HTTPS counterparts, an essential aspect of modern web security.

    This module implements two key functions, `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: Initiates an HTTP check for the specified URL. It converts the URL to its HTTP form, follows any redirects, and maps the HTTP responses to a standardized schema. The function returns a map with the HTTP response details.

    - `qualify_check/3`: Evaluates the final HTTP response from `perform_check/2`. If the final destination URL starts with "https://", the check is marked as `:success`, indicating proper redirection to HTTPS. Otherwise, the check results in `:failure`. If no response is received, the status is marked as `:inactive`.

  ## Usage

    To use this module, define a check for HTTP and specify a target URL:

    ```elixir
    # Define the check and the URL
    check = "http"
    url = "example.com"

    # Perform the HTTP check
    {:ok, check_result} = SslMoon.Checks.HttpCheck.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.HttpCheck.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :failure, or :inactive)
    ```
    This module is crucial for ensuring that web applications are enforcing HTTPS, thereby enhancing security by ensuring encrypted communications.

  ## Current Status and Limitations

    Note: This module currently needs improvement for more robust and comprehensive HTTP checks. Users should be aware of potential limitations in its current implementation.

  ## Completion Status

    The current state of the module reflects an initial operational phase, with room for enhancements to achieve more robust and comprehensive HTTP checks. Recognizing the potential limitations in its present form, future development efforts will be directed towards strengthening the module's capabilities. This includes expanding the scope of HTTP checks, enhancing accuracy, and ensuring compatibility with a wider range of HTTP scenarios and configurations. The goal is to evolve this module into a more versatile and reliable tool, aligning with the latest advancements in HTTP protocols and web security practices. Continued refinement and updates are planned, focusing on addressing the known issues and incorporating user feedback for improvements.
  """
  require Logger
  alias SslMoon.Http.HttpUtils
  alias SslMoon.Checks.Protocols.Utils

  @impl true
  def perform_check(_check, url) do
    resp =
      case HttpUtils.get_follow_redirect("http://#{url}", true) do
        {:ok, resp} ->
          Enum.map(resp, fn el -> Utils.http_resp_to_schema(el) end)

        _other ->
          []
      end

    {:ok, %{http: resp}}
  end

  @impl true
  def qualify_check(_check, _url, %{http: data}) do
    status =
      case data do
        # The link is not valid
        [] ->
          :inactive

        [resp | _] ->
          if String.starts_with?(resp.request_url, "https://") do
            :success
          else
            :failure
          end
      end

    {:ok, status}
  end
end
