defmodule SslMoon.Checks.HttpCheck.Schema do
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  alias SslMoon.Checks.Protocols.Response

  @moduledoc """
    Http schema.
  """

  @primary_key false

  embedded_schema do
    embeds_many(:http, Response, on_replace: :delete)
  end

  @impl true
  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [])
    |> cast_embed(:http, with: &Response.changeset/2)
  end
end
