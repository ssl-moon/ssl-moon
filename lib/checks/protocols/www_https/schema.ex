defmodule SslMoon.Checks.WwwHttpsCheck.Schema do
  @moduledoc """
    www_https schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  alias SslMoon.Checks.Protocols.Response

  @primary_key false
  embedded_schema do
    embeds_many(:www_https, Response, on_replace: :delete)
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [])
    |> cast_embed(:www_https, with: &Response.changeset/2)
  end
end
