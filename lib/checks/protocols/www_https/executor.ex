defmodule SslMoon.Checks.WwwHttpsCheck.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.WwwHttpsCheck.Executor` module is an essential part of the SslMoon security suite, specifically designed to evaluate the HTTPS configuration of URLs with a 'www' prefix. This check is crucial for ensuring that web traffic, especially when directed to 'www' prefixed URLs, is securely managed and properly encrypted through HTTPS.

    This module incorporates two key functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: Initiates an HTTPS check for URLs prefixed with 'www'. The function makes an HTTPS request to the provided URL, follows any redirects, and documents the HTTP responses in a standardized format. It returns a map containing these response details.

    - `qualify_check/3`: Analyzes the final HTTPS response from `perform_check/2`. The check is marked as `:success` if the final URL maintains the "https://" prefix, indicating a secure connection. If the final URL does not maintain "https://", the check is marked as `:failure`. The status is `:inactive` if no response is received.

  ## Usage

    To use this module, define a check for 'www' prefixed HTTPS URLs and specify a target URL:

    ```elixir
    # Define the check and the URL
    check = "www_https"
    url = "example.com"

    # Perform the 'www' prefixed HTTPS check
    {:ok, check_result} = SslMoon.Checks.WwwHttpsCheck.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.WwwHttpsCheck.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :failure, or :inactive)
    ```
    Ensuring secure HTTPS configurations for 'www' prefixed URLs is vital for maintaining the security and integrity of web communications.

  ## Completion Status

    The module is currently operational and effectively assesses the implementation of WWW HTTPS for web applications, a critical factor in safeguarding data integrity and confidentiality. At this stage, the module successfully identifies whether a web application is using WWW HTTPS and ensures that data transmission between the user's browser and the site is secure. However, there is always potential for further development. Future plans include enhancing the module's ability to detect more nuanced aspects of WWW HTTPS configurations and ensuring it stays up-to-date with the latest security protocols and encryption standards. Regular updates and refinements are intended to maintain the module's effectiveness in a rapidly evolving digital landscape, ensuring it remains a reliable tool for assessing WWW HTTPS security.
  """
  require Logger
  alias SslMoon.Checks.Protocols.Utils
  alias SslMoon.Http.HttpUtils

  @impl true
  @spec perform_check(any(), any()) :: {:ok, %{www_https: list()}}
  def perform_check(_check, url) do
    resp =
      case HttpUtils.get_follow_redirect("https://www.#{url}", true) do
        {:ok, resp} -> Enum.map(resp, fn el -> Utils.http_resp_to_schema(el) end)
        {:error, _reason} -> []
      end

    {:ok, %{www_https: resp}}
  end

  @impl true
  def qualify_check(_check, _url, %{www_https: data}) do
    status =
      case data do
        [] ->
          :inactive

        [resp | _] ->
          if String.starts_with?(resp.request_url, "https://") do
            :success
          else
            :failure
          end
      end

    {:ok, status}
  end
end
