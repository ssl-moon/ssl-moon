defmodule SslMoon.Checks.WwwHttpsCheck.Template do
  @moduledoc """
  The `WwwHttpsComponent` module provides functions to render the checks for WwwHttps.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers
  alias SslMoon.Checks.Protocols.Utils

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <%= for request <- @check_data.check_result.www_https do %>
        <div class="card glass p-3">
          <div class="p-3">
            <p class="flex items-center text-lg text-blue-500 ">
              <MaterialIcons.link style="outlined" size={22} /> &nbsp <%= request.request_url %>
            </p>
            <span class="text-sm text-green-700 font-semibold">
              <%= gettext("STATUS") %> <%= request.status_code %> <%= Utils.show_status_code(
                request.status_code
              ) %>
            </span>
          </div>
          <div class="overflow-x-auto">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th><%= gettext("Header") %></th>
                  <th><%= gettext("Value") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for {key, value} <- request.headers do %>
                  <tr>
                    <th class="w-80"><%= key %></th>
                    <td>
                      <%= value %>
                    </td>
                    <td></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </div>
        <br />
      <% end %>
      <p class="p-3">
        <b><%= gettext("WWW HTTPS is properly configured") %></b> <br /> <br />
        <%= gettext(
          "Properly configuring WWW and HTTPS for a website means setting up the website's settings to ensure secure access and data transmission. It includes enabling HTTPS encryption with a valid SSL/TLS certificate, implementing security measures like security headers, considering SEO practices, and optionally configuring redirection for consistency. This configuration enhances security, user trust, and website performance while providing a seamless browsing experience."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <%= for request <- @check_data.check_result.www_https do %>
        <div class="card glass p-3">
          <div class="p-3">
            <p class="flex items-center text-lg text-blue-500 ">
              <MaterialIcons.link style="outlined" size={22} /> &nbsp <%= request.request_url %>
            </p>
            <span class="text-sm text-green-700 font-semibold">
              <%= gettext("STATUS") %> <%= request.status_code %> <%= Utils.show_status_code(
                request.status_code
              ) %>
            </span>
          </div>
          <div class="overflow-x-auto">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th><%= gettext("Header") %></th>
                  <th><%= gettext("Value") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for {key, value} <- request.headers do %>
                  <tr>
                    <th class="w-80"><%= key %></th>
                    <td>
                      <%= value %>
                    </td>
                    <td></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </div>
        <br />
      <% end %>
      <p class="p-3">
        <b><%= gettext("WWW HTTPS is not properly configured") %></b> <br /> <br />
        <%= gettext(
          "Improper configuration of WWW and HTTPS for a website can result in inconsistent access, security risks, SSL/TLS certificate problems, SEO challenges, user confusion, and maintenance difficulties. It may lead to a fragmented user experience, potential data breaches, and SEO issues. To mitigate these problems, it's essential to configure HTTPS correctly with a valid SSL/TLS certificate and consider whether to implement redirects between URL formats for consistency and security."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    "#{gettext("WWW in a URL stands for \"World Wide Web.\" It's a traditional prefix indicating the resource is part of the web.")} #{gettext("HTTPS is an extension of HTTP that ensures secure data transfer over the web using encryption protocols like TLS or SSL. It's vital for websites handling sensitive information, signified by a padlock icon in the browser's address bar.")}"
  end
end
