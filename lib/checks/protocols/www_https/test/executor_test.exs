defmodule SslMoon.Checks.WwwHttpsTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks
  import SslMoon.Checks.Http.Factory

  @moduletag executor: SslMoon.Checks.WwwHttpsCheck.Executor
  @moduletag template: SslMoon.Checks.WwwHttpsCheck.Template
  @moduletag validation_schema: SslMoon.Checks.WwwHttpsCheck.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "sucess status on valid response", %{check: check, checks_group: checks_group} do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn
      :get, "https://www.test.com", _, _, _ ->
        resp = build(:http_response)

        req_url = "https://www.test.com"
        resp = Map.put(resp, :request_url, req_url)

        {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, "test.com")
    assert resp.status == :success
    assert {:ok, _res} = create_check(check, resp, checks_group.id, "test.com")
  end

  test "inactive status when the host is not reachable", %{
    check: check,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, "https://www.test.com", _, _, _ ->
      {:error, :timeout}
    end)

    {:ok, resp} = execute_check(check, "test.com")
    assert resp.status == :inactive
    assert {:ok, _res} = create_check(check, resp, checks_group.id, "test.com")
  end
end
