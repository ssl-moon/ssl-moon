defmodule SslMoon.Checks.Protocols.Utils do
  @moduledoc """
    Utils for protocols
  """
  require Logger

  alias SslMoon.Http.HttpClient

  def http_resp_to_schema(resp) do
    %{
      type: get_type(resp),
      headers: Enum.into(resp.headers, %{}),
      redirect_url: get_redirect_url(resp),
      status_code: resp.status_code,
      request_url: resp.request_url
    }
  end

  defp get_type(%HttpClient.MaybeRedirect{}), do: :redirect
  defp get_type(%HttpClient.Response{}), do: :response

  defp get_redirect_url(%HttpClient.MaybeRedirect{} = resp), do: resp.redirect_url
  defp get_redirect_url(%HttpClient.Response{}), do: nil

  @doc """
    Executes request to the url, in case of redirects it chains the redirects until status 3xx is no longer returned.

    Returns list of all redirects and responses.
    Short circuits and returns early in case a http error is encountered.
  """
  def hop_redirect(url) do
    hop_redirect(url, [])
  end

  defp hop_redirect(url, acc) do
    case HttpClient.request(:get, url, "", [], ssl: [verify: :verify_none]) do
      {:ok, %HttpClient.MaybeRedirect{} = redirect} ->
        hop_redirect(redirect.redirect_url, [redirect | acc])

      {:ok, %HttpClient.Response{} = response} ->
        Enum.reverse([response | acc])

      {:error, _reason} ->
        Enum.reverse(acc)
    end
  end

  def check_http_url(url, list) do
    case HTTPoison.get(url, [], ssl: [verify: :verify_none]) do
      {:ok, content} ->
        handle_http_response(content, url, list)

      {:error, _reason} ->
        list
    end
  end

  defp handle_http_response(response, url, list) do
    case response.status_code do
      200 ->
        list ++
          [%{"url" => url, "status_code" => 200, "headers" => Enum.into(response.headers, %{})}]

      status when status >= 300 and status < 400 ->
        handle__http_redirect(
          response.headers,
          url,
          list ++
            [
              %{
                "url" => url,
                "status_code" => status,
                "headers" => Enum.into(response.headers, %{})
              }
            ]
        )

      _ ->
        list ++
          [
            %{
              "url" => url,
              "status_code" => response.status_code,
              "headers" => Enum.into(response.headers, %{})
            }
          ]
    end
  end

  defp handle__http_redirect(headers, url, list) do
    case find_header_value(headers, "Location") do
      nil ->
        list

      location ->
        location
        |> check_http_location(url)
        |> check_http_url(list)
    end
  end

  defp check_http_location(location, url) do
    case String.starts_with?(location, "/") do
      false -> location
      true -> "#{url}#{location}"
    end
  end

  defp find_header_value(headers, key) do
    headers
    |> Enum.find_value(fn {k, v} -> String.match?(key, ~r/\A#{k}\z/i) && v end)
  end

  def show_status_code(status) when is_integer(status) do
    codes = %{
      100 => "Continue",
      101 => "Switching Protocols",
      102 => "Processing",
      200 => "OK",
      201 => "Created",
      202 => "Accepted",
      203 => "Non-Authoritative Information",
      204 => "No Content",
      205 => "Reset Content",
      206 => "Partial Content",
      207 => "Multi-Status",
      208 => "Already Reported",
      226 => "IM Used",
      300 => "Multiple Choices",
      301 => "Moved Permanently",
      302 => "Found",
      303 => "See Other",
      304 => "Not Modified",
      305 => "Use Proxy",
      307 => "Temporary Redirect",
      308 => "Permanent Redirect",
      400 => "Bad Request",
      401 => "Unauthorized",
      402 => "Payment Required",
      403 => "Forbidden",
      404 => "Not Found",
      405 => "Method Not Allowed",
      406 => "Not Acceptable",
      407 => "Proxy Authentication Required",
      408 => "Request Timeout",
      409 => "Conflict",
      410 => "Gone",
      411 => "Length Required",
      412 => "Precondition Failed",
      413 => "Payload Too Large",
      414 => "URI Too Long",
      415 => "Unsupported Media Type",
      416 => "Range Not Satisfiable",
      417 => "Expectation Failed",
      418 => "I'm a teapot",
      421 => "Misdirected Request",
      422 => "Unprocessable Entity",
      423 => "Locked",
      424 => "Failed Dependency",
      426 => "Upgrade Required",
      428 => "Precondition Required",
      429 => "Too Many Requests",
      431 => "Request Header Fields Too Large",
      451 => "Unavailable For Legal Reasons",
      500 => "Internal Server Error",
      501 => "Not Implemented",
      502 => "Bad Gateway",
      503 => "Service Unavailable",
      504 => "Gateway Timeout",
      505 => "HTTP Version Not Supported",
      506 => "Variant Also Negotiates",
      507 => "Insufficient Storage",
      508 => "Loop Detected",
      510 => "Not Extended",
      511 => "Network Authentication Required"
    }

    Map.get(codes, status, "Unknown status code: #{status}")
  end
end
