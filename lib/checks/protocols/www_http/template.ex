defmodule SslMoon.Checks.WwwHttpCheck.Template do
  @moduledoc """
  The `WwwHttpComponent` module provides functions to render the checks for WwwHttp.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers
  alias SslMoon.Checks.Protocols.Utils

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <%= for request <-  @check_data.check_result.www_http do %>
        <div class="card glass p-3">
          <div class="p-3">
            <p class="flex items-center text-lg text-blue-500 ">
              <MaterialIcons.link style="outlined" size={22} /> &nbsp <%= request.request_url %>
            </p>
            <span class="text-sm text-green-700 font-semibold">
              <%= gettext("STATUS") %> <%= request.status_code %> <%= Utils.show_status_code(
                request.status_code
              ) %>
            </span>
          </div>
          <div class="overflow-x-auto">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th><%= gettext("Header") %></th>
                  <th><%= gettext("Value") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for {key, value} <- request.headers do %>
                  <tr>
                    <th class="w-80"><%= key %></th>
                    <td>
                      <%= value %>
                    </td>
                    <td></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </div>
        <br />
      <% end %>
      <p class="p-3">
        <b>
          <%= gettext("WWW HTTP is properly configured") %>
        </b>
        <br /> <br />
        <%= gettext(
          "Properly configuring WWW and HTTP for a website involves ensuring that both URL formats (with and without WWW) lead to the same content, that the website loads content efficiently over HTTP, and that appropriate SEO and redirection measures are in place. While security can be enhanced through other means, it's crucial to note that modern web standards strongly recommend the use of HTTPS for secure communication. Proper configuration aims to provide a seamless user experience and maintain the website's accessibility and performance."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <%= for request <- @check_data.check_result.www_http do %>
        <div class="card glass p-3">
          <div class="p-3">
            <p class="flex items-center text-lg text-blue-500 ">
              <MaterialIcons.link style="outlined" size={22} /> &nbsp <%= request.request_url %>
            </p>
            <span class="text-sm text-green-700 font-semibold">
              <%= gettext("STATUS") %> <%= request.status_code %> <%= Utils.show_status_code(
                request.status_code
              ) %>
            </span>
          </div>
          <div class="overflow-x-auto">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th><%= gettext("Header") %></th>
                  <th><%= gettext("Value") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for {key, value} <- request.headers do %>
                  <tr>
                    <th class="w-80"><%= key %></th>
                    <td>
                      <%= value %>
                    </td>
                    <td></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </div>
        <br />
      <% end %>
      <p class="p-3">
        <b><%= gettext("WWW HTTP is not properly configured") %></b> <br /> <br />
        <%= gettext(
          "Improper configuration of WWW and HTTP for a website leads to inconsistent access, content delivery problems, security risks, SEO challenges, user confusion, and maintenance complexities. It can result in a fragmented user experience, potential data breaches, and SEO issues. To mitigate these problems, it's recommended to configure redirects between URL formats or consider using HTTPS for secure communication in conjunction with the preferred URL format."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    "#{gettext("WWW in a URL stands for \"World Wide Web.\" It's a traditional prefix indicating the resource is part of the web.")} #{gettext("HTTP, or Hypertext Transfer Protocol, is the underlying protocol used for communication between web browsers and servers on the internet. It enables the transfer of hypertext, such as HTML pages, images, and other resources, across the World Wide Web.")}"
  end
end
