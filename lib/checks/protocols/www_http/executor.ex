defmodule SslMoon.Checks.WwwHttpCheck.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.WwwHttpCheck.Executor` module is a specialized component of the SslMoon security suite, focused on evaluating the behavior of HTTP requests specifically for URLs prefixed with 'www'. This check is important for understanding how traffic is managed for standard web addresses, ensuring that they are properly redirected to secure HTTPS channels.

    The module includes two primary functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: This function performs a check on the given URL with a 'www' prefix, using HTTP. It follows any redirects and captures the HTTP responses, formatting them into a standardized schema. The function returns a map containing the details of these responses.

    - `qualify_check/3`: Assesses the final response obtained from `perform_check/2`. If the final destination URL starts with "https://", the check is marked as `:success`, indicating proper secure redirection. If not, the check results in `:failure`. An `:inactive` status is assigned if no response is received.

  ## Usage

    To utilize this module, specify a check for 'www' prefixed HTTP URLs and provide a target URL:

    ```elixir
    # Define the check and the URL
    check = "www_http"
    url = "example.com"

    # Perform the www-prefixed HTTP check
    {:ok, check_result} = SslMoon.Checks.WwwHttpCheck.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.WwwHttpCheck.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :failure, or :inactive)
    ```
    This module is crucial for ensuring that 'www' prefixed URLs are correctly redirected to HTTPS, enhancing the overall security of the web application.

  ## Completion Status

    The module is currently operational and effectively assesses the implementation of WWW HTTPS for web applications, a critical factor in safeguarding data integrity and confidentiality. At this stage, the module successfully identifies whether a web application is using WWW HTTPS and ensures that data transmission between the user's browser and the site is secure. However, there is always potential for further development. Future plans include enhancing the module's ability to detect more nuanced aspects of WWW HTTPS configurations and ensuring it stays up-to-date with the latest security protocols and encryption standards. Regular updates and refinements are intended to maintain the module's effectiveness in a rapidly evolving digital landscape, ensuring it remains a reliable tool for assessing WWW HTTPS security.
  """
  require Logger
  alias SslMoon.Checks.Protocols.Utils
  alias SslMoon.Http.HttpUtils

  @impl true
  def perform_check(_check, url) do
    resp =
      case HttpUtils.get_follow_redirect("http://www.#{url}", true) do
        {:ok, resp} -> Enum.map(resp, fn el -> Utils.http_resp_to_schema(el) end)
        {:error, _reason} -> []
      end

    {:ok, %{www_http: resp}}
  end

  @impl true
  def qualify_check(_check, _url, %{www_http: data}) do
    status =
      case data do
        [] ->
          :inactive

        [resp | _] ->
          if String.starts_with?(resp.request_url, "https://") do
            :success
          else
            :failure
          end
      end

    {:ok, status}
  end
end
