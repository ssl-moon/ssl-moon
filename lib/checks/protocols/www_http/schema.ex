defmodule SslMoon.Checks.WwwHttpCheck.Schema do
  @moduledoc """
    www_http schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  alias SslMoon.Checks.Protocols.Response

  @primary_key false
  embedded_schema do
    embeds_many(:www_http, Response, on_replace: :delete)
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [])
    |> cast_embed(:www_http, with: &Response.changeset/2)
  end
end
