defmodule SslMoon.Checks.WwwHttpTest.TemplateTest do
  use SslMoon.CheckCase
  use SslMoonWeb.ConnCase

  import Phoenix.LiveViewTest
  import SslMoon.Checks.ExecutorTest

  @moduletag executor: SslMoon.Checks.WwwHttpCheck.Executor
  @moduletag template: SslMoon.Checks.WwwHttpCheck.Template
  @moduletag validation_schema: SslMoon.Checks.WwwHttpCheck.Schema

  test "render success template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :success, %{
        www_http: [
          %{
            id: nil,
            type: :response,
            headers: %{
              "Connection" => "Keep-Alive",
              "Content-Length" => "0",
              "Location" => "https://example.md/",
              "Server" => "BigIP"
            },
            redirect_url: nil,
            status_code: 302,
            request_url: "http://www.example.md"
          },
          %{
            id: nil,
            type: :response,
            headers: %{
              "Cache-Control" => "no-cache, private",
              "Content-Type" => "text/html; charset=UTF-8",
              "Date" => "Thu, 23 Nov 2023 17:30:19 GMT",
              "Set-Cookie" =>
                "TS015ad914=01c9cd733064656e15bd8e36359c03b4cb403c6e06d389d4b9e43edeb2173a5b9901a4d97ce972bd767c810d2b7ab95ceb34655a7074e2f36fb780cda3d8728b002fed53f5884885f88946596e0d25fccc268dc1f3; Path=/; Secure; HTTPOnly",
              "Transfer-Encoding" => "chunked"
            },
            redirect_url: nil,
            status_code: 200,
            request_url: "https://example.md"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|<div test_tag="check_success">|
  end

  test "render failure template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :failure, %{
        www_http: [
          %{
            id: nil,
            type: :response,
            headers: %{
              "Connection" => "Keep-Alive",
              "Content-Length" => "0",
              "Location" => "http://example.md/",
              "Server" => "BigIP"
            },
            redirect_url: nil,
            status_code: 302,
            request_url: "http://www.example.md"
          },
          %{
            id: nil,
            type: :response,
            headers: %{
              "Cache-Control" => "no-cache, private",
              "Content-Type" => "text/html; charset=UTF-8",
              "Date" => "Thu, 23 Nov 2023 17:30:19 GMT",
              "Set-Cookie" =>
                "TS015ad914=01c9cd733064656e15bd8e36359c03b4cb403c6e06d389d4b9e43edeb2173a5b9901a4d97ce972bd767c810d2b7ab95ceb34655a7074e2f36fb780cda3d8728b002fed53f5884885f88946596e0d25fccc268dc1f3; Path=/; Secure; HTTPOnly",
              "Transfer-Encoding" => "chunked"
            },
            redirect_url: nil,
            status_code: 200,
            request_url: "http://example.md"
          }
        ]
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_failure"|
  end

  test "render inactive template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :inactive, %{
        www_http: []
      })

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_inactive"|
  end

  test "render not found template", %{
    check: check,
    host: host
  } do
    check_entry =
      create_response(check, :not_found, %{})

    assert render_component(check.template.module, %{
             id: check.name,
             check: check,
             domain_name: host,
             check_data: check_entry
           }) =~
             ~s|test_tag="check_not_found"|
  end
end
