defmodule SslMoon.Checks.HttpsCheck.Template do
  @moduledoc """
  The `HttpsComponent` module provides functions to render the checks for Https.
  """
  use SslMoon.Checks.Template

  alias SslMoon.Checks.TemplateHelpers
  alias SslMoon.Checks.Protocols.Utils

  @impl true
  def render(assigns) do
    case(assigns.check_data.status) do
      :success -> render_success(assigns)
      :failure -> render_failure(assigns)
      :inactive -> TemplateHelpers.render_generic_inactive(assigns)
      :not_found -> TemplateHelpers.render_generic_no_check(assigns)
    end
  end

  defp render_success(assigns) do
    ~H"""
    <div test_tag="check_success">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <br />
      <div class="alert alert-success">
        <MaterialIcons.check_circle style="outlined" size={42} class="fill-green-600" />
        <span><%= gettext("Everything is ok") %></span>
      </div>
      <br />
      <%= for request <- @check_data.check_result.https do %>
        <div class="card glass p-3">
          <div class="p-3">
            <p class="flex items-center text-lg text-blue-500 ">
              <MaterialIcons.link style="outlined" size={22} /> &nbsp <%= request.request_url %>
            </p>
            <span class="text-sm text-green-700 font-semibold">
              <%= gettext("STATUS") %> <%= request.status_code %> <%= Utils.show_status_code(
                request.status_code
              ) %>
            </span>
          </div>
          <div class="overflow-x-auto">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th><%= gettext("Header") %></th>
                  <th><%= gettext("Value") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for {key, value} <- request.headers do %>
                  <tr>
                    <th class="w-80"><%= key %></th>
                    <td>
                      <%= value %>
                    </td>
                    <td></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </div>
        <br />
      <% end %>
      <p class="p-3">
        <b><%= gettext("HTTPS is properly configured") %></b> <br /> <br />
        <%= gettext(
          "When HTTPS is set up correctly, it ensures secure communication between a website and its visitors. This is achieved through the use of a valid SSL/TLS certificate, which encrypts data exchanged between the user's browser and the server. Security headers like Content Security Policy (CSP) and HTTP Strict Transport Security (HSTS) are in place to enhance protection against cyber threats. All resources on the website are loaded securely over HTTPS, preventing mixed content issues. This not only improves security but also boosts the website's SEO ranking and builds trust with users, as indicated by the padlock icon in the browser's address bar."
        ) %>
      </p>
    </div>
    """
  end

  defp render_failure(assigns) do
    ~H"""
    <div test_tag="check_failure">
      <p class="p-3">
        <%= template_description() %>
      </p>
      <div class="alert alert-error">
        <MaterialIcons.error style="outlined" size={42} class="fill-red-600" />
        <span><%= gettext("Check failed") %></span>
      </div>
      <br />
      <%= for request <- @check_data.check_result.https do %>
        <div class="card glass p-3">
          <div class="p-3">
            <p class="flex items-center text-lg text-blue-500 ">
              <MaterialIcons.link style="outlined" size={22} /> &nbsp <%= request.request_url %>
            </p>
            <span class="text-sm text-green-700 font-semibold">
              <%= gettext("STATUS") %> <%= request.status_code %> <%= Utils.show_status_code(
                request.status_code
              ) %>
            </span>
          </div>
          <div class="overflow-x-auto">
            <table class="table table-zebra">
              <thead>
                <tr>
                  <th><%= gettext("Header") %></th>
                  <th><%= gettext("Value") %></th>
                </tr>
              </thead>
              <tbody>
                <%= for {key, value} <- request.headers do %>
                  <tr>
                    <th class="w-80"><%= key %></th>
                    <td>
                      <%= value %>
                    </td>
                    <td></td>
                  </tr>
                <% end %>
              </tbody>
            </table>
          </div>
        </div>
        <br />
      <% end %>
      <p class="p-3">
        <b><%= gettext("HTTPS is not properly configured") %></b> <br /> <br />
        <%= gettext(
          "When HTTPS is not properly configured, data transmission is vulnerable to interception and tampering, as it lacks encryption. Problems can arise from expired or incorrectly configured SSL/TLS certificates, resulting in browser warnings that discourage users. Failure to implement security headers can expose the site to risks like cross-site scripting (XSS). Loading insecure resources over HTTP on a secure page can trigger mixed content warnings and security concerns. Furthermore, websites without HTTPS may suffer in search engine rankings and face user distrust, as the absence of encryption indicators raises suspicions about security."
        ) %>
      </p>
    </div>
    """
  end

  defp template_description do
    gettext(
      "HTTPS is an extension of HTTP that ensures secure data transfer over the web using encryption protocols like TLS or SSL. It's vital for websites handling sensitive information, signified by a padlock icon in the browser's address bar."
    )
  end
end
