defmodule SslMoon.Checks.HttpsCheck.Schema do
  @moduledoc """
    Https schema.
  """
  use SslMoon.Checks.Schema
  import Ecto.Changeset

  alias SslMoon.Checks.Protocols.Response

  @primary_key false

  embedded_schema do
    embeds_many(:https, Response, on_replace: :delete)
  end

  @impl true
  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [])
    |> cast_embed(:https, with: &Response.changeset/2)
  end
end
