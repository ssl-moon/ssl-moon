defmodule SslMoon.Checks.HttpsTest.ExecutorTest do
  use SslMoon.DataCase
  use SslMoon.CheckCase

  import Mox
  import SslMoon.Checks
  import SslMoon.Checks.Http.Factory

  @moduletag executor: SslMoon.Checks.HttpsCheck.Executor
  @moduletag template: SslMoon.Checks.HttpsCheck.Template
  @moduletag validation_schema: SslMoon.Checks.HttpsCheck.Schema

  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  test "success status on hops ending with https", %{check: check, checks_group: checks_group} do
    SslMoon.Http.MockHttpClient
    |> expect(:request, 2, fn
      :get, "https://test.com", _, _, _ ->
        resp = build(:http_redirect)

        redirect_url = "https://www.test.com"
        resp = Map.put(resp, :redirect_url, redirect_url)

        {:ok, resp}

      :get, "https://www.test.com", _, _, _ ->
        resp = build(:http_response)

        req_url = "https://www.test.com"
        resp = Map.put(resp, :request_url, req_url)

        {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, "test.com")

    assert resp.status == :success
    assert Enum.count(resp.data.https) == 2
    assert {:ok, _res} = create_check(check, resp, checks_group.id, "test.com")
  end

  test "inactive status when the host is unreachable", %{check: check, checks_group: checks_group} do
    SslMoon.Http.MockHttpClient
    |> expect(:request, fn :get, "https://test.com", _, _, _ ->
      {:error, :timeout}
    end)

    {:ok, resp} = execute_check(check, "test.com")
    assert resp.status == :inactive
    assert {:ok, _res} = create_check(check, resp, checks_group.id, "test.com")
  end

  test "failure status when https gets redirected to http", %{
    check: check,
    checks_group: checks_group
  } do
    SslMoon.Http.MockHttpClient
    |> expect(:request, 3, fn
      :get, "https://test.com", _, _, _ ->
        resp = build(:http_redirect)

        redirect_url = "https://www.test.com"
        resp = Map.put(resp, :redirect_url, redirect_url)

        {:ok, resp}

      :get, "https://www.test.com", _, _, _ ->
        resp = build(:http_redirect)

        redirect_url = "http://www.test.com"
        resp = Map.put(resp, :redirect_url, redirect_url)

        {:ok, resp}

      :get, "http://www.test.com", _, _, _ ->
        resp = build(:http_response)

        req_url = "http://www.test.com"
        resp = Map.put(resp, :request_url, req_url)

        {:ok, resp}
    end)

    {:ok, resp} = execute_check(check, "test.com")

    assert resp.status == :failure
    assert Enum.count(resp.data.https) == 3
    assert {:ok, _res} = create_check(check, resp, checks_group.id, "test.com")
  end
end
