defmodule SslMoon.Checks.HttpsCheck.Executor do
  use SslMoon.Checks.Executor

  @moduledoc """
    The `SslMoon.Checks.HttpsCheck.Executor` module is an integral part of the SslMoon security suite, designed to verify the proper implementation and functioning of HTTPS on a given URL. HTTPS, being a protocol for secure communication over a computer network, is vital for ensuring data privacy and security on the web.

    This module comprises two primary functions: `perform_check/2` and `qualify_check/3`.

  ## Functions

    - `perform_check/2`: Initiates an HTTPS check for the specified URL. It transforms the URL into its HTTPS version, follows any potential redirects, and captures the HTTP responses, converting them to a standardized schema. The function returns a map containing details of the HTTPS responses.

    - `qualify_check/3`: Assesses the final HTTPS response obtained from `perform_check/2`. The check is considered a `:success` if the final URL starts with "https://", signifying a secure connection. If the final URL does not start with "https://", the check results in `:failure`. An `:inactive` status is returned if no response is received.

  ## Usage

    To use this module, specify a check for HTTPS and provide a target URL:

    ```elixir
    # Define the check and the URL
    check = "https"
    url = "example.com"

    # Perform the HTTPS check
    {:ok, check_result} = SslMoon.Checks.HttpsCheck.Executor.perform_check(check, url)

    # Qualify the check based on the result
    {:ok, status} = SslMoon.Checks.HttpsCheck.Executor.qualify_check(check, url, check_result)

    # `status` will indicate the outcome of the check (:success, :failure, or :inactive)
    ```
    Ensuring the correct implementation of HTTPS is critical for the security of web applications, as it protects the data integrity and confidentiality between the user's browser and the site.

  ## Completion Status

    The module is currently operational and effectively assesses the implementation of HTTPS for web applications, a critical factor in safeguarding data integrity and confidentiality. At this stage, the module successfully identifies whether a web application is using HTTPS and ensures that data transmission between the user's browser and the site is secure. However, there is always potential for further development. Future plans include enhancing the module's ability to detect more nuanced aspects of HTTPS configurations and ensuring it stays up-to-date with the latest security protocols and encryption standards. Regular updates and refinements are intended to maintain the module's effectiveness in a rapidly evolving digital landscape, ensuring it remains a reliable tool for assessing HTTPS security.
  """
  require Logger
  alias SslMoon.Checks.Protocols.Utils
  alias SslMoon.Http.HttpUtils

  @impl true
  def perform_check(_check, url) do
    resp =
      case HttpUtils.get_follow_redirect("https://#{url}", true) do
        {:ok, resp} -> Enum.map(resp, fn el -> Utils.http_resp_to_schema(el) end)
        {:error, _reason} -> []
      end

    {:ok, %{https: resp}}
  end

  @impl true
  def qualify_check(_check, _url, %{https: data}) do
    status =
      case data do
        [] ->
          :inactive

        [resp | _] ->
          if String.starts_with?(resp.request_url, "https://") do
            :success
          else
            :failure
          end
      end

    {:ok, status}
  end
end
