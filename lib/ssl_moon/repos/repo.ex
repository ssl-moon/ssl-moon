defmodule SslMoon.Repo do
  defp db_engine(), do: Application.fetch_env!(:ssl_moon, :db_engine)

  def checks_repo() do
    if db_engine() == "postgres" do
      SslMoon.ChecksRepo.Postgres
    else
      SslMoon.ChecksRepo.Sqlite
    end
  end

  def tasks_repo() do
    if db_engine() == "postgres" do
      SslMoon.TasksRepo.Postgres
    else
      SslMoon.TasksRepo.Sqlite
    end
  end
end
