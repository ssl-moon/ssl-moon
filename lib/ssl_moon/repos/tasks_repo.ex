defmodule SslMoon.TasksRepo.Sqlite do
  @moduledoc """
    Tasks repo with sqlite driver.
  """
  use Ecto.Repo,
    otp_app: :ssl_moon,
    adapter: Ecto.Adapters.SQLite3
end

defmodule SslMoon.TasksRepo.Postgres do
  @moduledoc """
    Tasks repo with postgres driver.
  """
  use Ecto.Repo,
    otp_app: :ssl_moon,
    adapter: Ecto.Adapters.Postgres
end
