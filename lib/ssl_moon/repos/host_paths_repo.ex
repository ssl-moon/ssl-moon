defmodule SslMoon.HostPathsRepo do
  @moduledoc """
  Host paths repo.
  """
  use Ecto.Repo,
    otp_app: :ssl_moon,
    adapter: Ecto.Adapters.SQLite3,
    read_only: true
end
