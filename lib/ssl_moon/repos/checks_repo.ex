defmodule SslMoon.ChecksRepo.Sqlite do
  @moduledoc """
    Checks repo with sqlite driver.
  """
  use Ecto.Repo,
    otp_app: :ssl_moon,
    adapter: Ecto.Adapters.SQLite3
end

defmodule SslMoon.ChecksRepo.Postgres do
  @moduledoc """
    Checks repo with postgres driver.
  """
  use Ecto.Repo,
    otp_app: :ssl_moon,
    adapter: Ecto.Adapters.Postgres
end
