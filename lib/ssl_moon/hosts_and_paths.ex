defmodule SslMoon.HostsAndPaths do
  @moduledoc """
  The `SslMoon.HostsAndPaths` module provides functions to query and interact with hosts and paths.

  ## Features

  1. Retrieving a combined list of hosts and paths based on specified folders.
  2. Recursive querying to get a specific path.
  3. Fetching child paths based on given folders and parent path.
  4. Listing hosts associated with a specific path.

  ## Usage

  To fetch a list of hosts and child paths for specific folders:
  ```elixir
  SslMoon.HostsAndPaths.list_hosts_and_paths(["folder1", "subfolder1"])
  ```
  """

  import Ecto.Query, warn: false
  alias SslMoon.HostPathsRepo

  @doc """
  Fetches a combined list of hosts and child paths for the given folders.

  It performs a recursive search to retrieve the appropriate path, and then
  fetches the associated hosts and child paths.

  ## Parameters

  - `folders`: A list of strings representing the folder names in their hierarchical order.

  ## Returns

  - `{:ok, map}`: On success, returns a tuple with an `:ok` atom and a map containing path details, associated hosts, and child paths.
  - `{:error, :not_found}`: On error, returns a tuple with an `:error` atom and an error message.

  ## Examples

      iex> list_hosts_and_paths(["folder1", "subfolder1"])
      [%Paths{}, ...]

  """
  def list_hosts_and_paths(folders) do
    with {:ok, path} <- get_path(Enum.join(folders, "/")) do
      result =
        path
        |> Map.put(:hosts, list_hosts(path.id))
        |> Map.put(:child_paths, list_child_paths(path.id))

      {:ok, result}
    end
  end

  @doc """
  Performs a recursive query to retrieve a specific path based on the given folder hierarchy.

  ## Parameters

  - `path`: A list of strings representing the folder names in their hierarchical order.

  ## Returns

  - `map`: On success, returns a map containing the path details.
  - `{:error, string}`: On error, returns a tuple with an `:error` atom and an error message.

  ## Examples

      iex> get_path("folder1/subfolder1")
      [%Paths{}, ...]

  """
  def get_path(path) when is_binary(path) do
    initial_query =
      SslMoon.Paths
      |> where([p], is_nil(p.parent_id))
      |> select_merge([p], %{temp_path: p.name})

    recursion_query =
      SslMoon.Paths
      |> join(:inner, [p], cte in "cte", on: p.parent_id == cte.id)
      |> select_merge([p, cte], %{temp_path: fragment("? || '/' || ?", cte.temp_path, p.name)})

    query =
      initial_query
      |> union_all(^recursion_query)

    query =
      {"cte", SslMoon.Paths}
      |> recursive_ctes(true)
      |> with_cte("cte", as: ^query)
      |> select_merge([cte], %{path: fragment("?.temp_path", cte)})
      |> where([cte], fragment("?.temp_path", cte) == ^path)

    case HostPathsRepo.all(query) do
      [] ->
        {:error, :not_found}

      paths ->
        {:ok, hd(paths)}
    end
  end

  def get_path(path_id) when is_number(path_id) do
    initial_query =
      SslMoon.Paths
      |> where([p], is_nil(p.parent_id))
      |> select_merge([p], %{temp_path: p.name})

    recursion_query =
      SslMoon.Paths
      |> join(:inner, [p], cte in "cte", on: p.parent_id == cte.id)
      |> select_merge([p, cte], %{temp_path: fragment("? || '/' || ?", cte.temp_path, p.name)})

    query =
      initial_query
      |> union_all(^recursion_query)

    query =
      {"cte", SslMoon.Paths}
      |> recursive_ctes(true)
      |> with_cte("cte", as: ^query)
      |> select_merge([cte], %{path: fragment("?.temp_path", cte)})
      |> where([cte], cte.id == ^path_id)

    case HostPathsRepo.all(query) do
      [] ->
        {:error, :not_found}

      paths ->
        {:ok, hd(paths)}
    end
  end

  @doc """
  Fetches child paths based on the given folders and parent path.

  ## Parameters

  - `path`: A map containing details of the parent path.

  ## Returns

  - `list`: Returns a list of maps, each representing a child path. An empty list indicates no child paths.

  ## Examples

      iex> list_child_paths(1)
      [%Paths{}, ...]

  """
  def list_child_paths(path_id) do
    SslMoon.Paths
    |> where([p], p.parent_id == ^path_id)
    |> HostPathsRepo.all()
  end

  @doc """
  Lists all hosts.

  ## Examples

    iex> list_hosts()
    [%Hosts{}, ...]
  """
  def list_hosts() do
    SslMoon.Hosts
    |> HostPathsRepo.all()
  end

  @doc """
  Lists hosts associated with a specific path.

  ## Parameters

  - `path_id`: An integer representing the ID of the path for which hosts are to be retrieved.

  ## Returns

  - `list`: Returns a list of maps, each representing a host associated with the given path. An empty list indicates no associated hosts.

  ## Examples

      iex> list_hosts(1)
      [%Hosts{}, ...]

  """
  def list_hosts(path_id) do
    SslMoon.Hosts
    |> where([h], h.path_id == ^path_id)
    |> HostPathsRepo.all()
  end

  def list_hosts_filtered_by_name(name) do
    SslMoon.Hosts
    |> where([h], like(h.domain_name, ^"%#{name}%"))
    |> HostPathsRepo.all()
  end

  def list_paths_filtered_by_name(name) do
    initial_query =
      SslMoon.Paths
      |> where([p], is_nil(p.parent_id))
      |> select([p], %{id: p.id, name: p.name, parent_id: p.parent_id, temp_path: p.name})

    recursion_query =
      SslMoon.Paths
      |> join(:inner, [p], cte in "cte", on: p.parent_id == cte.id)
      |> select([p, cte], %{
        id: p.id,
        name: p.name,
        parent_id: p.parent_id,
        temp_path: fragment("? || '/' || ?", cte.temp_path, p.name)
      })

    combined_query =
      initial_query
      |> union_all(^recursion_query)

    query =
      {"cte", SslMoon.Paths}
      |> recursive_ctes(true)
      |> with_cte("cte", as: ^combined_query)
      |> join(:left, [cte], h in SslMoon.Hosts, on: h.path_id == cte.id)
      |> group_by([cte], [cte.id, cte.temp_path, cte.name, cte.parent_id])
      |> select([cte, h], %{
        id: cte.id,
        name: cte.name,
        parent_id: cte.parent_id,
        path: fragment("?.temp_path", cte),
        host_count: count(h.id)
      })
      |> where([p], like(fragment("lower(?)", p.name), ^"%#{String.downcase(name)}%"))

    HostPathsRepo.all(query)
  end

  @doc """
  Get the host associated with a specific id.

  ## Parameters

  - `id`: An integer representing the ID of the host id.

  ## Returns

  - `host`: Returns a the host,  associated with the given id.

  ## Examples

      iex> get_host_id(1)
      %Hosts{}

  """
  def get_host_id(id) do
    HostPathsRepo.get(SslMoon.Hosts, id)
  end

  def get_host_count() do
    HostPathsRepo.aggregate(SslMoon.Hosts, :count, :id)
  end

  def get_folders_with_host_count() do
    initial_query =
      SslMoon.Paths
      |> where([p], is_nil(p.parent_id))
      |> select([p], %{id: p.id, name: p.name, parent_id: p.parent_id, temp_path: p.name})

    recursion_query =
      SslMoon.Paths
      |> join(:inner, [p], cte in "cte", on: p.parent_id == cte.id)
      |> select([p, cte], %{
        id: p.id,
        name: p.name,
        parent_id: p.parent_id,
        temp_path: fragment("? || '/' || ?", cte.temp_path, p.name)
      })

    combined_query =
      initial_query
      |> union_all(^recursion_query)

    query =
      {"cte", SslMoon.Paths}
      |> recursive_ctes(true)
      |> with_cte("cte", as: ^combined_query)
      |> join(:left, [cte], h in SslMoon.Hosts, on: h.path_id == cte.id)
      |> group_by([cte], [cte.id, cte.temp_path, cte.name, cte.parent_id])
      |> select([cte, h], %{
        id: cte.id,
        name: cte.name,
        parent_id: cte.parent_id,
        path: fragment("?.temp_path", cte),
        host_count: count(h.id)
      })

    HostPathsRepo.all(query)
  end

  def get_domains(path_id, page_size \\ 10, page \\ 1) when page >= 1 do
    offset = (page - 1) * page_size

    SslMoon.Hosts
    |> where([h], h.path_id == ^path_id)
    |> limit(^page_size)
    |> offset(^offset)
    |> HostPathsRepo.all()
  end

  def get_domains_count(path_id) do
    SslMoon.Hosts
    |> where([h], h.path_id == ^path_id)
    |> select([h], count(h.id))
    |> HostPathsRepo.one()
  end

  def to_nested_structure(paths) do
    paths
    |> Enum.filter(fn path -> path.parent_id == nil end)
    |> to_nested_structure(paths)
  end

  defp to_nested_structure([], _all_paths) do
    []
  end

  defp to_nested_structure([path | t], all_paths) do
    # Remove already found paths to avoid circular infinite recurison
    {children, paths_left} = Enum.split_with(all_paths, fn p -> p.parent_id == path.id end)

    [
      Map.put(path, :child_paths, to_nested_structure(children, paths_left))
      | to_nested_structure(t, all_paths)
    ]
  end
end
