defmodule SslMoon.Storage.Checks do
  @moduledoc """
  The Checks context.
  """

  import Ecto.Query, warn: false
  import SslMoon.Repo

  alias SslMoon.Schemas.Check
  alias SslMoon.Schemas.CheckGroup

  @doc """
    Creates a new check.
  """
  @spec create_check(map()) :: {:ok, Check.t()} | {:error, Ecto.Changeset.t()}
  def create_check(attrs \\ %{}) do
    %Check{}
    |> Check.changeset(attrs)
    |> checks_repo().insert()
  end

  @doc """
    Creates a new check, raises in case insertion is not possible.
  """
  @spec create_check!(map()) :: Ecto.Schema.t()
  def create_check!(attrs \\ %{}) do
    %Check{}
    |> Check.changeset(attrs)
    |> checks_repo().insert!()
  end

  @doc """
    Gets the latest check for the specific domain, raises if no entry was found.
  """
  @spec get_latest_check!(String.t(), String.t()) :: Ecto.Schema.t()
  def get_latest_check!(domain_name, check_name) do
    query = last_checks_group_query(domain_name)

    case checks_repo().one(query) do
      nil ->
        nil

      domain_checks ->
        domain_checks.checks
        |> Enum.find(fn check ->
          check.check_name == check_name
        end)
    end
  end

  @doc """
    Gets the latest check for the specific domain, returns nil if no entry was found.
  """
  @spec get_latest_check(String.t(), String.t()) :: Ecto.Schema.t() | term() | nil
  def get_latest_check(domain_name, check_name) do
    query = last_checks_group_query(domain_name)

    case checks_repo().one(query) do
      nil ->
        nil

      domain_checks ->
        domain_checks.checks
        |> Enum.find(fn check ->
          check.check_name == check_name
        end)
    end
  end

  @doc """
    Gets checks for the domain grouped by their type.
  """
  @spec get_domain_checks(String.t()) :: [Check.t()]
  def get_domain_checks(domain_name) do
    query = last_checks_group_query(domain_name)

    case checks_repo().one(query) do
      nil -> []
      domain_group -> domain_group.checks
    end
  end

  defp last_checks_group_query(domain_name) do
    CheckGroup
    |> where([c], c.domain_name == ^domain_name)
    |> order_by([c], desc: c.checked_at)
    |> limit(1)
    |> preload(:checks)
  end

  @doc """
    Gets the number of total checks from the database.
  """
  @spec get_checks_count() :: integer()
  def get_checks_count() do
    Check
    |> select([c], count())
    |> checks_repo().one()
  end

  def create_check_group!(url) do
    %CheckGroup{}
    |> CheckGroup.changeset(%{domain_name: url, checked_at: DateTime.utc_now()})
    |> checks_repo().insert!()
  end
end
