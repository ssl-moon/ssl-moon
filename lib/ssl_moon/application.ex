defmodule SslMoon.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  import SslMoon.Repo

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      SslMoonWeb.Telemetry,
      # Start the Ecto repository
      SslMoon.HostPathsRepo,
      checks_repo(),
      tasks_repo(),
      # Start the PubSub system
      {Phoenix.PubSub, name: SslMoon.PubSub},
      # Start Finch
      {Finch, name: SslMoon.Finch},
      # Start the Endpoint (http/https)
      SslMoonWeb.Endpoint,
      # Start oban
      {Oban, Application.fetch_env!(:ssl_moon, Oban)}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SslMoon.Supervisor]

    # :telemetry.attach("oban-errors", [:oban, :job, :exception], &ErrorReporter.handle_event/4, [])

    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    SslMoonWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
