defmodule SslMoon.Checks.Template do
  @moduledoc """
    Module that defines a custom type of live_component used in conjunction with `SslMoon.CheckLiveComponent`.

    All the component functionality is the same as live_component.

    Each template is ensured to have assigns `check_identifier` and `domain_name` in `update/2`.

    `update/2` is defined by default and fetches the last check, the function can be overriden for custom behavior.
    If using the default `update/2` function, the following fields will be present:

    * `status` - Status of the check. Can be either one of [:not_found, :success, :failure, :warning, :inactive]

    * `check` - Content of the check. The format of the data is transformed and managed via `SslMoon.Checks.Migrator`.
  """

  @callback before_render(Phoenix.LiveView.Socket.t()) :: {:ok, Phoenix.LiveView.Socket.t()}

  defmacro __using__(_params) do
    quote do
      use SslMoonWeb, :live_component
      @behaviour unquote(__MODULE__)
      import SslMoon.Checks.TemplateUtils

      # Differentiate between simple live_component and this one
      def __check_live__() do
        __live__()
      end

      @impl true
      def before_render(socket) do
        {:ok, socket}
      end

      @impl true
      def update(%{check: check, domain_name: domain_name, check_data: check_data}, socket) do
        socket =
          assign(socket, check: check, check_data: check_data, domain_name: domain_name)

        before_render(socket)
      end

      defoverridable update: 2, before_render: 1
    end
  end
end
