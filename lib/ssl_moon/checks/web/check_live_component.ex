defmodule SslMoon.CheckLiveComponent do
  @moduledoc false

  @doc """
  A function component for rendering `Phoenix.LiveComponent` within a parent LiveView.

  While `LiveView`s can be nested, each LiveView starts its own process. A `LiveComponent` provides
  similar functionality to `LiveView`, except they run in the same process as the `LiveView`,
  with its own encapsulated state. That's why they are called stateful components.

  ## Attributes

  * `id` (`:string`) (required) - A unique identifier for the LiveComponent. Note the `id` won't
  necessarily be used as the DOM `id`. That is up to the component to decide.

  * `check` (`SslMoon.Checks.Check`) (required) - Check configuration structure.

  * `check_data` (`SslMoon.Schemas.Check`) (required) - The check data struct to be rendered.

  * `domain_name` (`:string`) (required) - Name of the domain.

  Any additional attributes provided will be passed to the LiveComponent as a map of assigns.
  See `Phoenix.LiveComponent` for more information.

  ## Examples

  ```heex
  <.check_live_component module={MyApp.Check} id="check_1" check_identifier="unique_check_identifier" domain_name="test.com" />
  ```
  """
  @doc type: :component
  def check_live_component(assigns) when is_map(assigns) do
    id = assigns[:id]
    check = assigns[:check]
    domain_name = assigns[:domain_name]
    check_data = assigns[:check_data]

    if id == nil do
      raise ArgumentError, ".check_live_component expects id={...} to be given, got: nil"
    end

    if check == nil do
      raise ArgumentError,
            ".check_live_component expects check={...} to be given, got: nil"
    end

    if domain_name == nil do
      raise ArgumentError, ".check_live_component expects domain_name={...} to be given, got: nil"
    end

    if check_data == nil do
      raise ArgumentError, ".check_live_component expects check_data={...} to be given, got: nil"
    end

    module = assigns.check.template.module

    if Keyword.fetch(module.__info__(:functions), :__check_live__) == :error do
      raise ArgumentError, ".check_live_component expects a check live component."
    end

    case module.__check_live__() do
      %{kind: :component} ->
        %Phoenix.LiveView.Component{
          id: id,
          assigns: assigns,
          component: module
        }

      %{kind: kind} ->
        raise ArgumentError, "expected #{inspect(module)} to be a component, but it is a #{kind}"
    end
  end
end
