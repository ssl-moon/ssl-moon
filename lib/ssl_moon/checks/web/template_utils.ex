defmodule SslMoon.Checks.TemplateUtils do
  @moduledoc false
  @type status :: :not_found | :success | :failure | :warning | :inactive

  alias SslMoon.Checks
  alias SslMoon.Storage.Checks, as: ChecksDB

  @doc """
    Looks in `SslMoon.DefaultChecksRouter` for the check with the specified name.

    Raises `ArgumentError` in case the check was not found.
  """

  def get_check!(check_name) do
    check = Checks.get_check_by_name(check_name)

    if check == nil do
      raise ArgumentError,
            "Check not found by identifier. Component #{__MODULE__} with check name #{check_name}"
    end

    check
  end

  @doc """
    Fetches and migrates latest check found.
  """
  @spec fetch_and_migrate_latest(SslMoon.Checks.Check.t(), binary()) :: {status(), any()}
  def fetch_and_migrate_latest(check, domain_name) when is_binary(domain_name) do
    case ChecksDB.get_latest_check(domain_name, check.name) do
      nil ->
        {:not_found, nil}

      result ->
        if check.migrator == nil do
          {result.status, result.check_result}
        else
          {result.status,
           check.migrator.module.migrate(result.version, check.version, result.check_result)}
        end
    end
  end
end
