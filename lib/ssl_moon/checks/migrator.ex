defmodule SslMoon.Checks.Migrator do
  @moduledoc """
    A check migrator contains definition for mapping between different versions of checks.

    All the functions are chained by the version ascending, versions that miss definition are ignored.

    The module is guaranteed to have functions `migrate/1` and `versions/0` exported.

    ## Example

      defmodule ExampleMigrator do
        use SslMoon.Checks.Migrator

        migrator do
          migrate(version: 1, migration_fun: fn data -> Map.put(data, :new_field, :wow) end)
        end
      end
  """

  @callback versions() :: list(pos_integer())
  @callback migrate(pos_integer(), pos_integer(), map()) :: map()

  defmacro __using__(_params) do
    quote do
      @behaviour unquote(__MODULE__)
      import unquote(__MODULE__)

      Module.register_attribute(__MODULE__, :migrations, accumulate: true)

      @after_compile unquote(__MODULE__)
    end
  end

  # Performs check to ensure that migrations were defined
  defmacro __after_compile__(_env, _bytecode) do
    quote do
      functions = __MODULE__.__info__(:functions)

      if !Keyword.has_key?(functions, :migrate) and !Keyword.has_key?(functions, :versions) do
        raise "Missing migrations definitons."
      end
    end
  end

  @doc """
    Defines a migrator that will contain functions for migrating to different versions.
  """
  defmacro migrator(do: block) do
    quote do
      unquote(block)

      migrations = Enum.sort_by(@migrations, fn {version, _} -> version end)

      if Enum.empty?(migrations) or has_duplicates?(migrations) do
        raise "Migration list should not be empty and not contain duplicates."
      end

      versions = Enum.map(migrations, fn {version, _} -> version end)
      Module.put_attribute(__MODULE__, :versions, versions)

      migrations = Enum.into(migrations, %{})
      Module.put_attribute(__MODULE__, :migrations_map, migrations)

      @impl unquote(__MODULE__)
      def versions() do
        @versions
      end

      @impl unquote(__MODULE__)
      def migrate(from_version, to_version, data)
          when from_version >= 0 and from_version == to_version do
        data
      end

      @impl unquote(__MODULE__)
      def migrate(from_version, to_version, data)
          when from_version < to_version and from_version >= 0 do
        Enum.reduce((from_version + 1)..to_version, data, fn version, acc ->
          if @migrations_map[version] != nil do
            apply(__MODULE__, migration_private_function(version), [acc])
          else
            acc
          end
        end)
      end
    end
  end

  @doc """
    Defines a migrate entity. Each entity should contain version and migration function.
  """
  @spec migrate(version: integer(), migration_fun: (map() -> map())) :: no_return()
  defmacro migrate(args) do
    version = Keyword.fetch!(args, :version)
    migration_fun = Keyword.fetch!(args, :migration_fun)

    fun_name = migration_private_function(version)

    quote do
      def unquote(fun_name)(data) do
        unquote(migration_fun).(data)
      end

      Module.put_attribute(
        __MODULE__,
        :migrations,
        {unquote(version), unquote(fun_name)}
      )
    end
  end

  def has_duplicates?(migrations) do
    duplicates = migrations -- Enum.uniq_by(migrations, fn {version, _fn} -> version end)
    Enum.count(duplicates) != 0
  end

  def migration_private_function(version) do
    :"__migrate_#{version}__"
  end
end
