defmodule SslMoon.Checks.Router do
  @moduledoc """
    Scaffolding for router definition for checks.
    Contains macro to define checks, and validations.

    Example usage:

      use SslMoon.Checks.Router

      checks do
        check "test_check" do
          display_name("Test Check")
          version(1)
          executor(SslMoon.Checks.TestCheck)
          template(__MODULE__, show: false)
          migrator(SslMoon.TestMigrator)
        end
      end
  """
  alias SslMoon.Checks.Check
  alias SslMoon.Checks.ModuleWithOpts

  @type module_with_opts :: %{module: module(), opts: keyword()}
  @callback checks() :: list(%Check{})
  defmacro __using__(_attrs) do
    quote do
      @behaviour unquote(__MODULE__)
      import unquote(__MODULE__)

      Module.register_attribute(__MODULE__, :checks, accumulate: true)
    end
  end

  @doc """
    Macro to define checks scaffolding.
  """
  defmacro checks(do: body) do
    quote do
      unquote(body)

      checks = Module.get_attribute(__MODULE__, :checks)
      duplicates = get_duplicates(checks)

      if(Enum.count(duplicates) > 0) do
        duplicates = Enum.map(duplicates, fn %{name: name} -> name end)

        raise """
        Found duplicate check names: #{inspect(duplicates)}.

        Ensure that check names are unique.
        """
      end

      @impl unquote(__MODULE__)
      def checks() do
        @checks
      end
    end
  end

  def get_duplicates(checks) do
    checks -- Enum.uniq_by(checks, fn %{name: name} -> name end)
  end

  @doc """
    Macro to define a check, name should be in string format.
  """
  defmacro check(name, do: body) do
    quote do
      unquote(body)

      name = unquote(name)
      version = Module.get_attribute(__MODULE__, :version)
      executor = Module.get_attribute(__MODULE__, :executor)
      template = Module.get_attribute(__MODULE__, :template)
      migrator = Module.get_attribute(__MODULE__, :migrator)
      display_name = Module.get_attribute(__MODULE__, :display_name, unquote(name))
      validation_schema = Module.get_attribute(__MODULE__, :validation_schema)

      if !is_binary(name) do
        raise "Invalid check name, expected string, got #{inspect(name)}"
      end

      if version == nil or executor == nil or template == nil or validation_schema == nil do
        raise "Missing one of the required attributes."
      end

      if migrator != nil do
        migrator_module = migrator.module
        # Check that migrator latest version is not higher than check version
        migrations = migrator_module.versions()
        migrator_last_ver = List.last(migrations)

        if migrator_last_ver > version do
          raise """
            Migrator version mismatch.
            Check #{unquote(name)} has version #{version}, but migrator latest version is #{migrator_last_ver}
          """
        end
      end

      Module.put_attribute(__MODULE__, :checks, %Check{
        name: unquote(name),
        display_name: display_name,
        version: version,
        executor: executor,
        template: template,
        migrator: migrator,
        validation_schema: validation_schema
      })
    end
  end

  @doc """
    Macro for version definition, the version should be always an integer.

    Example: 12
  """
  defmacro version(version) do
    quote do
      version = unquote(version)

      if !is_integer(version) do
        raise("Invalid version format, expected integer.")
      end

      Module.put_attribute(__MODULE__, :version, unquote(version))
    end
  end

  @doc """
    Macro for defining the display name of the check, should always be a binary.

    Example: "Test Check"
  """
  defmacro display_name(name) do
    quote do
      name = unquote(name)

      if !is_binary(name) do
        raise("Invalid display_name format, expected string.")
      end

      Module.put_attribute(__MODULE__, :display_name, unquote(name))
    end
  end

  defmacro validate_module(module_ast) do
    quote do
      module = unquote(module_ast)

      if !is_atom(module) do
        raise "Invalid format, expected atom."
      end

      Code.ensure_compiled!(module)
    end
  end

  @doc """
    Macro for executor module, should contain an atom pointing to an existing module.

    Additional options:

      * `active` - boolean flag that determines if check will be executed, defaults to true

    Example: executor(SslMoon.Application, active: false)
  """
  defmacro executor(module_ast, opts \\ []) do
    quote do
      opts = generate_opts(:executor, unquote(opts))
      module = validate_module(unquote(module_ast))

      Module.put_attribute(__MODULE__, :executor, %ModuleWithOpts{
        module: module,
        opts: opts
      })
    end
  end

  @doc """
    Macro for template module, should contain an atom pointing to an existing module.

    Additional options:

      * `show` - boolean flag that determines if check will be showed on frontend, defaults to true

    Example: template(SslMoon.Application, show: false)
  """
  defmacro template(module_ast, opts \\ []) do
    quote do
      opts = generate_opts(:template, unquote(opts))
      module = validate_module(unquote(module_ast))

      Module.put_attribute(__MODULE__, :template, %ModuleWithOpts{
        module: module,
        opts: opts
      })
    end
  end

  @doc """
    Macro for migrator module, should contain an atom pointing to an existing module.

    Example: migrator(SslMoon.Application)
  """
  defmacro migrator(module_ast, opts \\ []) do
    quote do
      opts = generate_opts(:migrator, unquote(opts))
      module = validate_module(unquote(module_ast))

      Module.put_attribute(__MODULE__, :migrator, %ModuleWithOpts{
        module: module,
        opts: opts
      })
    end
  end

  defmacro validation_schema(module_ast, opts \\ []) do
    quote do
      opts = generate_opts(:validation_schema, unquote(opts))
      module = validate_module(unquote(module_ast))

      Module.put_attribute(__MODULE__, :validation_schema, %ModuleWithOpts{
        module: module,
        opts: opts
      })
    end
  end

  @doc """
    Function for generating the correct internal format for opts for executor, template and migrator.
  """
  @spec generate_opts(:executor, keyword()) :: %{active: boolean()}
  def generate_opts(:executor, opts) do
    active = Keyword.get(opts, :active, true)

    %{active: active}
  end

  @spec generate_opts(:template, keyword()) :: %{show: boolean()}
  def generate_opts(:template, opts) do
    show = Keyword.get(opts, :show, true)

    %{show: show}
  end

  @spec generate_opts(:migrator, keyword()) :: map()
  def generate_opts(:migrator, _opts) do
    %{}
  end

  @spec generate_opts(:validation_schema, keyword()) :: map()
  def generate_opts(:validation_schema, _opts) do
    %{}
  end
end
