defmodule SslMoon.Checks.RouterTest do
  @moduledoc """
    Utilities and functions for emulating and creating tests for router or components
    that are part of it.
  """

  alias SslMoon.Checks.Check
  alias SslMoon.Checks.ModuleWithOpts
  alias SslMoon.Checks.Router

  @doc """
    Creates a sample check structure to be used in checks. Compile-time validations are not active here,
    as only a data structure is genenrated.
  """
  def create_check(params \\ []) do
    name = Keyword.get(params, :name, "test_check")
    display_name = Keyword.get(params, :display_name, "Test Check")
    version = Keyword.get(params, :version, 0)

    executor =
      Keyword.get(params, :executor, %ModuleWithOpts{
        module: :test,
        opts: Router.generate_opts(:executor, [])
      })

    template =
      Keyword.get(params, :template, %ModuleWithOpts{
        module: :test,
        opts: Router.generate_opts(:executor, [])
      })

    migrator =
      Keyword.get(params, :migrator, %ModuleWithOpts{
        module: :test,
        opts: Router.generate_opts(:executor, [])
      })

    %Check{
      name: name,
      display_name: display_name,
      version: version,
      executor: executor,
      template: template,
      migrator: migrator,
      validation_schema: nil
    }
  end
end
