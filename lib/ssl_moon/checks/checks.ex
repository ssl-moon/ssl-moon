defmodule SslMoon.Checks do
  @moduledoc """
    Module that knows how to operate with checks.
  """
  import Phoenix.Component
  import SslMoon.CheckLiveComponent

  alias SslMoon.Storage.Checks
  alias SslMoon.Checks.Check
  alias SslMoon.Checks.CheckResult

  alias SslMoon.Schemas.Check, as: CheckSchema

  @doc """
    Performs and qualifies a check using the defined executor.
  """
  @spec execute_check(Check.t(), String.t()) :: {:ok, CheckResult.t()} | {:error, any()}
  def execute_check(%Check{} = check, url) do
    module = check.executor.module

    result =
      try do
        with {:ok, result} <- module.perform_check(check, url),
             {:ok, status} <- module.qualify_check(check, url, result) do
          {:ok, %CheckResult{status: status, data: result}}
        end
      rescue
        err ->
          module.on_error(check, url, err, __STACKTRACE__)
      end

    case result do
      {:error, err} ->
        module.on_error(check, url, err, nil)

      other ->
        other
    end
  end

  @doc """
    Performs validation of the check if a validator is present and creates a new entry.
  """
  @spec create_check(Check.t(), CheckResult.t(), integer(), String.t()) ::
          {:ok, any()} | {:error, any()}
  def create_check(%Check{} = check, %CheckResult{} = check_result, check_group_id, url) do
    data = check_result.data
    status = check_result.status

    changeset = check.validation_schema.module.changeset(data)

    if changeset.valid? != true do
      {:error,
       """
       Validation failed for #{check.name} using schema #{check.validation_schema.module} with data: #{inspect(data)}

       Changeset error: #{inspect(changeset)}
       """}
    else
      Checks.create_check(%{
        domain_name: url,
        check_name: check.name,
        version: check.version,
        status: status,
        check_group_id: check_group_id,
        check_result: check.validation_schema.module.__to_map__(data)
      })
    end
  end

  @spec create_check!(Check.t(), CheckResult.t(), integer(), String.t()) :: any()
  def create_check!(%Check{} = check, %CheckResult{} = check_result, check_group_id, url) do
    case create_check(check, check_result, check_group_id, url) do
      {:ok, resp} -> resp
      {:error, msg} -> raise msg
    end
  end

  @spec get_check_by_name(router_module :: module(), check_name :: String.t()) ::
          SslMoon.Checks.Check.t() | nil
  def get_check_by_name(router_module \\ SslMoon.DefaultChecksRouter, check_name) do
    Enum.find(router_module.checks(), nil, fn %{name: name} ->
      check_name == name
    end)
  end

  @doc """
    Transforms check result from map to a schema based on validation_schema from check.
  """
  @spec cast_check_result(check :: Check.t(), data :: CheckSchema.t()) ::
          CheckSchema.t()
  def cast_check_result(_check, %CheckSchema{status: :not_found} = data), do: data

  def cast_check_result(check, data) do
    schema_module = check.validation_schema.module
    CheckSchema.result_to_schema(data, &schema_module.changeset/1)
  end

  @spec render_latest_check(check :: Check.t(), url :: String.t()) :: any()
  def render_latest_check(check, url) do
    data = Checks.get_latest_check(url, check.name) || CheckSchema.not_found(check)
    render_check(check, url, data)
  end

  @spec render_check(check :: Check.t(), url :: String.t(), data :: CheckSchema.t()) :: any()
  def render_check(%Check{} = check, url, data) do
    data = cast_check_result(check, data)
    render_template(%{check: check, domain_name: url, check_data: data})
  end

  defp render_template(assigns) do
    ~H"""
    <.check_live_component
      id={@check.name}
      check={@check}
      domain_name={@domain_name}
      check_data={@check_data}
    />
    """
  end
end
