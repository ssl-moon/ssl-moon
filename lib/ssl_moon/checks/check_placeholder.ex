defmodule SslMoon.Check do
  @moduledoc """
    Placeholder module for deprecated feature.

    THE FUNCIONALITY OF THIS MODULE WAS DEPRECATED AND REMOVED, IT IS ONLY A PLACEHOLDER
    TO AVOID COMPILE-TIME ERRORS.
  """

  @deprecated "Use the new system for managing checks from SslMoon.Checks.Router"

  @callback perform_check(any()) :: any()
  @callback save_data(any()) :: any()

  defmacro __using__(_opts) do
    quote do
      @behaviour unquote(__MODULE__)
    end
  end
end
