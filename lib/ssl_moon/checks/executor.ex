defmodule SslMoon.Checks.Executor do
  @moduledoc """
    Defines a check executor.

    The module has the responsibility for defining callbacks
  """
  require Logger
  alias SslMoon.Checks.Check

  @type status :: :success | :warning | :failure | :inactive

  @doc """
    Invoked when performing a check.

    Returning {:error, message} will be considered as fail to perform check. If the check has to handle errors,
    all errors should be handled inside of function and error should be returned only when something bad happens.
  """
  @callback perform_check(%Check{}, String.t()) :: {:ok, any()} | {:error, any()}

  @doc """
    Qualifies check with one of the statuses.
    Invoked after perform_check.

    Returning {status, data} will mark the check as complete and will save it.

    Returning {:error, message} will mark that there was a problem with saving data and will mark check as erroneous.
  """
  @callback qualify_check(%Check{}, String.t(), map()) ::
              {:ok, status()} | {:error, any()}

  @doc """
    Invoked when the check cannot be performed.

    Returning {:ok, any()} will mark this check as handled, otherwise an error will be logged.
  """
  @callback on_error(%Check{}, String.t(), any(), any()) :: {:ok, SslMoon.Checks.CheckResult.t()}
  defmacro __using__(_params) do
    quote do
      @behaviour unquote(__MODULE__)
      require Logger

      @impl true
      def on_error(%Check{name: name, version: version}, url, error, stacktrace) do
        reraise(error, stacktrace)
      end

      defoverridable on_error: 4
    end
  end
end
