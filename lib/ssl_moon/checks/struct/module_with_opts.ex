defmodule SslMoon.Checks.ModuleWithOpts do
  @moduledoc """
    Structure for module options from checks router.
  """
  @derive Jason.Encoder

  @enforce_keys [:module, :opts]
  defstruct [:module, :opts]

  @type t :: %__MODULE__{
          module: module(),
          opts: map()
        }
end
