defmodule SslMoon.Checks.CheckResult do
  @moduledoc """
    A result returned after performing a check.
  """

  @enforce_keys [:data, :status]
  defstruct [:data, :status]

  @type t :: %__MODULE__{
          data: any(),
          status: :success | :warning | :failure | :inactive
        }
end
