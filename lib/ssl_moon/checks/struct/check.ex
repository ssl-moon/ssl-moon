defmodule SslMoon.Checks.Check do
  @moduledoc """
    Struct that contains all the check metadata.
  """
  alias SslMoon.Checks.ModuleWithOpts

  @enforce_keys [
    :name,
    :display_name,
    :version,
    :executor,
    :template,
    :migrator,
    :validation_schema
  ]
  defstruct [:name, :display_name, :version, :executor, :template, :migrator, :validation_schema]

  @type t :: %__MODULE__{
          name: String.t(),
          display_name: String.t(),
          version: integer(),
          executor: ModuleWithOpts.t(),
          template: ModuleWithOpts.t(),
          migrator: ModuleWithOpts.t() | nil,
          validation_schema: ModuleWithOpts.t() | nil
        }
end
