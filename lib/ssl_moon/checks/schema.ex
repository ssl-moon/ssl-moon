defmodule SslMoon.Checks.Schema do
  @moduledoc """
    Schema module used for checks, uses Ecto.Schema under the hood.
  """
  @callback changeset(params :: any()) :: Ecto.Changeset.t()

  defmacro __using__(_params) do
    quote do
      use Ecto.Schema
      @behaviour unquote(__MODULE__)

      def __to_map__(data) do
        Map.take(data, __MODULE__.__schema__(:fields))
      end
    end
  end
end
