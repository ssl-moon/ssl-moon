defmodule SslMoon.Paths do
  @moduledoc """
  Tls schema.
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "paths" do
    field(:name, :string)
    belongs_to(:parent, SslMoon.Paths, foreign_key: :parent_id)
    field(:path, :string, virtual: true)
    # timestamps()
  end

  def changeset(path, attrs \\ %{}) do
    path
    |> cast(attrs, [:name, :parent_id])
    |> validate_required([:name, :parent_id])
  end
end
