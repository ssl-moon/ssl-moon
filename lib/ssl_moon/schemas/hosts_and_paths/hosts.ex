defmodule SslMoon.Hosts do
  @moduledoc """
  Tls schema.
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "hosts" do
    field(:domain_name, :string)
    belongs_to(:path, SslMoon.Paths, foreign_key: :path_id)
    # timestamps()
  end

  def changeset(host, attrs \\ %{}) do
    host
    |> cast(attrs, [:domain_name, :path_id])
    |> validate_required([:domain_name, :path_id])
  end
end
