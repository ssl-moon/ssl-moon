defmodule SslMoon.Schemas.CheckGroup do
  @moduledoc """
  CheckGroup schema.
  """
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{}

  schema "checks_group" do
    field(:domain_name, :string)
    field(:checked_at, :naive_datetime)
    has_many :checks, SslMoon.Schemas.Check

    timestamps()
  end

  def changeset(group, attrs \\ %{}) do
    group
    |> cast(attrs, [:domain_name, :checked_at])
    |> validate_required([:domain_name, :checked_at])
  end
end
