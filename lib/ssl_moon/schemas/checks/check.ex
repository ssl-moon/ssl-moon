defmodule SslMoon.Schemas.Check do
  @moduledoc """
  Check schema.
  """
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{}

  schema "checks" do
    field(:check_name, :string)
    field(:version, :integer)
    field(:status, Ecto.Enum, values: [:success, :failure, :inactive, :warning, :not_found])
    field(:check_result, :map)

    belongs_to :check_group, SslMoon.Schemas.CheckGroup

    timestamps()
  end

  def changeset(check, attrs \\ %{}) do
    check
    |> cast(attrs, [:check_name, :version, :status, :check_result, :check_group_id])
    |> validate_required([:check_name, :version, :status, :check_result])
  end

  @doc """
    Generates a not found entry to be sent to template.
  """
  def not_found(%SslMoon.Checks.Check{} = check) do
    %__MODULE__{
      check_name: check.name,
      version: check.version,
      status: :not_found,
      check_result: %{}
    }
  end

  @spec result_to_schema(check :: __MODULE__.t(), cast_fun :: fun()) ::
          __MODULE__.t()
  def result_to_schema(check, cast_fun) when is_function(cast_fun, 1) do
    result =
      check.check_result
      |> cast_fun.()
      |> Ecto.Changeset.apply_action!(:update)

    Map.put(check, :check_result, result)
  end
end
