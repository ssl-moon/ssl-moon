defmodule ErrorReporter do
  @moduledoc """
    Simple logger for failed jobs.
    TODO: Replace with a better logging backend.
  """

  require Logger

  def handle_event([:oban, :job, :exception], _, %{attempt: _attempt} = meta, _) do
    Logger.error(meta.reason)
  end
end
