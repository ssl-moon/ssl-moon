defmodule SslMoon.Oban.BatchEnqueueWorker do
  use Oban.Worker,
    queue: :batch,
    max_attempts: 3

  require Logger

  @moduledoc """
    Worker responsible for enqueing all the hosts checks as oban jobs.
  """
  alias SslMoon.Oban.Jobs

  @impl Oban.Worker
  def perform(_job) do
    try do
      Jobs.batch_enqueue_all_checks()
      :ok
    rescue
      err ->
        Logger.error(Exception.format(:error, err, __STACKTRACE__))
        {:error, err}
    end
  end
end
