defmodule SslMoon.Oban.Jobs do
  @moduledoc """
    Module that contains all public functions for enqueuing oban jobs.
  """
  alias SslMoon.Storage.Checks
  alias SslMoon.Checks.Check
  alias SslMoon.HostsAndPaths
  alias SslMoon.Hosts

  @doc """
    Executes all checks for all hosts.
  """
  def batch_enqueue_all_checks() do
    hosts = HostsAndPaths.list_hosts()

    for %Hosts{domain_name: url} <- hosts do
      check_group = Checks.create_check_group!(url)
      enqueue_all_checks_for_url(check_group.id, url)
    end
  end

  @doc """
    Enqueues all checks for the specific url to be executed by SslMoon.Oban.CheckWorker.
    The check types are fetched from router module.
  """
  def enqueue_all_checks_for_url(check_group_id, url, check_module \\ SslMoon.DefaultChecksRouter) do
    checks = check_module.checks()

    for %Check{} = check <- checks do
      %{url: url, check_name: check.name, check_group_id: check_group_id}
      |> SslMoon.Oban.CheckWorker.new()
      |> Oban.insert!()
    end
  end
end
