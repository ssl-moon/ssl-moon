defmodule SslMoon.Oban.CheckWorker do
  use Oban.Worker,
    queue: :check,
    max_attempts: max_retries()

  require Logger

  @moduledoc """
    Worker responsible for executing the actual check.
  """
  alias SslMoon.Checks

  defp max_retries do
    Application.fetch_env!(:ssl_moon, :max_retries)
  end

  @impl Oban.Worker
  def perform(%Oban.Job{
        attempt: attempt,
        args: %{"url" => url, "check_name" => check_name, "check_group_id" => check_group_id}
      }) do
    try do
      check = Checks.get_check_by_name(check_name)

      case Checks.execute_check(check, url) do
        {:error, error} ->
          if attempt == max_retries() do
            save_error(check, error)
          end

        {:ok, resp} ->
          Checks.create_check!(check, resp, check_group_id, url)
      end

      :ok
    rescue
      err ->
        Logger.error(Exception.format(:error, err, __STACKTRACE__))
        {:error, err}
    end
  end

  @impl Oban.Worker
  def timeout(_job), do: :timer.minutes(10)

  defp save_error(_check, _error) do
    # Save error for future processing
  end
end
