defmodule SslMoon.Http.HttpClient.Request do
  @moduledoc """
    Struct for http redirect.
  """
  @derive Jason.Encoder

  @enforce_keys [:body, :headers, :method, :options, :params, :url]
  defstruct [:body, :headers, :method, :options, :params, :url]

  @type t :: %__MODULE__{}
end
