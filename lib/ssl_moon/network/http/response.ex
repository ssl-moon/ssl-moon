defmodule SslMoon.Http.HttpClient.Response do
  @moduledoc """
    Struct for http response.
  """
  @derive Jason.Encoder

  @enforce_keys [:status_code, :body, :headers, :request_url, :request]
  defstruct [:status_code, :body, :headers, :request_url, :request]

  @type t :: %__MODULE__{}
end
