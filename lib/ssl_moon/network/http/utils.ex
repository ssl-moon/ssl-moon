defmodule SslMoon.Http.HttpUtils do
  @moduledoc """
    Utils functions for http requests.
  """

  alias SslMoon.Http.HttpClient
  alias SslMoon.Http.HttpClient.{MaybeRedirect, Response}

  @spec get_follow_redirect(url :: String.t(), return_hops :: boolean()) ::
          {:ok, Response.t() | list(Response.t() | MaybeRedirect.t())} | {:error, any()}
  def get_follow_redirect(url, return_hops \\ false) do
    get_follow_redirect([], url, return_hops)
  end

  defp get_follow_redirect(acc, url, return_hops) do
    resp = HttpClient.request(:get, url)

    case resp do
      {:ok, %MaybeRedirect{} = resp} ->
        get_follow_redirect([resp | acc], resp.redirect_url, return_hops)

      {:ok, %Response{} = resp} ->
        {:ok, return_hops([resp | acc], return_hops)}

      error ->
        error
    end
  end

  defp return_hops(resp_list, true), do: resp_list
  defp return_hops([resp | _], false), do: resp
end
