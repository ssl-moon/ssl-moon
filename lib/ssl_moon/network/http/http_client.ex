defmodule SslMoon.Http.HttpClient do
  @moduledoc """
    Http client. Configurable via:

      config :ssl_moon, http_client: SslMoon.Http.HttpClientImpl

    Currently default configuration uses the SslMoon.Http.HttpClientImpl module.
  """

  @callback request(atom(), binary(), binary(), keyword(), keyword()) ::
              {:ok,
               SslMoon.Http.HttpClient.Response.t() | SslMoon.Http.HttpClient.MaybeRedirect.t()}
              | {:error, any()}

  @behaviour __MODULE__

  defp http_client() do
    Application.get_env(:ssl_moon, :http_client, SslMoon.Http.HttpClientImpl)
  end

  @impl SslMoon.Http.HttpClient
  def request(method, url, body \\ "", headers \\ [], opts \\ []) do
    http_client().request(method, url, body, headers, opts)
  end
end
