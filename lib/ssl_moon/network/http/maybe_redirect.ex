defmodule SslMoon.Http.HttpClient.MaybeRedirect do
  @moduledoc """
    Struct for http redirect.
  """
  @derive Jason.Encoder

  @enforce_keys [:status_code, :redirect_url, :headers, :request_url, :request]
  defstruct [:status_code, :redirect_url, :headers, :request_url, :request]

  @type t :: %__MODULE__{}
end
