defmodule SslMoon.Http.HttpClientImpl do
  @moduledoc """
    Http client implementation. Currently using HTTPoison as the client.

    WARNING: Do not use this module directly, instead use functions from SslMoon.Http.HttpClient.
  """
  @behaviour SslMoon.Http.HttpClient

  alias SslMoon.Http.HttpClient
  alias SslMoon.Http.HttpClient.{Response, MaybeRedirect, Request}

  @impl HttpClient
  def request(method, url, body \\ "", headers \\ [], opts \\ []) do
    resp = HTTPoison.request(method, url, body, headers, opts)

    case resp do
      {:ok, %HTTPoison.Response{} = resp} -> {:ok, convert_to_response(resp)}
      other -> other
    end
  end

  defp convert_to_response(%HTTPoison.Response{status_code: code} = resp)
       when code >= 300 and code < 400 do
    %MaybeRedirect{
      status_code: code,
      headers: resp.headers,
      redirect_url: get_redirect_url(resp),
      request_url: resp.request_url,
      request: convert_request(resp.request)
    }
  end

  defp convert_to_response(%HTTPoison.Response{} = resp) do
    %Response{
      status_code: resp.status_code,
      headers: resp.headers,
      body: resp.body,
      request_url: resp.request_url,
      request: convert_request(resp.request)
    }
  end

  defp convert_request(%HTTPoison.Request{} = request) do
    %Request{
      body: request.body,
      headers: request.headers,
      method: request.method,
      options: request.options,
      params: request.params,
      url: request.url
    }
  end

  defp get_redirect_url(%HTTPoison.Response{headers: headers}) do
    {_, redirect_url} =
      Enum.find(headers, fn {name, _val} -> String.downcase(name) == "location" end) ||
        raise "Location header not found"

    redirect_url
  end
end
