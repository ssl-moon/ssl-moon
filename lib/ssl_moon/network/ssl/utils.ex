defmodule SslMoon.Ssl.Certificates.CertificateUtils do
  @moduledoc false
  require Logger

  @pubkey_schema Record.extract_all(from_lib: "public_key/include/OTP-PUB-KEY.hrl")

  def get_field(record, field) do
    record_type = elem(record, 0)

    idx =
      @pubkey_schema[record_type]
      |> Keyword.keys()
      |> Enum.find_index(&(&1 == field))

    elem(record, idx + 1)
  end

  def clean_time(time_tuple) do
    {type, time_charlist} = time_tuple

    time_string =
      time_charlist
      |> to_string

    output =
      time_string
      |> String.split("+")
      |> List.first()
      |> (fn foo ->
            last = String.last(foo)

            case last do
              "Z" -> foo
              _ -> foo <> "Z"
            end
          end).()
      |> to_charlist

    {type, output}
  end

  def to_generalized_time({:generalTime, time}), do: time

  def to_generalized_time({:utcTime, time}) do
    year = time |> Enum.take(2) |> List.to_integer()
    prefix = if year >= 50, do: ~c"19", else: ~c"20"
    prefix ++ time
  end

  def asn1_to_epoch(asn1_time) do
    {year, rest} = Enum.split(asn1_time, 4)

    date =
      case rest |> Enum.chunk_every(2) do
        [month, day, hour, minute, second, ~c"Z"] ->
          [year, month, day, hour, minute, second]

        [month, day, hour, minute, ~c"Z"] ->
          [year, month, day, hour, minute, ~c"00"]

        _ ->
          Logger.error("Unhandled ASN1 time structure - #{asn1_time}}")
          nil
      end

    date_args = date |> Enum.map(&(to_string(&1) |> String.to_integer()))

    case apply(NaiveDateTime, :new, date_args) do
      {:ok, ~N[9999-12-31 23:59:59]} ->
        :no_expiration

      {:ok, datetime} ->
        datetime |> DateTime.from_naive!("Etc/UTC") |> DateTime.to_unix()

      _ ->
        Logger.error("Unhandled ASN1 time structure - #{date_args}}")
        nil
    end
  end

  def ip_to_string(ip) do
    ip
    |> :binary.bin_to_list()
    |> Enum.map_join(".", &to_string/1)
  end

  def join_usage_types(key_usage) do
    key_usage
    |> Enum.reduce([], fn usage_atom, output ->
      [usage_atom |> camel_to_spaces | output]
    end)
    |> Enum.reverse()
    |> Enum.join(", ")
  end

  def camel_to_spaces(atom) do
    atom
    |> Atom.to_charlist()
    |> Enum.reduce([], fn char, charlist ->
      charlist = [char | charlist]

      case char in 65..90 do
        true -> List.insert_at(charlist, 1, ~c" ")
        false -> charlist
      end
    end)
    |> Enum.reverse()
    |> to_string
    |> String.split()
    |> Enum.map_join(" ", &String.capitalize/1)
  end
end
