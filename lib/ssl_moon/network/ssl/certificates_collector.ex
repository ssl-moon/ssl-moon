defmodule SslMoon.Ssl.CertificateCollector do
  @moduledoc false
  use Agent

  def start_link(_opts \\ []) do
    Agent.start_link(fn -> [] end)
  end

  def add_certificate(pid, cert, status) do
    Agent.update(pid, fn state ->
      [%{certificate: cert, status: status} | state]
    end)
  end

  def get_certificates(pid) do
    Agent.get(pid, & &1)
  end
end
