defmodule SslMoon.Ssl.Certificate do
  @moduledoc """
    Utilities for parsing https certificates.
  """
  require Logger
  alias SslMoon.Ssl.Certificates.CertificateUtils

  @extended_key_usages %{
    {1, 3, 6, 1, 5, 5, 7, 3, 1} => "TLS Web server authentication",
    {1, 3, 6, 1, 5, 5, 7, 3, 2} => "TLS Web client authentication",
    {1, 3, 6, 1, 5, 5, 7, 3, 3} => "Code signing",
    {1, 3, 6, 1, 5, 5, 7, 3, 4} => "E-mail protection",
    {1, 3, 6, 1, 5, 5, 7, 3, 8} => "Timestamping",
    {1, 3, 6, 1, 5, 5, 7, 3, 9} => "OCSPstamping",
    {1, 3, 6, 1, 5, 5, 7, 3, 5} => "IP security end system",
    {1, 3, 6, 1, 5, 5, 7, 3, 6} => "IP security tunnel termination",
    {1, 3, 6, 1, 5, 5, 7, 3, 7} => "IP security user"
  }

  @authority_information_access_oids %{
    {1, 3, 6, 1, 5, 5, 7, 48, 1} => "OCSP - URI",
    {1, 3, 6, 1, 5, 5, 7, 48, 2} => "CA Issuers - URI"
  }

  def parse_der(cert, opts \\ [all_domains: false, serialize: false]) do
    tbs_cert = CertificateUtils.get_field(cert, :tbsCertificate)

    serialized_certificate =
      %{}
      |> Map.put(:fingerprint, fingerprint_cert(cert))
      |> Map.put(
        :serial_number,
        tbs_cert |> CertificateUtils.get_field(:serialNumber) |> Integer.to_string(16)
      )
      |> Map.put(:signature_algorithm, parse_signature_algo(tbs_cert))
      |> Map.put(:subject, parse_rdnsequence(tbs_cert, :subject))
      |> Map.put(:issuer, parse_rdnsequence(tbs_cert, :issuer))
      |> Map.put(:extensions, parse_extensions(cert, tbs_cert))
      |> Map.merge(parse_expiry(tbs_cert))

    Enum.reduce(opts, serialized_certificate, fn {option, flag}, serialized_certificate ->
      case option do
        :all_domains when flag == true ->
          serialized_certificate
          |> Map.put(:all_domains, get_all_domain_names(tbs_cert, serialized_certificate))

        _ ->
          serialized_certificate
      end
    end)
  end

  def get_all_domain_names(cert, serialized_cert) do
    domain_names = MapSet.new()

    domain_names =
      case serialized_cert[:subject][:CN] do
        nil -> domain_names
        _ -> MapSet.put(domain_names, serialized_cert[:subject][:CN])
      end

    extensions = cert |> CertificateUtils.get_field(:extensions)

    extensions
    |> Enum.reduce(domain_names, &reduce_extensions/2)
    |> MapSet.to_list()
  end

  defp reduce_extensions(extension, domain_names) do
    case extension do
      {:Extension, {2, 5, 29, 17}, _critical, san_entries} ->
        reduce_san_entries(san_entries, domain_names)

      :asn1_NOVALUE ->
        domain_names

      _ ->
        domain_names
    end
  end

  defp reduce_san_entries(san_entries, domain_names) do
    san_entries
    |> Enum.reduce(domain_names, &reduce_san_entry/2)
  end

  defp reduce_san_entry({:dNSName, dns_name}, names) do
    MapSet.put(names, dns_name |> to_string)
  end

  defp reduce_san_entry(_, names) do
    names
  end

  defp fingerprint_cert(certificate) do
    certificate = :public_key.pkix_encode(:OTPCertificate, certificate, :otp)

    :crypto.hash(:sha, certificate)
    |> Base.encode16()
    |> String.to_charlist()
    |> Enum.chunk_every(2, 2, :discard)
    |> Enum.join(":")
  end

  defp parse_signature_algo(cert) do
    cert
    |> CertificateUtils.get_field(:signature)
    |> CertificateUtils.get_field(:algorithm)
    |> :public_key.pkix_sign_types()
    |> Tuple.to_list()
    |> Enum.map_join(", ", &Atom.to_string/1)
  end

  defp parse_rdnsequence(cert, field) do
    rdnsequence = %{
      :CN => nil,
      :C => nil,
      :L => nil,
      :ST => nil,
      :O => nil,
      :OU => nil,
      :emailAddress => nil
    }

    {:rdnSequence, rdnsequence_attribute} = cert |> CertificateUtils.get_field(field)

    rdnsequence =
      rdnsequence_attribute
      |> List.flatten()
      |> Enum.reduce(rdnsequence, &reduce_attribute/2)

    Map.put(rdnsequence, :aggregated, rdnsequence |> aggregate_rdnsequence)
  end

  defp reduce_attribute(attr, rdnsequence) do
    {:AttributeTypeAndValue, oid, attribute_value} = attr

    attr_atom = get_attr_atom(oid)

    case attr_atom do
      nil -> rdnsequence
      _ -> %{rdnsequence | attr_atom => attribute_value |> coerce_to_string |> to_string}
    end
  end

  defp get_attr_atom(oid) do
    case oid do
      {2, 5, 4, 3} -> :CN
      {2, 5, 4, 6} -> :C
      {2, 5, 4, 7} -> :L
      {2, 5, 4, 8} -> :ST
      {2, 5, 4, 10} -> :O
      {2, 5, 4, 11} -> :OU
      {1, 2, 840, 113_549, 1, 9, 1} -> :emailAddress
      _ -> nil
    end
  end

  defp coerce_to_string(attribute_value) do
    case attribute_value do
      {:printableString, string} ->
        string

      {:utf8String, string} ->
        string

      {:teletexString, string} ->
        string

      string when is_list(string) ->
        string

      _ ->
        Logger.error("Unhandled RDN attribute type #{inspect(attribute_value)}")
        nil
    end
  end

  defp aggregate_rdnsequence(rdnsequence) do
    [
      {:CN, rdnsequence[:CN]},
      {:O, rdnsequence[:O]},
      {:OU, rdnsequence[:OU]},
      {:L, rdnsequence[:L]},
      {:ST, rdnsequence[:ST]},
      {:C, rdnsequence[:C]},
      {:emailAddress, rdnsequence[:emailAddress]}
    ]
    # Filter out empty values
    |> Enum.filter(fn {_, v} -> v != nil end)
    # Turn everything in to a string so C=blah.com
    |> Enum.map_join(", ", fn {k, v} -> (k |> to_string) <> "=" <> (v |> to_string) end)
  end

  defp parse_expiry(cert) do
    {:Validity, not_before, not_after} = cert |> CertificateUtils.get_field(:validity)
    not_before = CertificateUtils.clean_time(not_before)
    not_after = CertificateUtils.clean_time(not_after)

    %{
      :not_before =>
        not_before
        |> CertificateUtils.to_generalized_time()
        |> CertificateUtils.asn1_to_epoch()
        |> DateTime.from_unix!(),
      :not_after =>
        not_after
        |> CertificateUtils.to_generalized_time()
        |> CertificateUtils.asn1_to_epoch()
        |> DateTime.from_unix!()
    }
  end

  def parse_extensions(cert, tbs_cert) do
    extensions = CertificateUtils.get_field(tbs_cert, :extensions)

    case extensions do
      :asn1_NOVALUE ->
        %{}

      _ ->
        extensions
        |> Enum.reduce(%{}, fn extension, acc ->
          parse_extension(extension, acc, cert)
        end)
    end
  end

  # Authority Key Identifier
  defp parse_extension(
         {:Extension, {2, 5, 29, 35}, _critical,
          {:AuthorityKeyIdentifier, authority_key_identifier, _authority_cert_issuer,
           _authority_cert_serial_number}},
         extension_map,
         _cert
       ) do
    case authority_key_identifier do
      value when is_binary(value) ->
        Map.put(
          extension_map,
          :authorityKeyIdentifier,
          authority_key_identifier
          |> Base.encode16()
          |> String.to_charlist()
          |> Enum.chunk_every(2, 2, :discard)
          |> Enum.join(":")
          |> String.replace_prefix("", "keyid:")
          |> String.replace_suffix("", "\n")
        )

      :asn1_NOVALUE ->
        extension_map
    end
  end

  # Subject Key Identifier
  defp parse_extension(
         {:Extension, {2, 5, 29, 14}, _critical, subject_key_identifier},
         extension_map,
         _cert
       ) do
    Map.put(
      extension_map,
      :subjectKeyIdentifier,
      subject_key_identifier
      |> Base.encode16()
      |> String.to_charlist()
      |> Enum.chunk_every(2, 2, :discard)
      |> Enum.join(":")
    )
  end

  # Key Usage
  defp parse_extension(
         {:Extension, {2, 5, 29, 15}, _critical, key_usage},
         extension_map,
         _cert
       ) do
    Map.put(
      extension_map,
      :keyUsage,
      key_usage
      |> CertificateUtils.join_usage_types()
    )
  end

  # Certificate Policies
  defp parse_extension(
         {:Extension, {2, 5, 29, 32}, _critical, policy_entries},
         extension_map,
         _cert
       ) do
    Map.put(extension_map, :certificatePolicies, formatted_policy_entries(policy_entries))
  end

  # Subject Alternative Name
  defp parse_extension(
         {:Extension, {2, 5, 29, 17}, _critical, subject_alternative_name},
         extension_map,
         _cert
       ) do
    Map.put(
      extension_map,
      :subjectAltName,
      subject_alternative_name
      |> Enum.reduce([], fn entry, san_list ->
        case entry do
          {:dNSName, dns_name} ->
            ["DNS:" <> (dns_name |> to_string) | san_list]

          {:uniformResourceIdentifier, identifier} ->
            ["URI:" <> (identifier |> to_string) | san_list]

          {:rfc822Name, identifier} ->
            ["RFC 822Name:" <> (identifier |> to_string) | san_list]

          {:iPAddress, ip} ->
            ["IP:" <> (ip |> CertificateUtils.ip_to_string()) | san_list]

          # Basically ignore those
          {:directoryName, _sequence} ->
            san_list

          {:otherName, _sequence} ->
            san_list

          _ ->
            Logger.error("Unhandled SAN entry type #{inspect(entry)}")
            san_list
        end
      end)
      |> Enum.join(", ")
    )
  end

  # Issuer Alternative Name
  defp parse_extension(
         {:Extension, {2, 5, 29, 18}, _critical, issuer_alternative_name},
         extension_map,
         _cert
       ) do
    Map.put(
      extension_map,
      :issuerAltName,
      issuer_alternative_name
      |> Enum.reduce([], fn entry, issuer_list ->
        case entry do
          {:uniformResourceIdentifier, identifier} ->
            ["URI:" <> (identifier |> to_string) | issuer_list]

          {:dNSName, dns_name} ->
            ["DNS:" <> (dns_name |> to_string) | issuer_list]

          {:rfc822Name, identifier} ->
            ["RFC 822Name:" <> (identifier |> to_string) | issuer_list]

          {:iPAddress, ip} ->
            ["IP:" <> (ip |> CertificateUtils.ip_to_string()) | issuer_list]

          # Ignore these
          {:directoryName, _sequence} ->
            issuer_list

          {:otherName, _sequence} ->
            issuer_list

          _ ->
            Logger.error("Unhandled IAN entry type #{inspect(entry)}")
            issuer_list
        end
      end)
      |> Enum.join(", ")
    )
  end

  # Basic Constraints
  defp parse_extension(
         {:Extension, {2, 5, 29, 19}, _critical, {:BasicConstraints, is_ca, _max_pathlen}},
         extension_map,
         _cert
       ) do
    Map.put(
      extension_map,
      :basicConstraints,
      case is_ca do
        true -> "CA:TRUE"
        false -> "CA:FALSE"
      end
    )
  end

  # Enhanced Key Usage
  defp parse_extension(
         {:Extension, {2, 5, 29, 37}, _critical, extended_key_usage},
         extension_map,
         _cert
       ) do
    Map.put(
      extension_map,
      :extendedKeyUsage,
      extended_key_usage
      |> Enum.map_join(", ", &@extended_key_usages[&1])
    )
  end

  # IMPORTANT: find a way to parse this extension without certificate in binary format.
  # Crl Distribution Points
  defp parse_extension(
         {:Extension, {2, 5, 29, 31}, _critical, _crl_distribution_points},
         extension_map,
         cert
       ) do
    crl_distribution_points = :public_key.pkix_dist_points(cert)

    Map.put(
      extension_map,
      :crlDistributionPoints,
      formatted_distribution_points(crl_distribution_points)
    )
  end

  # Authority Information Access
  defp parse_extension(
         {:Extension, {1, 3, 6, 1, 5, 5, 7, 1, 1}, _critical, authority_information_access},
         extension_map,
         _cert
       ) do
    Map.put(
      extension_map,
      :authorityInfoAccess,
      authority_information_access
      |> Enum.reduce([], fn match, entries ->
        case match do
          {:AccessDescription, oid, {:uniformResourceIdentifier, url}} ->
            ["#{@authority_information_access_oids[oid]}:#{url}" | entries]

          _ ->
            entries
        end
      end)
      |> Enum.join("\n")
      |> String.replace_suffix("", "\n")
    )
  end

  defp parse_extension(
         {:Extension, {1, 3, 6, 1, 4, 1, 11_129, 2, 4, 2}, _critical, sct_data},
         extension_map,
         _cert
       ) do
    Map.put(
      extension_map,
      :ctlSignedCertificateTimestamp,
      Base.url_encode64(sct_data)
    )
  end

  defp parse_extension(
         {:Extension, {1, 3, 6, 1, 4, 1, 11_129, 2, 4, 3}, _critical, _null_data},
         extension_map,
         _cert
       ) do
    Map.put(
      extension_map,
      :ctlPoisonByte,
      true
    )
  end

  defp parse_extension(
         {:Extension, oid, _critical, _payload},
         extension_map,
         _cert
       ) do
    Map.put_new(extension_map, :extra, [])
    |> Map.update!(:extra, fn x ->
      [oid |> Tuple.to_list() |> Enum.join(".") | x]
    end)
  end

  defp formatted_policy_entries(policy_entries) do
    policy_entries
    |> List.flatten()
    |> Enum.reduce([], &reduce_entries/2)
    |> Enum.join("\n")
  end

  defp reduce_entries(entry, policy_entries) do
    case entry do
      {:PolicyInformation, oid, :asn1_NOVALUE} ->
        ["Policy: #{format_oid(oid)}" | policy_entries]

      {:PolicyInformation, oid, policy_information} ->
        oid_string = "Policy: #{format_oid(oid)}"

        message = [oid_string]

        policy_information
        |> Enum.reduce(message, &reduce_policy_information/2)
        |> Enum.reverse()
    end
  end

  defp format_oid(oid) do
    oid
    |> Tuple.to_list()
    |> Enum.join(".")
  end

  defp reduce_policy_information(policy, message) do
    case policy do
      {:PolicyQualifierInfo, {1, 3, 6, 1, 5, 5, 7, 2, 1}, cps_data} ->
        cps_string = format_cps(cps_data)

        [cps_string | message]

      {:PolicyQualifierInfo, {1, 3, 6, 1, 5, 5, 7, 2, 2}, user_notice_data} ->
        user_notice = format_user_notice(user_notice_data)

        [user_notice | message]
    end
  end

  defp format_cps(cps_data) do
    cps_data
    |> to_charlist
    |> Enum.drop(2)
    |> to_string
    |> String.replace_prefix("", "  CPS: ")
  end

  defp format_user_notice(user_notice_data) do
    <<_::binary-size(8), user_notice::binary>> = user_notice_data

    user_notice
    |> String.codepoints()
    |> Enum.filter(&String.printable?/1)
    |> Enum.join("")
    |> String.replace_prefix("", "  User Notice: ")
  end

  defp formatted_distribution_points(crl_distribution_points) do
    crl_distribution_points
    |> Enum.reduce([], &reduce_distribution_points/2)
    |> Enum.join("\n")
  end

  defp reduce_distribution_points(distro_point, output) do
    case distro_point do
      {:DistributionPoint, {:fullName, crls}, :asn1_NOVALUE, :asn1_NOVALUE} ->
        crl_string = formatted_crls(crls)

        output = ["Full Name:" | output]
        output = [crl_string | output]

        output
        |> Enum.reverse()

      _ ->
        Logger.error("Unhandled CRL distribution point #{inspect(distro_point)}")
        output
    end
  end

  defp formatted_crls(crls) do
    crls
    |> Enum.map_join("\n", &format_identifier/1)
  end

  defp format_identifier(identifier) do
    case identifier do
      {:uniformResourceIdentifier, uri} -> " URI:#{uri}"
      {:rfc822Name, identifier} -> " RFC 822 Name: #{identifier}"
      # Just skip this for now, not commonly used.
      {:directoryName, _rdn_sequence} -> ""
    end
  end
end
