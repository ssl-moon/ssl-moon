defmodule SslMoon.Ssl.SslClientImpl do
  @moduledoc """
    Ssl client implementation. Currently using erlang ssl as the client.

    WARNING: Do not use this module directly, instead use functions from SslMoon.Ssl.SslClient.
  """
  @behaviour SslMoon.Ssl.SslClient

  require Logger
  alias SslMoon.Ssl.CertificateCollector

  @impl SslMoon.Ssl.SslClient
  def connect(host, port, params) do
    host = to_charlist(host)
    :ssl.connect(host, port, params)
  end

  @impl SslMoon.Ssl.SslClient
  def peercert(socket) do
    :ssl.peercert(socket)
  end

  @impl SslMoon.Ssl.SslClient
  def close(socket) do
    :ssl.close(socket)
  end

  @impl SslMoon.Ssl.SslClient
  def get_chain_certificates(domain) when is_binary(domain) do
    domain = to_charlist(domain)

    with {:ok, pid} <- CertificateCollector.start_link(),
         {:ok, socket} <-
           :ssl.connect(
             domain,
             443,
             [
               verify: :verify_none,
               verify_fun: {&verify_fun/3, pid},
               cacertfile: to_charlist(ca_file_location())
             ],
             5000
             # certificate_authorities: true
           ),
         :ok <- :ssl.close(socket) do
      {:ok, CertificateCollector.get_certificates(pid)}
    end
  end

  defp verify_fun(cert, valid, pid) when valid in [:valid, :valid_peer] do
    CertificateCollector.add_certificate(pid, cert, :valid)

    {:valid, pid}
  end

  defp verify_fun(cert, {:bad_cert, :selfsigned_peer}, pid) do
    CertificateCollector.add_certificate(pid, cert, :selfsigned_peer)

    {:fail, :selfsigned_peer}
  end

  defp verify_fun(cert, {:bad_cert, reason}, pid) do
    CertificateCollector.add_certificate(pid, cert, reason)

    {:valid, pid}
  end

  defp verify_fun(_param1, {:extension, _}, pid) do
    {:valid, pid}
  end

  defp ca_file_location do
    priv_dir = :code.priv_dir(:ssl_moon) |> List.to_string()
    Path.join([priv_dir, "certs", "ca-bundle.crt"])
  end
end
