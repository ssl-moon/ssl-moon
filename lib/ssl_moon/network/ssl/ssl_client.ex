defmodule SslMoon.Ssl.SslClient do
  @moduledoc """
    Ssl client. Configurable via:

      config :ssl_moon, ssl_client: SslMoon.Ssl.SslClientImpl

    Currently default configuration uses the SslMoon.Ssl.SslClientImpl module.
  """
  @callback connect(host :: String.t(), port :: integer(), params :: keyword()) ::
              {:ok, any()} | {:error, any()} | {:option_not_a_key_value_tuple, any()}
  @callback peercert(socket :: any()) :: {:ok, binary()} | {:error, any()}
  @callback close(socket :: any()) :: :ok
  @callback get_chain_certificates(domain :: String.t()) ::
              {:ok, list(%{cert: map(), status: {:valid, atom()} | {:bad_cert, atom()}})}
              | {:error, any()}

  @behaviour __MODULE__

  defp ssl_client() do
    Application.get_env(:ssl_moon, :ssl_client, SslMoon.Ssl.SslClientImpl)
  end

  @impl SslMoon.Ssl.SslClient
  def connect(host, port, params) do
    ssl_client().connect(host, port, params)
  end

  @impl SslMoon.Ssl.SslClient
  def peercert(socket) do
    ssl_client().peercert(socket)
  end

  @impl SslMoon.Ssl.SslClient
  def close(socket) do
    ssl_client().close(socket)
  end

  @impl SslMoon.Ssl.SslClient
  def get_chain_certificates(domain) do
    ssl_client().get_chain_certificates(domain)
  end
end
