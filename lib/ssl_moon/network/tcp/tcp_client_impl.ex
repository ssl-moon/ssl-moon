defmodule SslMoon.Tcp.TcpClientImpl do
  @moduledoc """
    Tcp client implementation. Currently using erlang ssl as the client.

    WARNING: Do not use this module directly, instead use functions from SslMoon.Tcp.TcpClient.
  """
  @behaviour SslMoon.Tcp.TcpClient

  @impl SslMoon.Tcp.TcpClient
  def connect(address, port, opts, timeout) do
    address = to_charlist(address)
    :gen_tcp.connect(address, port, opts, timeout)
  end

  @impl SslMoon.Tcp.TcpClient
  def close(socket) do
    :gen_tcp.close(socket)
  end
end
