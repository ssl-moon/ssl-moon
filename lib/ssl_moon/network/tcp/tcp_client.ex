defmodule SslMoon.Tcp.TcpClient do
  @moduledoc """
    Tcp client. Configurable via:

      config :ssl_moon, tcp_client: SslMoon.Tcp.TcpClientImpl

    Currently default configuration uses the SslMoon.Tcp.TcpClientImpl module.
  """

  @callback connect(
              address :: String.t(),
              port :: integer(),
              opts :: keyword(),
              timeout :: timeout()
            ) ::
              {:ok, any()} | {:error, any()}

  @callback close(socket :: any()) :: :ok

  @behaviour __MODULE__

  def tcp_client() do
    Application.get_env(:ssl_moon, :tcp_client, SslMoon.Tcp.TcpClientImpl)
  end

  @impl SslMoon.Tcp.TcpClient
  def connect(address, port, opts, timeout) do
    tcp_client().connect(address, port, opts, timeout)
  end

  @impl SslMoon.Tcp.TcpClient
  def close(socket) do
    tcp_client().close(socket)
  end
end
