defmodule SslMoon.Dns.DnsResponse do
  @moduledoc """
    Struct that contains result of a dns query.
  """
  defstruct [:domain, :type, :class, :ttl, :data]

  @type t :: %__MODULE__{}
end
