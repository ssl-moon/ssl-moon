defmodule SslMoon.Dns.DnsHelper do
  @moduledoc false

  @spec parse_response(SslMoon.Dns.DnsClient.dns_record_type(), String.t()) ::
          {:ok, map()} | {:error, any()}
  def parse_response(type, data) do
    parse_record(type, data)
  end

  defp parse_record(:caa, data) do
    [flags, type, authority] =
      data
      |> String.trim()
      |> String.replace("\"", "")
      |> String.split(" ")

    {flags, ""} = Integer.parse(flags)

    resp = %{
      flags: flags,
      type: type,
      authority: authority
    }

    {:ok, resp}
  end

  defp parse_record(_, _) do
    {:error, :not_implemented}
  end
end
