defmodule SslMoon.Dns.DnsClientImpl do
  @moduledoc """
    Dns client implementation. Currently using dig as external command for dns queries.

    WARNING: Do not use this module directly, instead use functions from SslMoon.Dns.DnsClient.
  """
  @behaviour SslMoon.Dns.DnsClient

  @command "dig"
  @default_args ["+noall", "+answer"]

  @impl SslMoon.Dns.DnsClient
  def resolve(name, type, opts \\ []) do
    dnssec_opt = Keyword.get(opts, :dnssec, false)

    args = add_arg([], :dnssec, dnssec_opt)

    args = @default_args ++ args ++ [type_to_string(type), name]

    case exec_cmd(args) do
      {:ok, resp} -> {:ok, parse_answer(resp)}
      other -> other
    end
  end

  @impl SslMoon.Dns.DnsClient
  def nslookup(name) do
    args = @default_args ++ [name, "NS"]

    case exec_cmd(args) do
      {:ok, resp} -> {:ok, parse_answer(resp)}
      other -> other
    end
  end

  @impl SslMoon.Dns.DnsClient
  def getaddrs(host, :inet) do
    alookup(host, "A")
  end

  def getaddrs(host, :inet6) do
    alookup(host, "AAAA")
  end

  defp alookup(host, class) do
    args = @default_args ++ [host, class]

    case exec_cmd(args) do
      {:ok, resp} ->
        {:ok,
         resp
         |> parse_answer()
         |> Enum.map(fn el -> el.data end)}

      other ->
        other
    end
  end

  defp exec_cmd(args) do
    case System.cmd(@command, args) do
      {response, 0} -> {:ok, response}
      {error, _other} -> {:error, error}
    end
  end

  defp parse_answer(dns_answers) when is_binary(dns_answers) and byte_size(dns_answers) > 0 do
    dns_answers = dns_answers |> String.trim() |> String.split("\n")

    Enum.map(dns_answers, fn dns_answer ->
      [name, ttl, dns_class, dns_record_type, data] =
        Regex.split(~R"[ \t]+", String.trim(dns_answer), parts: 5)

      %SslMoon.Dns.DnsResponse{
        domain: name,
        type: to_atom(dns_record_type),
        class: to_atom(dns_class),
        ttl: ttl,
        data: data
      }
    end)
  end

  defp parse_answer(_other) do
    []
  end

  defp to_atom(string) when is_binary(string) do
    string
    |> String.downcase()
    |> String.to_existing_atom()
  end

  defp type_to_string(type) when is_atom(type) do
    type
    |> Atom.to_string()
    |> String.upcase()
  end

  defp add_arg(args, :dnssec, true), do: args ++ ["+dnssec"]
  defp add_arg(args, :dnssec, false), do: args ++ ["+nodnssec"]
end
