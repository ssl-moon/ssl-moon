defmodule SslMoon.Dns.DnsClient do
  @moduledoc """
    Dns client. Configurable via:

      config :ssl_moon, dns_client: SslMoon.Dns.DnsClientImpl

    Currently default configuration uses the SslMoon.Dns.DnsClientImpl module.
  """
  @callback nslookup(name :: dns_name() | dns_ip()) :: {:ok, dns_answer()} | {:error, any()}

  @callback resolve(
              name :: dns_name() | dns_ip(),
              type :: dns_record_type(),
              opts :: dns_opts()
            ) :: {:ok, dns_answer()} | {:error, any()}

  @callback getaddrs(
              host :: dns_name() | dns_ip(),
              family :: address_family()
            ) :: {:ok, [ip_address()]} | {:error, any()}

  @behaviour SslMoon.Dns.DnsClient

  defp dns_client() do
    Application.get_env(:ssl_moon, :dns_client, SslMoon.Dns.DnsClientImpl)
  end

  @impl SslMoon.Dns.DnsClient
  def resolve(name, type, opts \\ []) do
    dns_client().resolve(name, type, opts)
  end

  @impl SslMoon.Dns.DnsClient
  def nslookup(name) do
    dns_client().nslookup(name)
  end

  @impl SslMoon.Dns.DnsClient
  def getaddrs(host, family) do
    dns_client().getaddrs(host, family)
  end

  @type address_family() :: :inet | :inet6
  @type ip_address() :: String.t()

  @type dns_answer() :: [SslMoon.Dns.DnsResponse.t()]

  @type dns_opts() :: [
          {:dnssec, boolean()}
        ]
  @type dns_name() :: String.t()
  @type dns_ip() :: String.t()

  @type dns_class() :: :in | :any
  @type dns_record_type() ::
          :a
          | :aaaa
          | :afsdb
          | :apl
          | :caa
          | :cdnskey
          | :cds
          | :cert
          | :cname
          | :csync
          | :dhcid
          | :dlv
          | :dname
          | :dnskey
          | :ds
          | :eui48
          | :eui64
          | :hinfo
          | :hip
          | :https
          | :ipseckey
          | :key
          | :kx
          | :loc
          | :mx
          | :naptr
          | :ns
          | :nsec
          | :nsec3
          | :nsec3param
          | :openpgpkey
          | :ptr
          | :rrsig
          | :rp
          | :sig
          | :smimea
          | :soa
          | :srv
          | :sshfp
          | :svcb
          | :ta
          | :tkey
          | :tlsa
          | :tsig
          | :txt
          | :uri
          | :zonemd

  def record_atoms() do
    [
      :a,
      :aaaa,
      :afsdb,
      :apl,
      :caa,
      :cdnskey,
      :cds,
      :cert,
      :cname,
      :csync,
      :dhcid,
      :dlv,
      :dname,
      :dnskey,
      :ds,
      :eui48,
      :eui64,
      :hinfo,
      :hip,
      :https,
      :ipseckey,
      :key,
      :kx,
      :loc,
      :mx,
      :naptr,
      :ns,
      :nsec,
      :nsec3,
      :nsec3param,
      :openpgpkey,
      :ptr,
      :rrsig,
      :rp,
      :sig,
      :smimea,
      :soa,
      :srv,
      :sshfp,
      :svcb,
      :ta,
      :tkey,
      :tlsa,
      :tsig,
      :txt,
      :uri,
      :zonemd
    ]
  end
end
