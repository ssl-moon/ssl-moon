defmodule SslMoon.Mailer do
  @moduledoc """
  Mailer module.
  """
  use Swoosh.Mailer, otp_app: :ssl_moon
end
