defmodule SslMoon.ApiSpec do
  @moduledoc """
  The spec module for the SSL Moon API.
  This module is responsible for generating the OpenAPI spec about the current application.
  """
  alias OpenApiSpex.{OpenApi, Server, Info, Paths}
  @behaviour OpenApi

  @impl OpenApi
  def spec do
    %OpenApi{
      servers: [
        # Populate the Server info from a phoenix endpoint
        %Server{url: "http://localhost:4000/"}
      ],
      info: %Info{
        title: "SSL Moon API",
        version: "1.0"
      },
      # populate the paths from a phoenix router
      paths: Paths.from_router(SslMoonWeb.Router)
    }
    # discover request/response schemas from path specs
    |> OpenApiSpex.resolve_schema_modules()
  end
end
