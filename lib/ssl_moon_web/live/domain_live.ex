defmodule SslMoonWeb.DomainLive do
  use SslMoonWeb, :live_view

  alias SslMoon.HostsAndPaths
  alias SslMoon.HostPathsRepo
  alias SslMoon.Checks

  def mount(%{"id" => id, "check_name" => check_name}, _session, socket) do
    check = Checks.get_check_by_name(check_name)
    domain = HostsAndPaths.get_host_id(id)

    with false <- is_nil(check),
         false <- is_nil(domain) do
      %{path: %{id: path_id}} = domain = HostPathsRepo.preload(domain, :path)
      {:ok, path_data} = HostsAndPaths.get_path(path_id)
      path = String.split(path_data.path, "/")

      {:ok,
       assign(socket,
         domain: domain,
         path_data: path_data,
         path: path,
         check: check
       )}
    else
      _ -> raise SslMoonWeb.DomainPageController.InvalidIdError
    end
  end

  def handle_params(%{"check_name" => check_name}, _uri, socket) do
    check = Checks.get_check_by_name(check_name)

    if check == nil do
      raise SslMoonWeb.DomainPageController.InvalidIdError
    end

    {:noreply, assign(socket, check: check)}
  end

  def render(assigns) do
    ~H"""
    <div class="flex columns-2">
      <div class="drawer lg:drawer-open">
        <input id="ssl-moon-drawer" type="checkbox" class="drawer-toggle" />
        <div class="drawer-content flex w-screen lg:w-auto">
          <%!-- Page content here --%>
          <div class="hidden lg:block">
            <.live_component
              id="check_list_menu"
              module={ChecksListComponent}
              domain_id={@domain.id}
              check_name={@check.name}
            />
          </div>
          <div class="p-4 w-full">
            <div>
              <div class="lg:hidden collapse bg-base-200 w-full collapse-arrow">
                <input type="checkbox" />
                <div class="collapse-title text-center">
                  Check List
                </div>
                <div class="collapse-content">
                  <.live_component
                    id="check_list_menu_collapse"
                    module={ChecksListComponent}
                    domain_id={@domain.id}
                    check_name={@check.name}
                  />
                </div>
              </div>
            </div>
            <div class="p-4 overflow-auto">
              <SslMoonWeb.Components.Paths.path_breadcrumb
                path={@path}
                last_path={@domain.domain_name}
              />
            </div>
            <h1>
              <%= Checks.render_latest_check(@check, @domain.domain_name) %>
            </h1>
          </div>
        </div>
        <div class="drawer-side z-10">
          <label for="ssl-moon-drawer" aria-label="close sidebar" class="drawer-overlay"></label>
          <ul class="p-2 w-80 min-h-full bg-base-200 text-base-content">
            <!-- Drawer content here -->
            <.live_component module={TreeViewComponent} id="tree_view" path={@path} />
          </ul>
        </div>
      </div>
    </div>
    """
  end

  def render_check(assigns) do
    ~H"""
    <.check_live_component
      module={@check.template.module}
      id={@check.name}
      check_identifier={@check.name}
      domain_name={@domain_name}
    />
    """
  end
end
