defmodule SslMoonWeb.RootDomainLive do
  use SslMoonWeb, :live_view
  alias SslMoon.HostsAndPaths
  alias SslMoon.HostPathsRepo

  def mount(%{"id" => id}, _assigns, socket) do
    case HostsAndPaths.get_host_id(id) do
      nil ->
        raise SslMoonWeb.DomainPageController.InvalidIdError

      domain ->
        %{path: %{id: path_id}} = domain = HostPathsRepo.preload(domain, :path)
        {:ok, path_data} = HostsAndPaths.get_path(path_id)
        path = String.split(path_data.path, "/")

        {:ok,
         assign(socket,
           domain: domain,
           path_data: path_data,
           path: path
         )}
    end
  end
end

defmodule SslMoonWeb.DomainPageController.InvalidIdError do
  defexception message: "id not found", plug_status: 404
end
