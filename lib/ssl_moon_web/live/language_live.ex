defmodule SslMoonWeb.LanguageLive do
  use SslMoonWeb, :live_view
  alias SslMoon.HostsAndPaths
  alias SslMoon.Storage.Checks

  @locales Gettext.known_locales(SslMoonWeb.Gettext)

  def mount(_assigns, _session, socket) do
    {:ok,
     socket
     |> assign(
       path: [],
       total_hosts: HostsAndPaths.get_host_count(),
       total_checks: Checks.get_checks_count()
     )
     |> assign(locales: @locales, locale: Gettext.get_locale(SslMoonWeb.Gettext))}
  end
end
