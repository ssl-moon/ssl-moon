defmodule SslMoonWeb.DashboardLive do
  use SslMoonWeb, :live_view

  alias SslMoon.HostsAndPaths
  alias SslMoon.Storage.Checks

  @status_priority %{
    success: 4,
    warning: 1,
    inactive: 5,
    failure: 3,
    error: 2
  }
  @items_per_page 10

  def handle_params(%{"path" => path} = params, url, socket) do
    page =
      case params["page"] do
        nil ->
          1

        page ->
          {page, ""} = Integer.parse(page)
          page
      end

    {:ok, path_data} =
      path
      |> Enum.join("/")
      |> HostsAndPaths.get_path()

    domains = HostsAndPaths.get_domains(path_data.id, @items_per_page, page)

    domains =
      Enum.map(domains, fn el ->
        checks = Checks.get_domain_checks(el.domain_name)
        checks_status = Enum.map(checks, fn el -> {el.check_name, el.status} end)
        Map.put(el, :checks_status, checks_status)
      end)

    {:noreply,
     socket
     |> assign(
       page: page,
       total_pages: calculate_pages(path_data.id),
       path: path,
       path_data: path_data,
       domains: domains,
       cheks_name: SslMoon.DefaultChecksRouter.checks(),
       current_url: url
       #  page_type: get_page_type(path, socket.assigns.tree_data)
     )}
  end

  def mount(_assigns, _session, socket) do
    {:ok, socket}
  end

  defp remove_last_element(list) do
    list
    |> :lists.reverse()
    |> tl()
    |> :lists.reverse()
  end

  # defp get_page_type(current_path, tree_data) do
  #   current_path = Enum.join(current_path, "/")
  #   entry = Enum.find(tree_data, fn el -> current_path == el.path end)

  #   case entry.host_count do
  #     0 -> :no_hosts
  #     _other -> :hosts
  #   end
  # end

  defp get_check_status(checks_status, check_name) do
    {_, status} =
      Enum.find(checks_status, {"", :not_found}, fn {name, _} -> name == check_name end)

    status
  end

  defp calculate_security_percentage(checks_status) do
    errors_count = Enum.count(checks_status, fn {_, status} -> status == :error end)
    checks_count = Enum.count(checks_status)

    if checks_count > 0 do
      (100 - errors_count * 100 / Enum.count(checks_status)) |> trunc()
    else
      100
    end
  end

  defp percentage_color(percentage) do
    cond do
      percentage >= 80 -> "text-success"
      percentage < 80 and percentage > 40 -> "text-warning"
      percentage <= 40 -> "text-error"
    end
  end

  def handle_event("sort_changed", %{"value" => "domain_name"}, socket) do
    sorted = Enum.sort(socket.assigns.domains, &compare_by_domain_name(&1, &2))

    {:noreply,
     socket
     |> assign(
       path: socket.assigns.path,
       path_data: socket.assigns.path_data,
       domains: sorted,
       cheks_name: SslMoon.DefaultChecksRouter.checks()
     )}
  end

  def handle_event("sort_changed", %{"value" => value}, socket) do
    sorted = sort_by_check_status(socket.assigns.domains, value)

    {:noreply,
     socket
     |> assign(
       page: socket.assigns.page,
       total_pages: socket.assigns.total_pages,
       path: socket.assigns.path,
       path_data: socket.assigns.path_data,
       domains: sorted,
       cheks_name: SslMoon.DefaultChecksRouter.checks()
     )}
  end

  def handle_event("page_change", %{"to" => value}, socket) do
    uri = URI.parse(socket.assigns.current_url)

    # Form a local path
    local_url =
      URI.parse(uri.path)
      |> Map.put(:query, URI.encode_query(%{"page" => value}))
      |> URI.to_string()

    {:noreply, push_navigate(socket, to: local_url)}
  end

  defp sort_by_check_status(hosts, check_name) do
    Enum.sort(hosts, &compare_by_check_status(&1, &2, check_name))
  end

  defp compare_by_check_status(host1, host2, check_name) do
    status1 = get_check_status_by_order(host1.checks_status, check_name)
    status2 = get_check_status_by_order(host2.checks_status, check_name)

    @status_priority[status1] <= @status_priority[status2]
  end

  defp compare_by_domain_name(host1, host2) do
    host1.domain_name <= host2.domain_name
  end

  defp get_check_status_by_order(checks_status, check_name) do
    checks_status
    |> Enum.find({"", :not_found}, fn {name, _} -> name == check_name end)
    |> elem(1)
  end

  def calculate_pages(path_data_id) do
    total_items = HostsAndPaths.get_domains_count(path_data_id)
    pages = div(total_items, @items_per_page)
    remainder = rem(total_items, @items_per_page)

    if remainder > 0 do
      pages + 1
    else
      pages
    end
  end
end
