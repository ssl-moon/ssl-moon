defmodule SslMoonWeb.RootDashboardLive do
  use SslMoonWeb, :live_view

  alias SslMoon.HostsAndPaths
  alias SslMoon.Storage.Checks

  def mount(_assigns, _session, socket) do
    {:ok,
     socket
     |> assign(
       path: [],
       total_hosts: HostsAndPaths.get_host_count(),
       total_checks: Checks.get_checks_count()
     )}
  end
end
