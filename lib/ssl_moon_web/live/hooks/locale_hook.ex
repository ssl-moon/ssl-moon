defmodule SslMoonWeb.RestoreLocale do
  @moduledoc """
  Module for on_mount hook to automatically restore the locale for every LiveView in application
  """
  def on_mount(:default, _params, session, socket) do
    Gettext.put_locale(SslMoonWeb.Gettext, session["locale"])
    {:cont, socket}
  end
end
