defmodule SslMoonWeb.Components.Paths do
  @moduledoc """
    Components related to paths.
  """
  use SslMoonWeb, :html

  attr :path, :list, required: true
  attr :last_path, :string, required: true

  def path_breadcrumb(assigns) do
    ~H"""
    <div class="text-sm breadcrumbs">
        <ul>
          <%= for {path, full_path} <- form_full_path(@path) do %>
            <li>
              <a href={"/dashboards/#{full_path}"} class="link-primary"><%= path %></a>
            </li>
          <% end %>
          <li>
          <%= @last_path %>
          </li>
        </ul>
      </div>
    """
  end

  defp form_full_path(path) do
    Enum.reduce(path, [], fn el, acc ->
      case acc do
        [] ->
          [{el, el}]

        _other ->
          {_el, path} = hd(acc)
          [{el, "#{path}/#{el}"} | acc]
      end
    end)
    |> Enum.reverse()
  end
end
