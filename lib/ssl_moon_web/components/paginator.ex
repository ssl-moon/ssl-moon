defmodule SslMoonWeb.Components.Pagination do
  @moduledoc """
    Module containing components related to pagination.
  """
  use Phoenix.Component
  import SslMoonWeb.Gettext

  @doc """
    Component that displays a page changer.

    Requires the current page and the total number of pages. On button click
    a phx-click liveview event is fired with the value "to" and default event "page_change".
  """

  attr :total_pages, :integer, required: true
  attr :page, :integer, required: true
  attr :on_click_name, :string, default: "page_change"

  def pager(assigns) do
    ~H"""
    <div class="flex justify-center mt-4">
      <div class="join">
        <%= if @page > 1 do %>
          <button phx-click="page_change" phx-value-to={@page - 1} class="join-item btn">
            «
          </button>
        <% else %>
          <button class="join-item btn btn-disabled">«</button>
        <% end %>
        <button phx-click="page_change" phx-value-to={@page} class="join-item btn">
          <%= gettext("Page") %> <%= @page %>
        </button>
        <%= if @page < @total_pages do %>
          <button phx-click="page_change" phx-value-to={@page + 1} class="join-item btn">
            »
          </button>
        <% else %>
          <button class="join-item btn btn-disabled">»</button>
        <% end %>
      </div>
    </div>
    """
  end
end
