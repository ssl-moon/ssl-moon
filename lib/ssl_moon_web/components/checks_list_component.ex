defmodule ChecksListComponent do
  @moduledoc """
  The `ChecksListComponent` module renders the checks list for a domain.
  """
  use SslMoonWeb, :live_component

  alias SslMoon.DefaultChecksRouter

  def mount(socket) do
    checks = DefaultChecksRouter.checks()

    {:ok, assign(socket, checks: checks)}
  end

  attr :domain_id, :integer, required: true
  attr :check_name, :string, default: ""

  @doc """
  Renders the template for the checks.
  """
  def render(assigns) do
    ~H"""
    <div class="menu rounded-box min-w-max">
      <ul>
        <h2 class="menu-title p-1 m-1"><%= gettext("Domain Name System") %></h2>
        <li>
          <%= for check <- checks_starting_with(@checks, "domain_name_system") do %>
            <%= if check.template.opts.show do %>
              <.link
                patch={~p"/domains/#{@domain_id}/#{check.name}"}
                class={if check.name == @check_name, do: "active"}
              >
                <%= check.display_name %>
              </.link>
            <% end %>
          <% end %>
        </li>
      </ul>
      <ul>
        <h2 class="menu-title p-1 m-1 "><%= gettext("Protocols") %></h2>
        <li>
          <%= for check <- checks_starting_with(@checks, "protocols") do %>
            <%= if check.template.opts.show do %>
              <.link
                patch={~p"/domains/#{@domain_id}/#{check.name}"}
                class={if check.name == @check_name, do: "active"}
              >
                <%= check.display_name %>
              </.link>
            <% end %>
          <% end %>
        </li>
        <h2 class="menu-title p-1 m-1 "><%= gettext("Secure Transport") %></h2>
        <li>
          <%= for check <- checks_starting_with(@checks, "secure_transport") do %>
            <%= if check.template.opts.show do %>
              <.link
                patch={~p"/domains/#{@domain_id}/#{check.name}"}
                class={if check.name == @check_name, do: "active"}
              >
                <%= check.display_name %>
              </.link>
            <% end %>
          <% end %>
        </li>
        <h2 class="menu-title p-1 m-1 "><%= gettext("Modern Security Features") %></h2>
        <li>
          <%= for check <- checks_starting_with(@checks, "modern_security_features") do %>
            <%= if check.template.opts.show do %>
              <.link
                patch={~p"/domains/#{@domain_id}/#{check.name}"}
                class={if check.name == @check_name, do: "active"}
              >
                <%= check.display_name %>
              </.link>
            <% end %>
          <% end %>
        </li>
        <h2 class="menu-title p-1 m-1 "><%= gettext("Application Security") %></h2>
        <li>
          <%= for check <- checks_starting_with(@checks, "application_secutity") do %>
            <%= if check.template.opts.show do %>
              <.link
                patch={~p"/domains/#{@domain_id}/#{check.name}"}
                class={if check.name == @check_name, do: "active"}
              >
                <%= check.display_name %>
              </.link>
            <% end %>
          <% end %>
        </li>
      </ul>
    </div>
    """
  end

  defp checks_starting_with(checks, string) do
    Enum.filter(checks, fn %{name: name} -> String.starts_with?(name, string) end)
  end
end
