defmodule SslMoonWeb.Layouts do
  @moduledoc """
  Layouts component.
  """
  use SslMoonWeb, :html

  embed_templates "layouts/*"

  def current_lang() do
    Gettext.get_locale(SslMoonWeb.Gettext)
  end
end
