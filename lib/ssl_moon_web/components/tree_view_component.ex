defmodule TreeViewComponent do
  @moduledoc """
  The `TreeViewComponent` module provides functions to render a tree view.
  """
  use SslMoonWeb, :live_component
  alias SslMoon.HostsAndPaths

  @doc """
  Updates the tree view.
  """

  def update(assigns, socket) do
    path = assigns[:path] || []
    url_path = Enum.join(path, "/")

    tree_data =
      HostsAndPaths.get_folders_with_host_count()
      |> HostsAndPaths.to_nested_structure()
      |> init_flags()

    {:ok,
     socket
     |> assign_url(url_path)
     |> assign_url_state(url_path, tree_data)}
  end

  defp assign_url(socket, url_path) do
    if socket.assigns[:url_path] == nil do
      assign(socket, :url_path, url_path)
    else
      socket
    end
  end

  def assign_url_state(socket, url_path, tree_data) do
    if socket.assigns[:tree_data] !== nil do
      new_tree_data = set_state_from_url(tree_data, url_path)

      assign(socket, :tree_data, new_tree_data)
    else
      socket
    end
  end

  def set_state_from_url(tree_data, url_path) do
    new_tree_data = tree_data
    Enum.map(new_tree_data, &check_path_url(&1, url_path))
  end

  defp check_path_url(item, url_path) do
    %{path: path, child_paths: child_paths} = item

    updated_item =
      cond do
        url_path == path ->
          item
          |> Map.update!(:selected, fn _ -> true end)
          |> Map.update!(:rotated, fn _ -> true end)

        String.contains?(url_path, path) ->
          Map.update!(item, :rotated, fn _ -> true end)

        true ->
          Map.update!(item, :selected, fn _ -> false end)
      end

    updated_child_paths = set_state_from_url(child_paths, url_path)
    Map.put(updated_item, :child_paths, updated_child_paths)
  end

  def mount(socket) do
    tree_data =
      HostsAndPaths.get_folders_with_host_count()
      |> HostsAndPaths.to_nested_structure()
      |> init_flags()

    {:ok, assign(socket, :tree_data, tree_data)}
  end

  defp init_flags([]), do: []

  defp init_flags(list) do
    Enum.map(list, fn map ->
      map
      |> Map.put_new(:rotated, false)
      |> Map.put_new(:selected, false)
      |> Map.update!(:child_paths, &init_flags(&1))
    end)
  end

  @doc """
  Renders a tree view.
  """
  def render(assigns) do
    ~H"""
    <ul>
      <%= render_tree(assigns) %>
    </ul>
    """
  end

  defp render_tree(assigns) do
    ~H"""
    <%= for item <- @tree_data do %>
      <li class="pl-4 w-full">
        <%= case {item.child_paths, item.host_count} do %>
          <% {[],0} -> %>
            <%= render_empty_path(%{
              id: item.id,
              name: item.name,
              path: item.path,
              selected: item.selected,
              rotated: item.rotated,
              myself: @myself
            }) %>
          <% {childrens,0} -> %>
            <%= render_folder(%{
              childrens: childrens,
              hosts: 0,
              id: item.id,
              name: item.name,
              path: item.path,
              selected: item.selected,
              rotated: item.rotated,
              myself: @myself
            }) %>
          <% {[],hosts} -> %>
            <%= render_hosts(%{
              hosts: hosts,
              id: item.id,
              name: item.name,
              path: item.path,
              selected: item.selected,
              rotated: item.rotated,
              myself: @myself
            }) %>
          <% {childrens,hosts} -> %>
            <%= render_folder(%{
              childrens: childrens,
              hosts: hosts,
              id: item.id,
              name: item.name,
              path: item.path,
              selected: item.selected,
              rotated: item.rotated,
              myself: @myself
            }) %>
        <% end %>
      </li>
    <% end %>
    """
  end

  def render_empty_path(assigns) do
    ~H"""
    <span class="flex pl-4">
      <img src={~p"/images/folder_icon.svg"} class="h-6 w-6" alt=" Folder Icon" />
      <.link
        class=" flex items-center hover:bg-blue-100 focus:text-primary active:text-blue"
        navigate={"/dashboards/#{@path}"}
      >
      </.link>
      <%= @name %>
    </span>
    """
  end

  def render_hosts(assigns) do
    ~H"""
    <div class={[if(@selected, do: "bg-blue-300"), "hover:bg-blue-100"]}>
      <div class="flex w-full pl-4 pr-2">
        <.link class="flex w-full flex justify-between" patch={"/dashboards/#{@path}"}>
          <span class="flex">
            <img src={~p"/images/file_icon.svg"} class="h-6 w-6" alt="File Icon" /> <%= @name %>
          </span>
          <%= @hosts %>
        </.link>
      </div>
    </div>
    """
  end

  def render_folder(assigns) do
    ~H"""
    <div class={[if(@selected, do: "bg-blue-300"), "hover:bg-blue-100"]}>
      <div class="flex">
        <button phx-click="toggle" phx-target={@myself} phx-value-id={@id}>
          <img
            src={~p"/images/chevron-right.svg"}
            alt="Chevron Right Icon"
            class={[
              if(@rotated, do: "rotate-90", else: "rotate-0"),
              "h-4",
              "w-4"
            ]}
            style="transition: transform 0.2s ease-in-out;"
          />
        </button>
        <.link patch={"/dashboards/#{@path}"} role="button" class="flex pl-2 w-full">
          <%= if @hosts==0 do %>
            <img src={~p"/images/folder_icon.svg"} class="h-6 w-6" alt="Folder Icon" />
          <% else %>
            <img
              src={~p"/images/folder_with_files_icon.svg"}
              class="h-6 w-6"
              alt="Folder With Files Icon"
            />
          <% end %>

          <%= if @hosts>0 do %>
            <div class="flex w-full pr-2">
              <span><%= @name %></span> <span class="grow"></span><span><%= @hosts %></span>
            </div>
          <% else %>
            <%= @name %>
          <% end %>
        </.link>
      </div>
    </div>
    <ul id={@name} class={if !@rotated, do: "hidden"}>
      <%= render_tree(%{tree_data: @childrens, myself: @myself}) %>
    </ul>
    """
  end

  def handle_event("toggle", %{"id" => id}, socket) do
    parsed_id = String.to_integer(id)
    new_tree_data = toggle_rotated(socket.assigns.tree_data, parsed_id)
    {:noreply, socket |> assign(tree_data: new_tree_data)}
  end

  defp toggle_rotated(data, id) do
    Enum.map(data, &check_id_toggle(&1, id))
  end

  def check_id_toggle(%{id: id} = item, id) do
    Map.update!(item, :rotated, fn current_value -> not current_value end)
  end

  def check_id_toggle(%{child_paths: child_paths} = item, id) do
    Map.put(item, :child_paths, toggle_rotated(child_paths, id))
  end
end
