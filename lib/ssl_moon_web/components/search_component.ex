defmodule SearchComponent do
  @moduledoc """
  The `SearchComponent` module provides functions to render a tree view.
  """
  use SslMoonWeb, :live_component
  alias SslMoon.HostsAndPaths

  @impl true
  def render(assigns) do
    ~H"""
    <div class="text-base-content">
      <div class="dropdown dropdown-bottom relative hidden md:block">
        <input
          type="search"
          class="input input-bordered w-full"
          placeholder={gettext("Type to search...")}
          type="search"
          role="combobox"
          aria-expanded="false"
          aria-controls="options"
          value={@search}
          phx-target={@myself}
          phx-keyup="do-search"
          phx-debounce="200"
        />
        <ul
          tabindex="0"
          class="dropdown-content absolute z-10 shadow bg-base-100 rounded-md w-full mt-1 max-h-60 overflow-auto"
        >
          <.link
            :for={host <- @hosts_result}
            navigate={~p"/domains/#{host.id}"}
            id={"host-#{host.domain_name}"}
          >
            <li
              class="cursor-pointer select-none rounded-md px-4 py-2 text-base bg-zinc-100 hover:bg-primary hover:text-white flex flex-row items-center space-x-2"
              id={"option-#{host.domain_name}"}
              role="option"
              tabindex="-1"
            >
              <MaterialIcons.link style="outlined" size={42} class="fill-black" />
              <div class="font-medium truncate">
                <%= host.domain_name %>
              </div>
            </li>
          </.link>

          <.link
            :for={path <- @paths_result}
            navigate={~p"/dashboards/#{path.path}"}
            id={"path-#{path.name}"}
          >
            <li
              class="cursor-pointer select-none rounded-md px-4 py-2 text-base bg-zinc-100 hover:bg-zinc-800 hover:text-white flex flex-row items-center space-x-2"
              id={"option-#{path.name}"}
              role="option"
              tabindex="-1"
            >
              <MaterialIcons.folder style="outlined" size={42} class="fill-black" />
              <div class="font-medium truncate">
                <%= path.name %>
              </div>
            </li>
          </.link>
        </ul>
      </div>

      <label
        aria-label="open sidebar"
        class="btn btn-square btn-ghost lg:hidden"
        phx-click="modal-state"
        phx-target={@myself}
      >
        <MaterialIcons.search type="button" style="outlined" size={22} class="fill-white" />
      </label>
      <dialog
        :if={@modal}
        id="my_modal_3"
        class="modal modal-open fixed inset-0 z-50 lg:hidden"
        tabindex="-1"
        aria-hidden="true"
        style="background: rgba(0, 0, 0, 0.6);"
      >
        <div class="modal-box w-full h-full overflow-y-auto">
          <div class="modal-action">
            <button
              class="btn btn-sm btn-circle absolute right-2 top-2"
              phx-click="modal-state"
              phx-target={@myself}
            >
              ✕
            </button>
          </div>
          <div class="p-4">
            <h3 class="font-bold text-lg"><%= gettext("Search Folders and Domains") %></h3>
            <input
              type="search"
              class="input input-bordered w-full mb-4"
              placeholder={gettext("Type to search...")}
              phx-target={@myself}
              phx-keyup="do-search"
              phx-debounce="200"
              value={@search}
            />
            <ul class="menu bg-base-100 p-2 rounded-box w-full">
              <.link
                :for={host <- @hosts_result}
                navigate={~p"/domains/#{host.id}"}
                id={"host-mob-#{host.domain_name}"}
              >
                <li class="menu-item">
                  <div class="flex items-center space-x-3">
                    <MaterialIcons.link style="outlined" size={22} class="fill-current" />
                    <span class="flex-1 min-w-0">
                      <span class="block text-sm font-medium truncate">
                        <%= host.domain_name %>
                      </span>
                    </span>
                  </div>
                </li>
              </.link>

              <.link
                :for={path <- @paths_result}
                navigate={~p"/dashboards/#{path.path}"}
                id={"path-mob-#{path.name}"}
              >
                <li class="menu-item">
                  <div class="flex items-center space-x-3">
                    <MaterialIcons.folder style="outlined" size={22} class="fill-current" />
                    <span class="flex-1 min-w-0">
                      <span class="block text-sm font-medium truncate">
                        <%= path.name %>
                      </span>
                    </span>
                  </div>
                </li>
              </.link>
            </ul>
          </div>
        </div>
      </dialog>
    </div>
    """
  end

  @impl true
  def mount(socket) do
    {:ok, socket}
  end

  @impl true
  def update(assigns, socket) do
    {:ok,
     socket
     |> assign(assigns)
     |> assign(paths_result: [])
     |> assign(hosts_result: [])
     |> assign(modal: false)
     |> assign(search: "")}
  end

  @impl true

  def handle_event("do-search", %{"value" => ""}, socket) do
    {:noreply, assign(socket, search: "", hosts_result: [], paths_result: [], modal: true)}
  end

  def handle_event("do-search", %{"value" => value}, socket) do
    hosts = HostsAndPaths.list_hosts_filtered_by_name(value)
    paths = HostsAndPaths.list_paths_filtered_by_name(value)

    {:noreply,
     assign(socket, search: value, hosts_result: hosts, paths_result: paths, modal: true)}
  end

  def handle_event("modal-state", _, socket) do
    modal_state = !socket.assigns.modal

    {:noreply,
     assign(socket,
       search: socket.assigns.search,
       hosts_result: [],
       paths_result: [],
       modal: modal_state
     )}
  end
end
