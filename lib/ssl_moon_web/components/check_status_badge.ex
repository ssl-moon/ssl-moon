defmodule SslMoonWeb.Components.CheckStatusBadge do
  @moduledoc """
    Component for displaying a badge with check status.
  """
  use SslMoonWeb, :html

  attr :name, :string, required: true
  attr :type, :atom, values: [:success, :warning, :error, :inactive, :not_found]

  def check_status(assigns) do
    badge = get_badge(assigns.type)
    tip_text = get_text(assigns.type)

    assigns = assign(assigns, tip_text: tip_text, badge: badge)

    ~H"""
    <div class="tooltip" data-tip={@tip_text}>
      <div class={["badge", "gap-2", @badge]}>
        <MaterialIcons.info style="outlined" class={["inline-block", "w-4", "h-4"]} />
        <%= @name %>
      </div>
    </div>
    """
  end

  defp get_badge(type) do
    case type do
      :success -> "badge-success"
      :warning -> "badge-warning"
      :failure -> "badge-error"
      :error -> "badge-error"
      :inactive -> "badge-ghost"
      :not_found -> "badge-info"
    end
  end

  defp get_text(type) do
    case type do
      :success -> gettext("Check passed successfully")
      :warning -> gettext("Check passed with warnings")
      :failure -> gettext("Check failed to pass")
      :error -> gettext("Check failed to pass")
      :inactive -> gettext("Check was disabled or never performed")
      :not_found -> gettext("Check was never performed")
    end
  end
end
