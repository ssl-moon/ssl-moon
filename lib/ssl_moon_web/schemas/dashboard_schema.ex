defmodule SslMoon.Schemas.Dashboard do
  @moduledoc """
  Spec schema for swagger dashboard model
  """
  @behaviour OpenApiSpex.Schema
  @derive [Jason.Encoder]
  alias OpenApiSpex.Schema

  @schema %Schema{
    title: "Dashboard",
    description: "A user of the app",
    type: :object,
    properties: %{
      id: %Schema{type: :integer, description: "Dashboard ID"},
      url: %Schema{type: :string, description: "Url of the dashboard"},
      inserted_at: %Schema{type: :string, description: "Creation timestamp", format: :datetime},
      updated_at: %Schema{type: :string, description: "Update timestamp", format: :datetime}
    },
    required: [:url],
    example: %{
      by: "Lightcyphers",
      description: "About 300 hosts from 5 banks and 5 local authorities",
      groups: [],
      id: 1,
      name: ".MD Reesiliance Report",
      url: "md-report"
    },
    "x-struct": __MODULE__
  }

  def schema, do: @schema
  defstruct Map.keys(@schema.properties)
end

defmodule SslMoon.Schemas.DashboardResponse do
  @moduledoc """
  Response spec schema for dashboard response
  """
  require OpenApiSpex
  alias SslMoon.Schemas.Dashboard
  # OpenApiSpex.schema/1 macro can be optionally used to reduce boilerplate code
  OpenApiSpex.schema(%{
    title: "DashboardResponse",
    description: "Response schema for dashboard",
    type: :object,
    properties: %{
      data: Dashboard
    }
  })
end
