defmodule SslMoonWeb.LanguageController do
  use SslMoonWeb, :controller

  @locales Gettext.known_locales(SslMoonWeb.Gettext)

  def index(conn, %{"locale" => locale}) do
    if locale in @locales do
      Gettext.put_locale(SslMoonWeb.Gettext, locale)

      conn
      |> put_session(:locale, locale)
      |> Plug.Conn.put_resp_cookie("locale", locale)
      |> redirect(to: "/language")
    else
      conn
      |> redirect(to: "/language")
    end
  end
end
