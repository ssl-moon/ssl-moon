defmodule SslMoonWeb.PageHTML do
  use SslMoonWeb, :html

  embed_templates "page_html/*"
end
