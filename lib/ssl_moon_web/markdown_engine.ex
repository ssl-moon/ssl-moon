defmodule SslMoonWeb.MarkdownEngine do
  @moduledoc """
  Engines need only to implement the compile/2 function, that receives the template file and the template name and outputs the template quoted expression:
  """

  @behaviour Phoenix.Template.Engine

  # --------------------------------------------------------
  @doc """
  Callback implementation for `Phoenix.Template.Engine.compile/2`

  Precompiles the String file_path into a function defintion, using the EEx

  ### Parameters
    * `path` path to the template being compiled
    * `name` name of the template being compiled

  """
  def compile(path, _name) do
    path
    |> EEx.compile_file([line: 1] ++ [engine: EEx.SmartEngine])
  end
end
