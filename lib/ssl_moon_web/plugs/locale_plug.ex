defmodule SslMoonWeb.Plugs.Locale do
  @moduledoc false

  import Plug.Conn
  @behaviour Plug

  @impl Plug
  def init(_), do: nil

  @impl Plug
  def call(conn, _opts) do
    locale =
      get_locale_from_cookie(conn) || "en"

    Gettext.put_locale(SslMoonWeb.Gettext, locale)

    conn
    |> put_session(:locale, locale)
    |> Plug.Conn.put_resp_cookie("locale", locale)
  end

  defp get_locale_from_cookie(conn), do: conn.cookies["locale"]
end
