defmodule SslMoonWeb.Router do
  use SslMoonWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {SslMoonWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug SslMoonWeb.Plugs.Locale
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug OpenApiSpex.Plug.PutApiSpec, module: SslMoon.ApiSpec
  end

  scope "/", SslMoonWeb do
    pipe_through :browser
    get "/", PageController, :home
    get "/language/:locale", LanguageController, :index
    live "/language/", LanguageLive
  end

  scope "/", SslMoonWeb do
    pipe_through :browser

    live "/dashboards", RootDashboardLive
    live "/dashboards/*path", DashboardLive
  end

  scope "/", SslMoonWeb do
    pipe_through :browser

    live "/domains/:id", RootDomainLive
    live "/domains/:id/:check_name", DomainLive
  end

  scope "/" do
    pipe_through :browser
    get "/swaggerui", OpenApiSpex.Plug.SwaggerUI, path: "/api/openapi"
  end

  scope "/api" do
    pipe_through :api
    get("/openapi", OpenApiSpex.Plug.RenderSpec, [])
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:ssl_moon, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: SslMoonWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
