### Check Categories

SSL MOON provides checks in the following categories to ensure comprehensive domain security:

1. **Application Security**: This category includes checks related to the security of web applications and their configurations.

   - `Content Type Options`: Verifies the proper setting of the Content-Type response header to prevent MIME-sniffing attacks.

   - `Frame Options`: Assesses whether the domain correctly sets the X-Frame-Options header to mitigate clickjacking attacks.

   - `XSS Protection`: Checks for the presence of the X-XSS-Protection header to protect against cross-site scripting (XSS) attacks.

2. **Domain Name System (DNS)**: Focuses on DNS configuration of the domains, including DNSSEC, DNS resolution, and record validity.

   - `Certification Authority Authorization`: Validates the presence of Certification Authority Authorization (CAA) records, ensuring authorized certificate issuance.

   - `DNS Zone`: Verifies the integrity and correctness of the domain's DNS zone records.

   - `DNSSEC`: Assesses the DNSSEC (Domain Name System Security Extensions) configuration, ensuring the authenticity and integrity of DNS data.

3. **Modern Security Features**: Evaluates the usage of modern security headers, secure cookie attributes, and other up-to-date security configurations.

   - `Content Security Policy`: Examines Content Security Policy (CSP) headers to protect against content injection attacks.

   - `Strict Transport Security`: Ensures that the domain has implemented HTTP Strict Transport Security (HSTS) to enforce secure connections.

   - `Subresource Integrity`: Verifies that subresources loaded by the domain are integrity-protected using Subresource Integrity (SRI) attributes.

4. **Protocols**: Assesses the usage of secure communication protocols, such as TLS/SSL, on the domains.

   - `HTTP`: Evaluates the security of HTTP connections.

   - `HTTPS`: Examines the implementation of HTTPS, ensuring secure communication over SSL/TLS.

   - `WWW HTTP`: Verifies the security of WWW HTTP connections.

   - `WWW HTTPS`: Assesses the implementation of secure WWW HTTPS connections.

5. **Secure Transport**: Includes checks related to secure data transport, ensuring that data transmitted to and from the domains is protected through encryption and secure transport mechanisms.

   - `Certificates`: Examines the validity and security of SSL/TLS certificates used by the domain.

   - `Cookies`: Evaluates the security and attributes of cookies set by the domain.

   - `Mixed Content`: Verifies that the domain does not load mixed content (HTTP content on HTTPS pages).

   - `Transport Layer Security`: Assesses the implementation and security of the Transport Layer Security (TLS) protocol.

Each category contains specific checks contributing to assessing the overall security posture of the domains.

