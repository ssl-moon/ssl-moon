# Installation

## Pre-requisites

The following external dependencies are required to be able to start the project:

- [dig](https://linux.die.net/man/1/dig)
- [nodejs](https://nodejs.org/en) v16 (not tested with other versions)
- [yarn](https://yarnpkg.com/)
- elixir and OTP (consult .tool-versions for specific versions, or ideally use [asdf](https://asdf-vm.com/))

## Getting Started

This section covers the steps required to start the server on your local machine:

1. Clone this repository to your local machine:
   - HTTP: `git clone https://gitlab.com/lightcyphers-open/csirt/ssl-moon/ssl-moon.git`
   - SSH: `git clone git@gitlab.com:lightcyphers-open/csirt/ssl-moon/ssl-moon.git`

2. Navigate to the project directory:
```bash
cd ssl-moon
```
3. Install dependencies using Mix, the Elixir package manager:
```bash
mix deps.get
```
4. Install frontend dependencies using yarn:
```bash
cd assets && yarn install && cd ..
```
5. Create and migrate the databases:
```bash
mix ecto.reset
```
6. Start the phoenix server:
```bash
mix phx.server
```

If everything went without any exceptions the server can be accessible on [localhost:4000](http://localhost:4000/).